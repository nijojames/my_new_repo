#!/bin/bash
# Starts the Gunicorn server
set -e

# Activate the virtualenv for this project
%(ACTIVATE)s

# Start gunicorn going
pushd %(PROJECT_PATH)s/server/
exec gunicorn api.wsgi:application -c %(PROJECT_PATH)s/server/gunicorn.conf.py
popd 
