Fabric==1.10.2
boto==2.5.2
django-storages==1.1.8
pycrypto==2.6.1
ssh==1.7.14
wsgiref==0.1.2
paramiko==1.15.4
