import time
from random import Random
import string

from django.db.models.loading import cache

from push_notifications.models import APNSDevice

'''
Add generic utility functions here
'''


class UtlRandom:
    '''
    Random # generation
    '''
    random = ""
    seed = ""

    def __init__(self, seed=None):
        if seed is not None:
            self.seed = seed
            print "INFO<Utl>: Random # seed=[%s][caller specified]" % self.seed
        else:
            self.seed = time.time()
            print "INFO<Utl>: Random # seed=[%s][auto generated]" % self.seed
            self.seed = time.time()

        self.random = Random(x=self.seed)

    def random_str(self, n):
        return ''.join(self.random.choice(string.ascii_uppercase +
                                          string.digits) for _ in range(n))

    def random_num(self, b, e):
        return self.random.randint(b, e)

    def random_lowercase_string(self, length):
        return ''.join(self.random.choice(string.lowercase) for i in range(length))

    def random_email(self, length):
        email_host = 'example.com'
        random_username = self.random_lowercase_string(length)
        return random_username+'@'+email_host

    def random_name(self, length):
        return ''.join(self.random.choice(string.ascii_letters) for i in range(length))




'''
Add test utility functions here
'''
from rest_framework.test import APITestCase


class UtlTest(APITestCase):
    '''
    Utility functions for test cases
    '''
    def assert_equal(self, first, second, msg=None):
        # create a new message if caller wants to diplay her own message
        if msg is not None:
            msg = "%s != %s\nINFO: %s" % (first, second, msg)
        self.assertEqual(first, second, msg)


def get_genric_object(model, id):
    """
    util function to get the object model name and id
    :param model:
    :param id:
    :return:
    """
    model = cache.get_model(model+'s', string.capwords(model))
    obj = model.objects.get(pk=id)
    return obj


def get_user_device(user):
    try:
        device = APNSDevice.objects.get(user=user)
    except APNSDevice.DoesNotExist:
        return None
    else:
        return device
