from api.settings.common import *  # NOQA

'''
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(os.path.dirname(BASE_DIR), "static", "static_dirs"),
)

STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static", "static_root")


MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "media", "media")
'''

#EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'
EMAIL_BACKEND = os.environ.get('ENV_EMAIL_BACKEND',
                               'django.core.mail.backends.console.EmailBackend')
# Host for sending e-mail.
EMAIL_HOST = 'smtp.gmail.com'

EMAIL_HOST_PASSWORD = 'developer123'
EMAIL_HOST_USER = 'devteamrnds@gmail.com'
DEFAULT_FROM_EMAIL = "devteamrnds@gmail.com"
EMAIL_PORT = 587
EMAIL_USE_TLS = True

AWS_ACCESS_KEY_ID = os.environ.get('ENV_AWS_ACCESS_KEY_ID', '')
AWS_SECRET_ACCESS_KEY = os.environ.get('ENV_AWS_SECRET_ACCESS_KEY', '')
AWS_ORIGINAL_VIDEO_S3_BUCKET = os.environ.get('ENV_AWS_ORIGINAL_VIDEO_S3_BUCKET', '')
AWS_WATERMARKED_VIDEO_S3_BUCKET = os.environ.get('ENV_AWS_WATERMARKED_VIDEO_S3_BUCKET', '')
AWS_RELEASE_DOC_S3_BUCKET = os.environ.get('ENV_AWS_RELEASE_DOC_S3_BUCKET', '')
AWS_REGION = os.environ.get('ENV_AWS_REGION', '')

# static and media files on s3
STATICFILES_LOCATION = 'static'
STATICFILES_STORAGE = 'api.custom_storages.StaticStorage'

MEDIAFILES_LOCATION = 'media'
DEFAULT_FILE_STORAGE = 'api.custom_storages.MediaStorage'

AWS_STORAGE_BUCKET_NAME = os.environ.get('ENV_AWS_STATIC_MEDIA_S3_BUCKET', '')
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
STATIC_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, STATICFILES_LOCATION)
MEDIA_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)
