from api.settings.common import *


STATICFILES_LOCATION = 'static'
STATICFILES_STORAGE = 'api.custom_storages.StaticStorage'

MEDIAFILES_LOCATION = 'media'
DEFAULT_FILE_STORAGE = 'api.custom_storages.MediaStorage'

AWS_ACCESS_KEY_ID = os.environ.get ('ENV_AWS_ACCESS_KEY_ID', '')
AWS_SECRET_ACCESS_KEY = os.environ.get ('ENV_AWS_SECRET_ACCESS_KEY', '')


AWS_STORAGE_BUCKET_NAME = "spyhop"
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
STATIC_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, STATICFILES_LOCATION)

MEDIA_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)




