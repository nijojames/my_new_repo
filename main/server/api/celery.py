from __future__ import absolute_import

import os
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'api.settings')

from django.conf import settings

app = Celery('spyhop')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.update(
    CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend',
)


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))


import redis


def celery_only_one(function=None, key="", timeout=None):
    """
    run with only one celery worker at a time.
    """
    def _dec(run_func):
        def _caller(*args, **kwargs):
            url = settings.BROKER_URL
            i = url.find("//") + len("//")
            j = url.rfind(":")
            host = url[i:j]
            port = url[j+1:len(url)]

            redis_client = redis.StrictRedis(host=host, port=port)

            """Caller."""
            ret_value = None
            have_lock = False
            lock = redis_client.lock(key, timeout=timeout)

            try:
                have_lock = lock.acquire(blocking=False)
                if have_lock:
                    # print "INFO: Got Lock"
                    ret_value = run_func(*args, **kwargs)
                else:
                    # print "No Lock"
                    pass
            finally:
                if have_lock:
                    lock.release()
            return ret_value
        return _caller
    return _dec(function) if function is not None else _dec
