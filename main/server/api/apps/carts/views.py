from django.views.generic.detail import SingleObjectMixin
from django.contrib.contenttypes.models import ContentType

from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status

from serializers import CartSerializer
from carts import MyCart
from api.apps.videos.models import Video
from api.apps.videos.models import PUBLISHED_TO_CONTEST
from api.apps.contests.models import Contest
from api.apps.contests.models import ContestEntry

from .models import Item


class CartViewSet(ModelViewSet):

    http_method_names = ['get', 'post', 'delete']
    permission_classes = [AllowAny]
    serializer_class = CartSerializer

    def get_object(self):
        cart_obj = MyCart(self.request)
        return cart_obj.cart

    def get_queryset(self):
        cart_obj = MyCart(self.request)
        return [cart_obj.cart]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if 'item_type' not in request.data:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        if 'item_id' not in request.data:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        # ToDo: re-validate the item's price
        serializer.is_valid(raise_exception=True)
        item_id = serializer.validated_data.get("item_id")
        item_type = serializer.validated_data.get("item_type")
        if item_type == 'video':
            cart_item = Video.objects.get(id=item_id)
            price = cart_item.price
            # if a video is in contest, then only the
            # contest owner can purchase the video
            if cart_item.status == PUBLISHED_TO_CONTEST:
                entry = ContestEntry.objects.get(video=cart_item)
                if request.user != entry.contest.user:
                    return Response(status=status.HTTP_401_UNAUTHORIZED)
        elif item_type == 'contest':
            cart_item = Contest.objects.get(id=item_id)
            price = cart_item.award_amount

        cart = self.get_object()
        Item.objects.create(content_type=ContentType.objects.get_for_model(cart_item),
                            object_id=item_id,
                            quantity=1,
                            unit_price=price,
                            cart=cart)
        data = self.serializer_class(cart, context={'request': self.request}).data
        return Response(data, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        item_id = kwargs.get('pk')
        item_type = request.query_params.get('item_type', None)
        if not item_id:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        if item_type == 'video':
            ct = ContentType.objects.get_for_model(Video)
        elif item_type == 'contest':
            ct = ContentType.objects.get_for_model(Contest)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        instance = self.get_object()
        instance.get_items.filter(object_id=item_id, content_type=ct).delete()
        data = self.serializer_class(instance, context={'request': self.request}).data
        return Response(data, status=status.HTTP_200_OK)
