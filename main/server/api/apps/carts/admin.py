from django.contrib import admin

from import_export.admin import ImportExportModelAdmin

from api.apps.carts.models import Item

from models import Cart


class ItemInlineAdmin(admin.TabularInline):
    model = Item


class CartAdmin(ImportExportModelAdmin):
    inlines = [ItemInlineAdmin]
    list_display = ['user', 'creation_date']


admin.site.register(Cart, CartAdmin)
