import datetime

from cart import cart

from api.apps.carts import models

from utils import copy_cart_items

CART_ID = 'CART-ID'


class MyCart(cart.Cart):

    def __init__(self, request):
        self.cart_id = request.session.get(CART_ID, None)
        if not request.user.is_authenticated():
            cart = self.get_cart_from_session(request)

        else:
            cart = self.get_valid_cart_for_user(request)
        self.cart = cart

    def get_cart_from_session(self, request):
        if self.cart_id:
            try:
                cart = models.Cart.objects.get(id=self.cart_id, checked_out=False)
                self.cart_id = cart.id

            except models.Cart.DoesNotExist:
                cart = self.new(request)
                self.cart_id = cart.id
                return cart

        else:
            cart = self.new(request)
            self.cart_id = request.session.get(CART_ID, None)

        return cart

    def get_valid_cart_for_user(self, request):
        user_cart = self.get_user_cart(request)
        # if self.cart_id:
        #     user_cart = self.merge_session_and_user_cart(request, user_cart)

        return user_cart

    def get_user_cart(self, request):
        session_cart = None
        if self.cart_id:
            try:
                session_cart = models.Cart.objects.get(id=self.cart_id, checked_out=False)

            except:
                session_cart = None

        try:
            cart = models.Cart.objects.get(user=request.user, checked_out=False)

        except models.Cart.DoesNotExist:
            if session_cart:
                session_cart.user = request.user
                session_cart.save()
                return session_cart
            else:
                cart = self.new(request)
                return cart

        if cart and session_cart:
            if not session_cart.id == cart.id:
                cart = copy_cart_items(session_cart, cart)
                session_cart.delete()

        return cart

    def new(self, request):

        if request.user.is_authenticated():
            cart = models.Cart(creation_date=datetime.datetime.now(), user=request.user)

        else:
            cart = models.Cart(creation_date=datetime.datetime.now())

        cart.save()
        request.session[CART_ID] = cart.id
        return cart
