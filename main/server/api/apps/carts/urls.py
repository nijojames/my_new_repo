from rest_framework import routers

from django.conf.urls import patterns, include, url

from views import CartViewSet


router = routers.DefaultRouter()

router.register(r'cart', CartViewSet, base_name='add-to-carts')

urlpatterns = [
    url('', include(router.urls))
]
