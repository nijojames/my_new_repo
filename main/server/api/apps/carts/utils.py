from copy import deepcopy

def copy_cart_items(copy_from_cart, copy_to_cart):
    for item in copy_from_cart.get_items.all():
        item.cart = copy_to_cart
        item.save()

    return copy_to_cart
