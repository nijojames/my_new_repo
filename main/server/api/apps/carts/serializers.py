from django.contrib.contenttypes.models import ContentType
from decimal import Decimal

from rest_framework import serializers

from api.apps.videos.serializers import VideoSerializer
from api.apps.contests.serializers import ContestSerializer
from api.apps.videos.models import Video
from api.apps.videos.models import PUBLISHED_TO_MARKETPLACE
from api.apps.videos.models import PUBLISHED_TO_CONTEST
from api.apps.contests.models import Contest
import models
import carts


class ItemSerializer(serializers.ModelSerializer):

    details = serializers.SerializerMethodField()
    content_type = serializers.SerializerMethodField()

    class Meta:
        model = models.Item
        fields = ('details', 'content_type' )

    def get_content_type(self, obj):
        return obj.content_type.model

    def get_details(self, obj):
        if obj.content_type.model == 'video':
            try:
                video = Video.objects.get(id=obj.object_id)
                # disallow unpublished videos
                if video.status != PUBLISHED_TO_MARKETPLACE and \
                   video.status != PUBLISHED_TO_CONTEST:
                    return None

                data = VideoSerializer(video,
                                       context={'request': self.context['request']})
            except Video.DoesNotExist:
                 # raise serializers.ValidationError({'error': "Video not found"})
                return None
        else:
            try:
                contest = Contest.objects.get(id=obj.object_id)
                data = ContestSerializer(contest,
                                         context={'request': self.context['request']})
            except Contest.DoesNotExist:
                # raise serializers.ValidationError({'error': "Request not found"})
                return None
        return data.data


class CartSerializer(serializers.ModelSerializer):
    item_type = serializers.CharField(write_only=True, max_length=16)
    item_id = serializers.IntegerField(write_only=True)
    # items = ItemSerializer(source='get_items', many=True, read_only=True)
    items = serializers.SerializerMethodField()
    item_count = serializers.SerializerMethodField()
    total_price = serializers.SerializerMethodField()

    class Meta:
        model = models.Cart
        fields = ('id',
                  'item_id',
                  'item_type',
                  'item_count',
                  'total_price',
                  'items')

    def get_items(self, obj):
        items = []
        for item in models.Item.objects.filter(cart=obj):
            serializer = ItemSerializer(item,
                                        context={'request': self.context['request']})
            if serializer.data['details']:
                items.append(serializer.data)
                continue
            # remove the non-existing items
            item.delete()
        return items

    def get_item_count(self, obj):
        items = models.Item.objects.filter(cart__id=obj.id)
        return items.count()

    def get_total_price(self, obj):
        price = 0
        for item in models.Item.objects.filter(cart=obj):
            price += float(item.total_price)
        return Decimal(price)

    def validate(self, data):
        item_id = data['item_id']
        item_type = data['item_type']
        cart = carts.MyCart(self.context['request']).cart

        if item_type == 'video':
            content_type = ContentType.objects.get_for_model(Video)
            mqs = cart.get_items.filter(object_id=item_id,
                                        content_type=content_type)
            if mqs.count():
                raise serializers.ValidationError({'error': "Video already in cart"})
            try:
                video = Video.objects.get(id=item_id)
            except Video.DoesNotExist:
                raise serializers.ValidationError({'error': "Video not found"})
            return data
        if item_type == 'contest':
            content_type = ContentType.objects.get_for_model(Contest)
            mqs = cart.get_items.filter(object_id=item_id,
                                        content_type=content_type)
            if mqs.count():
                raise serializers.ValidationError({'error': "Request already in cart"})
            try:
                contest = Contest.objects.get(id=item_id)
            except Contest.DoesNotExist:
                raise serializers.ValidationError({'error': "Request not found"})
            return data
        raise serializers.ValidationError({'error': "Invalid item_type "+item_type})
