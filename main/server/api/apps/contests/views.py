from decimal import Decimal
from urlparse import urlparse

from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework import filters

from serializers import ContestSerializer
from serializers import ContestMessageSerializer
from serializers import ContestEntrySerializer
from serializers import ContestSearchSerializer
from serializers import ContestFacetSerializer
from serializers import StoryBoardSerializer

from drf_haystack.viewsets import HaystackViewSet

from models import Contest
from models import ContestMessage
from models import ContestEntry
from models import StoryBoard
from models import InvitedVideoGraphers
from api.apps.utils.mixins import ContestSearchMixin, VideoSearchMixin
from api.apps.profiles.models import Profile

from rest_framework import status
from django.db.models import Q

from api.apps.videos.models import PUBLISHED_TO_CONTEST
from api.apps.videos.models import PUBLISHED_TO_MARKETPLACE


class ContestView(ModelViewSet):
    """
    Two types of contests S: Sponsored, SP: Spyhop
    """

    # permission_classes = [permissions.IsAuthenticatedOrReadOnly, ]
    permission_classes = [permissions.AllowAny, ]
    serializer_class = ContestSerializer
    model = Contest
    # queryset = Contest.objects.filter(expired=False)
    queryset = Contest.objects.all()
    lookup_field = 'slug'

    # ToDo: disable contest listing once ios starts
    #       using search api to list the contests
    def list(self, request, *args, **kwargs):
        # ToDo: fix this
        self.queryset = self.queryset.exclude(Q(private=True) |
                                              Q(status='incomplete') |
                                              Q(status='draft'))
        serializer = ContestSerializer(self.queryset,
                                       context={'request': request},
                                       many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        contest = self.get_object()
        if not contest:
            return Response(status=status.HTTP_404_NOT_FOUND)
        # ToDo: no contest must be created anonymously
        if contest.user and contest.user != request.user:
            if contest.private or \
               contest.status == 'incomplete' or \
               contest.status == 'draft':
                return Response(status=status.HTTP_401_UNAUTHORIZED)

        serializer = ContestSerializer(contest,
                                       context={'request': request})
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        # ToDo: no contest must be created anonymously
        # if not request.user.is_authenticated():
        #     return Response(status=status.HTTP_401_UNAUTHORIZED)
        if hasattr(request.data, '_mutable'):
            request.data._mutable = True
        request.data['user'] = request.user.id
        invited_people = request.data.get('invited_people')
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        id = serializer.data.get('id')
        contest = Contest.objects.get(id=id)
        if invited_people:
            self.send_invites(invited_people, contest)
        return Response(serializer.data,
                        status=status.HTTP_201_CREATED,
                        headers=headers)

    def update(self, request, *args, **kwargs):
        contest = self.get_object()
        # ToDo: no anonymous contests
        if contest.user and request.user != contest.user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        if hasattr(request.data, '_mutable'):
            request.data._mutable = True
        request.data['user'] = request.user.id
        invited_people = request.data.get('invited_people')
        if invited_people is not None:
            self.send_invites(invited_people, contest)
        return super(ContestView, self).update(request, *args, **kwargs)

    # ToDo: send actual invitation push notification only on
    #       contest publish
    def send_invites(self, invited_people, contest):
        '''    
        invited_ids = invited_people.split(',')[:-1]
        current_invites = contest.invited_videographers.filter(videographers__id__in=invited_ids,
        contest=contest).delete()
         profiles = Profile.objects.filter(id__in=invited_ids)
         for profile in profiles:
             InvitedVideoGraphers.objects.create(contest=contest, videographers=profile)
        '''
        #if invited_people:
        # invited_ids = invited_people.split(',')[:-1]
        invited_ids = invited_people
        #else:
        #invited_ids = []
        # print "put:"
        # print invited_ids    
        # delete the removed ids
        riqs = contest.invited_videographers.exclude(videographers__id__in=invited_ids)
        #rilist = riqs.values_list('videographers_id', flat=True)
        # print "removed: "
        # print rilist
        riqs.delete()
        # get the preserved ids
        piqs = contest.invited_videographers.filter(videographers__id__in=invited_ids)
        # piqs now should be same as contest.invited_videographers.all()
        pilist = piqs.values_list('videographers_id', flat=True)
        # print "preserved: "
        # print pilist
        # only add new ids
        # print "invited_ids:"
        # print invited_ids
        for ii in pilist:
            # print "removing: %d" % ii
            # invited_ids.remove(unicode(ii))
            if ii in invited_ids:
                invited_ids.remove(ii)
        # add new ids
        # print "new: "
        # print invited_ids
        profiles = Profile.objects.filter(id__in=invited_ids)
        for profile in profiles:
            InvitedVideoGraphers.objects.create(contest=contest,
                                                videographers=profile)

    def process_category(self, category):
        list = [int(item) for item in category.split(',')]
        return list


class ExpiredContestView(ContestView):
    queryset = Contest.objects.filter(expired=True)
    http_method_names = ['get', ]


# class ContestSearch(HaystackViewSet):
#     index_models = [Contest]
#     serializer_class = ContestSearchSerializer

import re
from drf_haystack.filters import HaystackFacetFilter
from haystack.query import SearchQuerySet
from haystack.forms import SearchForm


class ContestSearch(HaystackViewSet, ContestSearchMixin, VideoSearchMixin):

    # `index_models` is an optional list of which models you would
    # like to include in the search result. You might have several models
    # indexed, and this provides a way to filter out those of no interest
    # for this particular view.
    # (Translates to `SearchQuerySet().models(*index_models)` behind the scenes
    index_models = [Contest]
    serializer_class = ContestSearchSerializer
    paginate_by_param = 'page_size'
    # filter_backends = [HaystackHighlightFilter, HaystackAutocompleteFilter]

    # This will be used to filter and serialize faceted results
    facet_serializer_class = ContestFacetSerializer
    # This is the default facet filter, and can be left out
    facet_filter_backends = [HaystackFacetFilter]

    def get_queryset(self):
        """
        :param args:
        :param kwargs:
        :return: Search Results based on query 'q'
        """
        request = self.request
        q = request.GET.get('q', "")

        orientation = request.GET.get('orientation', None)
        orientation = self._get_orientation(orientation)

        resolution = request.GET.get('resolution', "")
        resolution = self._get_resolution(resolution)

        video_type = request.GET.get('video_type', "")
        video_type = self._get_video_type(video_type)

        price_range = request.GET.get('price_range', None)
        price_range = self._get_price_range(price_range)

        query = re.sub(' +', ' ', re.sub('[^a-zA-Z0-9\n\.]', ' ', q))

        levels = request.GET.get('levels', None)
        levels = self._get_levels(levels)

        permission = request.GET.get('permissions', None)

        published = request.GET.get('age', None)

        status = request.GET.get('status', None)

        sqs = SearchQuerySet()

        if query:
            form = SearchForm(request.query_params)
            print "INFO: query_params=%s" % request.query_params
            # search_query = urlparse(request.build_absolute_uri())
            if form.is_valid():
                sqs = form.search()
        sqs = sqs.models(Contest)
        sqs = sqs.exclude(status='draft')
        sqs = sqs.exclude(status='incomplete')
        print "INFO: sqs count1=%d" % sqs.count()
        '''
        include private contests only if the requester is the owner
        or requestor is one of the invited videographers
        '''
        '''
        print "INFO: request.user=%s, %d" % (request.user,
                                             request.user.is_authenticated())
        '''
        sqs = sqs.filter(Q(private='false') |
                         Q(user=request.user) |
                         Q(invited_videographers__contains=request.user))
        print "INFO: sqs count2=%d" % sqs.count()

        if price_range:
            # sqs = sqs.filter(award_amount__range=price_range)
            sqs = sqs.filter(award_amount__gt=price_range[0])
            sqs = sqs.filter(award_amount__lte=price_range[1])

        if orientation:
            sqs = sqs.filter(orientation__in=orientation)

        if video_type:
            sqs = sqs.filter(video_type__in=video_type)

        if resolution:
            sqs = sqs.filter(resolution__in=resolution)

        if levels:
            sqs = sqs.filter(level_required__in=levels)

        if permission:
            sqs = sqs.filter(permission=permission)

        if published:
            if published == 'new':
                sqs = sqs.order_by('-published')
            if published == 'old':
                sqs = sqs.order_by('published')

        if status:
            if status == 'open':
                sqs = sqs.filter(status='publish')
            if status == 'closed':
                sqs = sqs.filter(status='closed')

        return sqs


class ContestMessageView(ModelViewSet):
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('contest', 'contest__id')

    serializer_class = ContestMessageSerializer
    model = ContestMessage
    queryset = ContestMessage.objects.all()
    lookup_field = "contest__id"

    def create(self, request, *args, **kwargs):
        if hasattr(request.data, '_mutable'):
            request.data._mutable = True
        request.data['user'] = request.user.id
        return super(ContestMessageView, self).create(request, *args, **kwargs)


class ContestEntryViewSet(ModelViewSet):
    serializer_class = ContestEntrySerializer
    model = ContestEntry
    # show only the entries which are published.
    # once contest is closed, entries may have
    # been published to marketplace
    queryset = ContestEntry.objects \
                           .filter(Q(video__status=PUBLISHED_TO_CONTEST) |
                                   Q(video__status=PUBLISHED_TO_MARKETPLACE))

    http_method_names = ['get', 'post', 'patch']

    def get_queryset(self):
        contest = self.request.GET.get('contest')
        if not contest:
            return []
        self.queryset = self.queryset \
                            .filter(contest__id=contest) \
                            .order_by('-winner')
        return self.queryset

    def update(self, request, *args, **kwargs):
        # allow multiple winners [purchase]
        # contest = self.request.GET.get('contest')
        # data = ContestEntry.objects.filter(contest__id=contest, winner=True)
        # if data:
        #     return Response(status=status.HTTP_403_FORBIDDEN)
        return super(ContestEntryViewSet, self).update(request,
                                                       *args,
                                                       **kwargs)


class StoryBoardViewSet(ModelViewSet):
    http_method_names = ['put', ]
    permission_classes = [permissions.AllowAny]
    serializer_class = StoryBoardSerializer
    lookup_field = 'slug'

    def get_queryset(self):
        return Contest.objects.all()

    def update(self, request, *args, **kwargs):
        contest = self.get_object()
        images_1 = request.data.get('1')
        images_2 = request.data.get('2')
        images_3 = request.data.get('3')
        images_4 = request.data.get('4')
        images_5 = request.data.get('5')
        if images_1:
            self.do_the_trick(images_1, contest, 1)
        else:
            contest.get_storyboards.filter(image_order=1).delete()
        if images_2:
            self.do_the_trick(images_2, contest, 2)
        else:
            contest.get_storyboards.filter(image_order=2).delete()
        if images_3:
            self.do_the_trick(images_3, contest, 3)
        else:
            contest.get_storyboards.filter(image_order=3).delete()
        if images_4:
            self.do_the_trick(images_4, contest, 4)
        else:
            contest.get_storyboards.filter(image_order=4).delete()
        if images_5:
            self.do_the_trick(images_5, contest, 5)
        else:
            contest.get_storyboards.filter(image_order=5).delete()
        return Response(status=status.HTTP_200_OK)

    def do_the_trick(self, image_obj, contest_obj, order):
        image_1_obj = None
        try:
            image_obj.name
            image_1_obj = StoryBoard.objects.get(contest=contest_obj,
                                                 image_order=order)
            image_1_obj.image = image_obj
            image_1_obj.save()
            return
        except Exception as e:
            print "ERROR: do_the_trick=" + str(e)

        if not image_1_obj:
            try:
                image_obj.name
                StoryBoard.objects.create(image=image_obj,
                                          contest=contest_obj,
                                          image_order=order)
            except AttributeError:
                pass
