from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User

from rest_framework import serializers

from drf_haystack.serializers import HaystackSerializerMixin
from drf_haystack.serializers import HaystackFacetSerializer

from models import Contest, ContestMessage, ContestEntry, StoryBoard, InvitedVideoGraphers
from search_indexes import ContestIndex

from follow.models import Follow
from voting.models import Vote

from api.apps.videos.serializers import VideoSerializer
from api.apps.profiles.serializers import ProfileSearchSerializer, UserProfileSerializer, ProfileSerializer
from api.apps.profiles.models import Profile


class StoryBoardSerializer(serializers.ModelSerializer):

    class Meta:
        model = StoryBoard
        fields = ('image', )


class InvitedVideographersSerializer(serializers.ModelSerializer):
    invited = serializers.SerializerMethodField()
    # profile_picture = serializers.ImageField(source='user_profile.profile_picture')
    first_name = serializers.CharField(source='user.first_name')
    last_name = serializers.CharField(source='user.last_name')

    class Meta:
        # model = User
        model = Profile
        fields = ('id', 'invited', 'profile_picture', 'first_name', 'last_name')

    def get_invited(self, obj):
        return True


class ContestSerializer(serializers.ModelSerializer):
    slug = serializers.CharField(required=False)
    user_detail = UserProfileSerializer(source='user', read_only=True)
    expired = serializers.BooleanField(required=False, read_only=True)
    is_following = serializers.SerializerMethodField(read_only=True)
    followers_count = serializers.SerializerMethodField(read_only=True)
    is_liked = serializers.SerializerMethodField(read_only=True)
    likes_count = serializers.SerializerMethodField(read_only=True)
    story_board_images = serializers.SerializerMethodField(read_only=True)
    invited_people = serializers.SerializerMethodField(read_only=True)
    thumbnail = serializers.SerializerMethodField(read_only=True)
    submitted_video = serializers.SerializerMethodField(read_only=True)
    uploaded = serializers.SerializerMethodField(read_only=True)
    contest_creator = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Contest
        fields = ('id', 'slug', 'type', 'award_amount','user_detail',
                  'expiry_date', 'contest_title', 'category', 'contest_creator',
                  'purpose', 'expired', 'user', 'thumbnail', 'location', 'fps_rate',
                  'resolution', 'video_type',
                  'permission', 'status', 'art_direction', 'orientation',
                  'environment', 'model_release', 'space_for_copy', 'private',
                  'accept_curated_videos', 'lighting', 'level_required', 'uploaded',
                  'is_following', 'video_max_length', 'followers_count', 'submitted_video',
                  'is_liked', 'likes_count', 'created', 'story_board_images',
                  'invited_people', 'published')

    # def validate_location(self, attrs):

    def get_is_following(self, obj):
        user = self.context['request'].user
        if user.is_authenticated():
            follow = Follow.objects.filter(target_contest=obj,
                                           user=user).exists()
            return follow
        return False

    def get_is_liked(self, obj):
        user = self.context['request'].user
        if user.is_authenticated():
            content_type = ContentType.objects.get_for_model(obj)
            like = Vote.objects.filter(content_type=content_type,
                                       object_id=obj.id,
                                       user=user).exists()
            return like
        return False

    def get_followers_count(self, obj):
        follow = Follow.objects.filter(target_contest=obj).count()
        return follow

    def get_submitted_video(self, obj):
        entry = ContestEntry.objects.filter(contest=obj).count()
        return entry

    def get_uploaded(self, obj):
        uploaders = ContestEntry.objects.filter(contest=obj) \
                                        .values_list('video__user', flat=True) \
                                        .distinct()
        user = User.objects.filter(id__in=uploaders)
        uploaded_user = UserProfileSerializer(user,
                                              many=True,
                                              context={'request': self.context['request']}).data
        return uploaded_user

    def get_likes_count(self, obj):
        content_type = ContentType.objects.get_for_model(obj)
        follow = Vote.objects.filter(content_type=content_type,
                                     object_id=obj.id).count()
        return follow

    def get_contest_creator(self, obj):
        if obj.user and self.context['request'].user:
            if self.context['request'].user.id == obj.user.id:
                return True
        return False

    def get_story_board_images(self, obj):
        return StoryBoardSerializer(obj.get_storyboards.all().order_by('image_order'),
                                    many=True,
                                    context={'request': self.context['request']}).data

    def get_invited_people(self, obj):
        '''
        profiles = User.objects.filter(id__in=obj.invited_videographers.all() \
        .values_list('videographers__user__id', flat=True))
        '''
        ivs = obj.invited_videographers.all() \
                                       .values_list('videographers_id',
                                                    flat=True)
        profiles = Profile.objects.filter(id__in=ivs)
        # iv = ""
        # for profile in profiles:
        #     iv = iv + str(profile.id) + ","
        # print "get:" + iv
        return InvitedVideographersSerializer(profiles,
                                              many=True,
                                              context={'request': self.context['request']}).data

    def get_thumbnail(self, obj):
        try:
            return obj.category.all()[0].thumbnail.url
        except IndexError:
            return None

    def save(self, **kwargs):
        contest = super(ContestSerializer, self).save(**kwargs)
        if self.validated_data['user']:
            contest.user = self.validated_data['user']
            contest.save()
            if contest.user.user_profile.type == 'videographer':
                contest.user.user_profile.type = 'both'
                contest.user.user_profile.save()


class ContestSearchSerializer(HaystackSerializerMixin, ContestSerializer):
    # ToDo: make more-like-this search

    class Meta(ContestSerializer.Meta):
        model = Contest
        search_fields = ("text", 'type', 'award_amount',
                         'expiry_date', 'contest_title', 'purpose',
                         'expired', 'user', 'permission', 'video_type',
                         'resolution', 'status', 'orientation',
                         'level_required', 'created', 'published')


class ContestFacetSerializer(HaystackFacetSerializer):
    serialize_objects = True  # Setting this to True will serialize the
                               # queryset into an `objects` list. This
                               # is useful if you need to display the faceted
                               # results. Defaults to False.

    class Meta:
        index_classes = [ContestIndex]
        fields = ['tags', 'type']


class ContestMessageSerializer(serializers.ModelSerializer):
    user_detail = UserProfileSerializer(source='user', read_only=True)

    class Meta:
        model = ContestMessage
        fields = ('user', 'contest', 'message', 'user_detail')


class ContestEntrySerializer(serializers.ModelSerializer):
    video_data = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = ContestEntry
        fields = ('id', 'contest', 'video', 'winner', 'video_data')

    def get_video_data(self, obj):
        return VideoSerializer(obj.video, context={'request':self.context['request']}).data
