from rest_framework import routers

from django.conf.urls import patterns, include, url

from views import ContestView, ContestSearch, ExpiredContestView, ContestMessageView, ContestEntryViewSet, StoryBoardViewSet


router = routers.DefaultRouter()
router.register(r'contest', ContestView, base_name='contest')
router.register(r'save-story-board', StoryBoardViewSet, base_name='save-story-board')
router.register(r'contest-expired', ExpiredContestView, base_name='contest-expired')
router.register(r'contest-entries', ContestEntryViewSet, base_name='contest-entries')
router.register(r'search', viewset=ContestSearch, base_name="contest-search")
router.register(r'messages', ContestMessageView, base_name='contest-message')

urlpatterns = [
    url('', include(router.urls)),
]

