import datetime

from celery.task.schedules import crontab
from celery.decorators import periodic_task

# from django.contrib.contenttypes.models import ContentType

from api.apps.contests.models import Contest, ContestEntry
# from api.apps.payments.models import BoughtItems

# from api.apps.videos.models import Video
from api.apps.videos.models import PUBLISHED_TO_MARKETPLACE
from api.apps.videos.models import REVIEW_FOR_MARKETPLACE
from api.apps.videos.models import PUBLISHED_TO_CONTEST
from api.apps.reviews.models import VideoToReview
from api.apps.general.models import ReviewSystemConfig
from api.apps.reputations.level_privileges import LevelPrivilegeFilter


def get_all_videos_of_this_contest(contest):
    videos = [obj.video for obj in ContestEntry.objects.filter(contest=contest)]
    return videos


def get_purchased_videos_of_this_contest(contest):
    # videos = get_all_videos_of_this_contest(contest)
    # video_ids = [obj.id for obj in videos]
    # content_type = ContentType.objects.get_for_model(Video)
    #
    # objs = BoughtItems.objects.filter(content_type=content_type, object_id__in=video_ids)
    # videos_purchased = Video.objects.filter(id__in=[obj.object_id for obj in objs])
    objs = ContestEntry.objects.filter(contest=contest, winner=True)
    videos_purchased = [obj.video for obj in objs]
    return videos_purchased


def get_all_non_purchased_videos(contest):
    objs = ContestEntry.objects.filter(contest=contest, winner=False)
    all__non_purchased_videos = [obj.video for obj in objs]

    # all_videos = [obj.id for obj in get_all_videos_of_this_contest(contest)]
    # purchased_videos = [obj.id for obj in get_purchased_videos_of_this_contest(contest)]
    #
    # all__non_purchased_videos = Video.objects.filter(id__in=list(set(all_videos)-set(purchased_videos)))

    return all__non_purchased_videos


def apply_logic_non_curated_contest(contest):

    review_settings = ReviewSystemConfig.get_solo()
    over_all_rating_required = review_settings.overall_rating_required
    overall_positive_reviews_required = review_settings.overall_positive_reviews_required

    level_filter = LevelPrivilegeFilter(contest.user)

    purchased_videos = get_purchased_videos_of_this_contest(contest)

    for video in purchased_videos:
        video.status = PUBLISHED_TO_MARKETPLACE
        video.save()

    non_purchased_videos = get_all_non_purchased_videos(contest)
    for video in non_purchased_videos:
        # Put the video for review
        video_to_review_obj = VideoToReview()
        video_to_review_obj.video = video
        video_to_review_obj.user_level = level_filter.user_level
        video_to_review_obj.reviews_max = over_all_rating_required
        video_to_review_obj.reviews_positive_required = overall_positive_reviews_required
        video_to_review_obj.save()

        video.status = REVIEW_FOR_MARKETPLACE
        video.save()


def apply_logic_curated_contest(contest):
    videos = get_all_videos_of_this_contest(contest)
    for video in videos:
        if video.status == PUBLISHED_TO_CONTEST:
            video.status = PUBLISHED_TO_MARKETPLACE
            video.save()


def close_contest(contest):
    contest.status = 'closed'
    contest.save()

    if contest.accept_curated_videos:
        apply_logic_curated_contest(contest)
        return
    apply_logic_non_curated_contest(contest)


from api.celery import celery_only_one


# execute daily at midnight
@periodic_task(run_every=(crontab(minute=0, hour=0)),
               name="check_contest_expiry_date",
               ignore_result=True)
# execute only one instance at a time
@celery_only_one(key="check_contest_expiry_date",
                 timeout=10*60)  # 10 minutes
def check_contest_expiry_date(**kwargs):
    # enable sleep for unit testing
    # import time
    # print "sleeping..."
    # time.sleep(3600)
    # ./manage.py shell
    # from api.apps.contests.tasks import check_contest_expiry_date
    # check_contest_expiry_date.apply()

    # contests = Contest.objects.filter(expired=False)
    contests = Contest.objects.filter(status='publish')
    for contest in contests:
        if contest.expiry_date.date() < datetime.datetime.now().date():
            contest.expired = True
            close_contest(contest)
            continue

        if not contest.closing_date:
            continue

        if contest.closing_date.date() < datetime.datetime.now().date():
            close_contest(contest)
