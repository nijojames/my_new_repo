from django.utils import timezone

from dirtyfields import DirtyFieldsMixin

from notifications.signals import notify
from actstream import action, models

from api.utl.utl import get_user_device
from api.apps.activities.follow_utiils import get_followers
from api.apps.utils import activities, utils
from api.apps.reviews.services import ContestReputationService


class ContestService(object):

    def contest_creation_activity_feed(self, **kwargs):
        """
        Actions to be performed after the creation of contest
        :param kwargs:
        :return:
        """
        instance = kwargs['instance']
        created = kwargs['created']
        if self.__need_create_contest_feed(instance, created):
            action.send(instance.user, verb=activities.FEED_CONTEST_CREATED, target=instance)

    def __need_create_contest_feed(self, instance, created):
        if created:
            if instance.status == 'publish':
                return True
        else:
            if instance.get_dirty_fields().get('status') and instance.status == 'publish' and \
                            instance.expiry_date > timezone.now():
                return True
        return False


class ContestEntryService(object):

    def contest_winning_activity(self, **kwargs):
        """
        Actions that to be done after a winner is selected for the contest
        :param kwargs:
        :return:
        """
        instance = kwargs['instance']
        created = kwargs['created']
        if self.__check_won_contest(instance, created):
            notify.send(instance.video.user, recipient=instance.video.user, verb=activities.NOTIFICATION_WON_CONTEST,
                        action_object=instance.video, target=instance.contest)
            # TODO send email to won person
            action.send(instance.video.user, verb=activities.WON_CONTEST,
                        action_object=instance.video, target=instance.contest)

    def __check_won_contest(self, instance, created):
        if not created and instance.winner:
            contest_reputation_service = ContestReputationService()
            contest_reputation_service.reputation_update_after_contest_won(instance.video)
            return True
        return False
