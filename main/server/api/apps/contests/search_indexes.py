from haystack import indexes

from models import Contest


class ContestIndex (indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    type = indexes.CharField(model_attr='type')
    award_amount = indexes.CharField(model_attr='award_amount')
    contest_title = indexes.CharField(model_attr='contest_title')
    concept = indexes.CharField(model_attr='purpose')
    permission = indexes.CharField(model_attr='permission')
    status = indexes.CharField(model_attr='status')
    orientation = indexes.CharField(model_attr='orientation')
    video_type = indexes.CharField(model_attr='video_type', null=True)
    resolution = indexes.CharField(model_attr='resolution', null=True)
    level_required = indexes.CharField(model_attr='level_required')
    created = indexes.DateTimeField(model_attr='created', null=True)
    user = indexes.CharField(model_attr='user', null=True)
    private = indexes.BooleanField(model_attr='private')
    invited_videographers = indexes.MultiValueField()
    published = indexes.DateTimeField(model_attr='published', null=True)

    def get_model(self):
        return Contest

    def index_queryset(self, using=None):
        """Used when the entire index for the recipe model is updated"""
        # return self.get_model().objects.filter(expired=False)
        return self.get_model().objects.all()

    def prepare_invited_videographers(self, obj):
        return [videographer.videographers.user.username
                for videographer in
                obj.invited_videographers.all()]
