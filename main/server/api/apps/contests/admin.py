from django.contrib import admin
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.db.models.fields.files import ImageField

from import_export.admin import ImportExportModelAdmin

import models


# class AdminImageWidget(AdminFileWidget):
#     def render(self, name, value, attrs=None):
#         output = []
#         if value and getattr(value, "url", None):
#             image_url = value.url
#             file_name=str(value)
#             output.append(u' <a href="%s" target="_blank"><img src="%s" width="100" height="100" alt="%s" /></a> %s ' % \
#                 (image_url, image_url, file_name, _(' ')))
#         output.append(super(AdminFileWidget, self).render(name, value, attrs))
#         return mark_safe(u''.join(output))


# class ImagePreviewEnabledAdmin(admin.ModelAdmin):
#
#     def formfield_for_dbfield(self, db_field, **kwargs):
#         if db_field.__class__ == ImageField:
#             request = kwargs.pop("request", None)
#             kwargs['widget'] = AdminImageWidget
#             return db_field.formfield(**kwargs)
#         return super(ImagePreviewEnabledAdmin, self).formfield_for_dbfield(db_field, **kwargs)


class StoryBoardInline(admin.TabularInline):
    model = models.StoryBoard
    max_num = 5


class InvitedVideoGraphersAdminInline(admin.TabularInline):
    model = models.InvitedVideoGraphers
    max_num = 10


class ContestAdmin(ImportExportModelAdmin):

    inlines = [StoryBoardInline, InvitedVideoGraphersAdminInline]
    search_fields = ['contest_title', ]
    list_filter = ('type', )
    list_display = ['contest_title',
                    'status',
                    'user',
                    'expiry_date']


class MessageAdmin(ImportExportModelAdmin):
    search_fields = ['contest', 'user']
    list_display = ['contest', 'user', 'created']


class ContestEntry(ImportExportModelAdmin):
    pass


admin.site.register(models.Contest, ContestAdmin)
admin.site.register(models.ContestEntry, ContestEntry)
admin.site.register(models.ContestMessage, MessageAdmin)
