# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0002_contest_expired'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contest',
            options={'verbose_name': 'Contest', 'verbose_name_plural': 'Contests'},
        ),
        migrations.AlterModelOptions(
            name='contestentry',
            options={'verbose_name': 'ContestEntry', 'verbose_name_plural': 'Contest Entries'},
        ),
        migrations.AlterModelOptions(
            name='requirement',
            options={'verbose_name': 'Requirement', 'verbose_name_plural': 'Requirements'},
        ),
    ]
