# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import autoslug.fields


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0006_auto_20160226_0724'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contest',
            name='slug',
            field=autoslug.fields.AutoSlugField(editable=True, unique=True, populate_from=b'contest_title'),
        ),
    ]
