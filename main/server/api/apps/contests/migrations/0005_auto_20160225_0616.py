# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0004_auto_20160121_1256'),
    ]

    operations = [
        migrations.AddField(
            model_name='contest',
            name='permission',
            field=models.CharField(default=b'free', max_length=20, choices=[(b'royalty_free', 'Royalty Free'), (b'commercial', 'Commercial'), (b'free', 'Free')]),
        ),
        migrations.AddField(
            model_name='contest',
            name='status',
            field=models.CharField(default=1, max_length=6, choices=[(b'open', b'Open'), (b'closed', b'Closed')]),
            preserve_default=False,
        ),
    ]
