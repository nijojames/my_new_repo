# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0023_auto_20160703_1014'),
    ]

    operations = [
        migrations.AddField(
            model_name='contest',
            name='published',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
