# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0013_auto_20160323_1349'),
    ]

    operations = [
        migrations.AddField(
            model_name='storyboard',
            name='image_order',
            field=models.IntegerField(default=0),
        ),
    ]
