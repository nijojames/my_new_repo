# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0010_contest_created'),
    ]

    operations = [
        migrations.AddField(
            model_name='contestentry',
            name='winner',
            field=models.BooleanField(default=False),
        ),
    ]
