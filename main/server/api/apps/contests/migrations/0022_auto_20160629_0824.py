# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0021_auto_20160626_1952'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contest',
            name='fps_rate',
            field=models.CharField(blank=True, max_length=10, null=True, choices=[(b'30', '30fps'), (b'60', '60fps'), (b'120', '120fps'), (b'240', '240fps')]),
        ),
        migrations.AlterField(
            model_name='contest',
            name='resolution',
            field=models.CharField(blank=True, max_length=10, null=True, choices=[(b'720', '720p'), (b'1080', '1080p'), (b'4096', '4k')]),
        ),
        migrations.AlterField(
            model_name='contest',
            name='video_type',
            field=models.CharField(blank=True, max_length=30, null=True, choices=[(b'real_time', 'Real Time'), (b'time_lapse', 'Time Lapse'), (b'slow_motion', 'Slow Motion')]),
        ),
    ]
