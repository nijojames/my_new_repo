# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0018_auto_20160503_0017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contest',
            name='model_release',
            field=models.BooleanField(default=False),
        ),
    ]
