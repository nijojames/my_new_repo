# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import geoposition.fields


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0014_storyboard_image_order'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contestlocations',
            name='contest',
        ),
        migrations.AddField(
            model_name='contest',
            name='location',
            field=geoposition.fields.GeopositionField(max_length=42, null=True, blank=True),
        ),
        migrations.DeleteModel(
            name='ContestLocations',
        ),
    ]
