# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0019_auto_20160621_1848'),
    ]

    operations = [
        migrations.AddField(
            model_name='contest',
            name='closing_date',
            field=models.DateTimeField(default=None, null=True),
        ),
    ]
