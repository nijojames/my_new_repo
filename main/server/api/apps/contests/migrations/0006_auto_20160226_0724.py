# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import autoslug.fields
import geoposition.fields
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0002_auto_20150616_2121'),
        ('profiles', '0003_auto_20160223_0551'),
        ('contests', '0005_auto_20160225_0616'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContestLocations',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('location', geoposition.fields.GeopositionField(max_length=42)),
            ],
            options={
                'verbose_name': 'Contest Location',
                'verbose_name_plural': 'Contest Locations',
            },
        ),
        migrations.CreateModel(
            name='InvitedVideoGraphers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name': 'Invited Videographer',
                'verbose_name_plural': 'Invited Videographers',
            },
        ),
        migrations.CreateModel(
            name='StoryBoard',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(null=True, upload_to=b'contest/storyboard', blank=True)),
                ('video_url', models.URLField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Story Board',
                'verbose_name_plural': 'Story Boards',
            },
        ),
        migrations.RemoveField(
            model_name='requirement',
            name='contest',
        ),
        migrations.RenameField(
            model_name='contest',
            old_name='concept',
            new_name='purpose',
        ),
        migrations.RemoveField(
            model_name='contest',
            name='reward_description',
        ),
        migrations.AddField(
            model_name='contest',
            name='accept_curated_videos',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='contest',
            name='art_direction',
            field=models.TextField(default=1, max_length=1500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='contest',
            name='environment',
            field=models.CharField(default=b'any', max_length=30, choices=[(b'any', 'Any'), (b'indoor', 'Indoor'), (b'outdoor', 'Outdoor')]),
        ),
        migrations.AddField(
            model_name='contest',
            name='level_required',
            field=models.IntegerField(default=1, choices=[(1, 'One'), (2, 'Two'), (3, 'Three'), (4, 'Four'), (5, 'Five')]),
        ),
        migrations.AddField(
            model_name='contest',
            name='model_release',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='contest',
            name='orientation',
            field=models.CharField(default=b'any', max_length=30, choices=[(b'any', 'Any'), (b'portrait', 'Portrait'), (b'landscape', 'Landscape'), (b'square', 'Square')]),
        ),
        migrations.AddField(
            model_name='contest',
            name='public',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='contest',
            name='slug',
            field=autoslug.fields.AutoSlugField(default=1, editable=False, populate_from=b'contest_title', unique=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='contest',
            name='space_for_copy',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='contest',
            name='tags',
            field=taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', blank=True, help_text=b'separate with commas', verbose_name='tags'),
        ),
        migrations.AddField(
            model_name='contest',
            name='video_max_length',
            field=models.IntegerField(default=10),
        ),
        migrations.AlterField(
            model_name='contest',
            name='status',
            field=models.CharField(max_length=6, choices=[(b'publish', b'Publish'), (b'review', b'Review'), (b'closed', b'Closed')]),
        ),
        migrations.DeleteModel(
            name='Requirement',
        ),
        migrations.AddField(
            model_name='storyboard',
            name='contest',
            field=models.ForeignKey(related_name='get_storyboards', to='contests.Contest'),
        ),
        migrations.AddField(
            model_name='invitedvideographers',
            name='contest',
            field=models.ForeignKey(related_name='invited_videographers', to='contests.Contest'),
        ),
        migrations.AddField(
            model_name='invitedvideographers',
            name='videographers',
            field=models.ForeignKey(to='profiles.Profile'),
        ),
        migrations.AddField(
            model_name='contestlocations',
            name='contest',
            field=models.ForeignKey(related_name='locations', to='contests.Contest'),
        ),
    ]
