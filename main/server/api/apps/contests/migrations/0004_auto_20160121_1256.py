# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0003_auto_20160118_1352'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requirement',
            name='resolution',
            field=models.CharField(default=b'3840X2160', max_length=20),
        ),
    ]
