# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0020_contest_closing_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contest',
            name='closing_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
