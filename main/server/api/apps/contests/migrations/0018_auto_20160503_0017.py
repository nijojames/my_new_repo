# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0017_auto_20160426_1423'),
    ]

    operations = [
        migrations.RenameField(
            model_name='contest',
            old_name='public',
            new_name='private',
        ),
    ]
