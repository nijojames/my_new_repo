# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contests', '0008_contest_thumbnail'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContestMessage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('message', models.TextField(max_length=1000)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
        ),
        migrations.AlterField(
            model_name='contest',
            name='status',
            field=models.CharField(max_length=7, choices=[(b'publish', b'Publish'), (b'review', b'Review'), (b'closed', b'Closed')]),
        ),
        migrations.AddField(
            model_name='contestmessage',
            name='contest',
            field=models.ForeignKey(to='contests.Contest'),
        ),
        migrations.AddField(
            model_name='contestmessage',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
