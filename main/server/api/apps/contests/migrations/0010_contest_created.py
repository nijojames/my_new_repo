# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0009_auto_20160303_0910'),
    ]

    operations = [
        migrations.AddField(
            model_name='contest',
            name='created',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
