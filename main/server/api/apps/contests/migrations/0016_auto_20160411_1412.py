# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0005_category'),
        ('contests', '0015_auto_20160408_0731'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contest',
            name='thumbnail',
        ),
        migrations.AddField(
            model_name='contest',
            name='category',
            field=models.ManyToManyField(to='general.Category'),
        ),
    ]
