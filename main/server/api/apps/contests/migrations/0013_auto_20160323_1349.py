# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0012_remove_contestentry_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='contest',
            name='lighting',
            field=models.CharField(default=b'any', max_length=30, choices=[(b'any', 'Any'), (b'natural', 'Natural'), (b'studio', 'Studio')]),
        ),
        migrations.AlterField(
            model_name='contest',
            name='status',
            field=models.CharField(max_length=10, choices=[(b'publish', b'Publish'), (b'review', b'Review'), (b'closed', b'Closed'), (b'draft', b'Draft'), (b'incomplete', b'Incomplete')]),
        ),
        migrations.AlterField(
            model_name='contest',
            name='user',
            field=models.ForeignKey(related_name='get_all_contest', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
