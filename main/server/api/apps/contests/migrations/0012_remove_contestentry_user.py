# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0011_contestentry_winner'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contestentry',
            name='user',
        ),
    ]
