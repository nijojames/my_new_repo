# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('videos', '0003_video_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=2, choices=[(b'S', b'Sponsored'), (b'SP', b'SpyHop')])),
                ('award_amount', models.DecimalField(max_digits=9, decimal_places=2)),
                ('expiry_date', models.DateTimeField()),
                ('contest_title', models.CharField(max_length=255)),
                ('concept', models.TextField()),
                ('reward_description', models.TextField()),
                ('user', models.ForeignKey(related_name='get_all_contest', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ContestEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('contest', models.ForeignKey(to='contests.Contest')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('video', models.ForeignKey(to='videos.Video')),
            ],
        ),
        migrations.CreateModel(
            name='Requirement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('orientation', models.CharField(default=b'Portrait', max_length=100)),
                ('resolution', models.CharField(default=b'1920X1080', max_length=20)),
                ('frame_rate', models.CharField(default=b'30fps', max_length=100)),
                ('audio', models.CharField(default=b'Not considered', max_length=255)),
                ('environment', models.CharField(default=b'Not considered', max_length=255)),
                ('people', models.CharField(default=b'Not considered', max_length=255)),
                ('release', models.CharField(default=b'Not considered', max_length=255)),
                ('contest', models.OneToOneField(to='contests.Contest')),
            ],
        ),
    ]
