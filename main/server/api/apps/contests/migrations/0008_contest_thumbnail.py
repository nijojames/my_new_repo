# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contests', '0007_auto_20160226_0844'),
    ]

    operations = [
        migrations.AddField(
            model_name='contest',
            name='thumbnail',
            field=models.ImageField(null=True, upload_to=b'contest/thumbnail', blank=True),
        ),
    ]
