from django.apps import AppConfig
from actstream import registry


class ContestAppConfig(AppConfig):
    name = 'api.apps.contests'

    def ready(self):
        registry.register(self.get_model('contest'))
