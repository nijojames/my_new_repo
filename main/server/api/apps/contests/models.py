from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from taggit.managers import TaggableManager
from follow.utils import follow
from notifications.signals import notify
from actstream import action
from autoslug import AutoSlugField
from geoposition.fields import GeopositionField
from dirtyfields import DirtyFieldsMixin

from services import ContestService, ContestEntryService
from api.apps.videos.models import Video
from api.apps.profiles.models import Profile
from api.apps.activities.follow_utiils import register, get_followers
from api.apps.utils import choices, activities
from api.apps.general.models import Category

from django_extensions.db.models import TimeStampedModel

from api.apps.reviews.services import ReviewReputationServices

from api.apps.reviews.models import Reviews


class Contest(DirtyFieldsMixin, models.Model):
    TYPE = (('S', 'Sponsored'),
            ('SP', 'SpyHop'))

    STATUS_TYPE = (('publish', 'Publish'),
                   ('review', 'Review'),
                   ('closed', 'Closed'),
                   ('draft', 'Draft'),
                   ('incomplete', 'Incomplete')
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             related_name='get_all_contest',
                             null=True,
                             blank=True)
    type = models.CharField(max_length=2, choices=TYPE)
    award_amount = models.DecimalField(decimal_places=2, max_digits=9)
    expiry_date = models.DateTimeField()
    contest_title = models.CharField(max_length=255)
    category = models.ManyToManyField(Category)
    slug = AutoSlugField(populate_from='contest_title',
                         unique=True,
                         editable=True)
    purpose = models.TextField()
    tags = TaggableManager(_('tags'),
                           help_text="separate with commas",
                           blank=True)
    art_direction = models.TextField(max_length=1500)
    permission = models.CharField(choices=choices.PERMISSION_TYPE,
                                  max_length=20,
                                  default=choices.COMMERCIAL)
    expired = models.BooleanField(default=False)
    level_required = models.IntegerField(choices=choices.LEVELS_REQUIRED,
                                         default=choices.ONE)
    status = models.CharField(max_length=10, choices=STATUS_TYPE)
    orientation = models.CharField(choices=choices.CONTEST_ORIENTATION_CHOICES,
                                   max_length=30,
                                   default=choices.ANY)
    environment = models.CharField(choices=choices.CONTEST_ENVIRONMENT_OPTIONS,
                                   max_length=30,
                                   default=choices.ANY)
    lighting = models.CharField(choices=choices.CONTEST_LIGHTNING_OPTIONS,
                                max_length=30,
                                default=choices.ANY)
    video_type = models.CharField(choices=choices.VIDEO_TYPE,
                                  max_length=30,
                                  null=True,
                                  blank=True)
    fps_rate = models.CharField(choices=choices.FPS_CHOICES,
                                max_length=10,
                                null=True,
                                blank=True)
    resolution = models.CharField(choices=choices.RESOLUTION_CHOICES,
                                  max_length=10,
                                  null=True,
                                  blank=True)
    video_max_length = models.IntegerField(default=10)
    space_for_copy = models.BooleanField(default=False)
    model_release = models.BooleanField(default=False)
    location = GeopositionField(null=True, blank=True)
    private = models.BooleanField(default=False)
    accept_curated_videos = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True,
                                   auto_now=False,
                                   null=True)
    # this date is set when the first purchase is made by the buyer
    closing_date = models.DateTimeField(null=True, blank=True)
    # date when contest is published
    published = models.DateTimeField(auto_now_add=False,
                                     auto_now=False,
                                     null=True,
                                     blank=True)

    class Meta:
        verbose_name = 'Contest'
        verbose_name_plural = 'Contests'

    def __unicode__(self):
        return self.contest_title

    def save(self, *args, **kwargs):
        dirty_fields = self.get_dirty_fields()
        if 'status' in dirty_fields:
            if self.status == 'publish':
                self.published = timezone.now()

        super(Contest, self).save(*args, **kwargs)


contest_service = ContestService()
post_save.connect(contest_service.contest_creation_activity_feed,
                  sender=Contest,
                  weak=False)

register(Contest)


class ContestEntry(DirtyFieldsMixin, models.Model):
    contest = models.ForeignKey(Contest)
    video = models.ForeignKey(Video)
    winner = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'ContestEntry'
        verbose_name_plural = 'Contest Entries'

    def __unicode__(self):
        return ('%s entry in ' %
                self.video.title +
                ' %s' %
                self.contest.contest_title)

contest_entry_service = ContestEntryService()
post_save.connect(contest_entry_service.contest_winning_activity,
                  sender=ContestEntry,
                  weak=False)


class StoryBoard(models.Model):
    contest = models.ForeignKey(Contest, related_name='get_storyboards')
    image = models.ImageField(upload_to='contest/storyboard',
                              null=True,
                              blank=True)
    video_url = models.URLField(null=True, blank=True)
    image_order = models.IntegerField(default=0)

    class Meta:
        verbose_name = 'Story Board'
        verbose_name_plural = 'Story Boards'

    def __unicode__(self):
        return self.contest.contest_title


class InvitedVideoGraphers(models.Model):
    contest = models.ForeignKey(Contest, related_name='invited_videographers')
    videographers = models.ForeignKey(Profile)

    class Meta:
        verbose_name = 'Invited Videographer'
        verbose_name_plural = 'Invited Videographers'

    def __unicode__(self):
        return '%s invited to %s' % (self.videographers.user,
                                     self.contest.contest_title)


class ContestMessage(TimeStampedModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    contest = models.ForeignKey(Contest)
    message = models.TextField(max_length=1000)

    def __unicode__(self):
        return unicode(self.user)


review_reputation_services = ReviewReputationServices()
post_save.connect(review_reputation_services._post_save_signal_callback,
                  sender=Reviews,
                  weak=False)
