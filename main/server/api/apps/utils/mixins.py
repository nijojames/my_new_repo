import re


class BaseSearchMixin(object):
    """
    Base mixin for search related flow
    """
    def _get_price_range(self, price_range):
        """
        Get qs based on range of award_price
        :param price_range: String
        :return: List
        """
        if price_range and re.search('\d+,\d+', price_range):
            price_range = price_range.split(',')
            price_range = [float(price.strip()) for price in price_range]
        else:
            price_range = None
        return price_range

    def _get_orientation(self, orientation):
        if orientation:
            if ',' in orientation:
                orientation = orientation.split(',')

            else:
                orientation = orientation.split()

        return orientation

    def _get_resolution(self, resolution):
        if resolution:
            if ',' in resolution:
                resolution = resolution.split(',')

            else:
                resolution = resolution.split()

        return resolution

    def _get_video_type(self, video_type):
        if video_type:
            if ',' in video_type:
                video_type = video_type.split(',')

            else:
                video_type = video_type.split()

        return video_type


class VideoSearchMixin(BaseSearchMixin):
    """
    Support methods for video search
    """
    def _get_levels(self, levels):
        if levels:
            if ',' in levels:
                levels = levels.split(',')

            else:
                levels = levels.split()

        return levels


class ContestSearchMixin(BaseSearchMixin):
    pass
