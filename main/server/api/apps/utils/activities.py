

LIKED = 'liked'
FOLLOWED = 'followed'
WON_CONTEST = 'won contest'
NOTIFICATION_WON_CONTEST = 'won contest'
SPAMMED_VIDEO = 'spammed video'

FEED_CONTEST_CREATED = 'created a new contest'
FEED_UPLOADED_VIDEO = 'uploaded a new video'

VIDEO_PUBLISHED = 'video published'

VIDEO_REJECTED = 'video rejected'

REVIEW_EXPIRED = 'review expired'

