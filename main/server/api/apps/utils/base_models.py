# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey


class TimeStampedModelBase(models.Model):
    """ TimeStampedModel
    An abstract base class model that provides "created" and
    "modified" fields.
    """
    created = models.DateTimeField(verbose_name=_('Created'))
    modified = models.DateTimeField(verbose_name=_('Modified'))

    class Meta:
        abstract = True


class GenericModelBase(TimeStampedModelBase):
    """
    Base model to create a generic relation
    """
    content_type = models.ForeignKey(ContentType)
    # this is the id of the object that is linked to the like model
    object_id = models.PositiveIntegerField()
    relation = GenericForeignKey('content_type', 'object_id')

    class Meta:
        abstract = True