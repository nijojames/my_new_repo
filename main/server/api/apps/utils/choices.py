from django.utils.translation import ugettext_lazy as _

ANY = 'any'

# Permissions
ROYALTY_FREE = 'royalty_free'
COMMERCIAL = 'commercial'

PERMISSION_TYPE = (
    (ROYALTY_FREE, _('Royalty Free')),
    (COMMERCIAL, _('Commercial')),
)


#Orientations
PORTRAIT = 'portrait'
SQUARE = 'square'
LANDSCAPE = 'landscape'

ORIENTATION_TYPE = (
    (PORTRAIT, _('Portrait')),
    (LANDSCAPE, _('Landscape')),
    (SQUARE, _('Square'))
)


#Contest orientations
CONTEST_ORIENTATION_CHOICES = (
    (ANY, _('Any')),
    (PORTRAIT, _('Portrait')),
    (LANDSCAPE, _('Landscape')),
    (SQUARE, _('Square'))
)

ONE = 1
TWO = 2
THREE = 3
FOUR = 4
FIVE = 5

LEVELS_REQUIRED = (
    (ONE, _('One')),
    (TWO, _('Two')),
    (THREE, _('Three')),
    (FOUR, _('Four')),
    (FIVE, _('Five'))
)

#Contest Environment
INDOOR = 'indoor'
OUTDOOR = 'outdoor'

CONTEST_ENVIRONMENT_OPTIONS = (
    (ANY, _('Any')),
    (INDOOR, _('Indoor')),
    (OUTDOOR, _('Outdoor'))
)

#Contest_Lightning
NATURAL = 'natural'
STUDIO = 'studio'

CONTEST_LIGHTNING_OPTIONS = (
    (ANY, _('Any')),
    (NATURAL, _('Natural')),
    (STUDIO, _('Studio'))
)

REAL_TIME = 'real_time'
TIME_LAPSE = 'time_lapse'
SLOW_MOTION = 'slow_motion'

VIDEO_TYPE = (
    (REAL_TIME, _('Real Time')),
    (TIME_LAPSE, _('Time Lapse')),
    (SLOW_MOTION, _('Slow Motion')),
)

FPS_CHOICES = (
    ('30', _('30fps')),
    ('60', _('60fps')),
    ('120', _('120fps')),
    ('240', _('240fps')),
)

RESOLUTION_CHOICES = (
    ('720', _('720p')),
    ('1080', _('1080p')),
    ('4096', _('4k')),
)
