from datetime import date
from django.contrib.auth.models import ContentType


def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))


def get_slug(obj):
    content_type = ContentType.objects.get_for_model(obj).model
    content = {}
    content['type'] = content_type
    if content_type == 'video':
        content['slug'] = obj.slug
    elif content_type == 'profile':
        content['slug'] = obj.user.username
    elif content_type == 'contest':
        content['slug'] = obj.slug
    else:
        content['slug'] = None
    return content