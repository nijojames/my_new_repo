from rest_framework import permissions


class LikePermission(permissions.BasePermission):
    """
    Permission for user edit only his own like instance.
    """

    def has_object_permission(self, request, view, obj):
        has_permission = True
        if request.method in ['PUT', 'DELETE', 'PATCH']:
            if request.user.username != obj.user.username:
                has_permission = False

        return has_permission