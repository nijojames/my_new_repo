from rest_framework import routers

from django.conf.urls import patterns, include, url

from views import UserLikeViewSet


router = routers.DefaultRouter()


router.register(r'like', UserLikeViewSet, base_name='like')

urlpatterns = [
    url('', include(router.urls)),
]
