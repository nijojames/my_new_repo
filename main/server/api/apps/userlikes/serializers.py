from voting.models import Vote

from rest_framework import serializers

from django.contrib.contenttypes.models import ContentType


class UserLikeSerializer(serializers.ModelSerializer):
    models_to_flag = ['videos.video', 'profiles.profile', ]  # entries can be added here

    content_type = serializers.CharField(required=True)

    class Meta:
        model = Vote
        fields = ('pk', 'content_type', 'object_id', 'vote', 'user')

    def validate_content_type(self, attrs):
        """
        Custom validations are given for content_type content
        :param attrs:
        :return:
        """
        if not self.context['request'].secretballot_token:
            raise serializers.ValidationError('Are you a Bot?')
        content_type = attrs
        if not(content_type in self.models_to_flag):
            raise serializers.ValidationError('Only %s can be Liked' % (', '.join(self.models_to_flag)))

        if '.' in content_type:
            app, model_name = content_type.split('.')
        else:
            raise serializers.ValidationError('value should be app_name.Model_name, ie videos.video')
        try:
            content_type = ContentType.objects.get(app_label=app, model__iexact=model_name)
        except:
            raise serializers.ValidationError('Not a valid content type')

        return content_type






