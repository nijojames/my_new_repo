# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('secretballot', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserLike',
            fields=[
                ('vote_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='secretballot.Vote')),
                ('user', models.ForeignKey(related_name='likes', to=settings.AUTH_USER_MODEL)),
            ],
            bases=('secretballot.vote',),
        ),
    ]
