from serializers import UserLikeSerializer

from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions, status
from rest_framework import response
from rest_framework import serializers

from django.contrib.contenttypes.models import ContentType

from voting.models import Vote
from permissions import LikePermission



class UserLikeViewSet(ModelViewSet):
    """
    API end point to like/unlike Videos. Here the object_id should be 'id' of the content to be flagged
    and content_type should be app_name.model_name ie,
    For Video videos.video
    To unlike give a request with DELETE header. The logic is deleting the entry from model.
    """
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, LikePermission, ]
    serializer_class = UserLikeSerializer
    http_method_names = ['post']
    model = Vote
    queryset = Vote.objects.all()

    def create(self, request, *args, **kwargs):
        if hasattr(request.data, '_mutable'):
            request.data._mutable = True
        request.data['user'] = request.user.id
        request.data['vote'] = +1
        content_type = request.data.get('content_type')
        if content_type and '.' in content_type:
            app, model_name = content_type.split('.')
        try:
            content_type = ContentType.objects.get(app_label=app, model__iexact=model_name)
            vote = Vote.objects.filter(user=request.user, object_id=request.data['object_id'], content_type=content_type)
            if vote:
                vote.delete()
                return response.Response(status=status.HTTP_204_NO_CONTENT)
        except Exception:
            raise serializers.ValidationError('Not a valid content type or object provided')

        return super(UserLikeViewSet, self).create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save()
