from rest_framework import routers

from django.conf.urls import patterns, include, url

from api.apps.activities.views import FollowViewSet, FeedViewSet, NotificationViewSet, \
    ReadAllNotificationViewSet, UnreadNotificationCount

router = routers.DefaultRouter()

# Register api urls to router here
router.register(r'follow', FollowViewSet, base_name='follow')
router.register(r'notification', NotificationViewSet, base_name='notification')
router.register(r'read-all-notifications', ReadAllNotificationViewSet, base_name='mark-read-all')
router.register(r'feeds', FeedViewSet, base_name='feeds')

urlpatterns = [
    url(r'^unread-count/$', UnreadNotificationCount.as_view(), name='unread-count'),
    url('', include(router.urls))
]
