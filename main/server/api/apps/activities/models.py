from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


from .services import ActivityServices
from notifications.models import Notification
from actstream.models import Action
from voting.models import Vote
from follow.models import Follow

activity_service = ActivityServices()
post_save.connect(activity_service.like_activity, sender=Vote, weak=False)
post_save.connect(activity_service.follow_activity, sender=Follow, weak=False)
post_save.connect(activity_service.send_push_notification, sender=Notification, weak=False)
