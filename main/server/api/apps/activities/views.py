from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_200_OK, HTTP_204_NO_CONTENT, HTTP_201_CREATED

from follow.models import Follow
from follow.utils import toggle

from actstream.models import Action

from serializers import FollowSerializer, FeedSerializer, NotificationSerializer
from api.utl.utl import get_genric_object
from api.apps.profiles.models import Profile
from api.apps.videos.models import Video
from api.apps.contests.models import Contest
from api.apps.activities.follow_utiils import get_followers_for_object


def exclude_user(user, qs):
    utype = ContentType.objects.get_for_model(user)
    return qs.exclude(actor_content_type__pk=utype.id,
                      actor_object_id=user.id)

class FollowViewSet(ModelViewSet):
    """
    Endpoint to follow or un-follow a particular video, profile, or contest
    """
    serializer_class = FollowSerializer
    http_method_names = ['post']
    permission_classes = [IsAuthenticated]
    queryset = Follow.objects.all()

    class Meta:
        model = Follow

    def create(self, request, *args, **kwargs):
        if hasattr(request.data, '_mutable'):
            request.data._mutable = True
        request.data['user'] = request.user.id
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        obj = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        content_type = serializer.data['content_type']
        kwargs = {'target_{0}'.format(content_type): obj, 'user': request.user}
        try:
            Follow.objects.get(**kwargs)
        except Follow.DoesNotExist:
            return Response(status=HTTP_204_NO_CONTENT)
        return Response(serializer.data, status=HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        user = self.request.user
        model_name = serializer.data['content_type']
        obj_id = serializer.data['obj_id']
        obj = get_genric_object(model_name, obj_id)
        toggle(user, obj)
        return obj


class NotificationViewSet(ModelViewSet):
    """
    Endpoint to view notifications of user and mark them read
    """
    serializer_class = NotificationSerializer
    http_method_names = ['get', 'put']
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return exclude_user(self.request.user,
                            self.request.user.notifications.all()).order_by('-timestamp')
        # return self.request.user.notifications.all().order_by('-timestamp')

    def update(self, request, *args, **kwargs):
        obj = self.get_object()
        serializer = self.get_serializer(obj, data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
        obj.mark_as_read()
        return Response(status=HTTP_200_OK)


class ReadAllNotificationViewSet(ModelViewSet):
    """
    Endpoint to mark read feeds of user
    """
    http_method_names = ['post', ]
    serializer_class = NotificationSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return exclude_user(self.request.user,
                            self.request.user.notifications.all())
        #return self.request.user.notifications.all()

    def create(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        queryset.mark_all_as_read(recipient=self.request.user)
        # serializer = self.serializer_class(queryset, many=True)
        # return Response(serializer.data)
        return Response(status=HTTP_200_OK)


class FeedViewSet(ModelViewSet):
    """
    Endpoint to get all the feeds of user
    """
    serializer_class = FeedSerializer
    queryset = Action.objects.all()
    permission_classes = [IsAuthenticated]
    http_method_names = ['get', ]

    def get_queryset(self):
        followers = self.request.user.following.exclude(target_profile_id=None).values_list('target_profile_id', flat=True)
        follower_users = Profile.objects.filter(id__in=followers)
        follower_users = User.objects.filter(id__in=follower_users)
        feeds = []
        for user in follower_users:
            for act in Action.objects.actor(user):
                feeds.append(act.id)
        following_videos = self.request.user.following.exclude(target_video_id=None).values_list('target_video_id', flat=True)
        following_videos = Video.objects.filter(id__in=following_videos)
        for video in following_videos:
            for act in Action.objects.target(video):
                feeds.append(act.id)
        following_contest = self.request.user.following.exclude(target_contest_id=None).values_list('target_contest_id', flat=True)
        following_contest = Contest.objects.filter(id__in=following_contest)
        for contest in following_contest:
            for act in Action.objects.target(contest):
                feeds.append(act.id)
        return Action.objects.filter(id__in=feeds)


class UnreadNotificationCount(APIView):
    """
    Endpoint to get count of unread notifications
    """
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        data = {
            #'unread_count': request.user.notifications.unread().count(),
            'unread_count': exclude_user(self.request.user,
                                         self.request.user.notifications.unread()).count(),
        }
        return Response(data, status=HTTP_200_OK)
