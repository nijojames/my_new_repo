# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('activities', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='feed',
            name='action',
        ),
        migrations.RemoveField(
            model_name='feed',
            name='feed_viewer',
        ),
        migrations.DeleteModel(
            name='Feed',
        ),
    ]
