# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('actstream', '0002_remove_action_data'),
    ]

    operations = [
        migrations.CreateModel(
            name='Feed',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('action', models.ForeignKey(to='actstream.Action')),
                ('feed_viewer', models.ForeignKey(related_name='get_my_feeds', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
