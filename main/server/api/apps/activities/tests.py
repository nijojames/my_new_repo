import os
from datetime import datetime

from follow.models import Follow

from api.apps.profiles.models import Profile
from api.apps.videos.models import Video

from api.apps.videos.tests import TestUtils

from api.utl.utl import UtlRandom, UtlTest

from django.core.urlresolvers import reverse


class FollowTest(UtlTest, TestUtils):

    def setUp(self):
        self.random = UtlRandom(os.environ.get('TEST_SEED'))
        self.register_user()
        self.profile = Profile.objects.get(user__username = self.username)

    def create_contest(self):
        data = {'concept': self.random.random_str(10),
                'expiry_date': datetime.now(),
                'reward_description': self.random.random_str(10),
                'contest_title': self.random.random_str(10),
                'type': 'SP',
                'award_amount': self.random.random_num(1, 4),
                'status':'review',
                'purpose':self.random.random_str(10)}

        response = self.client.post(reverse('contest-list'),
                                         data,
                                         format='json',
                                         HTTP_AUTHORIZATION=self.token)
        return response.data['id']



    # def test_follow_your_profile_when_registered(self):
    #     response = self.client.get(reverse('profile-list-detail', kwargs={'username':self.response.data['username']}),
    #                              format='json',
    #                              HTTP_AUTHORIZATION=self.token)
    #
    #     self.assertEqual(response.data['is_following'], True)


    # def test_auto_follow_own_videos(self):
    #     self.post_video(201)
    #
    #     response = self.client.get(reverse('video-detail', kwargs={'slug':self.response.data['slug']}),
    #                              format='json',
    #                              HTTP_AUTHORIZATION=self.token)
    #     self.assertEqual(response.data['is_following'], True)
    #
    #
    # def test_auto_follow_own_contest(self):
    #     pk = self.create_contest()
    #
    #     response = self.client.get(reverse('contest-detail', kwargs={'pk':pk}),
    #                              format='json',
    #                              HTTP_AUTHORIZATION=self.token)
    #     self.assertEqual(response.data['is_following'], True)

    def test_follow_user_profile(self):
        profile = Profile.objects.get(user__username=self.username)
        self.register_user()

        data = {'content_type': 'profile', 'obj_id': profile.id}
        self.response = self.client.post(reverse('follow-list'),
                                 data,
                                 format='json',
                                 HTTP_AUTHORIZATION=self.token)
        self.assertEqual(self.response.status_code, 201)
        response = self.client.get(reverse('profile-list-detail', kwargs={'username':profile.user.username}),
                                 format='json',
                                 HTTP_AUTHORIZATION=self.token)
        
        self.assertEqual(response.data['is_following'], True)


        #Test un-follow
        self.response = self.client.post(reverse('follow-list'),
                                 data,
                                 format='json',
                                 HTTP_AUTHORIZATION=self.token)

        response = self.client.get(reverse('profile-list-detail', kwargs={'username':profile.user.username}),
                                 format='json',
                                 HTTP_AUTHORIZATION=self.token)

        self.assertEqual(response.data['is_following'], False)


    def test_follow_videos(self):
        self.post_video(201)
        video_id = self.response.data['id']
        self.register_user()

        data = {'content_type': 'video', 'obj_id': video_id}
        self.response = self.client.post(reverse('follow-list'),
                                 data,
                                 format='json',
                                 HTTP_AUTHORIZATION=self.token)

        self.assertEqual(self.response.status_code, 201)

    def test_follow_contest(self):
        contest_id = self.create_contest()
        self.register_user()

        data = {'content_type': 'contest', 'obj_id': contest_id}
        self.response = self.client.post(reverse('follow-list'),
                                 data,
                                 format='json',
                                 HTTP_AUTHORIZATION=self.token)

        self.assertEqual(self.response.status_code, 201)
