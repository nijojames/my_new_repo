import string
import json
from django.utils.timezone import now
from django.db.models.loading import cache
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.contenttypes.models import ContentType

from rest_framework import serializers
from follow.models import Follow
from actstream.models import Action
from notifications.models import Notification

from api.apps.profiles.serializers import UserSerializer



class FollowSerializer(serializers.ModelSerializer):
    FOLLOW_CONTENT_TYPE = (('video', 'Video'),
                           ('profile', 'Profile'),
                           ('contest', 'Contest'))

    content_type = serializers.ChoiceField(choices=FOLLOW_CONTENT_TYPE)
    obj_id = serializers.IntegerField()

    class Meta:
        model = Follow
        fields = ('id', 'content_type', 'obj_id')

    def validate_obj_id(self, attrs):
        """
        Custom validations are given for content_type object
        :param attrs:
        :return:
        """
        data = self.context['request'].data

        model_name = data.get('content_type', None)
        if model_name:
            model = cache.get_model(model_name+'s', string.capwords(model_name))
            try:
                obj = model.objects.get(pk=attrs)

            except ObjectDoesNotExist:
                raise serializers.ValidationError('Requested obj_id doesnt exist.')

        return attrs


class FeedSerializer(serializers.ModelSerializer):
    user = UserSerializer(source='actor', read_only=True)
    feed = serializers.SerializerMethodField()
    target_details = serializers.SerializerMethodField()
    time = serializers.SerializerMethodField(method_name='time_since')

    class Meta:
        model = Action
        fields = ('feed', 'user', 'target_details', 'time')

    def get_actor(self, obj):
        response = {}
        target_type = ContentType.objects.get_for_model(obj.actor)
        response['state'] = target_type.name
        try:
            response['slug'] = obj.target.slug
        except AttributeError:
            try:
                response['slug'] = obj.target.username
            except AttributeError:
                response['slug'] = None
        return response

    def get_feed(self, obj):
        ctx = {
            'actor': obj.actor,
            'verb': obj.verb,
            'action_object': obj.action_object,
            'target': obj.target,
        }
        if obj.target:
            # if obj.action_object:
            #     return u'%(actor)s %(verb)s %(action_object)s on %(target)s' % ctx
            return u'%(actor)s %(verb)s %(target)s' % ctx
        # if obj.action_object:
        #     return u'%(actor)s %(verb)s %(action_object)s' % ctx
        return u'%(actor)s %(verb)s' % ctx

    def time_since(self, obj):
        return "%s ago" % obj.timesince(now=now())

    def get_target_details(self, obj):
        data = {}
        if obj.target:
            content_type = ContentType.objects.get_for_model(obj.target)
            if content_type.name == 'Contest':
                data['type'] = 'contest'
                try:
                    data['image'] = obj.target.category.all()[0].thumbnail.url
                except IndexError:
                    data['image'] = None
                data['slug'] = obj.target.slug
            elif content_type.name == 'video':
                data['type'] = 'video'
                if obj.target.thumbnail:
                    data['image'] = obj.target.thumbnail.url
                data['slug'] = obj.target.slug
            elif content_type.name == 'profile':
                data['type'] = 'user'
                if obj.target.profile_picture:
                    data['image'] = obj.target.profile_picture.url
                data['slug'] = obj.target.user.username
            elif content_type.name == 'user':
                data['type'] = 'user'
                if obj.target.user_profile.profile_picture:
                    data['image'] = obj.target.user_profile.profile_picture.url
                data['slug'] = obj.target.user_profile.user.username
        else:
            return None
        return data


class NotificationSerializer(serializers.ModelSerializer):
    user = UserSerializer(source='actor', read_only=True)
    notification = serializers.SerializerMethodField()
    target_details = serializers.SerializerMethodField()
    time = serializers.SerializerMethodField(method_name='time_since')

    class Meta:
        model = Notification
        fields = ('id', 'notification', 'time', 'user', 'target_details', 'unread', )

    def get_notification(self, obj):
        ctx = {
            'actor': obj.actor,
            'verb': obj.verb,
            'action_object': obj.action_object,
            'target': obj.target,
        }
        if obj.target:
            # if obj.action_object:
            #     return u'%(actor)s %(verb)s %(action_object)s on %(target)s' % ctx
            return u'%(actor)s %(verb)s %(target)s' % ctx
        # if obj.action_object:
        #     return u'%(actor)s %(verb)s %(action_object)s' % ctx
        return u'%(actor)s %(verb)s' % ctx

    def time_since(self, obj):
        return "%s ago" % obj.timesince(now=now())

    def get_target_details(self, obj):
        data = {}
        if obj.target:
            content_type = ContentType.objects.get_for_model(obj.target)
            if content_type.name == 'Contest':
                data['type'] = 'contest'
                try:
                    data['image'] = obj.target.category.all()[0].thumbnail.url
                except IndexError:
                    data['image'] = None
                data['slug'] = obj.target.slug
            elif content_type.name == 'video':
                data['type'] = 'video'
                if obj.target.thumbnail:
                    data['image'] = obj.target.thumbnail.url
                data['slug'] = obj.target.slug
            elif content_type.name == 'profile':
                data['type'] = 'user'
                if obj.target.profile_picture:
                    data['image'] = obj.target.profile_picture.url
                data['slug'] = obj.target.user.username
            elif content_type.name == 'user':
                data['type'] = 'user'
                if obj.target.user_profile.profile_picture:
                    data['image'] = obj.target.user_profile.profile_picture.url
                data['slug'] = obj.target.user_profile.user.username
        else:
            return None
        return data

