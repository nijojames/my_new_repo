from string import lower

from notifications.signals import notify
from actstream import action, models
from voting.models import Vote
from flaggit.models import Flag

from api.apps.utils import activities, utils
from api.utl.utl import get_genric_object, get_user_device

class ActivityServices(object):
    """
    Video model related activity manipulation
    """

    def like_activity(self, **kwargs):
        instance = kwargs['instance']
        created = kwargs['created']

        if created and instance.vote == +1:

            model = lower(instance.content_type.model)
            verb = activities.LIKED + ' ' + model
            own_verb = activities.LIKED + ' your ' + model
            liked_obj = get_genric_object(model, instance.object_id)
            if hasattr(liked_obj, 'user') and (liked_obj.user.id != instance.user.id):
                notify.send(instance.user, recipient=liked_obj.user, verb=own_verb, action_object=instance, target=liked_obj)
                action.send(instance.user, verb=verb, action_object=instance, target=liked_obj)


    def follow_activity(self, **kwargs):
        instance = kwargs['instance']
        created = kwargs['created']

        if created:
            liked_obj = instance._get_target()
            verb = activities.FOLLOWED + ' ' +  lower(instance._get_target().__class__.__name__)
            notify.send(instance.user, recipient=instance._get_target().user,
                        verb=verb, action_object=instance, target=liked_obj)

    def send_push_notification(self, **kwargs):
        instance = kwargs['instance']
        created = kwargs['created']
        if created:
            device = get_user_device(instance.recipient)
            payload = utils.get_slug(instance.target)
            payload['notification'] = instance.id
            if device:
                device.send_message(instance.verb, badge=1, extra=payload)