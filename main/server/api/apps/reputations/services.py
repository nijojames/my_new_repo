from api.apps.general.models import ReputationConfig


class UserLevelService(object):

    def _post_save_signal_callback(self, **kwargs):
        self.config = ReputationConfig.get_solo()
        instance = kwargs['instance']
        created = kwargs['created']
        level_now, level_count = self.calculate_level(instance)
        if instance.level != level_now:
            instance.level = level_now
            instance.level_count = level_count
            instance.save()

    def calculate_level(self, instance):
        reputation = instance.reputation
        level_one_check = self._check_for_level1(reputation)
        if level_one_check:
            return level_one_check, 1

        level_two_check = self._check_for_level2(reputation)
        if level_two_check:
            return level_two_check, 2

        level_three_check = self._check_for_level3(reputation)
        if level_three_check:
            return level_three_check, 3

        level_four_check = self._check_for_level4(reputation)
        if level_four_check:
            return level_four_check, 4

        return self.config.level_five_name, 5


    def _check_for_level1(self, reputation):
        # TODO make all or as and later and save the review points in reputations
        level_two_video_points_required = self.config.level_two_video_points_required
        level_two_review_points_required = self.config.level_two_review_points_required

        if reputation < level_two_video_points_required or reputation < level_two_review_points_required:
            return self.config.level_one_name
        return False

    def _check_for_level2(self, reputation):
        level_three_video_points_required = self.config.level_three_video_points_required
        level_three_review_points_required = self.config.level_three_review_points_required

        if reputation < level_three_video_points_required or reputation < level_three_review_points_required:
            return self.config.level_two_name
        return False

    def _check_for_level3(self, reputation):
        level_four_video_points_required = self.config.level_four_video_points_required
        level_four_review_points_required = self.config.level_four_review_points_required

        if reputation < level_four_video_points_required or reputation < level_four_review_points_required:
            return self.config.level_three_name
        return False

    def _check_for_level4(self, reputation):
        level_five_video_points_required = self.config.level_five_video_points_required
        level_five_review_points_required = self.config.level_five_review_points_required

        if reputation < level_five_video_points_required or reputation < level_five_review_points_required:
            return self.config.level_four_name
        return False
