from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from models import Reputation, ReputationAction


class ReputationAdmin(ImportExportModelAdmin):
    pass


class ReputationActionAdmin(ImportExportModelAdmin):
    pass


admin.site.register(Reputation, ReputationAdmin)
admin.site.register(ReputationAction, ReputationActionAdmin)
