import os

from api.apps.reputations.models import Reputation

from api.apps.videos.tests import TestUtils

from api.utl.utl import UtlRandom, UtlTest
from api.apps.general.models import ReputationConfig



class ReputationTest(UtlTest, TestUtils):

    def setUp(self):
        self.random = UtlRandom(os.environ.get('TEST_SEED'))
        self.register_user()

    def test_reputation_on_video_upload(self):
        reputation = Reputation.objects.all()
        self.assertFalse(bool(reputation))
        self.post_video(201)

        reputation = Reputation.objects.get(user__username=self.username)
        self.assertTrue(bool(reputation))

        self.assertEqual(reputation.reputation, 1)


class UserLevelTest(UtlTest, TestUtils):

    def setUp(self):
        self.random = UtlRandom(os.environ.get('TEST_SEED'))
        self.register_user()
        self.config = ReputationConfig.get_solo()

    def test_user_initial_level(self):
        self.post_video(201)
        reputation = Reputation.objects.get(user__username=self.username)
        self.assertEqual(self.config.level_one_name, reputation.level)

    def test_go_next_level(self):
        self.post_video(201)
        reputation = Reputation.objects.get(user__username=self.username)
        reputation.reputation = self.config.level_two_video_points_required
        reputation.save()

        self.assertEqual(self.config.level_two_name, reputation.level)
