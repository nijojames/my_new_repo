from decimal import Decimal

from django.utils import timezone

from api.apps.general.models import ReputationConfig, LevelPrivilegeConfig

from api.apps.reputations.models import Reputation

from api.apps.videos.models import Video

# from api.apps.reviews.models import Reviews
from api.apps.reviews.models import UserReview

user_level_dic = {1: 'level_one',
                  2: 'level_two',
                  3: 'level_three',
                  4: 'level_four',
                  5: 'level_five'}


class ServiceBase(object):

    def calculate_level(self, instance):
        reputation = instance.reputation
        level_one_check = self._check_for_level1(reputation)
        if level_one_check:
            return level_one_check, 1

        level_two_check = self._check_for_level2(reputation)
        if level_two_check:
            return level_two_check, 2

        level_three_check = self._check_for_level3(reputation)
        if level_three_check:
            return level_three_check, 3

        level_four_check = self._check_for_level4(reputation)
        if level_four_check:
            return level_four_check, 4

        return self.config.level_five_name, 5


    def _check_for_level1(self, reputation):
        # TODO make all or as and later and save the review points in reputations
        level_two_video_points_required = self.config.level_two_video_points_required
        level_two_review_points_required = self.config.level_two_review_points_required

        if (reputation < level_two_video_points_required or
            reputation < level_two_review_points_required):
            return self.config.level_one_name
        return False

    def _check_for_level2(self, reputation):
        level_three_video_points_required = self.config.level_three_video_points_required
        level_three_review_points_required = self.config.level_three_review_points_required

        if (reputation < level_three_video_points_required or
            reputation < level_three_review_points_required):
            return self.config.level_two_name
        return False

    def _check_for_level3(self, reputation):
        level_four_video_points_required = self.config.level_four_video_points_required
        level_four_review_points_required = self.config.level_four_review_points_required

        if (reputation < level_four_video_points_required or
            reputation < level_four_review_points_required):
            return self.config.level_three_name
        return False

    def _check_for_level4(self, reputation):
        level_five_video_points_required = self.config.level_five_video_points_required
        level_five_review_points_required = self.config.level_five_review_points_required

        if (reputation < level_five_video_points_required or
            reputation < level_five_review_points_required):
            return self.config.level_four_name
        return False


class LevelPrivilegeFilter(ServiceBase):
    """
    Usage:
    >>>obj = LevelPrivilegeFilter(self.request.user)
    >>>obj.if_user_can_upload_video_current_day(price='Optional')
    """

    def __init__(self, user):
        self.user = user
        self.config = ReputationConfig.get_solo()
        self.setup()

    def setup(self):
        self.reputation_obj = Reputation.objects.get(user=self.user)
        self.level_name, self.user_level = self.calculate_level(self.reputation_obj)
        self.level_privilege_config = LevelPrivilegeConfig.get_solo()

    def if_user_can_upload_video(self, price=None):
        """
        Price check, video upload per day check
        :return:
        """
        throw_validation_error = False
        video_check_errors = {}
        start_time_date = timezone.now().replace(hour=0, minute=0, second=0)
        this_day_video_uploaded_count = Video.objects.filter(user=self.user,
                                                             created__range=[start_time_date,
                                                                             timezone.now()]).count()
        # check if user can upload one more
        if not self.check_video_upload_count_for_day(this_day_video_uploaded_count):
            throw_validation_error = True
            video_check_errors.update({'upload_limit': ['max number of videos than can be uploaded is over']})

        # check if user can upload with this price
        if price and not self.check_video_upload_price(price):
            throw_validation_error = True
            video_check_errors.update({'upload_price': ['Invalid price, this price cannot be set at this level']})

        if throw_validation_error:
            return video_check_errors

        return True

    def check_video_upload_count_for_day(self, uploaded_count):
        level_value = user_level_dic[self.user_level]
        attr = level_value+'_video_count_per_day'
        upload_limit_config_value = getattr(self.level_privilege_config, attr)
        if uploaded_count >= upload_limit_config_value:
            return False

        return True

    def check_video_upload_price(self, price):
        level_value = user_level_dic[self.user_level]
        attr = level_value+'_video_max_price'
        price_limit_config_value = getattr(self.level_privilege_config, attr)
        if Decimal(price) > price_limit_config_value:
            return False

        return True

    def todays_review_count(self):
        start_time_date = timezone.now().replace(hour=0, minute=0, second=0)
        # return Reviews.objects.filter(user=self.user,
        return UserReview.objects.filter(user=self.user,
                                         created__range=[start_time_date,
                                                         timezone.now()]).count()

    def reviews_left_today(self):
        level_value = user_level_dic[self.user_level]
        attr = level_value + '_review_count_per_day'
        review_limit_config_value = getattr(self.level_privilege_config, attr)

        reviewed_count_this_day = self.todays_review_count()
        return review_limit_config_value-reviewed_count_this_day

    def if_user_can_review_this_day(self):
        """
        Checks if the user can review anymore this day
        """
        if not self.reviews_left_today():
            return {'review_limit': ['max number of reviews this day is over']}
        return True

    def get_the_amount_to_be_paid_to_owner(self, original_price):
        level_value = user_level_dic[self.user_level]
        attr = level_value+'_commission'
        commission_config_value = getattr(self.level_privilege_config, attr)

        original_price = Decimal(original_price)
        price_to_be_paid = round((original_price *
                                  Decimal(commission_config_value)) / 100,
                                 3)  # TODO check the round
        return price_to_be_paid

    def get_commission_config_value_based_on_user_level(self):
        level_value = user_level_dic[self.user_level]
        attr = level_value+'_commission'
        return getattr(self.level_privilege_config, attr)

    def check_if_user_can_review_today(self):
        # TODO check if user can do any reviews this day.
        return True
