# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reputations', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='reputation',
            name='level',
            field=models.CharField(default=b'level 1', max_length=100),
        ),
    ]
