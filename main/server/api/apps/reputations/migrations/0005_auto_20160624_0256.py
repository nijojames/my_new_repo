# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reputations', '0004_reputation_review_reputation'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reputation',
            name='level_count',
            field=models.IntegerField(default=1, null=True, blank=True),
        ),
    ]
