# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reputations', '0003_reputation_level_count'),
    ]

    operations = [
        migrations.AddField(
            model_name='reputation',
            name='review_reputation',
            field=models.IntegerField(default=0),
        ),
    ]
