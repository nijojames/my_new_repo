# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reputations', '0002_reputation_level'),
    ]

    operations = [
        migrations.AddField(
            model_name='reputation',
            name='level_count',
            field=models.IntegerField(default=1, max_length=10, null=True, blank=True),
        ),
    ]
