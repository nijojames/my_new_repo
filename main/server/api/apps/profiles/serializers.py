from django.contrib.contenttypes.models import ContentType

from rest_framework import serializers
from rest_framework.authtoken.models import Token

from taggit_serializer.serializers import (TagListSerializerField,
                                           TaggitSerializer)

from rest_auth.serializers import TokenSerializer
from rest_auth.serializers import UserDetailsSerializer
from rest_auth.serializers import PasswordResetSerializer

from search_indexes import ProfileIndex

from drf_haystack.serializers import HaystackSerializerMixin

from models import Profile
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist

from forms import CustomPasswordRestForm

from api.apps.reputations.models import Reputation

from api.apps.general.models import ReputationConfig

from follow.models import Follow
from voting.models import Vote

import logging
logger = logging.getLogger(__name__)


class ProfileSerializer(TaggitSerializer, serializers.ModelSerializer):
    tags = TagListSerializerField()
    level = serializers.SerializerMethodField()

    class Meta:
        model = Profile
        fields = ['id', 'about', 'tags', 'level', 'type', 'profile_picture', 'cover_picture', 'location']

    def get_level(self, obj):
        user = obj.user
        reputation = Reputation.objects.filter(user=user)
        if reputation:
            return reputation[0].level
        return ''


class UserSerializer(TaggitSerializer, UserDetailsSerializer):
    # add profile fields here
    about = serializers.CharField(source="profile.about")
    tags = TagListSerializerField(source="profile.tags", required=False)

    class Meta(UserDetailsSerializer.Meta):
        fields = UserDetailsSerializer.Meta.fields + ('about', 'tags')

    # NB: never called
    def create(self, validated_data):
        profile_data = validated_data.pop('profile', None)
        instance = super(UserSerializer, self).create(validated_data)
        self.create_or_update_profile(instance, profile_data)
        return instance

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('profile', None)
        self.create_or_update_profile(instance, profile_data)
        return super(UserSerializer, self).update(instance, validated_data)

    def create_or_update_profile(self, instance, profile_data):
        # tags cannot be added at object creation
        tags = []
        if profile_data and 'tags' in profile_data:
            tags = profile_data.pop('tags')

        profile, created = Profile.objects.get_or_create(user=instance,
                                                         defaults=profile_data)
        # now add back the tags
        if tags:
            profile_data['tags'] = tags
        # import pdb
        # pdb.set_trace()
        if tags or (not created and profile_data is not None):
            super(UserSerializer, self).update(profile, profile_data)

    def to_representation(self, instance):
        try:
            email = self.get_email(instance)
            profile = Profile.objects.get(user=instance)
            serializer = ProfileSerializer(profile)
            value = serializer.data
        except Profile.DoesNotExist:
            value = dict()
        # add User fields
        for field in UserDetailsSerializer.Meta.fields:
            if field not in value:
                value[field] = getattr(instance, field)
        value.update({'email': email})
        return value

    def get_email(self, obj):
        if self.context['request'].user.id == obj.id:
            return obj.email
        return ""


class NullableImageField(serializers.ImageField):
    def to_internal_value(self, data):
        if data == '' and self.allow_null:
            return None
        return super(serializers.ImageField, self).to_internal_value(data)


class UserProfileSerializer(TaggitSerializer, UserDetailsSerializer):
    about = serializers.CharField(source="profile.about", required=False)
    tags = TagListSerializerField(source="profile.tags", required=False)
    type = serializers.CharField(source="profile.type")
    profile_picture = NullableImageField(source='profile.profile_picture',
                                         write_only=True,
                                         allow_null=True)
    cover_picture = NullableImageField(source='profile.cover_picture',
                                       write_only=True,
                                       allow_null=True)
    location = serializers.CharField(source='profile.location', allow_blank=True)
    profile_id = serializers.SerializerMethodField(read_only=True)
    reputation = serializers.SerializerMethodField(read_only=True)
    is_following = serializers.SerializerMethodField(read_only=True)
    followers_count = serializers.SerializerMethodField(read_only=True)
    is_liked = serializers.SerializerMethodField(read_only=True)
    likes_count = serializers.SerializerMethodField(read_only=True)

    class Meta(UserDetailsSerializer.Meta):
        fields = UserDetailsSerializer.Meta.fields + ('profile_id', 'about', 'tags', 'is_following', 'followers_count',
                                                      'profile_picture', 'cover_picture', 'location',
                                                      'is_liked', 'likes_count', 'type', 'reputation',)
        new_fields = ('profile_id', 'reputation', 'is_following', 'followers_count', 'is_liked', 'likes_count',
                      'profile_picture_url', 'cover_picture_url')

    #
    # # NB: never called
    def create(self, validated_data):
        profile_data = validated_data.pop('profile', None)
        instance = super(UserProfileSerializer, self).create(validated_data)
        self.create_or_update_profile(instance, profile_data)
        return instance


    def update(self, instance, validated_data):
        profile_data = validated_data.pop('profile', None)
        self.create_or_update_profile(instance, profile_data)
        return super(UserProfileSerializer, self).update(instance, validated_data)

    def create_or_update_profile(self, instance, profile_data):
        # tags cannot be added at object creation
        tags = []
        if profile_data and 'tags' in profile_data:
            tags = profile_data.pop('tags')

        profile, created = Profile.objects.get_or_create(user=instance,
                                                         defaults=profile_data)
        # now add back the tags
        if tags:
            profile_data['tags'] = tags
        # import pdb
        # pdb.set_trace()
        if tags or (not created and profile_data is not None):
            super(UserProfileSerializer, self).update(profile, profile_data)

    def to_representation(self, instance):
        try:
            email = self.get_email(instance)
            profile = Profile.objects.get(user=instance)
            serializer = ProfileSerializer(profile)
            value = serializer.data
            reputation = self.get_reputation(instance)
            is_following = self.get_is_following(instance)
            followers_count = self.get_followers_count(instance)
            is_liked = self.get_is_liked(instance)
            likes_count = self.get_likes_count(instance)
            profile_id = self.get_profile_id(instance)
            profile_picture_url = self.get_profile_picture_url(instance)
            cover_picture_url = self.get_cover_picture_url(instance)
            value.update({'email': email,
                          'profile_id': profile_id,
                          'reputation': reputation,
                          'is_following': is_following,
                          'followers_count': followers_count,
                          'is_liked': is_liked,
                          'likes_count': likes_count,
                          'profile_picture_url': profile_picture_url,
                          'cover_picture_url': cover_picture_url})
        except Profile.DoesNotExist:
            value = dict()
        # add User fields
        for field in UserDetailsSerializer.Meta.fields:
            if field not in value:
                value[field] = getattr(instance, field)
        return value

    def get_reputation(self, obj):
        try:
            obj = Reputation.objects.get(user=obj)
            return obj.reputation
        except ObjectDoesNotExist:
            return 0

    def get_is_following(self, obj):
        user = self.context['request'].user
        if user.is_authenticated():
            if user.id == obj.id:
                return None
            profile = Profile.objects.get(user=obj)
            follow = Follow.objects.filter(target_profile=profile, user=user).exists()
            return follow
        return False

    def get_is_liked(self, obj):
        user = self.context['request'].user
        if user.is_authenticated():
            profile = Profile.objects.get(user=obj)
            content_type = ContentType.objects.get_for_model(profile)
            like = Vote.objects.filter(content_type=content_type, object_id=profile.id, user=user).exists()
            return like
        return False

    def get_followers_count(self, obj):
        profile = Profile.objects.get(user=obj)
        follow = Follow.objects.filter(target_profile=profile).count()
        return follow

    def get_likes_count(self, obj):
        profile = Profile.objects.get(user=obj)
        content_type = ContentType.objects.get_for_model(profile)
        follow = Vote.objects.filter(content_type=content_type, object_id=profile.id).count()
        return follow

    def get_profile_id(self, obj):
        try:
            profile = Profile.objects.get(user=obj)
        except Profile.DoesNotExist:
            return None
        return profile.pk

    def get_profile_picture_url(self, obj):
        if obj.user_profile.profile_picture_url:
            return obj.user_profile.profile_picture_url
        elif obj.user_profile.profile_picture:
            return obj.user_profile.profile_picture.url

    def get_cover_picture_url(self, obj):
        if obj.user_profile.cover_picture_url:
            return obj.user_profile.cover_picture_url
        elif obj.user_profile.cover_picture:
            return obj.user_profile.cover_picture.url


    def get_email(self, obj):
        if self.context['request'].user.id == obj.id:
            return obj.email
        return ""

class UserMobileProfileSerializer(UserProfileSerializer):

    profile_picture_url = serializers.URLField(source='profile.profile_picture_url')
    cover_picture_url = serializers.URLField(source='profile.cover_picture_url')

    class Meta(UserDetailsSerializer.Meta):
        fields = UserDetailsSerializer.Meta.fields + ('profile_id', 'about', 'tags', 'is_following', 'followers_count',
                                                      'profile_picture_url', 'cover_picture_url', 'location',
                                                      'is_liked', 'likes_count', 'type', 'reputation',)
        new_fields = ('profile_id', 'reputation', 'is_following', 'followers_count', 'is_liked', 'likes_count', )


class UserTokenSerializer(serializers.ModelSerializer):
    """
    Serializer that will work for both token and username
    """
    key = serializers.SerializerMethodField()
    username = serializers.SerializerMethodField()

    class Meta:
        model = get_user_model()
        fields = ('username', 'key')

    def get_key(self, obj):
        """
        Get key based on user object / token object
        :param obj: User/Token class objects
        :return: String
        """
        if obj.__class__ == Token:
            return obj.key
        if obj.__class__ == get_user_model():
            token = Token.objects.get(user=obj)
            return token.key

    def get_username(self, obj):
        """
        Get username based on user object / token object
        :param obj: User/Token class objects
        :return: String
        """
        if obj.__class__==Token:
            return obj.user.username

        if obj.__class__ == get_user_model():
            return obj.username


class CustomPasswordResetSerializer(PasswordResetSerializer):
    password_reset_form_class = CustomPasswordRestForm


class ProfileSearchSerializer (HaystackSerializerMixin, ProfileSerializer):
    user = UserSerializer()

    class Meta:
       model = Profile
       search_fields = ("text", "tags", 'type',)
       fields = ['about', 'tags', 'type', 'user']


from drf_haystack.serializers import HaystackFacetSerializer
class  ProfileFacetSerializer(HaystackFacetSerializer):
    serialize_objects = True  # Setting this to True will serialize the
                               # queryset into an `objects` list. This
                               # is useful if you need to display the faceted
                               # results. Defaults to False.
    class Meta:
        index_classes = [ProfileIndex]
        fields = ['tags', 'type', ]


class LevelSerializer(serializers.ModelSerializer):
    data = serializers.SerializerMethodField()

    class Meta:
        model = ReputationConfig
        fields = ['data']

    def get_data(self, obj):
        return [{'value': "1", 'level': obj.level_one_name},
                {'value': "2", 'level': obj.level_two_name},
                {'value': "3", 'level': obj.level_three_name},
                {'value': "4", 'level': obj.level_four_name},
                {'value': "5", 'level': obj.level_five_name}]
