import json

from django.contrib.auth.models import User

from rest_framework.authtoken.models import Token
from rest_framework.test import APIRequestFactory, APIClient

# Create your tests here.
from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase

# from django.contrib.auth.models import User

#from .serializers import ProfileSerializer
from api.apps.profiles.models import Profile
# from .search_indexes import ProfileIndex

from django.core.management import call_command
#from django.test.utils import override_settings

from haystack import connections
from django.conf import settings

from django.utils.encoding import force_text

import json
import os

from api.utl.utl import UtlRandom, UtlTest


class UserProfileMixin(object):
    random = ""
    seed = ""
    token = ""
    response = ""
    data = ""
    username = ""
    email = ""
    password = ""
    about = ""
    tags = []

    def register_user(self):
        n = self.random.random_num(6, 30)
        self.username = self.random.random_str(n)

        n = self.random.random_num(6, 30)
        self.email = self.random.random_str(n)
        self.username = self.email

        n = self.random.random_num(6, 30)
        self.email = "%s@%s.com" % (self.email, self.random.random_str(n))

        n = self.random.random_num(6, 30)
        self.password = self.random.random_str(n)

        self.data = {
            "first_name": self.username,
            "last_name": self.username,
            "type": 'both',
            "email": self.email,
            "password1": self.password,
            "password2": self.password,
            'about': self.about
        }
        # print self.data
        self.response = self.client.post(reverse('rest_register'),
                                         self.data,
                                         format='json')
        self.assert_equal(self.response.status_code,
                          status.HTTP_201_CREATED,
                          self.data)
        self.token = json.loads(force_text(self.response.content))['key']
        self.token = 'Token %s' % self.token


class PutGetProfileTest(UtlTest, UserProfileMixin):

        # print User.objects.all()

    def update_profile(self, estatus):
        n = self.random.random_num(3, 10) + 1
        for i in range(3, n):
            self.about = '%s %s' % (self.random.random_str(i), self.about)

        self.about = self.about.strip()

        n = self.random.random_num(3, 10) + 1
        self.tags = []
        for n in range(3, n):
            tag = self.random.random_str(n)
            tag = tag.strip()
            self.tags.append(tag)

        # print self.tags
        self.data = {
            "about": self.about,
            "tags": json.dumps(self.tags),
            "email": self.email,
            "type": 'both',
            "username": self.username
        }
        # print self.data
        self.response = self.client.patch(reverse('profile-list-detail',
                                                args=[self.username]),
                                        self.data,
                                        format='json',
                                        HTTP_AUTHORIZATION=self.token)
        # self.assert_equal(self.response.status_code, estatus, self.data)

    def get_profile(self, estatus, num):
        self.response = self.client.get(reverse('profile-list-list'),
                                        HTTP_AUTHORIZATION=self.token)
        self.assert_equal(self.response.status_code, estatus)
        self.assert_equal(Profile.objects.count(), num)
        # we can do detail check only one profile
        # make sure we have the expected reference data in self.about/tags
        if num != 1:
            return
        for i in range(0, num):
            profile = self.response.data[i]
            self.assert_equal(profile["about"], self.about)
            self.assert_equal(sorted(profile["tags"]), sorted(self.tags))

    def search_profile_by_tag(self, tag, estatus):
        self.response = self.client.get(reverse('profile-search-list'),
                                        {'tags__contains': tag},
                                        format='json',
                                        HTTP_AUTHORIZATION=self.token)
        self.assert_equal(self.response.status_code, estatus)
        self.assert_equal(len(self.response.data['results']), 1)
        profile = self.response.data['results'][0]
        self.assert_equal(profile["about"], self.about)

    def build_search_index(self):
        # override to create a test index
        settings.HAYSTACK_CONNECTIONS['default']['INDEX_NAME'] = 'test_index'
        connections.reload('default')
        call_command('rebuild_index', interactive=False, verbosity=0)

    def clear_search_index(self):
        call_command('clear_index', interactive=False, verbosity=0)

    def setUp(self):
        self.random = UtlRandom(os.environ.get('TEST_SEED'))
        super(PutGetProfileTest, self).setUp()

    def tearDown(self):
        pass

    def test_user_can_update_profile(self):
        self.register_user()
        self.update_profile(status.HTTP_200_OK)

    def test_anonymous_can_not_update_profile(self):
        self.register_user()
        self.token = ""
        self.client.force_authenticate(token=None)
        self.update_profile(status.HTTP_401_UNAUTHORIZED)

    def test_user_can_get_profile_list(self):
        n = self.random.random_num(1, 5)
        for i in range(0, n):
            self.register_user()
            self.update_profile(status.HTTP_200_OK)
        self.get_profile(status.HTTP_200_OK, n)

    def test_anonymous_can_get_profile_list(self):
        n = self.random.random_num(1, 5)
        for i in range(0, n):
            self.register_user()
            self.update_profile(status.HTTP_200_OK)
        # clear the token and access anonymously
        self.token = ''
        self.get_profile(status.HTTP_200_OK, n)

    '''
    def test_user_can_search_profile_by_tags(self):
        self.register_user()
        self.update_profile(status.HTTP_201_CREATED)
        self.build_search_index()
        self.search_profile_by_tag(self.tags[self.random.random_num(0, len(self.tags)-1)],
                                   status.HTTP_200_OK)

    def test_anonymous_can_search_profile_by_tags(self):
        self.register_user()
        self.update_profile(status.HTTP_201_CREATED)
        self.build_search_index()
        # clear the token and search anonymously
        self.token = ''
        self.search_profile_by_tag(self.tags[self.random.random_num(0, len(self.tags)-1)],
                                   status.HTTP_200_OK)
    '''


class TestAPIAuthentication(APITestCase):
    """
    Test Cases for Rest Login
    """
    def setUp(self):
        self.factory = APIRequestFactory()
        self.client = APIClient()
        self.random = UtlRandom(os.environ.get('TEST_SEED'))
        self.test_username = self.random.random_lowercase_string(10)
        self.test_password = self.random.random_str(10)
        self.test_email = self.random.random_email(10)
        self.user = User.objects.create_user(self.test_username, email=self.test_email, password=self.test_password)
        self.user.save()
        self.token = Token.objects.create(user=self.user)
        self.token.save()

    def test_login(self):
        user_has_logged_in = self.client.login(username=self.test_username, password=self.test_password)
        self.assertTrue(user_has_logged_in)

    def test_wrong_user_name(self):
        user_has_logged_in = self.client.login(username=self.random.random_lowercase_string(10), password=self.test_password)
        self.assertFalse(user_has_logged_in)

    def test_wrong_password(self):
        user_has_logged_in = self.client.login(username=self.test_username, password=self.random.random_lowercase_string(10))
        self.assertFalse(user_has_logged_in)

    def test_logout(self):
        self.client.login(username=self.test_username, password=self.test_password)
        self.client.logout()
        self.assertIsNone(self.client.handler._force_user)
        self.assertIsNone(self.client.handler._force_token)

    def test_authorization(self):
        header = {}
        response = self.client.post(reverse('rest_login'), {'email':self.test_email,
                                                            'password': self.test_password}, **header)
        response_data = response.data
        self.assertEqual(response.status_code, 200, "REST token-auth failed")
        self.assertIn('key', response_data)
        self.assertIn('username', response_data)

    def test_authorization_with_valid_token(self):
        header = {}
        response = self.client.post(reverse('rest_login'), {'email':self.test_email,
                                                            'password': self.test_password}, **header)
        response_data = response.data
        header = {'HTTP_AUTHORIZATION': 'Token {}'.format(response_data['key'])}
        response = self.client.get(reverse('profile-list-list'), **header)
        self.assertEqual(response.status_code, 200, "REST token-auth failed")

    def test_authorization_with_invalid_token(self):
        invalid_token = self.random.random_str(20)
        header = {'HTTP_AUTHORIZATION': 'Token {}'.format(invalid_token)}
        response = self.client.get(reverse('profile-list-list'), **header)
        self.assertEqual(response.status_code, 401)
