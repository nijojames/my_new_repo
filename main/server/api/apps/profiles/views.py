import re
from urlparse import urlparse

from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions

from drf_haystack.viewsets import HaystackViewSet
from drf_haystack.filters import HaystackFacetFilter

from haystack.query import  SearchQuerySet
from haystack.forms import SearchForm

from rest_auth.views import LoginView
from rest_auth.views import UserDetailsView
from rest_auth.registration.views import SocialLoginView

from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.socialaccount.providers.oauth2.client import OAuth2Client
from allauth.socialaccount.providers.oauth.client import OAuthClient
from allauth.socialaccount.providers.twitter.views import TwitterOAuthAdapter
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from allauth.socialaccount.providers.instagram.views import InstagramOAuth2Adapter
from allauth.account.signals import user_signed_up

from voting.models import Vote

from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.views.generic.base import View
from django.shortcuts import render
from django.dispatch import receiver
from django.http import Http404
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import get_user_model

from permissions import UserProfileUpdatePermission
from twitter import CustomTwitterOAuthAdapter,CustomOAuth

from models import Profile
from follow.models import Follow
from api.apps.general.models import ReputationConfig
from serializers import UserSerializer,\
    UserProfileSerializer,\
    UserTokenSerializer, ProfileFacetSerializer, ProfileSearchSerializer, LevelSerializer, UserMobileProfileSerializer

from api.apps.utils.mixins import VideoSearchMixin
from api.apps.videos.models import Video
from api.apps.videos.models import PUBLISHED_TO_MARKETPLACE
from api.apps.videos.serializers import VideoSerializer
from api.apps.contests.models import Contest
from api.apps.contests.serializers import ContestSerializer

import logging
logger = logging.getLogger(__name__)


# class ProfileList(APIView):
#     permission_classes = [UserProfileUpdatePermission]
#     """
#     List all user profiles
#     """
#     def get(self, request, format=None):
#         User = get_user_model()
#         users = User.objects.all().filter(is_staff=False)
#         serializer = UserSerializer(users, many=True)
#         return Response(serializer.data)


# class ProfileDetail(UserDetailsView):
#     """
#     Retrieve, update or delete a profile instance.
#     """
#     permission_classes = [permissions.IsAuthenticatedOrReadOnly]

#     def get(self, request, username, format=None):
#         try:
#             User = get_user_model()
#             user = User.objects.get(username=username)
#             serializer = UserSerializer(user)
#             return Response(serializer.data)
#         except ObjectDoesNotExist:
#             raise Http404


class ProfileView(ModelViewSet):
    """
    Use /api/rest-auth/registration/ for Registration and can use /api/profiles/profile-list/ for profile listing
    and profile edit /api/profiles/profile-list/pk/
    """

    permission_classes = [UserProfileUpdatePermission, ]
    serializer_class = UserProfileSerializer # UserMobileProfileSerializer
    model = User
    lookup_field = 'username'

    def get_queryset(self):
        users = User.objects.all().filter(is_staff=False)
        return users

    def create(self, request, *args, **kwargs):
        if 'from_web' in request.data:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
        else:
            serializer = UserMobileProfileSerializer(data=request.data, context={'request': request})
            serializer.is_valid(raise_exception=True)

        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        # ToDo: remove this hack and keep a single serializer
        if 'from_web' in request.data:
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            # serializer.is_valid()
            # print serializer.errors
            serializer.is_valid(raise_exception=True)
        else:
            serializer = UserMobileProfileSerializer(instance, data=request.data, partial=partial,
                                                     context={'request': request})
            serializer.is_valid(raise_exception=True)

        self.perform_update(serializer)
        return Response(serializer.data)


from knox.models import AuthToken
from knox.serializers import UserSerializer
from django.conf import settings

from django.contrib.auth import login

from rest_auth.registration.views import RegisterView
from django.http import HttpRequest
from allauth.account.utils import complete_signup
from allauth.account import app_settings



class CustomLoginView(LoginView):

    def login(self):
        self.user = self.serializer.validated_data['user']
        token = AuthToken.objects.create(self.user)
        if getattr(settings, 'REST_SESSION_LOGIN', True):
            login(self.request, self.user)
            return token

    def post(self, request, *args, **kwargs):
        self.serializer = self.get_serializer(data=self.request.data)
        self.serializer.is_valid(raise_exception=True)
        token = self.login()

        data = UserSerializer(request.user).data

        return Response({
            "username": data['username'],
            "key": token,
        })


class CustomRegisterView(RegisterView):

    token_model = AuthToken
    serializer_class = UserSerializer

    def form_valid(self, form):
        self.user = form.save(self.request)
        self.token = AuthToken.objects.create(self.user)

        if isinstance(self.request, HttpRequest):
            request = self.request
        else:
            request = self.request._request
        return complete_signup(request, self.user,
                               app_settings.EMAIL_VERIFICATION,
                               self.get_success_url())

    def get_response(self):
        return Response({'key': self.token, 'username': self.user.username}, status=status.HTTP_201_CREATED)

    def get_response_with_errors(self):
        return Response(self.form.errors, status=status.HTTP_400_BAD_REQUEST)


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter
    callback_url = Site.objects.get_current()
    client_class = OAuth2Client


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter
    callback_url = Site.objects.get_current()
    client_class = OAuth2Client


class TwitterLogin(SocialLoginView):
    adapter_class =CustomTwitterOAuthAdapter
    # callback_url = Site.objects.get_current()
    # client_class = CustomOAuth


class InstagramLogin(SocialLoginView):
    adapter_class = InstagramOAuth2Adapter
    adapter_class.access_token_url = 'https://api.instagram.com/oauth/access_token'
    callback_url = Site.objects.get_current()
    client_class = OAuth2Client


@receiver(user_signed_up)
def user_signed_up_(request, user, sociallogin=None, **kwargs):
    '''
    When a social account is created successfully and this signal is received,
    django-allauth passes in the sociallogin param, giving access to metadata on the remote account, e.g.:

    sociallogin.account.provider  # e.g. 'twitter'
    sociallogin.account.get_avatar_url()
    sociallogin.account.get_profile_url()
    sociallogin.account.extra_data['screen_name']

    See the socialaccount_socialaccount table for more in the 'extra_data' field.
    '''

    if sociallogin:
        # Extract first / last names from social nets and store on User record

        if sociallogin.account.provider == 'instagram':
            user.first_name = sociallogin.account.extra_data['full_name']

        user.save()


'''
from drf_haystack.viewsets import HaystackViewSet
from serializers import ProfileSearchSerializer

    
class ProfileSearch(HaystackViewSet):

    # `index_models` is an optional list of which models you would like to include
    # in the search result. You might have several models indexed, and this provides
    # a way to filter out those of no interest for this particular view.
    #(Translates to `SearchQuerySet().models(*index_models)` behind the scenes.
    index_models = [Profile]

    serializer_class = ProfileSearchSerializer
    #filter_backends = [HaystackHighlightFilter, HaystackAutocompleteFilter]

    # This will be used to filter and serialize faceted results
    #facet_serializer_class = ProfileFacetSerializer
    #facet_filter_backends = [HaystackFacetFilter]   # This is the default facet filter, and
                                                     # can be left out.


'''    

'''

from rest_framework.mixins import ListModelMixin
from drf_haystack.generics import HaystackGenericAPIView
from serializers import ProfileSearchSerializer

class ProfileSearch(ListModelMixin, HaystackGenericAPIView):

    serializer_class = ProfileSearchSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

'''

class ProfileSearch(HaystackViewSet, VideoSearchMixin):

    # `index_models` is an optional list of which models you would
    # like to include in the search result. You might have several models
    # indexed, and this provides a way to filter out those of no interest
    # for this particular view.
    # (Translates to `SearchQuerySet().models(*index_models)` behind the scenes
    index_models = [Profile]

    serializer_class = ProfileSearchSerializer
    paginate_by_param = 'page_size'
    # filter_backends = [HaystackHighlightFilter, HaystackAutocompleteFilter]

    # This will be used to filter and serialize faceted results
    facet_serializer_class = ProfileFacetSerializer
    # This is the default facet filter, and can be left out
    facet_filter_backends = [HaystackFacetFilter]

    def get_queryset(self):
        """
        :param args:
        :param kwargs:
        :return: Search Results based on query 'q'
        """
        request = self.request
        q = request.GET.get('q', "")
        query = re.sub(' +', ' ', re.sub('[^a-zA-Z0-9\n\.]', ' ', q))

        levels = request.GET.get('levels', None)
        levels = self._get_levels(levels)

        sqs = SearchQuerySet()

        if query:
            form = SearchForm(request.query_params)
            search_query = urlparse(request.build_absolute_uri())
            if form.is_valid():
                sqs = form.search()

        if levels:
            sqs = sqs.filter(level__in=levels)
        sqs = sqs.models(Profile)
        return sqs


class LevelsView(ModelViewSet):
    """
    Use /api/rest-auth/registration/ for Registration and can use /api/profiles/profile-list/ for profile listing
    and profile edit /api/profiles/profile-list/pk/
    """

    permission_classes = [permissions.IsAuthenticatedOrReadOnly, ]
    serializer_class = LevelSerializer
    model = ReputationConfig

    def get_queryset(self):
        config = ReputationConfig.objects.all()
        return config


class ProfileFollowersDetails(ModelViewSet):
    model = Follow
    lookup_field = 'username'
    serializer_class = UserProfileSerializer
    paginate_by_param = 'page_size'

    def get_queryset(self):
        username = self.kwargs['username']
        profile = Profile.objects.filter(user__username=username)
        followers = Follow.objects.filter(target_profile=profile).exclude(user__username=username)
        users = [obj.user for obj in followers]
        return users

    def list(self, request, *args, **kwargs):
        username = request.GET.get('username', None)
        if username:
            self.kwargs['username'] = username
            queryset = self.filter_queryset(self.get_queryset())

            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)

            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)

        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


class ProfileFollowingDetails(ProfileFollowersDetails):

    paginate_by_param = 'page_size'

    def get_queryset(self):
        username = self.kwargs['username']
        profile = Profile.objects.filter(user__username=username)
        followers = Follow.objects.filter(user__username=username,
                                          target_profile__isnull=False) \
                                  .exclude(target_profile=profile)
        users = [obj.target_profile.user for obj in followers]
        return users


class ProfileVideosUploadedDetails(ProfileFollowersDetails):
    model = Video
    lookup_field = 'username'
    serializer_class = VideoSerializer
    paginate_by_param = 'page_size'

    def get_queryset(self):
        username = self.kwargs['username']
        if self.request.user.username != username:
            return Video.objects.filter(user__username=username,
                                        status=PUBLISHED_TO_MARKETPLACE)

        status = self.request.GET.get('status', None)
        if status:
            return Video.objects.filter(user__username=username,
                                        status=status)
        return Video.objects.filter(user__username=username)


class ProfileVideosLikedDetails(ProfileFollowersDetails):
    model = Video
    lookup_field = 'username'
    serializer_class = VideoSerializer
    paginate_by_param = 'page_size'

    def get_queryset(self):
        username = self.kwargs['username']
        content_type = ContentType.objects.get(app_label='videos', model__iexact='Video')
        vote_objs = Vote.objects.filter(content_type=content_type, user__username=username)
        video_ids = [obj.object_id for obj in  vote_objs]
        videos = Video.objects.filter(id__in=video_ids)

        return videos


class ProfileCreatedContest(ProfileFollowersDetails):
    model = Contest
    lookup_field = 'username'
    serializer_class = ContestSerializer
    paginate_by_param = 'page_size'

    def get_queryset(self):
        username = self.kwargs['username']
        contests = Contest.objects.filter(user__username=username)
        return contests


from api.apps.payments.models import BoughtItems
from django.contrib.contenttypes.models import ContentType


class ProfilePurchasedVideos(ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = VideoSerializer
    model = BoughtItems
    http_method_names = ('get')

    def get_queryset(self):
        content_type = ContentType.objects.get_for_model(Video)
        objs = BoughtItems.objects.filter(order__user=self.request.user, content_type=content_type)
        videos_purchased = Video.objects.filter(id__in=[obj.object_id for obj in objs])
        return videos_purchased
