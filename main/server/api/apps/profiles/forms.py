import re
from random import randint
from datetime import timedelta

try:
    from django.utils.timezone import now
except ImportError:
    from datetime import datetime
    now = datetime.now

from django import forms
from django.utils import timezone
from django.conf import settings
from django.contrib.sites.models import Site
from django.contrib.auth.forms import UserCreationForm
from django.core.urlresolvers import reverse
from allauth.account.adapter import get_adapter

from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.models import User
from django.core.validators import validate_email

from allauth.account.utils import user_email
from allauth.account.models import EmailAddress, EmailConfirmation

from models import Profile


class CustomSignUpForm(UserCreationForm):
    type = forms.CharField(widget=forms.RadioSelect(choices=Profile.USER_TYPE), help_text="User Type?", max_length=15)
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=30, required=True)

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", 'type')

    def clean_email(self):
        email = self.cleaned_data.get("email")
        validate_email(email)
        if User.objects.filter(email=email).count() > 0:
            raise forms.ValidationError('Email is already registered')
        return email

    def save(self, commit=True):
        user = super(CustomSignUpForm, self).save(commit=True)
        user.username = self._create_user_name(self.cleaned_data["email"])
        user.save()
        profile = Profile.objects.create(user=user, type=self.cleaned_data["type"])
        profile.save()

        # TODO make user inactive, and make it active in email confirmation view

        if settings.EMAIL_VERIFICATION_REQUIRED_CUSTOM:
            self.send_email_confirmation(user)
        return user

    def _create_user_name(self, email):
        data = email.split('@')[0]
        username = re.sub('\.', '_', data)

        if User.objects.filter(username=username).exists():
            random_number = str(randint(1000, 9999))
            username = username+random_number
            return username

        return username

    def send_email_confirmation(self, user):
        """
        E-mail verification mails are sent:
        a) Explicitly: when a user signs up
        b) Implicitly: when a user attempts to log in using an unverified
        e-mail while EMAIL_VERIFICATION is mandatory.

        Especially in case of b), we want to limit the number of mails
        sent (consider a user retrying a few times), which is why there is
        a cooldown period before sending a new mail.
        """

        COOLDOWN_PERIOD = timedelta(minutes=3)
        email = user_email(user)
        if email:
            try:
                email_address = EmailAddress.objects.get_for_user(user, email)
                if not email_address.verified:
                    send_email = not EmailConfirmation.objects \
                        .filter(sent__gt=now() - COOLDOWN_PERIOD,
                                email_address=email_address) \
                        .exists()
                    if send_email:
                        self.send_confirmation(email_address)
                else:
                    send_email = False
            except EmailAddress.DoesNotExist:
                send_email = True
                email_address = EmailAddress.objects.create(user=user, email=email)
                self.send_confirmation(email_address)
                assert email_address

    def send_confirmation(self, email_address):
        confirmation = EmailConfirmation.create(email_address)

        # TODO Send email here
        self.send(confirmation)
        return confirmation

    def send(self, confirmation, **kwargs):
        current_site = kwargs["site"] if "site" in kwargs \
            else Site.objects.get_current()
        # activate_url = reverse("account_confirm_email", args=[confirmation.key])
        activate_url = current_site.domain+'#/profile?key='+confirmation.key
        ctx = {
            "user": confirmation.email_address.user,
            "activate_url": activate_url,
            "current_site": current_site,
            "key": confirmation.key,
        }
        email_template = 'email/email_confirmation'
        get_adapter().send_mail(email_template,
                                confirmation.email_address.email,
                                ctx)
        confirmation.sent = timezone.now()
        confirmation.save()


class CustomPasswordRestForm(PasswordResetForm):

    def send_mail(self, subject_template_name, email_template_name,
                  context, from_email, to_email, html_email_template_name=None):
        """
        Sends a django.core.mail.EmailMultiAlternatives to `to_email`.
        """
        html_email_template_name = 'email/password_reset.html'
        super(CustomPasswordRestForm, self).send_mail(subject_template_name, email_template_name,
                  context, from_email, to_email, html_email_template_name=html_email_template_name)

