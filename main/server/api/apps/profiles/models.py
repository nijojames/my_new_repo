from django.db import models

from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from taggit.managers import TaggableManager
from follow.utils import follow
from notifications.signals import notify

from api.apps.reputations.models import Reputation
from api.apps.activities.follow_utiils import register, get_followers_except_me


class Profile(models.Model):
    """ Profile fields extends the user model by adding extra
        fields tied to a users profile """
    GENDER_CHOICES = (
        ('M', _('Male')),
        ('F', _('Female')),
        ('T', _('Transgender')),
    )
    USER_TYPE = (
        ('videographer', _('Videographer')),
        ('video_buyer', _('Video Buyer')),
        ('both', _('Both'))

    )
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='user_profile')
    '''
    null=False,
    primary_key=True,
    verbose_name=_('user'))
    '''
    about = models.TextField(_('about'),
                             blank=True,
                             default="Tell something about yourself")
    gender = models.CharField(_('gender'),
                              max_length=len('Transgender'),
                              choices=GENDER_CHOICES,
                              help_text="Gender?",
                              null=True,
                              default='None')
    type = models.CharField(_('type'), max_length=15, choices=USER_TYPE,
                            help_text="User Type?", null=True, default='None')
    url = models.URLField(_('url'), blank=True)
    location = models.TextField(_('location'),
                                blank=True,
                                default='')
    created = models.DateTimeField(auto_now_add=True,
                                   auto_now=False,
                                   null=True)
    updated = models.DateTimeField(auto_now_add=False,
                                   auto_now=True,
                                   null=True)
    # time when the profile was published
    published = models.DateTimeField(auto_now_add=False,
                                     auto_now=False,
                                     null=True)
    # tags that interests the user
    tags = TaggableManager(_('tags'),
                           help_text="separate with commas",
                           blank=True)
    profile_picture = models.ImageField(upload_to='profile_pictures', null=True, blank=True)
    cover_picture = models.ImageField(upload_to='cover_pictures', null=True, blank=True)

    # to store url obtained from mobile upload
    profile_picture_url = models.URLField(null=True, blank=True)
    cover_picture_url = models.URLField(null=True, blank=True)

    stripe_customer_id = models.CharField(max_length=50, null=True, blank=True)
    remember_card = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = _('Profiles')

    def __unicode__(self):
        return self.user.get_username()


@receiver(post_save, sender=Profile, dispatch_uid="post_save_profile")
def activity_signal(sender, instance, created, **kwargs):
    if created:
        follow(instance.user, instance)

@receiver(post_save, sender=Profile, dispatch_uid="post_make_reputation_entry")
def make_reputation_entry(sender, instance, created, **kwargs):
    if created:
        obj = Reputation()
        obj.user = instance.user
        obj.save()

register(Profile)
