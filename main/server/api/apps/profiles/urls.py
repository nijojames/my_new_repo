from rest_framework import routers

from django.conf.urls import patterns, include, url

from views import ProfileView, ProfileSearch, LevelsView, ProfileFollowersDetails,\
    ProfileFollowingDetails, ProfileVideosUploadedDetails, ProfileVideosLikedDetails, \
    ProfileCreatedContest, ProfilePurchasedVideos



router = routers.DefaultRouter()

# Register api urls to router here
router.register(r'profile-list', ProfileView, base_name='profile-list')
router.register(r'level', LevelsView, base_name='level')
router.register(r'search', viewset=ProfileSearch, base_name="profile-search")
router.register(r'profile-followers', viewset=ProfileFollowersDetails, base_name="profile-followers")
router.register(r'profile-following', viewset=ProfileFollowingDetails, base_name="profile-following")
router.register(r'profile-videos-uploaded', viewset=ProfileVideosUploadedDetails, base_name="profile-videos-uploaded")
router.register(r'profile-videos-liked', viewset=ProfileVideosLikedDetails, base_name="profile-videos-liked")
router.register(r'profile-created-contest', viewset=ProfileCreatedContest, base_name="profile-created-contest")
router.register(r'profile-purchased-videos',
                viewset=ProfilePurchasedVideos,
                base_name="profile-purchased-videos")

urlpatterns = [
    # url(r'^$', ProfileList.as_view (), name='profile-list'),
    # url(r'^(?P<username>.*)/$', ProfileDetail.as_view(), name='profile-detail'),
    url('', include(router.urls)),
]

'''
router = routers.DefaultRouter ()
router.register ("search", viewset=ProfileSearch, base_name="profile-search")  # MLT name will be 'search-more-like-this'.
urlpatterns += router.urls
'''
