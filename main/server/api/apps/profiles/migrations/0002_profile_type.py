# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='type',
            field=models.CharField(default=b'None', choices=[(b'videographer', 'Videographer'), (b'video_buyer', 'Video Buyer'), (b'both', 'Both')], max_length=15, help_text=b'User Type?', null=True, verbose_name='type'),
        ),
    ]
