# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0004_auto_20160405_1154'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='location',
            field=models.TextField(default=b'', verbose_name='location', blank=True),
        ),
    ]
