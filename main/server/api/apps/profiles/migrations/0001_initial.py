# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0002_auto_20150616_2121'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('about', models.TextField(default=b'Tell something about yourself', verbose_name='about', blank=True)),
                ('gender', models.CharField(default=b'None', choices=[(b'M', 'Male'), (b'F', 'Female'), (b'T', 'Transgender')], max_length=11, help_text=b'Gender?', null=True, verbose_name='gender')),
                ('url', models.URLField(verbose_name='url', blank=True)),
                ('location', models.CharField(default=b'', max_length=150, verbose_name='location', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated', models.DateTimeField(auto_now=True, null=True)),
                ('published', models.DateTimeField(null=True)),
                ('tags', taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', blank=True, help_text=b'separate with commas', verbose_name='tags')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Profiles',
            },
        ),
    ]
