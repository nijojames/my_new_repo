# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0005_auto_20160415_1256'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='remember_card',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='profile',
            name='stripe_customer_id',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
