# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0003_auto_20160223_0551'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='cover_picture',
            field=models.ImageField(null=True, upload_to=b'cover_pictures', blank=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='profile_picture',
            field=models.ImageField(null=True, upload_to=b'profile_pictures', blank=True),
        ),
    ]
