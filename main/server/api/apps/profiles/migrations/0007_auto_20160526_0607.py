# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0006_auto_20160429_0942'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='cover_picture_url',
            field=models.URLField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='profile_picture_url',
            field=models.URLField(null=True, blank=True),
        ),
    ]
