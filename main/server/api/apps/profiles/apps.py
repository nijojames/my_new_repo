from django.apps import AppConfig
from actstream import registry


class ProfileAppConfig(AppConfig):
    name = 'api.apps.profiles'

    def ready(self):
        registry.register(self.get_model('profile'))
