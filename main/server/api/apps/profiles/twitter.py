import json
import requests
from requests_oauthlib import OAuth1
from datetime import timedelta

from django.utils import timezone
from django.utils.translation import gettext as _

from allauth.socialaccount.providers.oauth.views import OAuthAdapter
from allauth.socialaccount.providers.twitter.views import TwitterProvider

from allauth.socialaccount.models import SocialToken

from allauth.socialaccount.providers.oauth.client import OAuth , OAuthError, get_token_prefix


class CustomOAuth(OAuth):

    def __init__(self, request, consumer_key, secret_key, key,  request_token_url):
        self.request = request
        self.consumer_key = consumer_key
        self.secret_key = secret_key
        self.request_token_url = request_token_url
        self.key = key  # Customised to get the resource_owner_secret

    def query(self, url, method="GET", params=dict(), headers=dict()):
        """
        Request a API endpoint at ``url`` with ``params`` being either the
        POST or GET data.
        """

        access_token = self._get_at_from_session()

        # Keep the commented code for reference
        # oauth = OAuth1(
        #     self.consumer_key,
        #     client_secret=self.secret_key,
        #     resource_owner_key=access_token['oauth_token'],
        #     resource_owner_secret=access_token['oauth_token_secret'])

        oauth = OAuth1(
            self.consumer_key,
            client_secret=self.secret_key,
            resource_owner_key=access_token.token,
            resource_owner_secret=self.key)
        response = getattr(requests, method.lower())(url,
                                                 auth=oauth,
                                                 headers=headers,
                                                 params=params)
        if response.status_code != 200:
            raise OAuthError(
                _('No access to private resources at "%s".')
                % get_token_prefix(self.request_token_url))

        return response.text


class CustomTwitterAPI(CustomOAuth):
    """
    Verifying twitter credentials
    """
    url = 'https://api.twitter.com/1.1/account/verify_credentials.json'

    def get_user_info(self):
        user = json.loads(self.query(self.url))
        return user


class CustomTwitterOAuthAdapter(OAuthAdapter):
    expires_in_key = None
    provider_id = TwitterProvider.id
    request_token_url = 'https://api.twitter.com/oauth/request_token'
    access_token_url = 'https://api.twitter.com/oauth/access_token'
    # Issue #42 -- this one authenticates over and over again...
    #authorize_url = 'https://api.twitter.com/oauth/authorize'
    authorize_url = 'https://api.twitter.com/oauth/authenticate'

    def complete_login(self, request, app,  token, response):
        request.session['oauth_api.twitter.com_access_token'] = token
        client = CustomTwitterAPI(request, app.client_id, app.secret, app.key,
                            self.request_token_url)
        extra_data = client.get_user_info()
        obj = self.get_provider().sociallogin_from_response(request,
                                                             extra_data)
        return obj

    # Parse token method added
    def parse_token(self, data):

        token = SocialToken(token=data['access_token'])
        token.token_secret = data.get('refresh_token', '')
        expires_in = data.get(self.expires_in_key, None)
        if expires_in:
            token.expires_at = timezone.now() + timedelta(
                seconds=int(expires_in))
        return token
