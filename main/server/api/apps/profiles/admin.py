from django.contrib import admin

from import_export.admin import ImportExportModelAdmin

from models import Profile


class ProfileAdmin(ImportExportModelAdmin):
    search_fields = ['about', 'user__username', 'user__first_name', 'user__last_name', ]
    
admin.site.register(Profile, ProfileAdmin)
