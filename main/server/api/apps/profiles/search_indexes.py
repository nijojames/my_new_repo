from django.utils import timezone
from haystack import indexes
from models import Profile
from api.apps.reputations.models import Reputation


class ProfileIndex (indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField (document=True, use_template=True)
    tags = indexes.MultiValueField()
    username = indexes.CharField()
    first_name = indexes.CharField()
    last_name = indexes.CharField()
    type = indexes.CharField(model_attr='type')
    user = indexes.CharField(model_attr='user')
    level = indexes.CharField()

    
    def get_model (self):
        return Profile

    def index_queryset (self, using=None):
        """Used when the entire index for the recipe model is updated"""
        return self.get_model().objects.filter (updated__lte=timezone.now())
        
    def prepare_tags (self, obj):
        return [tag.name for tag in obj.tags.all()]

    def prepare_username(self, obj):
        return obj.user.username

    def prepare_first_name(self, obj):
        return obj.user.first_name

    def prepare_last_name(self, obj):
        return obj.user.last_name

    def prepare_level(self, obj):
        user = obj.user
        reputation = Reputation.objects.filter(user=user)
        if reputation:
            return reputation[0].level_count
