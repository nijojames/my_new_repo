from celery.task.schedules import crontab
from celery.decorators import periodic_task



#Task to run every minute
@periodic_task(
    run_every=(crontab()),
    name="test_task",
    ignore_result=True
)
def test_task(**kwargs):
    return None
