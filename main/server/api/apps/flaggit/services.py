from api.apps.videos.models import Video
from api.apps.general.models import SpamHandlingConfig, ReputationConfig

from flaggit.models import Flag

from django.contrib.auth.models import User

from api.apps.general.models import ContactUsConfig
from api.apps.reputations.models import Reputation

from django.core.mail import EmailMessage

from django.conf import settings


class AutoSpamServices(object):
    def __init__(self, video_id, user):
        # self.video = Video.objects.get(video_id=video_id)
        self.video_id = video_id
        self.user = user

    def setup(self):
        self.reputation_obj = Reputation.objects.get(user=self.user)
        self.level_name, self.user_level = self.calculate_level(self.reputation_obj)

        solo = SpamHandlingConfig.get_solo()
        self.reputation_config = ReputationConfig.get_solo()
        self.spam_limit = solo.video_spam_count_limit


    def do_video_spam_check(self):
        flags_count = Flag.objects.filter(object_id=self.video_id).count()
        if flags_count+1==self.spam_limit:
            self.spam_video()

    def spam_video(self):
        video = Video.objects.get(id=self.video_id)
        video.is_spam = True
        video.save()
        self.shoot_mail()

    def shoot_mail(self):
        video_id =  Video.objects.get(id=self.video_id)
        user = User.objects.get(id=video_id.user.id)
        contact_admin = ContactUsConfig.objects.all().values_list('contact_email', flat=True)
        spam_admin = EmailMessage("spam", "Hi \nThis video is reported as a spam.", to=contact_admin)
        spam_admin.send()
        spam_user = EmailMessage("spam", "Hi \nYour video is reported as a spam.", to=[user.email])
        spam_user.send()