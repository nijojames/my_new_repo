from rest_framework import routers

from django.conf.urls import patterns, include, url

from views import FlaggitViewSet


router = routers.DefaultRouter()


router.register(r'flag-it', FlaggitViewSet, base_name='flag-it')

urlpatterns = [
    url('', include(router.urls)),
]
