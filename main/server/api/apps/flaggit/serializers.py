from flaggit.models import Flag

from rest_framework import serializers

from django.contrib.contenttypes.models import ContentType


class FlagittSerializer(serializers.ModelSerializer):
    models_to_flag = ['videos.video', ]  # entries can be added here

    content_type = serializers.CharField(required=True)

    class Meta:
        model = Flag
        fields = ('pk', 'content_type', 'object_id', 'comment', 'reviewer')

    def validate_content_type(self, attrs):
        """
        Custom validations are given for content_type content
        :param attrs:
        :return:
        """
        content_type = attrs
        if not(content_type in self.models_to_flag):
            raise serializers.ValidationError('Only %s can be flagged' % (', '.join(self.models_to_flag)))

        if '.' in content_type:
            app, model_name = content_type.split('.')
        else:
            raise serializers.ValidationError('value should be app_name.Model_name, ie videos.video')
        try:
            content_type = ContentType.objects.get(app_label=app, model__iexact=model_name)
        except:
            raise serializers.ValidationError('Not a valid content type')

        return content_type

    def validate_object_id(self, attrs):
        """
        Custom validations are given for content_type content
        :param attrs:
        :return:
        """
        user = self.context['request'].user
        obj = Flag.objects.filter(reviewer=user, object_id=attrs)
        if obj.exists():
            raise serializers.ValidationError('Already Flagged this video')

        return attrs

