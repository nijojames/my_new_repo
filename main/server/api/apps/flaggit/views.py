from django.contrib.contenttypes.models import ContentType
from serializers import FlagittSerializer

from notifications.signals import notify
from rest_framework import status
from rest_framework import response
from rest_framework import serializers
from rest_framework.viewsets import ModelViewSet
from flaggit.models import Flag

from api.apps.utils import activities, utils
from services import AutoSpamServices
from api.utl.utl import get_user_device


class FlaggitViewSet(ModelViewSet):
    """
    API end point to flag Videos. Here the object_id should be 'id' of the content to be flagged
    and content_type should be app_name.model_name ie,
    For Video videos.video
    """

    serializer_class = FlagittSerializer
    http_method_names = ['post', 'delete', ]
    model = Flag
    queryset = Flag.objects.all()

    def create(self, request, *args, **kwargs):
        if hasattr(request.data, '_mutable'):
            request.data._mutable = True
        request.data['reviewer'] = request.user.id
        content_type = request.data.get('content_type')
        if content_type and '.' in content_type:
            app, model_name = content_type.split('.')
        try:
            content_type = ContentType.objects.get(app_label=app, model__iexact=model_name)
            video = content_type.get_object_for_this_type(pk=request.data['object_id'])
            flag = Flag.objects.filter(reviewer=request.user, object_id=request.data['object_id'], content_type=content_type)
            if flag:
                flag.delete()
                return response.Response(status=status.HTTP_204_NO_CONTENT)
        except Exception:
            raise serializers.ValidationError('Not a valid content type or object provided')
        notify.send(flag.reviewer, recipient=video.user, verb=activities.SPAMMED_VIDEO,
                    action_object=flag, target=video)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer, request.user)
        headers = self.get_success_headers(serializer.data)
        return response.Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer, user):
        video_id = serializer.data['object_id']

        services = AutoSpamServices(video_id, user)
        services.do_video_spam_check()
        serializer.save()
