import string

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.db.models.signals import post_save

from api.apps.videos.models import Video

from api.apps.videos.models import REJECTED
from api.apps.videos.models import APPROVED_FOR_MARKETPLACE
from api.apps.videos.models import APPROVED_FOR_CONTEST


class NegativeReviewReason(models.Model):

    user = models.ForeignKey(User)
    video = models.ForeignKey(Video)
    # common reasons
    insufficient_model_releases = models.IntegerField(null=True, blank=True)
    insufficient_property_releases = models.IntegerField(null=True, blank=True)
    inappropriate_content = models.IntegerField(null=True, blank=True)
    copyright_violation = models.IntegerField(null=True, blank=True)
    poor_video_quality = models.IntegerField(null=True, blank=True)
    comments = models.TextField(null=True, blank=True)
    # contest specific reasons
    mismatched_art_direction = models.IntegerField(null=True, blank=True)
    mismatched_environment = models.IntegerField(null=True, blank=True)
    mismatched_lighting = models.IntegerField(null=True, blank=True)
    mismatched_space_for_copy = models.IntegerField(null=True, blank=True)



RESULT_CHOICES = ((REJECTED, 'Rejected'),
                  (APPROVED_FOR_MARKETPLACE, 'Approved for marketplace'),
                  (APPROVED_FOR_CONTEST, 'Approved for contest'))


class Reviews(models.Model):
    user = models.ForeignKey(User)
    video = models.ForeignKey(Video)
    result = models.CharField(null=True,
                              blank=True,
                              max_length=50,
                              choices=RESULT_CHOICES)
    reason = models.ForeignKey(NegativeReviewReason, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Review'
        verbose_name_plural = 'Reviews'
        unique_together = ['user', 'video']


class ReviewStatus(models.Model):
    video = models.ForeignKey(Video)
    number_of_reviews = models.IntegerField(default=0)
    over_all_rating = models.IntegerField(default=0)
    is_published = models.BooleanField(default=False)
    is_not_appropriate = models.BooleanField(default=False)
    positive_reviews_received = models.IntegerField(default=0, help_text='Auto saves from review config')
    expected_reviews = models.IntegerField(default=0, help_text='Auto saves from review config')
    overall_rating_required = models.IntegerField(default=0, help_text='Auto saves from review config')
    positives_reviews_required = models.IntegerField(default=0, help_text='Auto saves from review config')
    min_rating_for_positive_review = models.IntegerField(default=0, help_text='Auto saves from review config')
    created = models.DateTimeField(auto_created=True)

    class Meta:
        verbose_name = 'Review Status'
        verbose_name_plural = 'Review Status'

    def __unicode__(self):
        return self.video.title


from api.apps.videos.models import STATUS


class VideoToReview(models.Model):
    video = models.ForeignKey(Video)
    user_level = models.IntegerField(default=1)
    created = models.DateTimeField(auto_now_add=True)

    reviews_max = models.IntegerField(default=0)
    reviews_completed = models.IntegerField(default=0)
    reviews_sent = models.IntegerField(default=0)
    reviews_positive_required = models.IntegerField(default=0)
    reviews_positive = models.IntegerField(default=0)
    reviews_negative = models.IntegerField(default=0)
    status = models.CharField(choices=STATUS, max_length=50)

    class Meta:
        verbose_name = 'Video To Review'
        verbose_name_plural = 'Videos To Review'

    def __unicode__(self):
        return self.video.title


class UserReview(models.Model):
    """
    class to keep track of # of videos requested by user for review.
    user may or may not have posted the actual review for the user.
    actual user review is saved in class Reviews
    """
    video = models.ForeignKey(Video)
    user = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.video.title + ' ' + self.user.username
