from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from models import Reviews
# from models import ReviewStatus
from models import NegativeReviewReason
from models import VideoToReview
from models import UserReview

'''
class ReviewStatusAdmin(ImportExportModelAdmin):
    list_display = ['video', 'number_of_reviews',
                    'is_published', 'is_not_appropriate', 'expected_reviews',
                    'positive_reviews_received', 'positives_reviews_required', 'created']
'''


class ReviewAdmin(ImportExportModelAdmin):
    list_display = ['user', 'video', 'result', 'reason', ]


admin.site.register(Reviews, ReviewAdmin)
admin.site.register(NegativeReviewReason)
admin.site.register(VideoToReview)
admin.site.register(UserReview)
