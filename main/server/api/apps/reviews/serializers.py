from rest_framework import serializers

from models import Reviews, NegativeReviewReason, RESULT_CHOICES
from api.apps.videos.models import Video


class ReviewVideoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Video
        fields = ('id', 'title', 'video', 'created', 'orientation',)


# class ReviewSerializer(serializers.ModelSerializer):
#     video = ReviewVideoSerializer(read_only=True)
#     class Meta:
#         model = Reviews
#         fields = ('id', 'video',
#                   'comments', 'capture_quality', 'exposure_quality',
#                   'sharpness', 'color', 'appropriate_content',
#                   'tag_accuracy', 'model_property_releases')


    def validate_capture_quality(self, attrs):
        """
        Custom validations are given for value content
        :param attrs:
        :return:
        """

        if not attrs or float(attrs) > 5 or float(attrs) < 0:
            raise serializers.ValidationError('Capture Quality Rating should be less than or equal to 5')

        return attrs

    def validate_exposure_quality(self, attrs):
        """
        Custom validations are given for value content
        :param attrs:
        :return:
        """

        if not attrs or float(attrs) > 5 or float(attrs) < 0:
            raise serializers.ValidationError('Exposure Quality Rating should be less than or equal to 5')

        return attrs

    def validate_sharpness(self, attrs):
        """
        Custom validations are given for value content
        :param attrs:
        :return:
        """

        if not attrs or float(attrs) > 5 or float(attrs) < 0:
            raise serializers.ValidationError('Sharpness Rating should be less than or equal to 5')

        return attrs

    def validate_color(self, attrs):
        """
        Custom validations are given for value content
        :param attrs:
        :return:
        """

        if not attrs or float(attrs) > 5 or float(attrs) < 0:
            raise serializers.ValidationError('Color Rating should be less than or equal to 5')

        return attrs

    def validate_appropriate_content(self, attrs):
        """
        Custom validations are given for value content
        :param attrs:
        :return:
        """

        if not attrs or float(attrs) > 5 or float(attrs) < 0:
            raise serializers.ValidationError('Appropriate Rating should be less than or equal to 5')

        return attrs

    def validate_tag_accuracy(self, attrs):
        """
        Custom validations are given for value content
        :param attrs:
        :return:
        """

        if not attrs or float(attrs) > 5 or float(attrs) < 0:
            raise serializers.ValidationError('Tag Accuracy Rating should be less than or equal to 5')

        return attrs

    def validate_comments(self, attrs):
        """
        Custom validations are given for value content
        :param attrs:
        :return:
        """

        if not attrs:
            raise serializers.ValidationError('null comments not accepted')

        return attrs


class ReviewSerializer(serializers.ModelSerializer):
    # video = ReviewVideoSerializer(read_only=True)
    result = serializers.ChoiceField(choices=RESULT_CHOICES,
                                     required=True)

    class Meta:
        model = Reviews
        # fields = ('id', 'video',
        #           'comments', 'capture_quality', 'exposure_quality',
        #           'sharpness', 'color', 'appropriate_content',
        #           'tag_accuracy', 'model_property_releases')


class NegativeReviewReasonSerializer(serializers.ModelSerializer):
    insufficient_model_releases = serializers.IntegerField(required=True)
    insufficient_property_releases = serializers.IntegerField(required=True)
    inappropriate_content = serializers.IntegerField(required=True)
    copyright_violation = serializers.IntegerField(required=True)
    poor_video_quality = serializers.IntegerField(required=True)
    # contest specific reasons
    mismatched_art_direction = serializers.IntegerField(required=True)
    mismatched_environment = serializers.IntegerField(required=True)
    mismatched_lighting = serializers.IntegerField(required=True)
    mismatched_space_for_copy = serializers.IntegerField(required=True)

    class Meta:
        model = NegativeReviewReason
