from itertools import groupby as g

from celery.task.schedules import crontab
from celery.decorators import periodic_task
from haystack.management.commands import rebuild_index
from celery.decorators import task
from notifications.signals import notify

from api.apps.videos.models import Video
from api.apps.videos.models import REJECTED
from api.apps.videos.models import PUBLISHED_TO_MARKETPLACE
from api.apps.videos.models import PUBLISHED_TO_CONTEST
from api.apps.videos.models import REVIEW_FOR_MARKETPLACE
from api.apps.videos.models import REVIEW_FOR_CONTEST
from api.apps.reviews.models import VideoToReview, UserReview, Reviews

from api.apps.payments.models import Payee, REVIEWER

from api.apps.reputations.models import Reputation
from api.apps.utils import activities

from django.db import transaction
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist


# Task to run every minute
# @periodic_task(
#     run_every=(crontab()),
#     name="update_search_index",
#     ignore_result=True
# )
# def update_search_index(**kwargs):
#     rebuild_index.Command().handle(interactive=False)

def modify_reputation(instance, value):
    Reputation.objects.log_reputation_action(user=instance.user,
                                             originating_user=instance.user,
                                             target_object=instance,
                                             action_value=value)
'''
def most_common_oneliner(reviews):
    L = [obj.result for obj in reviews]
    return max(g(sorted(L)), key=lambda(x, v):(len(list(v)),-L.index(x)))[0]
'''


@task(name="check_and_publish_video")
def publish_video_is_review_done(video):
    with transaction.atomic():
        video = Video.objects.get(pk=video)
        video_to_review = VideoToReview.objects.get(video=video)

        reviews_positive_required = video_to_review.reviews_positive_required
        reviews_positive = video_to_review.reviews_positive

        if reviews_positive >= reviews_positive_required:
            # 'review to publish' map
            r_to_p_dict = {REVIEW_FOR_MARKETPLACE: PUBLISHED_TO_MARKETPLACE,
                           REVIEW_FOR_CONTEST: PUBLISHED_TO_CONTEST}

            video.status = r_to_p_dict[video.status]
            verb = activities.VIDEO_PUBLISHED
            # ToDo: get from config
            points_for_rejection = -1
            points_for_approval = 1
            video.published = timezone.now()

            negative_reviews = Reviews.objects.filter(video=video,
                                                      result=REJECTED)
            # Delete incorrect reviewers from Payee entries
            for obj in negative_reviews:
                payee_obj = Payee.objects.filter(user__id=obj.user.id,
                                                 video_id=video.id)
                payee_obj.delete()
        else:
            video.status = REJECTED
            verb = activities.VIDEO_REJECTED
            points_for_rejection = 1
            points_for_approval = -1

        video.save()
        notify.send(video.user,
                    recipient=video.user,
                    verb=verb,
                    action_object=video,
                    target=video)

        reviews = Reviews.objects.filter(video=video)
        for review in reviews:
            if review.result == REJECTED:
                modify_reputation(review, points_for_rejection)
            else:
                modify_reputation(review, points_for_approval)
            # Send notification to reviewers
            notify.send(review.user,
                        recipient=review.user,
                        verb=verb,
                        action_object=review,
                        target=video)

        # We don't want to delete it now
        # objs = VideoToReview.objects.filter(video=video)
        # if objs:
        #     objs.delete()


@task(name="validate_review_job")
def validate_review_job(user, video_id, *args, **kwargs):
    try:
        Payee.objects.get(user__id=user,
                          video_id=video_id,
                          type=REVIEWER)
    except ObjectDoesNotExist:
        # if the user has not responded with review yet,
        # then decrement the reviews_sent and delete the
        # user review entry
        with transaction.atomic():
            video_to_review = VideoToReview.objects \
                                           .select_for_update() \
                                           .get(video__id=video_id)
            video_to_review.reviews_sent -= 1
            video_to_review.save()

            user_review = UserReview.objects.filter(user__id=user,
                                                    video__id=video_id)
            # ToDo: why check the existence of user_review ?
            #       it must be present
            if user_review:
                #     obj = user_review[0]
                #     notify.send(obj.user,
                #                 recipient=obj.user,
                #                 verb=activities.REVIEW_EXPIRED,
                #                 action_object=obj.video,
                #                 target=obj.video)

                # NB: don't DELETE only if we have to restrict the user
                #     from getting the same Video again on expiry
                user_review.delete()
