from rest_framework import routers

from django.conf.urls import patterns, include, url

# from views import ReviewViewSet, PastReviewViewSet
from views import VideoForReview, ReviewRemainingView


router = routers.DefaultRouter()

# # Register api urls to router here
# router.register(r'to-review', ReviewViewSet, base_name='review')
# router.register(r'reviewed', PastReviewViewSet, base_name='reviewed')
router.register(r'video-for-review', VideoForReview, base_name='video-for-review')

urlpatterns = [
    url('', include(router.urls)),
    url(r'^video-for-review', VideoForReview.as_view(), name='video-for-review'),
    url(r'^reviews-remaining', ReviewRemainingView.as_view(), name='reviews-remaining')
]
