from django.db.models import F
from django.db import transaction

from api.apps.reputations.level_privileges import ServiceBase

from api.apps.general.models import ReputationConfig

from api.apps.reputations.models import Reputation
from api.apps.reputations.level_privileges import LevelPrivilegeFilter

from .models import VideoToReview, UserReview


class ReviewServices(ServiceBase):
    pass


class ReviewServices(ReviewServices):

    def __init__(self, user):
        self.user = user
        self.config = ReputationConfig.get_solo()
        self.setup()

    def setup(self):
        self.reputation_obj = Reputation.objects.get(user=self.user)
        self.level_name, self.user_level = self.calculate_level(self.reputation_obj)
        self.level_filter = LevelPrivilegeFilter(self.user)

    def get_a_video_for_review(self):
        with transaction.atomic():
            videos_already_reviewed = [obj.video.id
                                       for obj in
                                       UserReview.objects.filter(user=self.user)]

            videos_to_review = VideoToReview.objects \
                                            .select_for_update() \
                                            .filter(user_level__lte=self.user_level,
                                                    reviews_max__gt=F('reviews_sent')) \
                                            .exclude(video__id__in=videos_already_reviewed) \
                                            .exclude(video__user=self.user)[:1]
            if not videos_to_review:
                return False

            video_to_review = videos_to_review[0]
            video_to_review.reviews_sent += 1
            video_to_review.save()

            # TODO Do push notification

            user_review = UserReview()
            user_review.user = self.user
            user_review.video = video_to_review.video
            user_review.save()

            return video_to_review.video

    def check_if_review_completed(self, video_to_review):
        # max # of reviews have completed ?
        if video_to_review.reviews_completed >= video_to_review.reviews_max:
            return True
        # all the sent reviews have completed ?
        if video_to_review.reviews_sent > video_to_review.reviews_completed:
            return False
        # all the sent reviews have completed and they
        # meet the positive threshold required
        if (video_to_review.reviews_positive >=
            video_to_review.reviews_positive_required):
            return True
        # all the sent reviews have completed and they
        # meet the negative threshold required
        if (video_to_review.reviews_negative >
            (video_to_review.reviews_max -
             video_to_review.reviews_positive_required)):
            return True

        return False
