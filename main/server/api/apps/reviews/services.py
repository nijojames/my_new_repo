from datetime import timedelta

from django.conf import settings
from django.db.models import F
from django.utils import timezone

from api.apps.reputations.models import Reputation
from api.apps.profiles.models import Profile
from api.apps.videos.models import Video
from api.apps.general.models import ReviewSystemConfig

from models import ReviewStatus, Reviews


class ReviewUtils(object):

    def chunks(self, input_list, n):
        """Yield successive n-sized chunks from l."""
        for i in xrange(0, len(input_list), n):
            yield input_list[i:i+n]


class ReviewMixin(object):

    def get_reviews_in_progress(self):
        """
        Reviews which are on going
        :return:
        """
        date = timezone.now()
        date = date.replace(hour=0, minute=0, second=0)
        reviews_in_progress = Reviews.objects.filter(is_reviewed=False, has_expired=False, created__lt=date)
        return reviews_in_progress

    def get_user_not_available(self, reviews_in_progress):
        """
        Gets the User ids who are not available for the review process
        :param reviews_in_progress: Reviews QuerySet
        :return: List of PKs
        """
        user_ids = [obj.user.id for obj in reviews_in_progress]
        return user_ids

    def get_random_video_graphers(self, user):
        """
        Gets the users whose to be reviewed bucket is empty
        :return: Profile QuerySet
        """
        reviews_in_progress = self.get_reviews_in_progress()
        user_ids = self.get_user_not_available(reviews_in_progress)
        user_ids = list(set(user_ids))
        profiles = Profile.objects.exclude(user__id__in=user_ids).exclude(type='buyers').order_by('?')
        profiles = self._filter_by_user_level(user, profiles)
        users_reached_limit_for_the_day = self._find_out_profiles_not_saturated(profiles)
        profiles = profiles.exclude(id__in=users_reached_limit_for_the_day)
        return profiles[:self.video_batch_limit]

    def _filter_by_user_level(self, user, profiles):
        """

        """
        reputation = Reputation.objects.get(user=user)
        user_ids = [obj.user.id for obj in profiles]
        reputation_objs = Reputation.objects.filter(user__id__in=user_ids, level_count__lt=reputation.level_count)
        user_ids = [obj.user.id for obj in reputation_objs]
        profiles = profiles.exclude(user__id__in=user_ids)
        return profiles.exclude(user__id__in=user_ids)

    def _find_out_profiles_not_saturated(self, profiles):
        """
        Takes out the profiles who have met the present day's limit
        we don't want to assign more videos to them
        """
        user_ids = []
        date = timezone.now() - timedelta(1)
        date = date.replace(hour=23, minute=59, second=59)

        for profile in profiles:
            objs = Reviews.objects.filter(user=profile.user,
                                          is_reviewed=False,
                                          has_expired=False,
                                          created__gt=date)
            if int(objs.count()) >= int(self.video_batch_limit):
                user_ids.append(profile.id)
            else:
                pass
        return user_ids


class ReviewServices(ReviewUtils, ReviewMixin):
    """
    Review system services
    usage:
    #>>> from api.apps.reviews import services
    #>>> obj = services.ReviewServices()
    """
    def __init__(self):
        review_settings = ReviewSystemConfig.get_solo()
        self.video_batch_limit = review_settings.videos_batches_count
        self.over_all_rating_required = review_settings.overall_rating_required
        self.positives_reviews_required = review_settings.overall_positive_reviews_required
        self.min_rating_for_positive_review = review_settings.min_rating_for_positive_review
        self.process_videos()

    def process_videos(self):
        """
        Gets the video chunks length of defined in the admin settings
        process the videos and random user selected
        :return:
        """
        videos = self.get_videos_to_be_reviewed()
        for video_batch in self.chunks(list(videos), self.video_batch_limit):
            self.process_video_and_video_graphers(video_batch)

    def process_video_and_video_graphers(self, video_batch):
        for video in video_batch:
            video_grapher_profiles = self.get_random_video_graphers(video.user)
            if not(len(video_grapher_profiles) == self.video_batch_limit):
                continue

            for video_grapher_profile in video_grapher_profiles:
                review = Reviews.objects.filter(user=video_grapher_profile.user, video=video)
                if not review:
                    review = Reviews(user=video_grapher_profile.user, video=video)
                    review.save()

            # Make the current video as reviewed when it is send to the N video-graphers
            video.sent_for_review = True
            video.save()
            if not ReviewStatus.objects.filter(video=video):
                review_status = ReviewStatus()
                review_status.video = video
                review_status.expected_reviews = self.video_batch_limit
                review_status.overall_rating_required = self.over_all_rating_required
                review_status.positives_reviews_required = self.positives_reviews_required
                review_status.min_rating_for_positive_review = self.min_rating_for_positive_review
                review_status.save()

    def get_videos_to_be_reviewed(self):
        """
        Videos which are not yet reviewed
        :return: Videos QuerySet
        """
        videos = Video.objects.filter(is_published=False, is_spam=False, sent_for_review=False)
        return videos


class ReviewCalculationServices(object):

    def __init__(self, obj):
        self.video = obj.video
        self.rating = self._compute_total_reviews(obj)
        review_settings = ReviewSystemConfig.get_solo()
        self.overall_rating_required = review_settings.overall_rating_required
        self.min_rating_for_positive_review = review_settings.min_rating_for_positive_review

    def _compute_total_reviews(self, obj):
        total_reviews = obj.capture_quality+obj.exposure_quality+\
                        obj.sharpness+obj.color+obj.appropriate_content+obj.tag_accuracy
        rating = round(float(total_reviews)/6)
        return int(rating)

    def compute_over_all_reviews(self):
        review_status = ReviewStatus.objects.get(video=self.video)
        review_status.number_of_reviews = F('number_of_reviews') + 1
        review_status.over_all_rating += self.rating

        #Increases the positive review count if the obj rated positive
        if self._check_if_review_positive():
            review_status.positive_reviews_received += 1

        # make the video mark as in appropriate if it not qualifies the rate limit
        if not self._check_for_video_publishing_future(review_status):
            review_status.is_not_appropriate = True
            # TODO send alert to the video-grapher stating that video cannot be placed in the marketplace

        # make the video published if it qualifies the rate limit
        review_status.save()
        self._check_if_to_publish_video(review_status)
        return True

    def _check_if_to_publish_video(self, over_all_status):
        """
        Video is published if the number of positive reviews got for the video is greater
        or equal to the required positive rating set from the sngleton config
        :param over_all_status: ReviewStatus object
        :return: None
        """
        if over_all_status.positive_reviews_received >= over_all_status.positives_reviews_required:
            over_all_status.is_published = True
            over_all_status.save()
            self.video.is_published = True
            self.video.save()

            # TODO mail service and send a notification to uploader/up voted reviewers

            # TODO add points to user reputation under review section
            self._shoot_mail_to_video_grapher(over_all_status)
            self._shoot_mail_to_reviewers(over_all_status)

    def _shoot_mail_to_video_grapher(self, over_all_status):
        """
        Shoot notifications to videographer and notifications
        :return:
        """
        pass

    def _shoot_mail_to_reviewers(self, over_all_status):
        """
        Shoot notifications to reviewers since the video is published and notifications
        :return:
        """
        pass


    def _check_for_video_publishing_future(self, over_all_status):
        """
        Check if the video will be published on future by analysing the over all reviews so far.
        :param over_all_status:
        :return:
        """
        positive_reviews_required = over_all_status.positives_reviews_required
        positive_reviews_got_so_far = over_all_status.positive_reviews_received
        reviews_left = over_all_status.expected_reviews - over_all_status.number_of_reviews
        if positive_reviews_required <= positive_reviews_got_so_far + reviews_left:
            return True
        else:
            return False

    def _check_if_review_positive(self):
        """
        Checks if the review is positive or not.
        :return: Boolean
        """

        if self.rating >= self.min_rating_for_positive_review:
            return True
        return False


class ReviewReputationServices(object):
    """
    Reputations related Video kind of activities
    """

    def _post_save_signal_callback(self, **kwargs):
        instance = kwargs['instance']
        created = kwargs['created']
        if not created:
            self.modify_reputation(instance)

    def check_conditions(self, instance):
        return True

    def get_target_object(self, instance):
        return instance

    def get_target_user(self, instance):
        return instance.user

    def get_originating_user(self, instance):
        return instance.user

    def get_value(self, instance):
        # TODO take it from DB
        return settings.DEFAULT_REVIEW_UPLOADED_REPUTATION

    def modify_reputation(self, instance):
        if self.check_conditions(instance):
            Reputation.objects.log_reputation_action(user = self.get_target_user(instance),
                                                     originating_user = self.get_originating_user(instance),
                                                     target_object = self.get_target_object(instance),
                                                     action_value = self.get_value(instance))


class ReviewExpiryCheck(ReviewMixin):
    """
    Makes the to-review entries those are expired and re-sends to other videographers if necessary
    """

    def __init__(self):
        review_settings = ReviewSystemConfig.get_solo()
        self.R = review_settings.resend_videographers_multiplier
        self.do_check_expiry_resend_if_needed()

    def do_check_expiry_resend_if_needed(self):
        """
        Check all the reviews pending, do check if they have not reviewed but expired
        and resend them if the video has not published ie review status != published
        :return:
        """
        review_statuses = ReviewStatus.objects.filter(is_published=False, is_not_appropriate=False)
        for review_status in review_statuses:
            reviews = Reviews.objects.filter(video=review_status.video, has_expired=False, is_reviewed=False)
            if reviews and self._check_if_to_process(reviews):
                self.process_reviews(review_status, pending_reviews=reviews)

    def process_reviews(self, review_status, pending_reviews=None):
        # Get all reviews related to video/review_status
        reviews = Reviews.objects.filter(video=review_status.video)
        # The count of remaining reviews/pending reviews
        not_done_reviews = review_status.expected_reviews - review_status.number_of_reviews

        # video-graphers qs who are free now to review and not participating in the current review
        video_graphers = self.get_random_video_graphers(not_done_reviews, reviews)

        # Create new Review entries based on R*N
        # Lets not send for review until we get enough video graphers
        # Lets expire only when we find enough videographers
        if len(video_graphers) == not_done_reviews*self.R:
            pending_reviews.update(has_expired=True)
            self.resend_for_new_videographers(video_graphers, review_status)


    # TODO reuse the code
    def resend_for_new_videographers(self, video_graphers, review_status):
        for profile in video_graphers:
            review = Reviews.objects.filter(user=profile.user, video=review_status.video)
            if not review:
                review = Reviews(user=profile.user, video=review_status.video)
                review.save()


    def _check_if_to_process(self, reviews):
        data_time_now  = timezone.now()
        # data_time_now = datetime.utcnow().replace(tzinfo=utc)
        # Because all objects in review connected to a video is created at the same time
        # We just need to check only one from it

        # Saves in server timezone, compares in server timezone
        time_diff_obj = data_time_now - reviews[0].created

        # Checks if the time created is behind 48 hours.
        time_diff = divmod(time_diff_obj.days * 86400 + time_diff_obj.seconds, 60)
        if time_diff[0] > 2880:
            return True

        return False

    def get_random_video_graphers(self, limit, reviews):
        """
        Gets the users whose to be reviewed bucket is empty
        :return: Profile QuerySet
        """
        reviews_in_progress = self.get_reviews_in_progress()
        user_ids = self.get_user_not_available(reviews_in_progress)
        user_ids_of_current_review = self.get_user_ids_in_current_review(reviews)
        user_ids = user_ids+user_ids_of_current_review
        user_ids = list(set(user_ids))
        profiles = Profile.objects.exclude(user__id__in=user_ids).exclude(type='buyers').order_by('?')
        return profiles[:limit]

    def get_user_ids_in_current_review(self, reviews):
        user_ids = [obj.user.id for obj in reviews]
        return user_ids


class ContestReputationService():
    """
    Reputation related to video won the contest
    """

    def modify_reputation(self, instance, value):
        Reputation.objects.log_reputation_action(user = instance.user,
                                                     originating_user = instance.user,
                                                     target_object = instance,
                                                     action_value = value)

    def reputation_update_after_contest_won(self, video):
        """
        Reputation update for video reviewers and uploader after winning a contest
        """
        self.modify_reputation(video, settings.DEFAULT_CONTEST_WIN_REPUTATION)
        reviews = Reviews.objects.filter(video=video, result=1)
        for review in reviews:
            self.modify_reputation(review, settings.DEFAULT_CONTEST_WIN_REPUTATION)
