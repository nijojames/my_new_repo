# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0015_auto_20160713_0032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='negativereviewreason',
            name='comments',
            field=models.TextField(null=True, blank=True),
        ),
    ]
