# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('videos', '0025_auto_20160621_1848'),
        ('reviews', '0007_auto_20160527_1119'),
    ]

    operations = [
        migrations.CreateModel(
            name='NegativeReviewReason',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('insufficient_model_releases', models.IntegerField(null=True, blank=True)),
                ('insufficient_property_releases', models.IntegerField(null=True, blank=True)),
                ('inappropriate_content', models.IntegerField(null=True, blank=True)),
                ('copyright_violation', models.IntegerField(null=True, blank=True)),
                ('poor_video_quality', models.IntegerField(null=True, blank=True)),
                ('comments', models.TextField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('video', models.ForeignKey(to='videos.Video')),
            ],
        ),
        migrations.CreateModel(
            name='VideoToReview',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_level', models.IntegerField(default=1)),
                ('reviews_max', models.IntegerField(default=0)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('video', models.ForeignKey(to='videos.Video')),
            ],
            options={
                'verbose_name': 'Video To Review',
                'verbose_name_plural': 'Videos To Review',
            },
        ),
        migrations.RenameField(
            model_name='reviews',
            old_name='appropriate_content',
            new_name='result',
        ),
        migrations.RemoveField(
            model_name='reviews',
            name='capture_quality',
        ),
        migrations.RemoveField(
            model_name='reviews',
            name='color',
        ),
        migrations.RemoveField(
            model_name='reviews',
            name='comments',
        ),
        migrations.RemoveField(
            model_name='reviews',
            name='exposure_quality',
        ),
        migrations.RemoveField(
            model_name='reviews',
            name='has_expired',
        ),
        migrations.RemoveField(
            model_name='reviews',
            name='is_reviewed',
        ),
        migrations.RemoveField(
            model_name='reviews',
            name='model_property_releases',
        ),
        migrations.RemoveField(
            model_name='reviews',
            name='sharpness',
        ),
        migrations.RemoveField(
            model_name='reviews',
            name='tag_accuracy',
        ),
        migrations.RemoveField(
            model_name='reviews',
            name='updated',
        ),
        migrations.AlterField(
            model_name='reviews',
            name='created',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AddField(
            model_name='reviews',
            name='reason',
            field=models.ForeignKey(blank=True, to='reviews.NegativeReviewReason', null=True),
        ),
    ]
