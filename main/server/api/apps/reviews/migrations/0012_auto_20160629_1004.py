# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0011_userreview'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reviews',
            name='result',
            field=models.IntegerField(blank=True, null=True, choices=[(0, b'Rejected'), (1, b'Approved for Market place'), (2, b'Approved for Contest')]),
        ),
        migrations.AlterField(
            model_name='videotoreview',
            name='status',
            field=models.CharField(default=b'review', max_length=50, choices=[(b'review_for_contest', b'Review for contest'), (b'review_for_market_place', b'Review for marketplace'), (b'published_to_contest', b'Published to contest'), (b'published_to_marketplace', b'Published to marketplace'), (b'completed', b'Review Completed'), (b'Rejected', b'Rejected'), (b'removed', b'Removed'), (b'review', b'In review'), (b'completed', b'Review Completed'), (b'published', b'Published')]),
        ),
    ]
