# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0004_auto_20160323_0537'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviews',
            name='appropriate_content',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='reviews',
            name='capture_quality',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='reviews',
            name='color',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='reviews',
            name='exposure_quality',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='reviews',
            name='model_property_releases',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='reviews',
            name='sharpness',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='reviews',
            name='tag_accuracy',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='reviews',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2016, 3, 30, 7, 7, 54, 52254, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='reviewstatus',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2016, 3, 30, 7, 7, 54, 53187, tzinfo=utc)),
        ),
    ]
