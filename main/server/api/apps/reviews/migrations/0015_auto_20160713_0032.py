# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0014_auto_20160711_0735'),
    ]

    operations = [
        migrations.AddField(
            model_name='negativereviewreason',
            name='mismatched_art_direction',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='negativereviewreason',
            name='mismatched_environment',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='negativereviewreason',
            name='mismatched_lighting',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='negativereviewreason',
            name='mismatched_space_for_copy',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
