# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0003_auto_20160317_0311'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewstatus',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2016, 3, 23, 5, 37, 36, 221317, tzinfo=utc)),
        ),
        migrations.AddField(
            model_name='reviewstatus',
            name='min_rating_for_positive_review',
            field=models.IntegerField(default=0, help_text=b'Auto saves from review config'),
        ),
        migrations.AddField(
            model_name='reviewstatus',
            name='positive_reviews_received',
            field=models.IntegerField(default=0, help_text=b'Auto saves from review config'),
        ),
        migrations.AlterField(
            model_name='reviews',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2016, 3, 23, 5, 37, 36, 219727, tzinfo=utc)),
        ),
    ]
