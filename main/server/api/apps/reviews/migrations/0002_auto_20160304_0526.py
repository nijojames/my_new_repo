# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reviews',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2016, 3, 4, 5, 26, 46, 513333, tzinfo=utc)),
        ),
        migrations.AlterUniqueTogether(
            name='reviews',
            unique_together=set([('user', 'video')]),
        ),
    ]
