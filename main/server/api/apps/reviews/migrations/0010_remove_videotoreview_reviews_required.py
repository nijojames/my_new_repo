# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0009_auto_20160622_0953'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='videotoreview',
            name='reviews_required',
        ),
    ]
