# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0013_auto_20160708_0621'),
    ]

    operations = [
        migrations.AlterField(
            model_name='videotoreview',
            name='status',
            field=models.CharField(max_length=50, choices=[(b'review_for_contest', b'Review for contest'), (b'review_for_marketplace', b'Review for marketplace'), (b'published_to_contest', b'Published to contest'), (b'published_to_marketplace', b'Published to marketplace'), (b'completed', b'Review Completed'), (b'rejected', b'Rejected'), (b'removed', b'Removed')]),
        ),
    ]
