# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0005_auto_20160330_0707'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reviews',
            name='rating',
        ),
        migrations.AlterField(
            model_name='reviews',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2016, 3, 30, 7, 11, 55, 918137, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='reviewstatus',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2016, 3, 30, 7, 11, 55, 918992, tzinfo=utc)),
        ),
    ]
