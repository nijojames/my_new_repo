# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0002_auto_20160304_0526'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='reviewstatus',
            options={'verbose_name': 'Review Status', 'verbose_name_plural': 'Review Status'},
        ),
        migrations.AddField(
            model_name='reviewstatus',
            name='expected_reviews',
            field=models.IntegerField(default=0, help_text=b'Auto saves from review config'),
        ),
        migrations.AddField(
            model_name='reviewstatus',
            name='overall_rating_required',
            field=models.IntegerField(default=0, help_text=b'Auto saves from review config'),
        ),
        migrations.AddField(
            model_name='reviewstatus',
            name='positives_reviews_required',
            field=models.IntegerField(default=0, help_text=b'Auto saves from review config'),
        ),
        migrations.AlterField(
            model_name='reviews',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2016, 3, 17, 3, 11, 40, 491019, tzinfo=utc)),
        ),
    ]
