# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('videos', '0008_video_is_spam'),
    ]

    operations = [
        migrations.CreateModel(
            name='Reviews',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_reviewed', models.BooleanField(default=False)),
                ('has_expired', models.BooleanField(default=False)),
                ('rating', models.IntegerField(null=True, blank=True)),
                ('created', models.DateTimeField(default=datetime.datetime(2016, 3, 3, 12, 52, 11, 883377, tzinfo=utc))),
                ('comments', models.TextField(null=True, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('video', models.ForeignKey(to='videos.Video')),
            ],
            options={
                'verbose_name': 'Review',
                'verbose_name_plural': 'Reviews',
            },
        ),
        migrations.CreateModel(
            name='ReviewStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number_of_reviews', models.IntegerField(default=0)),
                ('over_all_rating', models.IntegerField(default=0)),
                ('is_published', models.BooleanField(default=False)),
                ('is_not_appropriate', models.BooleanField(default=False)),
                ('video', models.ForeignKey(to='videos.Video')),
            ],
            options={
                'verbose_name': 'ReviewStatus',
                'verbose_name_plural': 'ReviewStatus',
            },
        ),
    ]
