# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0006_auto_20160330_0711'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviews',
            name='updated',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='reviews',
            name='created',
            field=models.DateTimeField(auto_created=True),
        ),
        migrations.AlterField(
            model_name='reviewstatus',
            name='created',
            field=models.DateTimeField(auto_created=True),
        ),
    ]
