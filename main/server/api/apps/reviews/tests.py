import os

from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.test import APIRequestFactory, APIClient
from rest_framework.authtoken.models import Token

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.conf import settings

from api.utl.utl import UtlRandom, UtlTest
from api.apps.videos.tests import TestUtils
from api.apps.general.models import ReviewSystemConfig
from api.apps.reviews.services import ReviewServices
from api.apps.videos.models import Video
from api.apps.videos.models import REJECTED
from api.apps.videos.models import APPROVED_FOR_MARKETPLACE
from api.apps.reviews.models import ReviewStatus
from api.apps.reputations.models import Reputation
from api.apps.reviews.models import UserReview
from api.apps.reviews.models import VideoToReview
from api.apps.payments.models import Payee

class TestReviewSystem(UtlTest, TestUtils):
    """
    Test Cases for Review system
    """

    def setUp(self):
        self.factory = APIRequestFactory()
        self.client = APIClient()
        self.random = UtlRandom(os.environ.get('TEST_SEED'))
        self.set_up_singleton()
        self.register_user()
        self.post_video(201)
        self.posted_video = self.response.data

    def set_up_singleton(self):
        review_conf = ReviewSystemConfig().get_solo()
        review_conf.videos_batches_count = 1
        review_conf.overall_positive_reviews_required = 1
        review_conf.max_num_of_reviews_required = 5
        review_conf.num_of_positive_reviews_required = 3
        review_conf.max_num_of_times_to_pay_reviewer = 1
        review_conf.review_expiry_limit = 5
        review_conf.review_expiry_margin = 3
        review_conf.save()

    def test_get_video_for_review_owner(self):
        '''
        owner must not get her own video for review
        '''
        response = self.client.get(reverse('video-for-review'),
                                   HTTP_AUTHORIZATION=self.token)
        self.assertEqual('video_data' not in response.data, True)

    def test_get_video_for_review_reviewer(self):
        '''
        a different user should be able to get other user's video for review
        '''
        # register and login as different user
        self.register_user()
        # get a video for review
        response = self.client.get(reverse('video-for-review'),
                                   HTTP_AUTHORIZATION=self.token)
        self.video_to_review = response.data
        self.assertEqual(self.video_to_review['expiry_time'], 5)
        self.assertEqual(self.video_to_review['video_data']['id'],
                         self.posted_video['id'])
        # check VideoToReview entries
        vtr = VideoToReview.objects.get(video__id=self.posted_video['id'])
        self.assertEqual(vtr.reviews_sent, 1)
        self.assertEqual(vtr.reviews_completed, 0)

        # check user review entry
        self.assertEqual(UserReview.objects.all().count(), 1)
        user_reviews = UserReview.objects \
                                 .filter(user__username=self.username) \
                                 .filter(video__id=self.posted_video['id'])
        self.assertEqual(user_reviews.count(), 1)

    def post_video_review(self, estatus, result, reason):
        self.test_get_video_for_review_reviewer()
        # post review
        self.data = {
            'result': result,
            'video': self.video_to_review['video_data']['id']
        }
        if reason:
            for key in reason:
                self.data[key] = reason[key]
        # print self.data
        response = self.client.post(reverse('video-for-review'),
                                    self.data,
                                    format='json',
                                    HTTP_AUTHORIZATION=self.token)
        # print response.status_code
        print response
        self.assertEqual(response.status_code, estatus)
        # check payee records
        cnt = 1 if response.status_code == status.HTTP_201_CREATED else 0
        payees = Payee.objects \
                      .filter(user__username=self.username) \
                      .filter(video_id=self.posted_video['id'])
        self.assertEqual(payees.count(), cnt)
        # check VideoToReview entries
        vtr = VideoToReview.objects.get(video__id=self.posted_video['id'])
        self.assertEqual(vtr.reviews_sent, 1)
        self.assertEqual(vtr.reviews_completed, cnt)

    def test_post_video_review_positive(self):
        self.post_video_review(status.HTTP_201_CREATED,
                               APPROVED_FOR_MARKETPLACE,
                               None)

    def test_post_video_review_negative_without_reason(self):
        self.post_video_review(status.HTTP_400_BAD_REQUEST,
                               REJECTED,
                               None)

    def test_post_video_review_negative_with_reason(self):
        marketplace_reasons = ['poor_video_quality',
                               'insufficient_model_releases',
                               'insufficient_property_releases',
                               'inappropriate_content',
                               'copyright_violation']
        contest_reasons = ['mismatched_art_direction',
                           'mismatched_environment',
                           'mismatched_lighting',
                           'mismatched_space_for_copy']

        n = self.random.random_num(0, 255)
        data = {'comments': self.random.random_str(n+1)}
        # randomly select reason(s)
        for r in marketplace_reasons:
            data[r] = self.random.random_num(0, 1)

        for r in contest_reasons:
            data[r] = 0

        self.post_video_review(status.HTTP_201_CREATED, REJECTED, data)

    # def test_review_entries(self):
    #     response = self.client.get(reverse('review-list'), HTTP_AUTHORIZATION=self.token)
    #     self.assertEqual(0, response.data['count'])
    #     ReviewServices()
    #     response = self.client.get(reverse('review-list'), HTTP_AUTHORIZATION=self.token)
    #     self.assertEqual(response.status_code, 200, "REST token-auth failed")
    #     self.assertEqual(1, response.data['count'])

    # def test_post_review(self):
    #     ReviewServices()
    #     response_to_review = self.client.get(reverse('review-list'), HTTP_AUTHORIZATION=self.token)
    #     video_id = response_to_review.data['results'][0]['video']['id']
    #     video_under_review = Video.objects.get(pk=video_id)

    #     self.assertEqual(video_under_review.is_published, False)
    #     self.assertEqual(video_under_review.sent_for_review, True)

    #     pk = response_to_review.data['results'][0]['id']

    #     data = {'rating': 4, 'comments': 'test_comment'}
    #     self.response = self.client.patch(reverse('review-detail',
    #                                               args=[pk]),
    #                                       data,
    #                                       format='json',
    #                                       HTTP_AUTHORIZATION=self.token)

    #     # Review status checks
    #     review_status = ReviewStatus.objects.get(video=video_under_review)
    #     self.assert_equal(review_status.number_of_reviews, 1)
    #     self.assert_equal(review_status.is_published, True)
    #     self.assert_equal(review_status.positive_reviews_received, 1)

    #     video_under_review = Video.objects.get(pk=video_id)
    #     self.assertEqual(video_under_review.is_published, True)

    #     response_to_review = self.client.get(reverse('review-list'), HTTP_AUTHORIZATION=self.token)
    #     self.assertEqual(0, response_to_review.data['count'])

    # def test_video_negative_reviews(self):
    #     ReviewServices()
    #     response_to_review = self.client.get(reverse('review-list'), HTTP_AUTHORIZATION=self.token)
    #     video_id = response_to_review.data['results'][0]['video']['id']

    #     video_under_review = Video.objects.get(pk=video_id)
    #     self.assertEqual(video_under_review.is_published, False)
    #     self.assertEqual(video_under_review.sent_for_review, True)

    #     pk = response_to_review.data['results'][0]['id']

    #     data = {'rating': 2, 'comments': 'test_comment'}
    #     self.response = self.client.patch(reverse('review-detail',
    #                                               args=[pk]),
    #                                       data,
    #                                       format='json',
    #                                       HTTP_AUTHORIZATION=self.token)

    #     # Review status checks
    #     review_status = ReviewStatus.objects.get(video=video_under_review)
    #     self.assert_equal(review_status.number_of_reviews, 1)
    #     self.assert_equal(review_status.is_published, False)
    #     self.assert_equal(review_status.positive_reviews_received, 0)

    #     video_under_review = Video.objects.get(pk=video_id)
    #     self.assertEqual(video_under_review.is_published, False)

    # def test_review_reputation(self):
    #     ReviewServices()
    #     reputation_object = Reputation.objects.get(user__username=self.username)
    #     self.assertEqual(reputation_object.review_reputation, 0)
    #     response_to_review = self.client.get(reverse('review-list'), HTTP_AUTHORIZATION=self.token)
    #     pk = response_to_review.data['results'][0]['id']

    #     data = {'rating': 4, 'comments': 'test_comment'}
    #     self.response = self.client.patch(reverse('review-detail',
    #                                               args=[pk]),
    #                                       data,
    #                                       format='json',
    #                                       HTTP_AUTHORIZATION=self.token)
    #     reputation_object = Reputation.objects.get(user__username=self.username)
    #     self.assertEqual(reputation_object.review_reputation, settings.DEFAULT_REVIEW_UPLOADED_REPUTATION)
