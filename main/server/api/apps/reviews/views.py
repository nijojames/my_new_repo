from rest_framework.viewsets import ModelViewSet
from rest_framework.viewsets import GenericViewSet
from rest_framework.views import View
from rest_framework.generics import GenericAPIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework import status

from models import Reviews

from serializers import ReviewSerializer, NegativeReviewReasonSerializer
# from services import ReviewServices
from api.apps.reputations.level_privileges import LevelPrivilegeFilter


# class ReviewViewSet(ModelViewSet):
#     serializer_class = ReviewSerializer
#     permissions = [ReviewPermissions, permissions.IsAuthenticated, ]
#     model = Reviews
#     http_method_names = ['put', 'get', 'patch']
#
#     def get_queryset(self):
#         # ReviewServices()
#         obj = LevelPrivilegeFilter(self.request.user)
#         print obj.if_user_can_upload_video(price=100)
#
#         if self.request.user.is_authenticated():
#             queryset = Reviews.objects.filter(user=self.request.user, is_reviewed=False, has_expired=False)
#             return queryset
#         return []
#
#     def update(self, request, *args, **kwargs):
#         partial = kwargs.pop('partial', False)
#         instance = self.get_object()
#
#         # Applying level check for max review per day
#         level_filter = LevelPrivilegeFilter(request.user)
#         can_review = level_filter.if_user_can_review_this_day()
#         if not (can_review == True):
#             raise serializers.ValidationError(can_review)
#
#
#         # self.check_instance(instance)
#         self.check_for_data(request.data)
#
#         serializer = self.get_serializer(instance, data=request.data, partial=partial)
#         serializer.is_valid(raise_exception=True)
#         self.perform_update(serializer)
#         return Response(serializer.data)
#
#     def perform_update(self, serializer):
#         serializer.data['is_reviewed'] = True
#         obj = serializer.save()
#         obj.is_reviewed = True
#         obj.updated = timezone.now()
#         obj.save()
#         review_calculation_service = ReviewCalculationServices(obj)
#         review_calculation_service.compute_over_all_reviews()
#         return obj
#
#     def check_instance(self, instance):
#         if instance.rating and instance.comments:
#             raise serializers.ValidationError('Already reviewed.')
#
#     def check_for_data(self, data):
#         missing_keys = []
#         required_fields = ['comments', 'capture_quality', 'exposure_quality',
#                   'sharpness', 'color', 'appropriate_content',
#                   'tag_accuracy', 'model_property_releases']
#         for key in required_fields:
#             if key not in data.keys():
#                 missing_keys.append(key)
#         if missing_keys:
#             if len(missing_keys) <= 2:
#                 message = ' and '.join(missing_keys) + ' - required'
#             else:
#                 message = ', '.join(missing_keys[:-1]) + ' and '+ missing_keys[-1]+ ' - required'
#             raise serializers.ValidationError(message)
#         return False


# class PastReviewViewSet(ReviewViewSet):
#     http_method_names = ['get', ]
#     def get_queryset(self):
#         if self.request.user.is_authenticated():
#             queryset = Reviews.objects.filter(user=self.request.user, is_reviewed=True)
#             return queryset
#         return []


# import json

from api.apps.reviews.review_services import ReviewServices
from api.apps.payments.models import Payee, REVIEWER
from api.apps.general.models import ReviewSystemConfig
from .tasks import publish_video_is_review_done, validate_review_job
from .models import VideoToReview, UserReview
from api.apps.videos.models import COMPLETED, REVIEW_FOR_CONTEST, REJECTED
from api.apps.videos.serializers import VideoSerializer
from api.apps.contests.serializers import ContestSerializer
from api.apps.contests.models import ContestEntry

from django.db import transaction


class VideoForReview(GenericAPIView):
    serializer_class = ReviewSerializer
    permissions = [permissions.IsAuthenticated, ]
    model = Reviews
    http_method_names = ['get', 'post']

    # def _handle_crone(self, user_id, video_id):
    #     config = ReviewSystemConfig().get_solo()
    #     review_expiry_limit = config.review_expiry_limit
    #
    #     interval, created = IntervalSchedule.objects.get_or_create(every=review_expiry_limit, period='seconds')
    #     name = 'user_'+str(user_id)+'_'+'video_'+str(video_id)
    #     task = PeriodicTask(name=name, task='validate_review_job',
    #                         interval=interval, kwargs=json.dumps({'video': video_id, 'user': user_id}))
    #     task.save()

    def get(self, request, *args, **kwargs):
        # TODO enter video max on video save in both areas
        level_privileges = LevelPrivilegeFilter(request.user)
        can_review_check = level_privileges.if_user_can_review_this_day()
        if isinstance(can_review_check, dict):
            raise serializers.ValidationError(can_review_check)

        services = ReviewServices(request.user)
        video_to_review = services.get_a_video_for_review()
        if not video_to_review:
            raise serializers.ValidationError({'video':
                                               'no video available for review now'})

        config = ReviewSystemConfig().get_solo()
        review_expiry_limit = config.review_expiry_limit
        review_expiry_margin = config.review_expiry_margin

        response = VideoSerializer(instance=video_to_review,
                                   context={'request': request}).data
        response.pop('user')
        response.pop('user_detail')
        # ToDo: remove model/property releases details
        #       only provide the model/property releases count
        data = {'expiry_time': review_expiry_limit,
                'video_data': response}
        if video_to_review.status == REVIEW_FOR_CONTEST:
            contest_entry = ContestEntry.objects.get(video=video_to_review)
            contest_data = ContestSerializer(instance=contest_entry.contest,
                                             context={'request': request}).data
            contest_data.pop('user_detail')
            contest_data.pop('uploaded')
            data['contest_data'] = contest_data

        # start server side timer to check for user timer expiration
        validate_review_job.apply_async(args=[request.user.id,
                                              video_to_review.id],
                                        countdown=(review_expiry_limit +
                                                   review_expiry_margin))
        # send the video to user for review
        return Response(data)

    def post(self, request, *args, **kwargs):
        if hasattr(request.data, '_mutable'):
            request.data._mutable = True

        review_services = ReviewServices(request.user)
        data = request.data
        data['user'] = request.user.id

        serializer = ReviewSerializer(data=data)
        negative_obj = None
        if not serializer.is_valid():
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

        video = serializer.validated_data['video']
        self._check_if_a_valid_request(video, request.user)

        with transaction.atomic():
            video_to_review = VideoToReview.objects \
                                           .select_for_update() \
                                           .get(video=video)
            if serializer.validated_data['result'] == REJECTED:
                negative_serializer = NegativeReviewReasonSerializer(data=data)
                if not negative_serializer.is_valid():
                    return Response(negative_serializer.errors,
                                    status=status.HTTP_400_BAD_REQUEST)
                negative_obj = negative_serializer.save()
                video_to_review.reviews_negative += 1

            review_obj = serializer.save()
            if negative_obj:
                review_obj.reason = negative_obj
                review_obj.save()
            else:
                video_to_review.reviews_positive += 1

            video_to_review.reviews_completed += 1

            if review_services.check_if_review_completed(video_to_review):
                video_to_review.status = COMPLETED
                video_to_review.save()
                publish_video_is_review_done.delay(video.id)
            else:
                video_to_review.save()

        level_services = LevelPrivilegeFilter(request.user)
        commission = level_services.get_commission_config_value_based_on_user_level()

        config = ReviewSystemConfig().get_solo()
        max_num_of_times_to_pay_reviewer = config.max_num_of_times_to_pay_reviewer
        # record the user payment details before returning
        # as the user level may change later
        payee = Payee()
        payee.video_id = review_obj.video.id
        payee.user = request.user
        payee.type = REVIEWER
        payee.max_num_of_times_to_be_payed = max_num_of_times_to_pay_reviewer
        payee.commission = commission
        payee.save()

        return Response(serializer.data,
                        status=status.HTTP_201_CREATED)

    def _check_if_a_valid_request(self, video, user):
        video_to_review = UserReview.objects.filter(video=video, user=user)
        if not video_to_review:
            raise serializers.ValidationError({'video':
                                               'You are not authorized to '
                                               'review this video now'})


class ReviewRemainingView(GenericAPIView):
    """
    Returns the remaining number of reviews that user can do today
    {'reviews_remaining': <reviews_left_today>}
    """
    serializer_class = ReviewSerializer
    permissions = [permissions.IsAuthenticated, ]
    model = Reviews
    http_method_names = ['get', ]

    def get(self, request, *args, **kwargs):
        level_privileges = LevelPrivilegeFilter(request.user)
        return Response({'reviews_remaining':
                         level_privileges.reviews_left_today()})
