from django.apps import AppConfig, apps
from actstream import registry


class VideoAppConfig(AppConfig):
    name = 'api.apps.videos'

    def ready(self):
        registry.register(self.get_model('video'))
        registry.register(apps.get_model('auth.user'))
