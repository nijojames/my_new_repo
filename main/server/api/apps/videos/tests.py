#from django.test import TestCase

# Create your tests here.
from django.core.urlresolvers import reverse
from rest_framework import status
#from rest_framework.test import APITestCase

#from .serializers import VideoSerializer
from api.apps.videos.models import Video
#from .search_indexes import VideoIndex

from django.core.management import call_command
#from django.test.utils import override_settings

from haystack import connections
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile

from django.utils.encoding import force_text

import json
import os

import requests

from api.utl.utl import UtlRandom, UtlTest


class TestUtils(object):
    random = ""
    token = ""
    response = ""
    data = ""
    username = ""
    email = ""
    password = ""
    title = ""
    tags = []

    def register_user(self):
        n = self.random.random_num(6, 30)
        self.username = self.random.random_str(n)

        n = self.random.random_num(6, 30)
        self.email = self.random.random_str(n)
        self.username = self.email

        n = self.random.random_num(6, 30)
        self.email = "%s@%s.com" % (self.email, self.random.random_str(n))

        n = self.random.random_num(6, 30)
        self.password = self.random.random_str(n)

        self.data = {
            "first_name": self.username,
            "last_name": self.username,
            "type": 'both',
            "email": self.email,
            "password1": self.password,
            "password2": self.password,
        }
        # print self.data
        self.response = self.client.post(reverse('rest_register'),
                                         self.data,
                                         format='json')
        self.assert_equal(self.response.status_code,
                          status.HTTP_201_CREATED,
                          self.data)
        self.token = json.loads(force_text(self.response.content))['key']
        self.token = 'Token %s' % self.token

    def post_video(self, estatus):
        n = self.random.random_num(3, 10) + 1
        for i in range(3, n):
            self.title = '%s %s' % (self.random.random_str(i), self.title)

        self.title = self.title.strip()
        n = self.random.random_num(3, 10) + 1
        self.tags = []
        for n in range(3, n):
            tag = self.random.random_str(n)
            tag = tag.strip()
            self.tags.append(tag)

        video = SimpleUploadedFile("file.mp4", "file_content", content_type="video/mp4")
        water_marked_video = SimpleUploadedFile("file.mp4", "file_content", content_type="video/mp4")
        self.data = {
            "title": self.title,
            "tags": json.dumps(self.tags),
            "video": video,
            "water_marked_video": water_marked_video,
            'slug': self.random.random_lowercase_string(10)
        }
        self.response = self.client.post(reverse('video-list'),
                                         self.data,
                                         format='multipart',
                                         HTTP_AUTHORIZATION=self.token)
        self.assert_equal(self.response.status_code, estatus, self.data)

    def get_video(self, estatus, num):
        if self.token:
            self.response = self.client.get(reverse('video-list'),
                                            HTTP_AUTHORIZATION=self.token)
        else:
            self.response = self.client.get(reverse('video-list'))

        self.assert_equal(self.response.status_code, estatus)
        self.assert_equal(Video.objects.count(), num)
        # we can do detail check only one video
        # make sure we have the expected reference data in self.title/tags
        if num != 1:
            return
        for i in range(0, num):
            video = self.response.data['results'][0]
            self.assert_equal(video["title"], self.title)
            self.assert_equal(sorted(video["tags"]), sorted(self.tags))

    def search_video_by_tag(self, tag, estatus):
        self.response = self.client.get(reverse('video-search-list'),
                                        {'tags__contains': tag},
                                        format='json',
                                        HTTP_AUTHORIZATION=self.token)
        self.assert_equal(self.response.status_code, estatus)
        self.assert_equal(len(self.response.data['results']), 1)
        video = self.response.data['results'][0]
        self.assert_equal(video["title"], self.title)

    def build_search_index(self):
        # override to create a test index
        settings.HAYSTACK_CONNECTIONS['default']['INDEX_NAME'] = 'test_index'
        connections.reload('default')
        call_command('rebuild_index', interactive=False, verbosity=0)

    def clear_search_index(self):
        call_command('clear_index', interactive=False, verbosity=0)


class PutGetVideoTest(UtlTest, TestUtils):

    def setUp(self):
        self.random = UtlRandom(os.environ.get('TEST_SEED'))
        self.register_user()
        super(PutGetVideoTest, self).setUp()

    def tearDown(self):
        pass

    def test_user_can_put_video(self):
        n = self.random.random_num(1, 5)
        for i in range(0, n):
            self.post_video(status.HTTP_201_CREATED)

    def test_anonymous_can_not_put_video(self):
        self.token = ""
        self.client.force_authenticate(token=None)
        self.post_video(status.HTTP_401_UNAUTHORIZED)

    def test_user_can_get_video_list(self):
        n = self.random.random_num(1, 5)
        for i in range(0, n):
            self.post_video(status.HTTP_201_CREATED)
        self.get_video(status.HTTP_200_OK, n)

    def test_anonymous_can_get_video_list(self):
        self.post_video(status.HTTP_201_CREATED)
        # clear the token and access anonymously
        # self.token = ''
        self.get_video(status.HTTP_200_OK, 1)

    def test_user_can_search_video_by_tags(self):
        self.register_user()
        self.post_video(status.HTTP_201_CREATED)
        self.build_search_index()
        self.search_video_by_tag(self.tags[self.random.random_num(0, len(self.tags)-1)],
                                 status.HTTP_200_OK)

    def test_anonymous_can_search_video_by_tags(self):
        self.register_user()
        self.post_video(status.HTTP_201_CREATED)
        self.build_search_index()
        # clear the token and search anonymously
        self.token = ''
        self.search_video_by_tag(self.tags[self.random.random_num(0, len(self.tags)-1)],
                                 status.HTTP_200_OK)

    def get_url_and_post_video(self,
                               get_estatus,
                               post_estatus=None):
        self.response = self.client.get(reverse('post-url'),
                                        {'type': 'video',
                                        #{'type': 'profile',
                                         'release_doc_count': 3},
                                        HTTP_AUTHORIZATION=self.token)
        #print "get post-url()="
        print self.response

        self.assert_equal(get_estatus, self.response.status_code)
        if self.response.status_code != status.HTTP_200_OK:
            return

        for key, value in self.response.data["results"].iteritems():
            for i in range(0, len(value)):
                data = value[i]
                #print data
                print "INFO: Uploading " + key + "[" + str(i) + "]"
                #  n = self.random.random_num(100*1024*1024, 200*1024*1024)
                n = self.random.random_num(1024*1024, 2*1024*1024)

                files = {'file': self.random.random_str(n)}
                response = requests.post(data["url"],
                                         data=data["fields"],
                                         files=files)
                #print "post="
                #print response.status_code
                #print response.text

                self.assert_equal(post_estatus, response.status_code)

    def test_user_can_get_url_and_post_video(self):
        self.get_url_and_post_video(status.HTTP_200_OK,
                                    status.HTTP_201_CREATED)

    def test_anonymous_can_not_get_url(self):
        self.token = ""
        self.client.force_authenticate(token=None)
        self.get_url_and_post_video(status.HTTP_401_UNAUTHORIZED)
