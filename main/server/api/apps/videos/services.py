from django.conf import settings

from api.apps.reputations.models import Reputation
from urllib2 import urlopen

import json

class VideoReputationServices(object):
    """
    Reputations related Video kind of activities
    """

    def _post_save_signal_callback(self, **kwargs):
        instance = kwargs['instance']
        created = kwargs['created']
        if created:
            self.modify_reputation(instance)
            if instance.location:
                location_name = self.getplace(instance.location.latitude,instance.location.longitude)
                instance.location_name = location_name
                instance.save()
        else:
            if instance.get_dirty_fields().get('location') and not instance.get_dirty_fields().get('location_name'):
                location_name = self.getplace(instance.location.latitude, instance.location.longitude)
                instance.location_name = location_name
                instance.save()

    def getplace(self, lat, lon):
        url = "http://maps.googleapis.com/maps/api/geocode/json?"
        url += "latlng=%s,%s&sensor=false" % (lat, lon)
        v = urlopen(url).read()
        j = json.loads(v)
        components = j['results'][0]['formatted_address']
        return components

    def check_conditions(self, instance):
        return True

    def get_target_object(self, instance):
        return instance

    def get_target_user(self, instance):
        return instance.user

    def get_originating_user(self, instance):
        return instance.user

    def get_value(self, instance):
        return settings.DEFAULT_VIDEO_UPLOADED_REPUTATION

    def modify_reputation(self, instance):
        if self.check_conditions(instance):
            Reputation.objects.log_reputation_action(user = self.get_target_user(instance),
                                                     originating_user = self.get_originating_user(instance),
                                                     target_object = self.get_target_object(instance),
                                                     action_value = self.get_value(instance))


class VideoServices(object):
    """
    Video model related activity manipulation
    """

    def video_upload_activity(self, **kwargs):
        instance = kwargs['instance']
        created = kwargs['created']
