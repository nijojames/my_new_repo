# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('videos', '0017_video_thumbnail_link'),
    ]

    operations = [
        migrations.CreateModel(
            name='PropertyRelease',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('address1', models.CharField(max_length=255)),
                ('address2', models.CharField(max_length=255)),
                ('state', models.CharField(max_length=255)),
                ('country', models.CharField(max_length=255)),
                ('zipcode', models.CharField(max_length=10)),
                ('property_picture', models.URLField(null=True, blank=True)),
                ('owner_first_name', models.CharField(max_length=100, null=True, blank=True)),
                ('owner_last_name', models.CharField(max_length=100, null=True, blank=True)),
                ('owner_email', models.EmailField(max_length=254, null=True, blank=True)),
                ('owner_dob', models.DateField(null=True, blank=True)),
                ('videographer', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RenameField(
            model_name='model',
            old_name='name',
            new_name='first_name',
        ),
        migrations.RenameField(
            model_name='model',
            old_name='parent_name',
            new_name='parent_first_name',
        ),
        migrations.AddField(
            model_name='model',
            name='address1',
            field=models.CharField(default='Address 1', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='model',
            name='address2',
            field=models.CharField(default='Address 2', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='model',
            name='country',
            field=models.CharField(default='Test Country', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='model',
            name='last_name',
            field=models.CharField(default='Last name', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='model',
            name='parent_last_name',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='model',
            name='phone',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='model',
            name='profile_picture',
            field=models.URLField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='model',
            name='state',
            field=models.CharField(default='Sample State', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='model',
            name='zipcode',
            field=models.CharField(default='21312', max_length=10),
            preserve_default=False,
        ),
    ]
