# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0024_video_video_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='reviews_completed',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='video',
            name='reviews_max',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='video',
            name='reviews_negative',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='video',
            name='reviews_positive',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='video',
            name='reviews_positive_required',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='video',
            name='reviews_required',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='video',
            name='reviews_sent',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='video',
            name='status',
            field=models.CharField(default=b'review', max_length=50, choices=[(b'review', b'In review'), (b'completed', b'Review Completed'), (b'published', b'Published'), (b'Rejected', b'Rejected')]),
        ),
    ]
