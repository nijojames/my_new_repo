# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0019_auto_20160525_1240'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='propertyrelease',
            name='first_name',
        ),
        migrations.RemoveField(
            model_name='propertyrelease',
            name='last_name',
        ),
        migrations.AddField(
            model_name='propertyrelease',
            name='name',
            field=models.CharField(default='asdsad1', max_length=255),
            preserve_default=False,
        ),
    ]
