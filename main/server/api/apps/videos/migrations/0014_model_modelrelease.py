# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('videos', '0013_video_location_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Model',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('email', models.EmailField(max_length=254)),
                ('dob', models.DateField(help_text=b'Provide parent details if model is minor')),
                ('parent_name', models.CharField(max_length=100, null=True, blank=True)),
                ('parent_email', models.EmailField(max_length=254, null=True, blank=True)),
                ('parent_dob', models.DateField(null=True, blank=True)),
                ('videographer', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ModelRelease',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('document', models.URLField(help_text=b'Provide url to amazon bucket')),
                ('model', models.ForeignKey(to='videos.Model')),
                ('video', models.ForeignKey(related_name='get_model_releases', to='videos.Video')),
            ],
        ),
    ]
