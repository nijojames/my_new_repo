# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0011_auto_20160321_0717'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='water_marked_video',
            field=models.FileField(default='sdfsdf', upload_to=b'videos/watermarked'),
            preserve_default=False,
        ),
    ]
