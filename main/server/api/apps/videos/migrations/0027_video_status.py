# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0026_auto_20160623_0605'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='status',
            field=models.CharField(default=b'review', max_length=50, choices=[(b'review', b'In review'), (b'completed', b'Review Completed'), (b'published', b'Published'), (b'Rejected', b'Rejected')]),
        ),
    ]
