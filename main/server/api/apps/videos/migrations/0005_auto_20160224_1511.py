# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0004_auto_20160125_1416'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='orientation',
            field=models.CharField(blank=True, max_length=20, null=True, choices=[(b'vertical', 'Vertical'), (b'horizontal', 'Horizontal'), (b'square', 'Square')]),
        ),
        migrations.AddField(
            model_name='video',
            name='permission',
            field=models.CharField(default=b'free', max_length=20, choices=[(b'royalty_free', 'Royalty Free'), (b'commercial', 'Commercial'), (b'free', 'Free')]),
        ),
    ]
