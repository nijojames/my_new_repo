# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0008_video_is_spam'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='state',
            field=models.CharField(default=b'review', max_length=50, choices=[(b'review', 'In review'), (b'review_and_contest', 'In both review and contest'), (b'contest', 'In contest'), (b'contest_and_market', 'In both contest and market place'), (b'market', 'In marketplace')]),
        ),
    ]
