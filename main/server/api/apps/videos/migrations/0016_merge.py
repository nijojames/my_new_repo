# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0015_auto_20160427_1105'),
        ('videos', '0010_video_sent_for_review'),
    ]

    operations = [
    ]
