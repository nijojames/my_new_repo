# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0018_auto_20160525_1116'),
    ]

    operations = [
        migrations.AddField(
            model_name='propertyrelease',
            name='document',
            field=models.URLField(default='http://google.com', help_text=b'Provide url to amazon bucket'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='propertyrelease',
            name='video',
            field=models.ForeignKey(related_name='get_property_releases', default=12, to='videos.Video'),
            preserve_default=False,
        ),
    ]
