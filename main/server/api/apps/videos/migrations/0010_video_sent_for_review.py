# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0009_video_is_published'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='sent_for_review',
            field=models.BooleanField(default=False),
        ),
    ]
