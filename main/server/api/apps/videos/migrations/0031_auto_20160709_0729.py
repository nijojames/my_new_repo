# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0030_auto_20160708_0621'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='published',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
