# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0012_auto_20160321_0728'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='location_name',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
