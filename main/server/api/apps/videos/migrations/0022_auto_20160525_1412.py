# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0021_auto_20160525_1330'),
    ]

    operations = [
        migrations.AlterField(
            model_name='propertyrelease',
            name='owner_dob',
            field=models.DateField(default=datetime.datetime(2016, 5, 25, 14, 12, 2, 321656, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='propertyrelease',
            name='owner_email',
            field=models.EmailField(default='admin@sample.com', max_length=254),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='propertyrelease',
            name='owner_first_name',
            field=models.CharField(default='First name', max_length=100),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='propertyrelease',
            name='owner_last_name',
            field=models.CharField(default='Last name', max_length=100),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='propertyrelease',
            name='video',
            field=models.ForeignKey(related_name='get_property_releases', default=12, to='videos.Video'),
            preserve_default=False,
        ),
    ]
