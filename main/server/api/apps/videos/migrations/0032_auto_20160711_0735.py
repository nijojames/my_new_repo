# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0031_auto_20160709_0729'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='status',
            field=models.CharField(default=b'review_for_marketplace', max_length=50, choices=[(b'review_for_contest', b'Review for contest'), (b'review_for_marketplace', b'Review for marketplace'), (b'published_to_contest', b'Published to contest'), (b'published_to_marketplace', b'Published to marketplace'), (b'completed', b'Review Completed'), (b'rejected', b'Rejected'), (b'removed', b'Removed')]),
        ),
    ]
