# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0009_video_state'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='is_contest_entry',
            field=models.BooleanField(default=False),
        ),
    ]
