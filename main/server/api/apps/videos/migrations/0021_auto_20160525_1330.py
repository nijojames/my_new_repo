# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0020_auto_20160525_1245'),
    ]

    operations = [
        migrations.AlterField(
            model_name='propertyrelease',
            name='video',
            field=models.ForeignKey(related_name='get_property_releases', blank=True, to='videos.Video', null=True),
        ),
    ]
