# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0007_auto_20160226_0837'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='is_spam',
            field=models.BooleanField(default=False),
        ),
    ]
