# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0028_auto_20160629_0824'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='permission',
            field=models.CharField(default=b'commercial', max_length=20, choices=[(b'royalty_free', 'Royalty Free'), (b'commercial', 'Commercial')]),
        ),
    ]
