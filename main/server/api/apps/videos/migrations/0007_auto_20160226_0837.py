# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import autoslug.fields
import geoposition.fields


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0006_auto_20160226_0818'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='duration',
            field=models.IntegerField(default=10, help_text=b'Duration in seconds'),
        ),
        migrations.AddField(
            model_name='video',
            name='frame_rate',
            field=models.DecimalField(null=True, max_digits=7, decimal_places=2, blank=True),
        ),
        migrations.AddField(
            model_name='video',
            name='location',
            field=geoposition.fields.GeopositionField(max_length=42, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='video',
            name='model_released',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='video',
            name='property_released',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='video',
            name='slug',
            field=autoslug.fields.AutoSlugField(default=1, populate_from=b'title', editable=True, unique=True),
            preserve_default=False,
        ),
    ]
