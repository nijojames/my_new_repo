# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0008_video_is_spam'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='is_published',
            field=models.BooleanField(default=False),
        ),
    ]
