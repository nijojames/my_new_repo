# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0025_auto_20160621_1848'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='video',
            name='reviews_completed',
        ),
        migrations.RemoveField(
            model_name='video',
            name='reviews_max',
        ),
        migrations.RemoveField(
            model_name='video',
            name='reviews_negative',
        ),
        migrations.RemoveField(
            model_name='video',
            name='reviews_positive',
        ),
        migrations.RemoveField(
            model_name='video',
            name='reviews_positive_required',
        ),
        migrations.RemoveField(
            model_name='video',
            name='reviews_required',
        ),
        migrations.RemoveField(
            model_name='video',
            name='reviews_sent',
        ),
        migrations.RemoveField(
            model_name='video',
            name='status',
        ),
    ]
