# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0005_auto_20160224_1511'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='orientation',
            field=models.CharField(blank=True, max_length=20, null=True, choices=[(b'portrait', 'Portrait'), (b'landscape', 'Landscape'), (b'square', 'Square')]),
        ),
    ]
