# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0023_auto_20160525_1507'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='video_type',
            field=models.CharField(blank=True, max_length=30, null=True, choices=[(b'normal', 'Normal'), (b'time_lapse', 'Time Lapse'), (b'slow_motion', 'Slow Motion')]),
        ),
    ]
