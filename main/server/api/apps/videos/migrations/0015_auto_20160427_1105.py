# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0014_model_modelrelease'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='video_link',
            field=models.URLField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='video',
            name='water_marked_video_link',
            field=models.URLField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='video',
            name='video',
            field=models.FileField(null=True, upload_to=b'videos', blank=True),
        ),
        migrations.AlterField(
            model_name='video',
            name='water_marked_video',
            field=models.FileField(null=True, upload_to=b'videos/watermarked', blank=True),
        ),
    ]
