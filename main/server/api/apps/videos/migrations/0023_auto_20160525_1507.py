# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0022_auto_20160525_1412'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='fps_rate',
            field=models.CharField(blank=True, max_length=10, null=True, choices=[(30, '30fps'), (60, '60fps'), (120, '120fps'), (240, '240fps')]),
        ),
        migrations.AddField(
            model_name='video',
            name='resolution',
            field=models.CharField(blank=True, max_length=10, null=True, choices=[(720, '720p'), (1080, '1080p'), (4000, '4kp')]),
        ),
    ]
