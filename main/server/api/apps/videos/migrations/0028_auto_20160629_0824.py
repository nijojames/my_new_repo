# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0027_video_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='fps_rate',
            field=models.CharField(blank=True, max_length=10, null=True, choices=[(b'30', '30fps'), (b'60', '60fps'), (b'120', '120fps'), (b'240', '240fps')]),
        ),
        migrations.AlterField(
            model_name='video',
            name='resolution',
            field=models.CharField(blank=True, max_length=10, null=True, choices=[(b'720', '720p'), (b'1080', '1080p'), (b'4096', '4k')]),
        ),
        migrations.AlterField(
            model_name='video',
            name='video_type',
            field=models.CharField(blank=True, max_length=30, null=True, choices=[(b'real_time', 'Real Time'), (b'time_lapse', 'Time Lapse'), (b'slow_motion', 'Slow Motion')]),
        ),
        migrations.AlterField(
            model_name='video',
            name='status',
            field=models.CharField(default=b'review', max_length=50, choices=[(b'review_for_contest', b'Review for contest'), (b'review_for_market_place', b'Review for marketplace'), (b'published_to_contest', b'Published to contest'), (b'published_to_marketplace', b'Published to marketplace'), (b'completed', b'Review Completed'), (b'Rejected', b'Rejected'), (b'removed', b'Removed'), (b'review', b'In review'), (b'completed', b'Review Completed'), (b'published', b'Published')]),
        ),
    ]
