# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0010_video_is_contest_entry'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='thumbnail',
            field=models.ImageField(null=True, upload_to=b'videos/thumbnails', blank=True),
        ),
        migrations.AddField(
            model_name='video',
            name='water_marked_video',
            field=models.FileField(null=True, upload_to=b'videos/watermarked', blank=True),
        ),
    ]
