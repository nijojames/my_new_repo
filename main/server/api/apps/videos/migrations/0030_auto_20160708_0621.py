# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0029_auto_20160703_1014'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='video',
            name='frame_rate',
        ),
        migrations.RemoveField(
            model_name='video',
            name='is_contest_entry',
        ),
        migrations.RemoveField(
            model_name='video',
            name='is_published',
        ),
        migrations.RemoveField(
            model_name='video',
            name='model_released',
        ),
        migrations.RemoveField(
            model_name='video',
            name='property_released',
        ),
        migrations.RemoveField(
            model_name='video',
            name='sent_for_review',
        ),
        migrations.RemoveField(
            model_name='video',
            name='state',
        ),
        migrations.AlterField(
            model_name='video',
            name='status',
            field=models.CharField(default=b'review_for_market_place', max_length=50, choices=[(b'review_for_contest', b'Review for contest'), (b'review_for_market_place', b'Review for marketplace'), (b'published_to_contest', b'Published to contest'), (b'published_to_marketplace', b'Published to marketplace'), (b'completed', b'Review Completed'), (b'rejected', b'Rejected'), (b'removed', b'Removed')]),
        ),
    ]
