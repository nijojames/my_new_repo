from urlparse import urlparse
import re

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework.viewsets import ModelViewSet
from rest_framework import serializers

from serializers import VideoSerializer
from serializers import RatingSerializer
from serializers import VideoFacetSerializer
from serializers import VideoModelSerializer
from serializers import ModelReleaseSerializer
from serializers import PropertyReleaseSerializer
from serializers import VideoSearchSerializer

from drf_haystack.viewsets import HaystackViewSet
from drf_haystack.filters import HaystackFacetFilter


from haystack.query import SearchQuerySet
from haystack.forms import SearchForm

# from django.http import Http404

from models import Video
from models import Rating
from models import Model
# from models import ModelRelease
from models import REVIEW_FOR_MARKETPLACE
from models import REVIEW_FOR_CONTEST
from models import PUBLISHED_TO_CONTEST
from models import PUBLISHED_TO_MARKETPLACE
# from permissions import VideoPermissions

from api.apps.utils.mixins import VideoSearchMixin

# from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope

'''
import logging
logger = logging.getLogger(__name__)

class VideoList (APIView):
    # permission_classes = [permissions.IsAuthenticatedOrReadOnly,
    #                       TokenHasReadWriteScope]
    permission_classes = [permissions.AllowAny, ]

    """
    List all videos, or create a new video
    """

    def get(self, request, format=None):
        videos = Video.objects.all()
        serializer = VideoSerializer(videos, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = VideoSerializer(data=request.data)
        logger.debug(serializer)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        logger.error(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        video = self.get_object(pk)
        video.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class VideoDetail(APIView):

    """
    Retrieve, update or delete a video instance.
    """
    def get_object(self, pk):
        try:
            return Video.objects.get(pk=pk)
        except Video.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        video = self.get_object(pk)
        serializer = VideoSerializer(video)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        video = self.get_object(pk)
        serializer = VideoSerializer(video, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        video = self.get_object(pk)
        video.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

'''


class VideoSearch(HaystackViewSet, VideoSearchMixin):

    # `index_models` is an optional list of which models you would
    # like to include in the search result. You might have several models
    # indexed, and this provides a way to filter out those of no interest
    # for this particular view.
    # (Translates to `SearchQuerySet().models(*index_models)` behind the scenes
    index_models = [Video]

    serializer_class = VideoSearchSerializer
    paginate_by_param = 'page_size'
    # filter_backends = [HaystackHighlightFilter, HaystackAutocompleteFilter]

    # This will be used to filter and serialize faceted results
    facet_serializer_class = VideoFacetSerializer
    # This is the default facet filter, and can be left out
    facet_filter_backends = [HaystackFacetFilter]

    def get_queryset(self):
        """
        :param args:
        :param kwargs:
        :return: Search Results based on query 'q'
        """
        request = self.request
        q = request.GET.get('q', "")

        orientation = request.GET.get('orientation',  "")
        orientation = self._get_orientation(orientation)

        resolution = request.GET.get('resolution',  "")
        resolution = self._get_resolution(resolution)

        video_type = request.GET.get('video_type',  "")
        video_type = self._get_video_type(video_type)

        levels = request.GET.get('levels', None)
        levels = self._get_levels(levels)

        price_range = request.GET.get('price_range', None)
        price_range = self._get_price_range(price_range)

        permission = request.GET.get('permissions', None)

        rating = request.GET.get('rating', None)

        published = request.GET.get('age', None)

        # ToDo: filter by location
        location = request.GET.get('location', None)

        query = re.sub(' +', ' ', re.sub('[^a-zA-Z0-9\n\.]', ' ', q))
        sqs = SearchQuerySet()

        if query:
            form = SearchForm(request.query_params)
            search_query = urlparse(request.build_absolute_uri())
            if form.is_valid():
                sqs = form.search()

        # list only videos published to marketplace
        sqs = sqs.filter(status=PUBLISHED_TO_MARKETPLACE)
        # Excluding spammed videos from search.
        sqs = sqs.filter(is_spam='false')

        if price_range:
            # sqs = sqs.filter(award_amount__range=price_range)
            sqs = sqs.filter(price__gte=price_range[0])
            sqs = sqs.filter(price__lte=price_range[1])

        if orientation:
            sqs = sqs.filter(orientation__in=orientation)

        if video_type:
            sqs = sqs.filter(video_type__in=video_type)

        if resolution:
            sqs = sqs.filter(resolution__in=resolution)

        if levels:
            sqs = sqs.filter(level__in=levels)

        if permission:
            sqs = sqs.filter(permission=permission)

        # if rating:
        #     videos = Rating.objects.filter(value=rating).values_list('video', flat=True)
        #     sqs = sqs.filter(id__in=videos)

        if published:
            if published == 'new':
                sqs = sqs.order_by('-published')
            if published == 'old':
                sqs = sqs.order_by('published')

        sqs = sqs.models(Video)
        return sqs


class VideoTitle(APIView):

    """
    List all video's title and tags
    """

    permission_classes = [permissions.AllowAny, ]

    def get(self, request):
        # ToDo: optimize this API. querying the db
        #       every single time will become too slow
        #       as the # of videos, tags increase
        videos = Video.objects.filter(status=PUBLISHED_TO_MARKETPLACE)
        tags = Video.tags \
                    .filter(video__status=PUBLISHED_TO_MARKETPLACE) \
                    .distinct()
        # ToDo: return queryset instead of list ?
        result = set()
        for title in videos:
            result.add(title.title)
        for tag in tags:
            result.add(tag.name)

        return Response(result)

'''

from rest_framework.mixins import ListModelMixin
from drf_haystack.generics import HaystackGenericAPIView
from serializers import VideoSearchSerializer

class VideoSearch(ListModelMixin, HaystackGenericAPIView):

    serializer_class = VideoSearchSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

'''

from api.apps.contests.models import Contest, ContestEntry
from django.shortcuts import get_object_or_404
from api.apps.reputations.level_privileges import LevelPrivilegeFilter
from api.apps.general.models import ReviewSystemConfig
from api.apps.payments.models import Payee, OWNER
from api.apps.reviews.models import VideoToReview

from api.apps.contests.tasks import check_contest_expiry_date


class VideoView(ModelViewSet):

    permission_classes = [permissions.IsAuthenticatedOrReadOnly, ]
    serializer_class = VideoSerializer
    model = Video
    queryset = Video.objects.all()
    lookup_field = 'slug'
    http_method_names = ('get', 'post')

    def create(self, request, *args, **kwargs):
        check_contest_expiry_date.delay()
        review_settings = ReviewSystemConfig.get_solo()
        self.max_num_of_reviews_required = review_settings.max_num_of_reviews_required
        self.num_of_positive_reviews_required = review_settings.num_of_positive_reviews_required

        if hasattr(request.data, '_mutable'):
            request.data._mutable = True

        contest_pk = request.data.get('contest', None)
        request.data['user'] = request.user.id
        # default is upload to market place
        request.data['status'] = REVIEW_FOR_MARKETPLACE
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Level check for uploading video for the day
        price = request.data.get('price', None)
        level_filter = LevelPrivilegeFilter(request.user)
        commission = level_filter.get_commission_config_value_based_on_user_level()
        can_upload = level_filter.if_user_can_upload_video(price=price)
        if isinstance(can_upload, dict):
            raise serializers.ValidationError(can_upload)

        video = serializer.save()

        # create Payee entry for video owner now
        # as the owner level may change later
        payee_obj = Payee()
        payee_obj.type = OWNER
        payee_obj.user = video.user
        payee_obj.video_id = video.id
        payee_obj.commission = commission
        payee_obj.max_num_of_times_to_be_payed = 0x7FFFFFFF
        payee_obj.save()

        headers = self.get_success_headers(serializer.data)
        video_id = serializer.data['id']

        # validate model and property releases
        mrd = 'model_release_details'
        # ToDo: why check if mrd is a required field ?
        if mrd in request.data:
            for model_data in request.data[mrd]:
                model_data.update({'video': video_id})
                model_serializer = ModelReleaseSerializer(data=model_data)
                if not model_serializer.is_valid():
                    return Response({mrd: model_serializer.errors},
                                    status=status.HTTP_400_BAD_REQUEST,
                                    headers=headers)
                model_serializer.save()

        prd = 'property_release_details'
        # ToDo: why check if prd is a required field ?
        if prd in request.data:
            for property_data in request.data[prd]:
                property_data.update({'video': video_id,
                                      'videographer': request.user.id})
                property_serializer = PropertyReleaseSerializer(data=property_data)
                if not property_serializer.is_valid():
                    return Response({prd: property_serializer.errors},
                                    status=status.HTTP_400_BAD_REQUEST,
                                    headers=headers)
                property_serializer.save()

        # handle videos submitted to contests
        if contest_pk:
            try:
                contest = get_object_or_404(Contest, pk=int(contest_pk))
                obj = ContestEntry()
                obj.contest = contest
                obj.video = video
                obj.save()

                if contest.accept_curated_videos:
                    video.status = REVIEW_FOR_CONTEST

                    # Put the video for review
                    video_to_review_obj = VideoToReview()
                    video_to_review_obj.video = video
                    video_to_review_obj.user_level = level_filter.user_level
                    video_to_review_obj.reviews_max = self.max_num_of_reviews_required
                    video_to_review_obj.reviews_positive_required = self.num_of_positive_reviews_required
                    video_to_review_obj.status = REVIEW_FOR_CONTEST
                    video_to_review_obj.save()
                else:
                    video.status = PUBLISHED_TO_CONTEST
                video.save()
            except Exception as e:
                print "ERROR: videos.views.py e=" + str(e)
        else:  # marketplace video needs to go to review first
            video_to_review_obj = VideoToReview()
            video_to_review_obj.user_level = level_filter.user_level
            video_to_review_obj.video = video
            video_to_review_obj.reviews_max = self.max_num_of_reviews_required
            video_to_review_obj.reviews_positive_required = self.num_of_positive_reviews_required
            video_to_review_obj.status = REVIEW_FOR_MARKETPLACE
            video_to_review_obj.save()

        return Response(serializer.data,
                        status=status.HTTP_201_CREATED,
                        headers=headers)


# ToDo: unused class
class RatingViewSet(ModelViewSet):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, ]
    serializer_class = RatingSerializer
    model = Rating
    queryset = Rating.objects.all()

    def create(self, request, *args, **kwargs):
        if hasattr(request.data, '_mutable'):
            request.data._mutable = True
        request.data['user'] = request.user.id
        return super(RatingViewSet, self).create(request, *args, **kwargs)


class ModelContactViewSet(ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = VideoModelSerializer
    model = Model
    http_method_names = ('get', 'post')

    def get_queryset(self):
        return self.model.objects.filter(videographer=self.request.user)

    def create(self, request, *args, **kwargs):
        if hasattr(request.data, '_mutable'):
            request.data._mutable = True
        request.data['videographer'] = request.user.id
        return super(ModelContactViewSet, self).create(request,
                                                       *args,
                                                       **kwargs)
