from decimal import Decimal

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.db.models import Q
from django.utils import timezone

from taggit.managers import TaggableManager
from follow.utils import follow
from notifications.signals import notify
from actstream import action
from autoslug import AutoSlugField
from geoposition.fields import GeopositionField
from dirtyfields import DirtyFieldsMixin

from api.apps.activities.follow_utiils import register, get_followers_except_me
from api.apps.profiles.models import Profile
from services import VideoReputationServices
from api.apps.utils import choices


MAX_TITLE_LENGTH = 256

#
# ToDo: remove these
#
# REVIEW = 'review'
# REVIEW_AND_CONTEST = 'review_and_contest'
# CONTEST = 'contest'
# CONTEST_AND_MARKET = 'contest_and_market'

# MARKET = 'market'
# PUBLISHED = 'published'

# VIDEO_STATES = (
#     (REVIEW, _('In review')),
#     (REVIEW_AND_CONTEST, _('In both review and contest')),
#     (CONTEST, _('In contest')),
#     (CONTEST_AND_MARKET, _('In both contest and market place')),
#     (MARKET, _('In marketplace'))
# )
######################


COMPLETED = 'completed'
REJECTED = 'rejected'
APPROVED_FOR_CONTEST = 'approved_for_contest'
APPROVED_FOR_MARKETPLACE = 'approved_for_marketplace'
REVIEW_FOR_CONTEST = 'review_for_contest'
REVIEW_FOR_MARKETPLACE = 'review_for_marketplace'
PUBLISHED_TO_CONTEST = 'published_to_contest'
PUBLISHED_TO_MARKETPLACE = 'published_to_marketplace'
REMOVED = 'removed'

STATUS = (
    (REVIEW_FOR_CONTEST, 'Review for contest'),
    (REVIEW_FOR_MARKETPLACE, 'Review for marketplace'),
    (PUBLISHED_TO_CONTEST, 'Published to contest'),
    (PUBLISHED_TO_MARKETPLACE, 'Published to marketplace'),
    (COMPLETED, 'Review Completed'),  # internal state only
    (REJECTED, 'Rejected'),
    (REMOVED, 'Removed'),
)


class Video(DirtyFieldsMixin, models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    title = models.CharField(_('title'),
                             max_length=MAX_TITLE_LENGTH,
                             unique=False)
    tags = TaggableManager(_('tags'),
                           help_text="separate with commas",
                           blank=True)
    slug = AutoSlugField(populate_from='title', unique=True, editable=True)
    #
    # ToDo: unify the *video and *video_link
    #
    video = models.FileField(upload_to='videos', null=True, blank=True)
    video_link = models.URLField(null=True, blank=True)
    water_marked_video = models.FileField(upload_to='videos/watermarked',
                                          null=True,
                                          blank=True)
    water_marked_video_link = models.URLField(null=True, blank=True)
    thumbnail = models.ImageField(upload_to='videos/thumbnails',
                                  null=True,
                                  blank=True)

    thumbnail_link = models.URLField(null=True, blank=True)
    # date when video is published for sale
    published = models.DateTimeField(auto_now_add=False,
                                     auto_now=False,
                                     null=True,
                                     blank=True)
    # category = models.ForeignKey ("Category", default=1)
    created = models.DateTimeField(auto_now_add=True,
                                   auto_now=False,
                                   null=True)
    updated = models.DateTimeField(auto_now_add=False,
                                   auto_now=True,
                                   null=True)
    # in USD
    price = models.DecimalField(max_digits=6,
                                decimal_places=2,
                                default=Decimal('10.00'))

    permission = models.CharField(choices=choices.PERMISSION_TYPE,
                                  max_length=20,
                                  default=choices.COMMERCIAL)
    orientation = models.CharField(choices=choices.ORIENTATION_TYPE,
                                   max_length=20,
                                   null=True,
                                   blank=True)
    video_type = models.CharField(choices=choices.VIDEO_TYPE,
                                  max_length=30,
                                  null=True,
                                  blank=True)
    fps_rate = models.CharField(choices=choices.FPS_CHOICES,
                                max_length=10,
                                null=True,
                                blank=True)
    resolution = models.CharField(choices=choices.RESOLUTION_CHOICES,
                                  max_length=10,
                                  null=True,
                                  blank=True)
    location = GeopositionField(null=True, blank=True)
    location_name = models.CharField(max_length=255, null=True, blank=True)

    duration = models.IntegerField(default=10, help_text='Duration in seconds')
    is_spam = models.BooleanField(default=False)

    status = models.CharField(choices=STATUS,
                              max_length=50,
                              default=REVIEW_FOR_MARKETPLACE)

    #
    # ToDo: fields to be removed
    #
    # frame_rate = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    # state = models.CharField(choices=VIDEO_STATES, max_length=50, default=REVIEW)
    # is_contest_entry = models.BooleanField(default=False) # TODO remove this
    # is_published = models.BooleanField(default=False) # TODO remove this
    # sent_for_review = models.BooleanField(default=False)
    # model_released = models.BooleanField(default=False)
    # property_released = models.BooleanField(default=False)

    class Meta:
        verbose_name = _("video")
        verbose_name_plural = _("videos")

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.__unicode__()

    def save(self, *args, **kwargs):
        dirty_fields = self.get_dirty_fields()
        if 'status' in dirty_fields:
            if self.status == PUBLISHED_TO_MARKETPLACE or \
               self.status == PUBLISHED_TO_CONTEST:
                self.published = timezone.now()

        super(Video, self).save(*args, **kwargs)


#
# Unused class
#
class Rating(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    value = models.DecimalField(_("Service rating"),
                                default=0,
                                max_digits=2,
                                decimal_places=1)
    video = models.ForeignKey(Video)

    class Meta:
        verbose_name = _("Rating")
        verbose_name_plural = _("Ratings")
        unique_together = ['user', 'video']

    def __unicode__(self):
        return self.user.username + ' Rated ' + str(self.value) + " on " + self.video.title[:8]


@receiver(post_save, sender=Video, dispatch_uid="post_save_video")
def activity_signal(sender, instance, created, **kwargs):
    if created:
        follow(instance.user, instance)
        recipients = get_followers_except_me(instance.user.user_profile)
        for recipient in recipients:
            '''
            notify.send(instance.user,
                        recipient=recipient,
                        verb='uploaded a new video',
                        action_object=instance)
            '''
            action.send(instance.user,
                        verb='uploaded a new video',
                        action_object=instance,
                        target=instance.user)

register(Video)

reputation_services = VideoReputationServices()

post_save.connect(reputation_services._post_save_signal_callback,
                  sender=Video,
                  weak=False)


def get_videographers():
    user_ids = Profile.objects.filter(type='videographer').values_list('user__is')
    return User.objects.filter(user_profile__type='videographer')


class Model(models.Model):
    videographer = models.ForeignKey(settings.AUTH_USER_MODEL,
                                     limit_choices_to=(Q(user_profile__type='both') |
                                                       Q(user_profile__type='videographer')))
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address1 = models.CharField(max_length=255)
    address2 = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    zipcode = models.CharField(max_length=10)
    email = models.EmailField()
    dob = models.DateField(help_text='Provide parent details if model is minor')
    profile_picture = models.URLField(null=True, blank=True)
    phone = models.CharField(max_length=255, null=True, blank=True)
    parent_first_name = models.CharField(max_length=100, null=True, blank=True)
    parent_last_name = models.CharField(max_length=100, null=True, blank=True)
    parent_email = models.EmailField(null=True, blank=True)
    parent_dob = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.first_name + self.last_name


class ModelRelease(models.Model):
    model = models.ForeignKey(Model)
    video = models.ForeignKey(Video, related_name='get_model_releases')
    document = models.URLField(help_text='Provide url to amazon bucket')

    def __str__(self):
        return 'Consent for ' + self.video.title


class PropertyRelease(models.Model):
    videographer = models.ForeignKey(settings.AUTH_USER_MODEL,
                                     limit_choices_to=(Q(user_profile__type='both') |
                                                       Q(user_profile__type='videographer')))

    name = models.CharField(max_length=255)
    address1 = models.CharField(max_length=255)
    address2 = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    zipcode = models.CharField(max_length=10)
    property_picture = models.URLField(null=True, blank=True)
    owner_first_name = models.CharField(max_length=100)
    owner_last_name = models.CharField(max_length=100)
    owner_email = models.EmailField()
    owner_dob = models.DateField()
    video = models.ForeignKey(Video, related_name='get_property_releases')
    document = models.URLField(help_text='Provide url to amazon bucket')

    def __str__(self):
        return self.name
