from django import forms

from .models import Model, Video
from api.apps.utils.utils import calculate_age


class VideoModelForm(forms.ModelForm):

    class Meta:
        model = Model
        fields = '__all__'

    def clean(self):
        form_data = self.cleaned_data
        dob = form_data.get('dob')
        parent_name = form_data.get('parent_name', None)
        parent_email = form_data.get('parent_email', None)
        parent_dob = form_data.get('parent_dob', None)
        model_age = calculate_age(dob)
        if not(parent_email and parent_dob and parent_name):
            if model_age < 18:
                raise forms.ValidationError('Provide parent details as model is minor')
        return form_data

    def clean_parent_dob(self):
        parent_dob = self.cleaned_data.get('parent_dob', None)
        if parent_dob and calculate_age(parent_dob) < 18:
            raise forms.ValidationError('Parent cannnot be a minor')
        else:
            return parent_dob
