from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from models import Video, Rating, ModelRelease, Model, PropertyRelease
from forms import VideoModelForm


class VideoModelAdmin(ImportExportModelAdmin):
    form = VideoModelForm


class ModelReleaseAdmin(admin.TabularInline):
    model = ModelRelease
    extra = 1
    show_change_link = True


class PropertyReleaseAdmin(admin.StackedInline):
    model = PropertyRelease
    extra = 0
    show_change_link = True


class RatingAdmin(ImportExportModelAdmin):
    pass


# class VideoModelAdmin(admin.ModelAdmin):


class VideoAdmin(ImportExportModelAdmin):
    list_display = ['title', 'status', 'user']
    search_fields = ['title', ]
    inlines = [ModelReleaseAdmin, PropertyReleaseAdmin]

admin.site.register(Video, VideoAdmin)
admin.site.register(Rating, RatingAdmin)
admin.site.register(Model, VideoModelAdmin)
