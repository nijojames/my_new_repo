from rest_framework import permissions


class VideoPermissions(permissions.BasePermission):
    """
    Permission for user edit only his own profile.
    """

    def has_object_permission(self, request, view, obj):
        has_permission = True
        if request.method in ['POST', 'PUT', 'DELETE', 'PATCH']:
            if request.user.username != obj.username:
                has_permission = False
        if not request.user._is_authenticated():
            has_permission = False

        return has_permission

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        has_permission = True
        return has_permission
