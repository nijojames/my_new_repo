from django.contrib.contenttypes.models import ContentType
from django.db.models import Avg

from rest_framework import serializers

from taggit_serializer.serializers import (TagListSerializerField,
                                           TaggitSerializer)
from models import Video, Rating, Model, ModelRelease, PropertyRelease
from api.apps.reputations.models import Reputation
from api.apps.profiles.serializers import UserProfileSerializer
from api.apps.carts.carts import MyCart
from api.apps.utils.utils import calculate_age
from api.apps.carts.models import Cart

from voting.models import Vote
from follow.models import Follow
from flaggit.models import Flag


from search_indexes import VideoIndex

from follow.models import Follow


class VideoModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Model

    def validate(self, attrs):
        dob = attrs.get('dob')
        parent_name = attrs.get('parent_name', None)
        parent_email = attrs.get('parent_email', None)
        parent_dob = attrs.get('parent_dob', None)
        model_age = calculate_age(dob)
        if not (parent_email and parent_dob and parent_name):
            if model_age < 18:
                raise serializers.ValidationError('Provide parent details as model is minor')
        if parent_dob and calculate_age(parent_dob) < 18:
            raise serializers.ValidationError('Parent cannnot be a minor')
        return attrs


class ModelReleaseSerializer(serializers.ModelSerializer):

    model_details = VideoModelSerializer(source='model', read_only=True)

    class Meta:
        model = ModelRelease
        fields = ('id', 'model', 'video', 'document', 'model_details')

    def get_model_details(self, instance):
        return VideoModelSerializer(instance.model).data


class PropertyReleaseSerializer(serializers.ModelSerializer):

    class Meta:
        model = PropertyRelease


class VideoSerializer(TaggitSerializer, serializers.ModelSerializer):
    video_link = serializers.URLField(write_only=True, required=False)
    user_detail = UserProfileSerializer(source='user', read_only=True)
    tags = TagListSerializerField()
    level = serializers.SerializerMethodField()
    is_following = serializers.SerializerMethodField(read_only=True)
    followers_count = serializers.SerializerMethodField(read_only=True)
    is_liked = serializers.SerializerMethodField(read_only=True)
    likes_count = serializers.SerializerMethodField(read_only=True)
    is_spammed = serializers.SerializerMethodField(read_only=True)
    rating = serializers.SerializerMethodField(read_only=True)
    rating_id = serializers.SerializerMethodField(read_only=True)
    average_rating = serializers.SerializerMethodField(read_only=True)
    model_release_details = serializers.SerializerMethodField(read_only=True)
    property_release_details = serializers.SerializerMethodField(read_only=True)
    slug = serializers.CharField(read_only=True, required=False)

    class Meta:
        model = Video
        fields = ('user', 'user_detail', 'tags', 'id',
                  'title', 'slug', 'water_marked_video',
                  'water_marked_video_link', 'video_link',
                  'thumbnail',
                  'published', 'created', 'updated',
                  'price', 'permission', 'orientation', 'resolution', 'fps_rate',
                  'duration', 'video_type', 'level', 'is_following', 'location',
                  'location_name', 'is_following', 'followers_count', 'is_liked',
                  'likes_count', 'is_spammed', 'rating', 'rating_id',
                  'average_rating', 'model_release_details',
                  'property_release_details', 'thumbnail_link')

    def get_level(self, obj):
        user = obj.user
        reputation = Reputation.objects.filter(user=user)
        if reputation:
            return reputation[0].level
        return ''

    def get_is_following(self, obj):
        user = self.context['request'].user
        if user.is_authenticated():
            follow = Follow.objects.filter(target_video=obj,
                                           user=user).exists()
            return follow
        return False

    def get_is_liked(self, obj):
        user = self.context['request'].user
        if user.is_authenticated():
            content_type = ContentType.objects.get_for_model(obj)
            like = Vote.objects.filter(content_type=content_type,
                                       object_id=obj.id,
                                       user=user).exists()
            return like
        return False

    def get_followers_count(self, obj):
        follow = Follow.objects.filter(target_video=obj).count()
        return follow

    def get_likes_count(self, obj):
        content_type = ContentType.objects.get_for_model(obj)
        follow = Vote.objects.filter(content_type=content_type,
                                     object_id=obj.id).count()
        return follow

    def get_is_spammed(self, obj):
        user = self.context['request'].user
        if user.is_authenticated():
            content_type = ContentType.objects.get_for_model(obj)
            spam = Flag.objects.filter(content_type=content_type,
                                       object_id=obj.id,
                                       reviewer=user).exists()
            return spam
        return False

    def get_rating(self, obj):
        user = self.context['request'].user
        if user.is_authenticated():
            try:
                rating = Rating.objects.get(video=obj, user=user)
                return rating.value
            except Rating.DoesNotExist:
                return False
        return False

    def get_rating_id(self, obj):
        user = self.context['request'].user
        if user.is_authenticated():
            try:
                rating = Rating.objects.get(video=obj, user=user)
                return rating.id
            except Rating.DoesNotExist:
                pass
        return None

    def get_average_rating(self, obj):
        rating = Rating.objects.filter(video=obj)
        avg = rating.aggregate(Avg('value')).get('value__avg', 0.00)
        return avg, rating.count()

    def get_model_release_details(self, obj):
        return ModelReleaseSerializer(obj.get_model_releases.all(),
                                      many=True).data

    def get_property_release_details(self, obj):
        return PropertyReleaseSerializer(obj.get_property_releases.all(),
                                         many=True).data


# ToDo: unused class
class RatingSerializer(serializers.ModelSerializer):
    value = serializers.CharField(required=True)

    class Meta:
        model = Rating
        # fields = ('user', 'value', 'id', 'video', )

    def validate_value(self, attrs):
        """
        Custom validations are given for value content
        :param attrs:
        :return:
        """
        if float(attrs) > 5:
            raise serializers.ValidationError('Rating should be less than or equal to 5')

        return attrs


# from drf_haystack.serializers import HaystackSerializer
# from search_indexes import VideoIndex

'''
class VideoSearchSerializer(HaystackSerializer):
    more_like_this = serializers.HyperlinkedIdentityField(view_name="video-search-more-like-this",
                                                           read_only=True)

    class Meta:
        # The `index_classes` attribute is a list of which search indexes
        # we want to include in the search.
        index_classes = [VideoIndex]

        # The `fields` contains all the fields we want to include.
        # NOTE: Make sure you don't confuse these with model attributes. These
        # fields belong to the search index!
        fields = [
            "title", "updated", "text", "tags",
        ]
'''

from drf_haystack.serializers import HaystackSerializerMixin


class VideoSearchSerializer(HaystackSerializerMixin, VideoSerializer):
    # ToDo: make more-like-this work
    '''
    more_like_this = serializers.HyperlinkedIdentityField(view_name="video-search-more-like-this",
                                                          read_only=True)
    '''
    class Meta(VideoSerializer.Meta):
        search_fields = ("text",
                         "title",
                         "tags",
                         "price",
                         'permission',
                         'orientation',
                         'video_type',
                         'resolution',
                         'id',
                         'created',
                         'published',
                         'location',
                         'location_name',
                         'status')


from drf_haystack.serializers import HaystackFacetSerializer


class VideoFacetSerializer(HaystackFacetSerializer):
    serialize_objects = True  # Setting this to True will serialize the
                               # queryset into an `objects` list. This
                               # is useful if you need to display the faceted
                               # results. Defaults to False.

    class Meta:
        index_classes = [VideoIndex]
        fields = ["title", "price", 'tags']

        # field_options = {
        #    "title": {'limit':10},
        #    "price": {'limit':10},
        # }


class VideoBriefSerializer(serializers.ModelSerializer):

    owner_details = serializers.SerializerMethodField()

    class Meta:
        model = Video
        fields = ('id',
                  'title',
                  'slug',
                  'water_marked_video_link',
                  'thumbnail',
                  'price',
                  'permission',
                  'video_type',
                  'resolution',
                  'orientation',
                  'duration',
                  'owner_details')

    def get_owner_details(self, video_obj):
        return {'name': video_obj.user.get_full_name(), 'username': video_obj.user.username}
