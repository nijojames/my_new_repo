from haystack import indexes

from django.utils import timezone

from models import Video
from api.apps.reputations.models import Reputation


class VideoIndex (indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    updated = indexes.DateTimeField(model_attr='updated')
    tags = indexes.MultiValueField()
    #content_auto = indexes.EdgeNgramField ()
    price = indexes.FloatField(model_attr='price')
    permission = indexes.CharField(model_attr='permission')
    video_type = indexes.CharField(model_attr='video_type', null=True)
    resolution = indexes.CharField(model_attr='resolution', null=True)
    orientation = indexes.CharField(model_attr='orientation', null=True)
    location = indexes.CharField(model_attr='location_name', null=True)
    level = indexes.CharField()
    is_spam = indexes.BooleanField(model_attr='is_spam')
    published = indexes.DateTimeField(model_attr='published', null=True)
    status = indexes.CharField(model_attr='status')

    def get_model(self):
        return Video

    def index_queryset(self, using=None):
        """Used when the entire index for the recipe model is updated"""
        return self.get_model().objects.filter(updated__lte=timezone.now())

    def prepare_tags(self, obj):
        return [tag.name for tag in obj.tags.all()]

    def prepare_level(self, obj):
        user = obj.user
        reputation = Reputation.objects.filter(user=user)
        if reputation:
            return reputation[0].level_count

    '''
    def prepare_content_auto (self, obj):
        return [tag.name for tag in obj.tags.all()]
    '''
