from rest_framework import routers

from django.conf.urls import patterns, include, url

# from views import VideoList
# from views import VideoDetail
from views import VideoSearch
from views import VideoView
from views import RatingViewSet
from views import VideoTitle
from views import ModelContactViewSet


router = routers.DefaultRouter()
router.register(r'video', VideoView, base_name='video')
router.register(r'rating', RatingViewSet, base_name='rating')
router.register(r'search',
                viewset=VideoSearch,
                base_name="video-search")
router.register(r'model-contact-list',
                viewset=ModelContactViewSet,
                base_name="model-contact-list")
# router.register(r'title', viewset=VideoTitle, base_name="video-title")

urlpatterns = [
    # url (r'^$', VideoList.as_view (), name='video-list'),
    # url (r'^(?P<pk>[0-9]+)/$', VideoDetail.as_view (), name='video-detail'),
    url('', include(router.urls)),
    url(r'^title', VideoTitle.as_view(), name='video-title')
]


'''
from haystack.query import SearchQuerySet
from haystack.views import FacetedSearchView
from haystack.forms import FacetedSearchForm

sqs = SearchQuerySet().facet('price')
urlpatterns += patterns ('haystack.views',
                         url (r'^$',
                              FacetedSearchView (form_class=FacetedSearchForm,
                                                 searchqueryset=sqs),
                              name='haystack_search'))

'''
