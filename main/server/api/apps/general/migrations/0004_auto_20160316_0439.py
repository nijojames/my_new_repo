# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0003_reviewsystemconfig'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewsystemconfig',
            name='min_rating_for_positive_review',
            field=models.IntegerField(default=6, help_text=b'A minimum rating required for a positive review'),
        ),
        migrations.AddField(
            model_name='reviewsystemconfig',
            name='overall_positive_reviews_required',
            field=models.IntegerField(default=6, help_text=b'Overall positive r'),
        ),
        migrations.AlterField(
            model_name='reviewsystemconfig',
            name='videos_batches_count',
            field=models.IntegerField(default=10, help_text=b'This defines the number of videos to be send to s videographer or number of videographers a videowill be send to'),
        ),
    ]
