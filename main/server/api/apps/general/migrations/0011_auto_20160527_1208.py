# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0010_auto_20160527_1111'),
    ]

    operations = [
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_five_spam_weight',
            field=models.IntegerField(default=5),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_four_spam_weight',
            field=models.IntegerField(default=4),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_one_spam_weight',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_three_spam_weight',
            field=models.IntegerField(default=3),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_two_spam_weight',
            field=models.IntegerField(default=2),
        ),
    ]
