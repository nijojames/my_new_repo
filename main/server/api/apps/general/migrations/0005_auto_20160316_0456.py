# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0004_auto_20160316_0439'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reviewsystemconfig',
            name='min_rating_for_positive_review',
            field=models.IntegerField(default=3, help_text=b'A minimum rating required for a positive review'),
        ),
        migrations.AlterField(
            model_name='reviewsystemconfig',
            name='overall_positive_reviews_required',
            field=models.IntegerField(default=6, help_text=b'Overall positive rating'),
        ),
    ]
