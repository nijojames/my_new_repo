# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0014_contestconfig'),
    ]

    operations = [
        migrations.CreateModel(
            name='VideoMiscConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('space_for_copy', models.BooleanField(default=False)),
                ('min_duration_sec', models.IntegerField(default=10)),
                ('max_duration_sec', models.IntegerField(default=30)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='VideoOrientation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('orientation', models.CharField(unique=True, max_length=20, choices=[(b'any', 'Any'), (b'portrait', 'Portrait'), (b'landscape', 'Landscape'), (b'square', 'Square')])),
                ('is_default', models.BooleanField(default=False)),
                ('supported', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='VideoTypeConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('video_type', models.CharField(max_length=30, choices=[(b'real_time', 'Real Time'), (b'time_lapse', 'Time Lapse'), (b'slow_motion', 'Slow Motion')])),
                ('fps_rate', models.CharField(max_length=10, choices=[(b'30', '30fps'), (b'60', '60fps'), (b'120', '120fps'), (b'240', '240fps')])),
                ('resolution', models.CharField(max_length=10, choices=[(b'720', '720p'), (b'1080', '1080p'), (b'4096', '4k')])),
                ('is_default', models.BooleanField(default=False)),
                ('supported', models.BooleanField(default=True)),
            ],
        ),
    ]
