# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0009_levelprivilegeconfig'),
    ]

    operations = [
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_five_commission',
            field=models.IntegerField(default=60),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_five_review_count_per_day',
            field=models.IntegerField(default=50),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_five_review_revenue_sharing',
            field=models.IntegerField(default=60),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_four_commission',
            field=models.IntegerField(default=70),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_four_review_count_per_day',
            field=models.IntegerField(default=40),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_four_review_revenue_sharing',
            field=models.IntegerField(default=70),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_one_commission',
            field=models.IntegerField(default=100),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_one_review_count_per_day',
            field=models.IntegerField(default=10),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_one_review_revenue_sharing',
            field=models.IntegerField(default=100),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_three_commission',
            field=models.IntegerField(default=80),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_three_review_count_per_day',
            field=models.IntegerField(default=30),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_three_review_revenue_sharing',
            field=models.IntegerField(default=80),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_two_commission',
            field=models.IntegerField(default=90),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_two_review_count_per_day',
            field=models.IntegerField(default=20),
        ),
        migrations.AddField(
            model_name='levelprivilegeconfig',
            name='level_two_review_revenue_sharing',
            field=models.IntegerField(default=90),
        ),
    ]
