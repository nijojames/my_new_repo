# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0015_videomiscconfig_videoorientation_videotypeconfig'),
    ]

    operations = [
        migrations.CreateModel(
            name='VideoEnvironment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('environment', models.CharField(max_length=30, choices=[(b'any', 'Any'), (b'indoor', 'Indoor'), (b'outdoor', 'Outdoor')])),
                ('is_default', models.BooleanField(default=False)),
                ('supported', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='VideoLighting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lighting', models.CharField(max_length=30, choices=[(b'any', 'Any'), (b'natural', 'Natural'), (b'studio', 'Studio')])),
                ('is_default', models.BooleanField(default=False)),
                ('supported', models.BooleanField(default=True)),
            ],
        ),
        migrations.AddField(
            model_name='videomiscconfig',
            name='accept_curated_videos',
            field=models.BooleanField(default=False),
        ),
    ]
