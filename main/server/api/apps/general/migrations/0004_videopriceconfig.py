# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0003_contactusconfig'),
    ]

    operations = [
        migrations.CreateModel(
            name='VideoPriceConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level_one_min_price', models.DecimalField(default=15.0, max_digits=7, decimal_places=2)),
                ('level_two_min_price', models.DecimalField(default=25.0, max_digits=7, decimal_places=2)),
                ('level_three_min_price', models.DecimalField(default=35.0, max_digits=7, decimal_places=2)),
                ('level_four_min_price', models.DecimalField(default=50.0, max_digits=7, decimal_places=2)),
                ('level_five_min_price', models.DecimalField(default=75.0, max_digits=7, decimal_places=2)),
                ('public_request_price', models.DecimalField(default=20.0, max_digits=7, decimal_places=2)),
                ('invite_price', models.DecimalField(default=20.0, max_digits=7, decimal_places=2)),
                ('location_price', models.DecimalField(default=20.0, max_digits=7, decimal_places=2)),
                ('model_release_price', models.DecimalField(default=20.0, max_digits=7, decimal_places=2)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
