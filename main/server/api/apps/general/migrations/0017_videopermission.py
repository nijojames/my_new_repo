# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0016_auto_20160629_2304'),
    ]

    operations = [
        migrations.CreateModel(
            name='VideoPermission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('permission', models.CharField(max_length=20, choices=[(b'royalty_free', 'Royalty Free'), (b'commercial', 'Commercial')])),
                ('is_default', models.BooleanField(default=False)),
                ('supported', models.BooleanField(default=True)),
            ],
        ),
    ]
