# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0008_auto_20160503_0017'),
    ]

    operations = [
        migrations.CreateModel(
            name='LevelPrivilegeConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level_one_video_count_per_day', models.IntegerField(default=1)),
                ('level_two_video_count_per_day', models.IntegerField(default=1)),
                ('level_three_video_count_per_day', models.IntegerField(default=3)),
                ('level_four_video_count_per_day', models.IntegerField(default=4)),
                ('level_five_video_count_per_day', models.IntegerField(default=5)),
                ('level_one_video_max_price', models.DecimalField(default=20.0, max_digits=10, decimal_places=2)),
                ('level_two_video_max_price', models.DecimalField(default=50.0, max_digits=7, decimal_places=2)),
                ('level_three_video_max_price', models.DecimalField(default=100.0, max_digits=7, decimal_places=2)),
                ('level_four_video_max_price', models.DecimalField(default=200.0, max_digits=7, decimal_places=2)),
                ('level_five_video_max_price', models.DecimalField(default=400.0, max_digits=7, decimal_places=2)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
