# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0005_category'),
        ('general', '0006_reviewsystemconfig_resend_videographers_multiplier'),
    ]

    operations = [
    ]
