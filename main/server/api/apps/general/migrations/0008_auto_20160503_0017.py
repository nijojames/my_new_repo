# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0007_merge'),
    ]

    operations = [
        migrations.RenameField(
            model_name='videopriceconfig',
            old_name='public_request_price',
            new_name='private_request_price',
        ),
    ]
