# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0002_spamhandlingconfig'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactUsConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('contact_email', models.EmailField(max_length=75, null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
