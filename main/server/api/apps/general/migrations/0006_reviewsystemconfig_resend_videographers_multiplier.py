# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0005_auto_20160316_0456'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewsystemconfig',
            name='resend_videographers_multiplier',
            field=models.IntegerField(default=1, help_text=b'R value,  where number R is the of videographers the video will be resend will be N*R'),
        ),
    ]
