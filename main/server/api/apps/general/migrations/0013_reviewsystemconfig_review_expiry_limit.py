# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0012_auto_20160621_1848'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewsystemconfig',
            name='review_expiry_limit',
            field=models.IntegerField(default=1, help_text=b'In seconds'),
        ),
    ]
