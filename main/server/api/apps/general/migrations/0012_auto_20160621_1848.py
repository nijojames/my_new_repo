# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0011_auto_20160527_1208'),
    ]

    operations = [
        migrations.CreateModel(
            name='Recaptcha',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('secret', models.CharField(max_length=256)),
                ('response', models.CharField(max_length=256)),
            ],
            options={
                'verbose_name_plural': 'Recaptcha',
            },
        ),
        migrations.AddField(
            model_name='reviewsystemconfig',
            name='max_num_of_times_to_pay_reviewer',
            field=models.IntegerField(default=1),
        ),
    ]
