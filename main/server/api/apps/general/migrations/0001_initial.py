# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ReputationConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('video_upload_point', models.IntegerField(default=1)),
                ('video_winning_contest_point', models.IntegerField(default=10)),
                ('video_sold_point', models.IntegerField(default=5)),
                ('video_spammed_point', models.IntegerField(default=-10)),
                ('positive_review_winning_contest_point', models.IntegerField(default=10)),
                ('positive_review_video_sold_point', models.IntegerField(default=5)),
                ('positive_review_reached_market_point', models.IntegerField(default=1)),
                ('positive_review_did_not_reached_market_point', models.IntegerField(default=-1)),
                ('negative_review_did_reached_market_point', models.IntegerField(default=-1)),
                ('positive_review_video_spammed', models.IntegerField(default=-10)),
                ('level_two_video_points_required', models.IntegerField(default=50)),
                ('level_two_review_points_required', models.IntegerField(default=50)),
                ('level_three_video_points_required', models.IntegerField(default=200)),
                ('level_three_review_points_required', models.IntegerField(default=200)),
                ('level_four_video_points_required', models.IntegerField(default=350)),
                ('level_four_review_points_required', models.IntegerField(default=400)),
                ('level_five_video_points_required', models.IntegerField(default=800)),
                ('level_five_review_points_required', models.IntegerField(default=1000)),
                ('level_one_name', models.CharField(default=b'Level 1', max_length=100)),
                ('level_two_name', models.CharField(default=b'Level 2', max_length=100)),
                ('level_three_name', models.CharField(default=b'Level 3', max_length=100)),
                ('level_four_name', models.CharField(default=b'Level 4', max_length=100)),
                ('level_five_name', models.CharField(default=b'Level 5', max_length=100)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
