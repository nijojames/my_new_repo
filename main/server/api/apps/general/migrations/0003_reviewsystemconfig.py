# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0002_spamhandlingconfig'),
    ]

    operations = [
        migrations.CreateModel(
            name='ReviewSystemConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('videos_batches_count', models.IntegerField(default=10, help_text=b'This defines the number of videos to be send to s videographer')),
                ('overall_rating_required', models.IntegerField(default=30)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
