# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0017_videopermission'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewsystemconfig',
            name='max_num_of_reviews_required',
            field=models.IntegerField(default=5),
        ),
        migrations.AddField(
            model_name='reviewsystemconfig',
            name='num_of_positive_reviews_required',
            field=models.IntegerField(default=3),
        ),
        migrations.AddField(
            model_name='reviewsystemconfig',
            name='review_expiry_margin',
            field=models.IntegerField(default=3, help_text=b'In seconds'),
        ),
        migrations.AlterField(
            model_name='reviewsystemconfig',
            name='resend_videographers_multiplier',
            field=models.IntegerField(default=1, help_text=b'R value, where number R is the of videographers the video will be resend will be N*R'),
        ),
        migrations.AlterField(
            model_name='reviewsystemconfig',
            name='review_expiry_limit',
            field=models.IntegerField(default=5, help_text=b'In seconds'),
        ),
        migrations.AlterField(
            model_name='reviewsystemconfig',
            name='videos_batches_count',
            field=models.IntegerField(default=10, help_text=b'This defines the number of videos to be sent to a videographer or number of videographers a video will be send to'),
        ),
    ]
