# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0013_reviewsystemconfig_review_expiry_limit'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContestConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('closing_margin', models.IntegerField(default=3)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
