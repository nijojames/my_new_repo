from django.apps import AppConfig, apps
from actstream import registry


class GeneralAppConfig(AppConfig):
    name = 'api.apps.general'

    def ready(self):
        registry.register(apps.get_model('auth.user'))
        registry.register(apps.get_model('follow.follow'))
        registry.register(apps.get_model('voting.vote'))
