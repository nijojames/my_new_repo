from rest_framework import routers

from django.conf.urls import patterns, include, url

from views import PriceConfigViewSet, CategoryViewSet, APNSDeviceRgisterViewSet
from views import RecaptchaViewSet
from views import S3SignatureView
from views import DefaultConfigView
from views import PostUrlView

router = routers.DefaultRouter()

# Register api urls to router here
router.register(r'price-config', PriceConfigViewSet, base_name='price-config')
router.register(r'categories', CategoryViewSet, base_name='categories')
router.register(r'recaptcha', RecaptchaViewSet, base_name='recaptcha')
router.register(r'register-device',
                APNSDeviceRgisterViewSet,
                base_name='register_apns_device')
#  router.register(r's3signature', S3SignatureViewSet, base_name='s3signature')

urlpatterns = [
    url('', include(router.urls)),
    url(r'^s3signature$', S3SignatureView.as_view(), name='s3signature'),
    url(r'^default-config', DefaultConfigView.as_view(), name='defaultconfig'),
    url(r'^post-url$', PostUrlView.as_view(), name='post-url'),
]
