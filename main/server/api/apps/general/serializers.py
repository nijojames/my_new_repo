from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from models import VideoPriceConfig, Category, Recaptcha
from models import ContestConfig
from models import VideoTypeConfig
from models import VideoOrientation
from models import VideoMiscConfig
from models import VideoEnvironment
from models import VideoLighting
from models import VideoPermission


class PriceConfigSerializer(ModelSerializer):

    class Meta:
        model = VideoPriceConfig


class CategorySerializer(ModelSerializer):

    class Meta:
        model = Category


class RecaptchaSerializer(ModelSerializer):

    class Meta:
        model = Recaptcha


class ContestConfigSerializer(ModelSerializer):

    class Meta:
        model = ContestConfig


class VideoTypeConfigSerializer(ModelSerializer):
    # use this field for display to user
    display_name = serializers.SerializerMethodField()

    class Meta:
        model = VideoTypeConfig

    def get_display_name(self, obj):
        return (obj.get_video_type_display() +
                "(" +
                obj.get_resolution_display() +
                "@" +
                obj.get_fps_rate_display() +
                ")")


class VideoOrientationSerializer(ModelSerializer):
    # use this field for display to user
    display_name = serializers.SerializerMethodField()

    class Meta:
        model = VideoOrientation

    def get_display_name(self, obj):
        return obj.get_orientation_display()


class VideoMiscConfigSerializer(ModelSerializer):

    class Meta:
        model = VideoMiscConfig


class VideoLightingSerializer(ModelSerializer):
    # use this field for display to user
    display_name = serializers.SerializerMethodField()

    class Meta:
        model = VideoLighting

    def get_display_name(self, obj):
        return obj.get_lighting_display()


class VideoEnvironmentSerializer(ModelSerializer):
    # use this field for display to user
    display_name = serializers.SerializerMethodField()

    class Meta:
        model = VideoEnvironment

    def get_display_name(self, obj):
        return obj.get_environment_display()


class VideoPermissionSerializer(ModelSerializer):
    # use this field for display to user
    display_name = serializers.SerializerMethodField()

    class Meta:
        model = VideoPermission

    def get_display_name(self, obj):
        return obj.get_permission_display()
