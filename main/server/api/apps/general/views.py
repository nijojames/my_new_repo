from rest_framework import views
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import status
from push_notifications.api.rest_framework import APNSDeviceAuthorizedViewSet
from push_notifications.models import APNSDevice

from serializers import PriceConfigSerializer, CategorySerializer
from serializers import RecaptchaSerializer
from serializers import ContestConfigSerializer
from serializers import VideoTypeConfigSerializer
from serializers import VideoOrientationSerializer
from serializers import VideoMiscConfigSerializer
from serializers import VideoLightingSerializer
from serializers import VideoEnvironmentSerializer
from serializers import VideoPermissionSerializer

from models import VideoPriceConfig, Category, ContestConfig
from models import VideoTypeConfig
from models import VideoOrientation
from models import VideoMiscConfig
from models import VideoEnvironment
from models import VideoLighting
from models import VideoPermission

from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

from django.conf import settings
#import json
import requests


class PriceConfigViewSet(ModelViewSet):

    serializer_class = PriceConfigSerializer
    permission_classes = [AllowAny]
    queryset = VideoPriceConfig.objects.all()


class CategoryViewSet(ModelViewSet):

    serializer_class = CategorySerializer
    permission_classes = [AllowAny]
    queryset = Category.objects.filter(published=True).order_by('name')


class RecaptchaViewSet(ModelViewSet):
    # serializer class is not actually used. it is just to
    # make swagger happy and show proper params in docs
    serializer_class = RecaptchaSerializer
    permission_classes = [AllowAny]
    http_method_names = ['post', ]

    def create(self, request, *args, **kwargs):
        # print json.dumps(request.data, indent=4, sort_keys=True)
        if 'recaptcha' not in request.data:
            return (Response(status=HTTP_400_BAD_REQUEST))

        r = requests.post('https://www.google.com/recaptcha/api/siteverify',
                          data={'secret': settings.RECAPTCHA_SECRET_KEY,
                                'response': request.data['recaptcha']})
        if r.status_code != HTTP_200_OK:
            return Response(status=r.status_code)
        r = r.json()
        if not r['success']:
            return(Response(status=HTTP_400_BAD_REQUEST))
        return(Response(status=HTTP_200_OK))


class APNSDeviceRgisterViewSet(APNSDeviceAuthorizedViewSet):

    http_method_names = ['get', 'post']

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        device, created = APNSDevice.objects.get_or_create(user=request.user)
        device.registration_id = serializer.data.get('registration_id')
        device.save()
        serializer = self.get_serializer(device)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


'''
ToDo: use boto3

API to allow clients like webapp to directly upload
contents to s3
'''
import base64
import hmac
import hashlib


def sign(key, msg):
    return hmac.new(key, msg.encode("utf-8"), hashlib.sha256).digest()


class S3SignatureView(views.APIView):
    # serializer class is not actually used. it is just to
    # make swagger happy and show proper params in docs
    #serializer_class = S3SignatureSerializer
    permission_classes = [AllowAny]  # ToDo: check this
    #http_method_names = ['get', ]
    #queryset = S3Signature.objects.all()

    def get(self, request, pk=None):
        #def retrieve(self, request, pk=None):
        from datetime import datetime, timedelta

        region_str = 'us-west-2'
        service_str = 's3'
        aws_str = 'aws4_request'
        t = datetime.utcnow()
        date_long = t.strftime('%Y%m%dT%H%M%SZ')
        date_now = t.strftime('%Y%m%d')
        expiration = (t + timedelta(seconds=60)).isoformat() + 'Z'
        credentials = (settings.AWS_ACCESS_KEY_ID +
                       '/' +
                       date_now +
                       '/' +
                       region_str +
                       '/' +
                       service_str +
                       '/' +
                       aws_str)
        policy = {
            'expiration': expiration,
            'conditions': [
                {"bucket": settings.AWS_STORAGE_BUCKET_NAME},
                {"acl": "private"},
                ["starts-with", "$key", ''],
                ['starts-with', '$Content-Type', ''],
                {"x-amz-credential": credentials},
                {"x-amz-algorithm": "AWS4-HMAC-SHA256"},
                {"x-amz-date": date_long}
            ]
        }
        base64_policy = base64.b64encode(str(policy).encode("utf-8"))
        date_key = sign(('AWS4' + settings.AWS_SECRET_ACCESS_KEY).encode('utf-8'),
                        date_now)
        date_region_key = sign(date_key, region_str)
        date_region_service_key = sign(date_region_key, service_str)
        signing_key = sign(date_region_service_key, aws_str)
        signature = hmac.new(signing_key, (base64_policy).encode('utf-8'),
                             hashlib.sha256).hexdigest()
        return (Response({'bucket': settings.AWS_STORAGE_BUCKET_NAME,
                          'dir': 'userimages/',
                          'signature': signature,
                          'policy': base64_policy,
                          'date': date_long,
                          'credentials': credentials,
                          'expiration': expiration},
                         status=status.HTTP_200_OK))


'''
API to provide presigned POST urls to clients
'''
import uuid
import boto3
from botocore.client import Config
import copy


class PostUrlView(views.APIView):
    permission_classes = [IsAuthenticated]
    http_method_names = ['get', ]

    def get(self, request, *args, **kwargs):
        # 'video', 'contest'
        object_type = request.query_params.get('type', None)
        if object_type == 'video':
            # minimum one model and one property release
            release_doc_count = request.query_params.get('release_doc_count',
                                                         2)
            try:
                release_doc_count = int(release_doc_count)
            except:
                return Response(status=status.HTTP_400_BAD_REQUEST)

            info = {
                'original_video': {
                    'bucket': settings.AWS_ORIGINAL_VIDEO_S3_BUCKET,
                    'fields': {
                        "acl": "private",
                        'success_action_status': '201'
                    },
                    'conditions': [
                        {"acl": "private"},
                        ["content-length-range", 1048576, 268435456],
                        {'success_action_status': '201'},
                        # ['eq', '$Content-Type', 'video/mp4'],
                    ]
                },
                'watermarked_video': {
                    'bucket': settings.AWS_WATERMARKED_VIDEO_S3_BUCKET,
                    'fields': {
                        "acl": "public-read",
                        'success_action_status': '201'
                    },
                    'conditions': [
                        {"acl": "public-read"},
                        ["content-length-range", 1048576, 134217728],
                        {'success_action_status': '201'},
                        # ['eq', '$Content-Type', 'video/mp4'],
                    ]
                },
                'release_doc': {
                    'count': release_doc_count,
                    'bucket': settings.AWS_RELEASE_DOC_S3_BUCKET,
                    'fields': {
                        "acl": "private",
                        'success_action_status': '201'
                    },
                    'conditions': [
                        {"acl": "private"},
                        ["content-length-range", 1024, 134217728],
                        {'success_action_status': '201'},
                        # ['eq', '$Content-Type', 'image/png'],
                    ]
                },
            }
        elif object_type == 'profile':
            info = {
                'profile_picture': {
                    'bucket': settings.AWS_STORAGE_BUCKET_NAME,
                    'dir': settings.MEDIAFILES_LOCATION,
                    'fields': {
                        "acl": "public-read",
                        'success_action_status': '201'
                    },
                    'conditions': [
                        {"acl": "public-read"},
                        ["content-length-range", 1024, 134217728],
                        {'success_action_status': '201'},
                        # ['eq', '$Content-Type', 'image/jpeg'],
                    ]
                },
                'cover_picture': {
                    'bucket': settings.AWS_STORAGE_BUCKET_NAME,
                    'dir': settings.MEDIAFILES_LOCATION,
                    'fields': {
                        "acl": "public-read",
                        'success_action_status': '201'
                    },
                    'conditions': [
                        {"acl": "public-read"},
                        ["content-length-range", 1024, 134217728],
                        {'success_action_status': '201'},
                        # ['eq', '$Content-Type', 'image/jpeg'],
                    ]
                },
            }
        elif object_type == 'contest':
            return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        # generate post url
        s3 = boto3.client('s3',
                          config=Config(signature_version='s3v4',
                                        s3={'addressing_style': 'virtual'}),
                          aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                          aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
                          region_name=settings.AWS_REGION)
        results = dict()
        for key, value in info.iteritems():
            list = []
            for i in range(0, value.get('count', 1)):
                data = copy.deepcopy(value)
                file_name = data.get('dir', None)
                if file_name:
                    file_name = file_name + "/" + str(uuid.uuid4())
                else:
                    file_name = str(uuid.uuid4())

                post = s3.generate_presigned_post(Bucket=data['bucket'],
                                                  Key=file_name,
                                                  Fields=data['fields'],
                                                  Conditions=data['conditions'])
                if not post:
                    return Response(status=status.HTTP_404_NOT_FOUND)
                list.append(post)

            results[key] = list

        return Response({'results': results},
                        status=status.HTTP_200_OK)


class ContestConfigViewSet(ModelViewSet):

    serializer_class = ContestConfigSerializer
    permission_classes = [AllowAny]
    queryset = ContestConfig.objects.all()


class VideoTypeConfigViewSet(ModelViewSet):

    serializer_class = VideoTypeConfigSerializer
    permission_classes = [AllowAny]
    queryset = VideoTypeConfig.objects.filter(supported=True).order_by('-is_default')


class VideoOrientationViewSet(ModelViewSet):

    serializer_class = VideoOrientationSerializer
    permission_classes = [AllowAny]
    queryset = VideoOrientation.objects.filter(supported=True).order_by('-is_default')


class VideoMiscConfigViewSet(ModelViewSet):

    serializer_class = VideoMiscConfigSerializer
    permission_classes = [AllowAny]
    queryset = VideoMiscConfig.objects.all()


class VideoEnvironmentViewSet(ModelViewSet):

    serializer_class = VideoEnvironmentSerializer
    permission_classes = [AllowAny]
    queryset = VideoEnvironment.objects.filter(supported=True).order_by('-is_default')


class VideoLightingViewSet(ModelViewSet):

    serializer_class = VideoLightingSerializer
    permission_classes = [AllowAny]
    queryset = VideoLighting.objects.filter(supported=True).order_by('-is_default')


class VideoPermissionViewSet(ModelViewSet):

    serializer_class = VideoPermissionSerializer
    permission_classes = [AllowAny]
    queryset = VideoPermission.objects.filter(supported=True).order_by('-is_default')


def call_view(viewset, request):
    # function to call a viewset
    view_fn = viewset.as_view({'get': 'list'})
    response = view_fn(request)
    return (response.data)


class DefaultConfigView(views.APIView):
    permission_classes = [AllowAny]
    http_method_names = ['get', ]

    def get(self, request):
        return (Response({'video_type': call_view(VideoTypeConfigViewSet, request),
                          'video_orientation': call_view(VideoOrientationViewSet, request),
                          'video_lighting': call_view(VideoLightingViewSet, request),
                          'video_environment': call_view(VideoEnvironmentViewSet, request),
                          'video_permission': call_view(VideoPermissionViewSet, request),
                          'video_misc_cfg': call_view(VideoMiscConfigViewSet, request)},
                         status=status.HTTP_200_OK))
