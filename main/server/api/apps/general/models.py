from string import lower

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _

from notifications.signals import notify
from solo.models import SingletonModel
from follow.models import Follow
from voting.models import Vote
from actstream.actions import action


from api.apps.activities.follow_utiils import get_followers_except_me
from api.utl.utl import get_genric_object

from api.apps.utils import choices


class ReputationConfig(SingletonModel):
    video_upload_point = models.IntegerField(default=1)
    video_winning_contest_point = models.IntegerField(default=10)
    video_sold_point = models.IntegerField(default=5)
    video_spammed_point = models.IntegerField(default=-10)

    positive_review_winning_contest_point = models.IntegerField(default=10)
    positive_review_video_sold_point = models.IntegerField(default=5)
    positive_review_reached_market_point = models.IntegerField(default=1)
    positive_review_did_not_reached_market_point = models.IntegerField(default=-1)
    negative_review_did_reached_market_point = models.IntegerField(default=-1)
    positive_review_video_spammed = models.IntegerField(default=-10)

    level_two_video_points_required = models.IntegerField(default=50)
    level_two_review_points_required = models.IntegerField(default=50)

    level_three_video_points_required = models.IntegerField(default=200)
    level_three_review_points_required = models.IntegerField(default=200)

    level_four_video_points_required = models.IntegerField(default=350)
    level_four_review_points_required = models.IntegerField(default=400)

    level_five_video_points_required = models.IntegerField(default=800)
    level_five_review_points_required = models.IntegerField(default=1000)

    level_one_name = models.CharField(default='Level 1', max_length=100)
    level_two_name = models.CharField(default='Level 2', max_length=100)
    level_three_name = models.CharField(default='Level 3', max_length=100)
    level_four_name = models.CharField(default='Level 4', max_length=100)
    level_five_name = models.CharField(default='Level 5', max_length=100)


class SpamHandlingConfig(SingletonModel):
    video_spam_count_limit = models.IntegerField(default=10)


class ContactUsConfig(SingletonModel):
    contact_email = models.EmailField(max_length=75, null=True, blank=True)

    def __unicode__(self):
        return self.contact_email


class ReviewSystemConfig(SingletonModel):
    # ToDo: remove the following        
    videos_batches_count = models.IntegerField(default=10,
                                               help_text='This defines the number of videos to be '
                                               'sent to a videographer or number of videographers a '
                                               'video will be send to')
    overall_rating_required = models.IntegerField(default=30)
    overall_positive_reviews_required = models.IntegerField(default=6,
                                                            help_text='Overall positive rating')
    min_rating_for_positive_review = models.IntegerField(default=3,
                                                         help_text='A minimum rating required for '
                                                         'a positive review')

    resend_videographers_multiplier = models.IntegerField(default=1,
                                                          help_text='R value, '
                                                          'where number R is the of videographers '
                                                          'the video will be resend will be N*R')
    # ---------------------------------------
    max_num_of_reviews_required = models.IntegerField(default=5)
    num_of_positive_reviews_required = models.IntegerField(default=3)
    max_num_of_times_to_pay_reviewer = models.IntegerField(default=1)
    # # of seconds for the client to review the video
    review_expiry_limit = models.IntegerField(default=5,
                                              help_text="In seconds")
    # margin to run the expiry task
    review_expiry_margin = models.IntegerField(default=3,
                                               help_text="In seconds")


class VideoPriceConfig(SingletonModel):
    level_one_min_price = models.DecimalField(max_digits=7, decimal_places=2, default=15.00)
    level_two_min_price = models.DecimalField(max_digits=7, decimal_places=2, default=25.00)
    level_three_min_price = models.DecimalField(max_digits=7, decimal_places=2, default=35.00)
    level_four_min_price = models.DecimalField(max_digits=7, decimal_places=2, default=50.00)
    level_five_min_price = models.DecimalField(max_digits=7, decimal_places=2, default=75.00)
    private_request_price = models.DecimalField(max_digits=7, decimal_places=2, default=20.00)
    invite_price = models.DecimalField(max_digits=7, decimal_places=2, default=20.00)
    location_price = models.DecimalField(max_digits=7, decimal_places=2, default=20.00)
    model_release_price = models.DecimalField(max_digits=7, decimal_places=2, default=20.00)


class Category(models.Model):
    name = models.CharField(max_length=120)
    thumbnail = models.ImageField(upload_to='categories/thumbnail')
    published = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = _('Categories')

    def __str__(self):
        return self.name


class LevelPrivilegeConfig(SingletonModel):
    level_one_video_count_per_day = models.IntegerField(default=1)
    level_two_video_count_per_day = models.IntegerField(default=1)
    level_three_video_count_per_day = models.IntegerField(default=3)
    level_four_video_count_per_day = models.IntegerField(default=4)
    level_five_video_count_per_day = models.IntegerField(default=5) # No limit

    level_one_video_max_price = models.DecimalField(max_digits=10, decimal_places=2, default=20.00)
    level_two_video_max_price = models.DecimalField(max_digits=7, decimal_places=2, default=50.00)
    level_three_video_max_price = models.DecimalField(max_digits=7, decimal_places=2, default=100.00)
    level_four_video_max_price = models.DecimalField(max_digits=7, decimal_places=2, default=200.00)
    level_five_video_max_price = models.DecimalField(max_digits=7, decimal_places=2, default=400.00) # No limit

    level_one_review_count_per_day = models.IntegerField(default=10)
    level_two_review_count_per_day = models.IntegerField(default=20)
    level_three_review_count_per_day = models.IntegerField(default=30)
    level_four_review_count_per_day = models.IntegerField(default=40)
    level_five_review_count_per_day = models.IntegerField(default=50) # No limit

    level_one_commission = models.IntegerField(default=100)
    level_two_commission = models.IntegerField(default=90)
    level_three_commission = models.IntegerField(default=80)
    level_four_commission = models.IntegerField(default=70)
    level_five_commission = models.IntegerField(default=60)

    level_one_review_revenue_sharing = models.IntegerField(default=100)
    level_two_review_revenue_sharing = models.IntegerField(default=90)
    level_three_review_revenue_sharing = models.IntegerField(default=80)
    level_four_review_revenue_sharing = models.IntegerField(default=70)
    level_five_review_revenue_sharing = models.IntegerField(default=60) # No limit

    level_one_spam_weight = models.IntegerField(default=1)
    level_two_spam_weight = models.IntegerField(default=2)
    level_three_spam_weight = models.IntegerField(default=3)
    level_four_spam_weight = models.IntegerField(default=4)
    level_five_spam_weight = models.IntegerField(default=5) # No limit


class Recaptcha(models.Model):
    secret = models.CharField(max_length=256, null=False)
    response = models.CharField(max_length=256, null=False)

    class Meta:
        verbose_name_plural = _('Recaptcha')

    def __str__(self):
        return "Recaptcha"+self.response


class ContestConfig(SingletonModel):
    # contest to be closed after 'closing_margin' days 
    # after the first purchase
    closing_margin = models.IntegerField(default=3)


def enforce_default(obj, model):
    if obj.is_default:
        try:
            temp = model.objects.get(is_default=True)
            if obj != temp:
                temp.is_default = False
                temp.save()
        except model.DoesNotExist:
            pass


class VideoTypeConfig(models.Model):
    video_type = models.CharField(choices=choices.VIDEO_TYPE,
                                  max_length=30)
    fps_rate = models.CharField(choices=choices.FPS_CHOICES,
                                max_length=10)
    resolution = models.CharField(choices=choices.RESOLUTION_CHOICES,
                                  max_length=10)
    is_default = models.BooleanField(default=False)
    supported = models.BooleanField(default=True)

    def __str__(self):
        return self.video_type

    # override save method to have only one default
    def save(self, *args, **kwargs):
        enforce_default(self, VideoTypeConfig)
        super(VideoTypeConfig, self).save(*args, **kwargs)


class VideoOrientation(models.Model):
    orientation = models.CharField(choices=choices.CONTEST_ORIENTATION_CHOICES,
                                   max_length=20,
                                   unique=True)
    is_default = models.BooleanField(default=False)
    supported = models.BooleanField(default=True)

    def __str__(self):
        return self.orientation

    def save(self, *args, **kwargs):
        enforce_default(self, VideoOrientation)
        super(VideoOrientation, self).save(*args, **kwargs)


class VideoMiscConfig(SingletonModel):
    space_for_copy = models.BooleanField(default=False)
    min_duration_sec = models.IntegerField(default=10)
    max_duration_sec = models.IntegerField(default=30)
    accept_curated_videos = models.BooleanField(default=False)

    def __str_(self):
        return "Video Misc Config"


class VideoEnvironment(models.Model):
    environment = models.CharField(choices=choices.CONTEST_ENVIRONMENT_OPTIONS,
                                   max_length=30)
    is_default = models.BooleanField(default=False)
    supported = models.BooleanField(default=True)

    def __str__(self):
        return self.environment

    def save(self, *args, **kwargs):
        enforce_default(self, VideoEnvironment)
        super(VideoEnvironment, self).save(*args, **kwargs)


class VideoLighting(models.Model):
    lighting = models.CharField(choices=choices.CONTEST_LIGHTNING_OPTIONS,
                                max_length=30)
    is_default = models.BooleanField(default=False)
    supported = models.BooleanField(default=True)

    def __str__(self):
        return self.lighting

    def save(self, *args, **kwargs):
        enforce_default(self, VideoLighting)
        super(VideoLighting, self).save(*args, **kwargs)


class VideoPermission(models.Model):
    permission = models.CharField(choices=choices.PERMISSION_TYPE,
                                  max_length=20)
    is_default = models.BooleanField(default=False)
    supported = models.BooleanField(default=True)

    def __str__(self):
        return self.permission

    def save(self, *args, **kwargs):
        enforce_default(self, VideoPermission)
        super(VideoPermission, self).save(*args, **kwargs)
