from django.contrib import admin

from import_export.admin import ImportExportModelAdmin

from solo.admin import SingletonModelAdmin
from actstream.models import Follow

from models import ReputationConfig, SpamHandlingConfig, ReviewSystemConfig, \
    ContactUsConfig, VideoPriceConfig, Category, LevelPrivilegeConfig
from models import ContestConfig
from models import VideoTypeConfig
from models import VideoOrientation
from models import VideoMiscConfig
from models import VideoEnvironment
from models import VideoLighting
from models import VideoPermission


class FieldSetAdmin(SingletonModelAdmin):

    fieldsets = (
        ('Video', {
            'fields': ('video_upload_point', 'video_winning_contest_point', 'video_sold_point', 'video_spammed_point')
        }),

        ('Video Review', {
            'fields': ('positive_review_winning_contest_point',
                       'positive_review_video_sold_point',
                       'positive_review_reached_market_point',
                       'positive_review_did_not_reached_market_point',
                       'negative_review_did_reached_market_point',
                       'positive_review_video_spammed')
        }),

        ('Video Review', {
            'fields': ('positive_review_winning_contest_point',
                       'positive_review_video_sold_point',
                       'positive_review_reached_market_point',
                       'positive_review_did_not_reached_market_point',
                       'negative_review_did_reached_market_point',
                       'positive_review_video_spammed')
        }),
        ('Levels', {
            'fields': (('level_two_video_points_required',
                       'level_two_review_points_required',),
                       ('level_three_video_points_required',
                       'level_three_review_points_required',),
                       ('level_four_video_points_required',
                       'level_four_review_points_required',),
                       ('level_five_video_points_required',
                       'level_five_review_points_required'),),
        }),
        ('Level Naming', {
            'fields': ('level_one_name',
                       'level_two_name',
                       'level_three_name',
                       'level_four_name',
                       'level_five_name'),
        }),
    )


class CategoryAdmin(ImportExportModelAdmin):
    pass


class VideoTypeConfigAdmin(ImportExportModelAdmin):
    list_display = ['video_type', 'fps_rate', 'resolution', 'is_default']


class VideoOrientationAdmin(ImportExportModelAdmin):
    list_display = ['orientation', 'is_default']


class VideoMiscConfigAdmin(ImportExportModelAdmin):
    pass


class VideoLightingAdmin(ImportExportModelAdmin):
    list_display = ['lighting', 'is_default']


class VideoEnvironmentAdmin(ImportExportModelAdmin):
    list_display = ['environment', 'is_default']


class VideoPermissionAdmin(ImportExportModelAdmin):
    list_display = ['permission', 'is_default']


admin.site.register(ReputationConfig, FieldSetAdmin)
admin.site.register(SpamHandlingConfig, SingletonModelAdmin)
admin.site.register(ReviewSystemConfig, SingletonModelAdmin)

admin.site.unregister(Follow)

admin.site.register(ContactUsConfig)
admin.site.register(VideoPriceConfig)
admin.site.register(Category, CategoryAdmin)
admin.site.register(LevelPrivilegeConfig, SingletonModelAdmin)
admin.site.register(ContestConfig, SingletonModelAdmin)
admin.site.register(VideoTypeConfig, VideoTypeConfigAdmin)
admin.site.register(VideoOrientation, VideoOrientationAdmin)
admin.site.register(VideoMiscConfig, VideoMiscConfigAdmin)
admin.site.register(VideoLighting, VideoLightingAdmin)
admin.site.register(VideoEnvironment, VideoEnvironmentAdmin)
admin.site.register(VideoPermission, VideoPermissionAdmin)
