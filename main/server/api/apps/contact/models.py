from django.db import models
from django.utils.translation import ugettext_lazy as _

from django_extensions.db.models import TimeStampedModel


class TopicType(models.Model):
    name = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class Contact(TimeStampedModel):

    USER_TYPE = (
        ('videographer', _('Videographer')),
        ('video_buyer', _('Video Buyer')),
        ('both', _('Both'))
    )
    topic = models.ForeignKey(TopicType)
    name = models.CharField(max_length=30)
    email = models.EmailField(max_length=75)
    type = models.CharField(_('type'), max_length=15, choices=USER_TYPE,
                            help_text="User Type?")
    message = models.TextField(max_length=1000)

    def __unicode__(self):
        return self.name


class Information(models.Model):

    address_line_1 = models.CharField(max_length=150)
    address_line_2 = models.CharField(max_length=150, null=True, blank=True)
    address_line_3 = models.CharField(max_length=150, null=True, blank=True)
    address_line_4 = models.CharField(max_length=150, null=True, blank=True)

    telephone = models.CharField(max_length=25, null=True, blank=True)
    fax = models.CharField(max_length=25, null=True, blank=True)
    email = models.EmailField(max_length=75, null=True, blank=True)
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    published = models.BooleanField(default=False)

    def __unicode__(self):
        return self.address_line_1
