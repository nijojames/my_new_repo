from rest_framework import serializers

from models import Contact, TopicType, Information

from api.apps.general.models import ContactUsConfig

from django.core.mail import EmailMessage

from django.conf import settings


class ContactSerializer(serializers.ModelSerializer):

    class Meta:
        model = Contact
        fields = ('name', 'email', 'type', 'message', 'topic')

    def save(self, **kwargs):
        contact = super(ContactSerializer, self).save(**kwargs)
        email = EmailMessage(contact.topic, "Hi " + contact.name + ",\nThank you for contact us.", to=[contact.email])
        email.send()
        contact_admin = ContactUsConfig.objects.all().values_list('contact_email', flat=True)
        admin_email = EmailMessage(contact.topic, "Hi,\nName : " + contact.name + "\nType : " + contact.type + "\nMessage : " + contact.message, to=contact_admin)
        admin_email.send()
        return contact


class TopicTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = TopicType


class InformationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Information
        fields = ('address_line_1', 'address_line_2', 'address_line_3', 'address_line_4', 'telephone', 'fax', 'email', 'latitude', 'longitude')
