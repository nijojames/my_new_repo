from rest_framework import routers

from django.conf.urls import patterns, include, url

from views import ContactView, TopicView, InformationView


router = routers.DefaultRouter()

# Register api urls to router here
router.register(r'contact', ContactView, base_name='contact-us')
router.register(r'topics', TopicView, base_name='contact-us')
router.register(r'information',InformationView, base_name='contact-us')

urlpatterns = [
    url('', include(router.urls))
]
