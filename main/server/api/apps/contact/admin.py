from django.contrib import admin

from import_export.admin import ImportExportModelAdmin

from models import Contact, TopicType, Information


class ContactAdmin(ImportExportModelAdmin):
    search_fields = ['type', 'name']
    list_display = ['topic', 'email', 'type', 'created']


class TopicTypeAdmin(ImportExportModelAdmin):
    pass


class InformationAdmin(ImportExportModelAdmin):
    pass


admin.site.register(TopicType, TopicTypeAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Information, InformationAdmin)
