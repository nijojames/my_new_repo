# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Information',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address_line_1', models.CharField(max_length=150)),
                ('address_line_2', models.CharField(max_length=150, null=True, blank=True)),
                ('address_line_3', models.CharField(max_length=150, null=True, blank=True)),
                ('address_line_4', models.CharField(max_length=150, null=True, blank=True)),
                ('telephone', models.CharField(max_length=25, null=True, blank=True)),
                ('fax', models.CharField(max_length=25, null=True, blank=True)),
                ('email', models.EmailField(max_length=75, null=True, blank=True)),
                ('latitude', models.DecimalField(max_digits=9, decimal_places=6)),
                ('longitude', models.DecimalField(max_digits=9, decimal_places=6)),
                ('published', models.BooleanField(default=False)),
            ],
        ),
        migrations.AlterField(
            model_name='contact',
            name='type',
            field=models.CharField(help_text=b'User Type?', max_length=15, verbose_name='type', choices=[(b'videographer', 'Videographer'), (b'video_buyer', 'Video Buyer'), (b'both', 'Both')]),
        ),
    ]
