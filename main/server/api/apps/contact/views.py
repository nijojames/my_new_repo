from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions

from serializers import ContactSerializer, TopicTypeSerializer, InformationSerializer

from models import Contact, TopicType, Information


class ContactView(ModelViewSet):
    serializer_class = ContactSerializer
    model = Contact
    queryset = Contact.objects.all()
    permission_classes = (permissions.AllowAny, )


class TopicView(ModelViewSet):
    serializer_class = TopicTypeSerializer
    model = TopicType
    queryset = TopicType.objects.all()


class InformationView(ModelViewSet):
    serializer_class = InformationSerializer
    model = Information
    queryset = Information.objects.filter(published=True)
