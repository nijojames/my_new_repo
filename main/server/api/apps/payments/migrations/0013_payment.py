# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('videos', '0029_auto_20160703_1014'),
        ('payments', '0012_orderhistory_created_at'),
    ]

    operations = [
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('task_performed', models.CharField(max_length=15, choices=[(b'owner', b'Owner'), (b'reviewed', b'Reviewed')])),
                ('datetime_of_task', models.DateTimeField(auto_now_add=True)),
                ('datetime_of_sale', models.DateTimeField(auto_now_add=True)),
                ('price_paid_by_buyer', models.DecimalField(max_digits=8, decimal_places=2)),
                ('commission_for_task', models.DecimalField(max_digits=8, decimal_places=2)),
                ('status', models.CharField(max_length=15, choices=[(b'unpaid', b'Unpaid'), (b'scheduled', b'Scheduled'), (b'paid', b'Paid')])),
                ('user', models.ForeignKey(related_name='payment_user', to=settings.AUTH_USER_MODEL)),
                ('video', models.ForeignKey(related_name='payment_video', to='videos.Video')),
            ],
        ),
    ]
