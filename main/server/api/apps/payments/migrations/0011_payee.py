# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('payments', '0010_transactions'),
    ]

    operations = [
        migrations.CreateModel(
            name='Payee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('video_id', models.IntegerField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('type', models.CharField(max_length=10, choices=[(b'owner', b'Owner'), (b'reviewer', b'Reviewer')])),
                ('num_of_times_payed', models.IntegerField(default=0)),
                ('commission', models.DecimalField(max_digits=5, decimal_places=2)),
                ('max_num_of_times_to_be_payed', models.IntegerField(null=True, blank=True)),
                ('user', models.ForeignKey(related_name='payee_users', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
