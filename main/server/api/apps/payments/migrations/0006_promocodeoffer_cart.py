# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carts', '0001_initial'),
        ('payments', '0005_auto_20160426_0420'),
    ]

    operations = [
        migrations.AddField(
            model_name='promocodeoffer',
            name='cart',
            field=models.ForeignKey(related_name='get_applied_offers', blank=True, to='carts.Cart', null=True),
        ),
    ]
