# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0008_auto_20160428_1052'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderhistory',
            name='customer_id',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='orderhistory',
            name='card_token',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='orderhistory',
            name='token',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
