# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import api.apps.payments.models
import django_extensions.db.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('payments', '0004_auto_20160328_1330'),
    ]

    operations = [
        migrations.CreateModel(
            name='PromoCode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=8)),
                ('discount_percentage', models.FloatField(blank=True, null=True, validators=[api.apps.payments.models.validate_percentage])),
                ('discount_amount', models.DecimalField(null=True, max_digits=6, decimal_places=2, blank=True)),
                ('expired', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='PromoCodeOffer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('applied', models.BooleanField(default=False)),
                ('applied_time', models.DateTimeField(null=True, blank=True)),
                ('promo_code', models.ForeignKey(to='payments.PromoCode')),
                ('user', models.ForeignKey(related_name='get_offers', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='boughtitems',
            name='content_type',
            field=models.ForeignKey(default=1, to='contenttypes.ContentType'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='boughtitems',
            name='object_id',
            field=models.PositiveIntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='promocodeoffer',
            unique_together=set([('user', 'promo_code')]),
        ),
    ]
