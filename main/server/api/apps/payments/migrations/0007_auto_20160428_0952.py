# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0006_promocodeoffer_cart'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='boughtitems',
            unique_together=set([('order', 'content_type', 'object_id')]),
        ),
        migrations.RemoveField(
            model_name='boughtitems',
            name='video',
        ),
    ]
