# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderhistory',
            name='email',
            field=models.EmailField(default='krishna.sayone@gmail.com', max_length=254, verbose_name='Email of buyer'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='orderhistory',
            name='name',
            field=models.CharField(default='Krishna G Nair', max_length=120),
            preserve_default=False,
        ),
    ]
