# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0007_auto_20160428_0952'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderhistory',
            name='card_token',
            field=models.CharField(default='card_185CRIFaEMkDKTkRYR9EFhda', max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='orderhistory',
            name='token',
            field=models.CharField(default='tok_185CRIFaEMkDKTkRYR9EFhda', max_length=50),
            preserve_default=False,
        ),
    ]
