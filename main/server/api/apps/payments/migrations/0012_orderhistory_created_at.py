# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0011_payee'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderhistory',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
