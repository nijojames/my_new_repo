# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0013_payment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='task_performed',
            field=models.CharField(max_length=15, choices=[(b'owner', b'Owner'), (b'reviewer', b'Reviewer')]),
        ),
    ]
