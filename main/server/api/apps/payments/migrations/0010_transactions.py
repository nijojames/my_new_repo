# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('payments', '0009_auto_20160428_1430'),
    ]

    operations = [
        migrations.CreateModel(
            name='Transactions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('video_id', models.CharField(max_length=250)),
                ('original_amount_paid_buyer', models.DecimalField(max_digits=8, decimal_places=3)),
                ('price_calculated', models.DecimalField(max_digits=8, decimal_places=3)),
                ('status', models.CharField(default=b'scheduled', max_length=10, choices=[(b'scheduled', b'Scheduled'), (b'paid', b'Paid')])),
                ('resolved', models.BooleanField(default=False)),
                ('user', models.ForeignKey(related_name='get_transactions', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
        ),
    ]
