# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0012_auto_20160321_0728'),
        ('payments', '0002_auto_20160323_1106'),
    ]

    operations = [
        migrations.CreateModel(
            name='BoughtItems',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price_paid', models.DecimalField(max_digits=6, decimal_places=2)),
            ],
            options={
                'verbose_name': 'Bought Item',
                'verbose_name_plural': 'Bought Items',
            },
        ),
        migrations.RemoveField(
            model_name='orderhistory',
            name='videos',
        ),
        migrations.AddField(
            model_name='boughtitems',
            name='order',
            field=models.ForeignKey(related_name='get_order_items', to='payments.OrderHistory'),
        ),
        migrations.AddField(
            model_name='boughtitems',
            name='video',
            field=models.ForeignKey(to='videos.Video'),
        ),
    ]
