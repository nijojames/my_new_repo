# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0003_auto_20160323_1301'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='boughtitems',
            unique_together=set([('order', 'video')]),
        ),
    ]
