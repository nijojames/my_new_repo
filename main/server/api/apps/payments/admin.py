from django.contrib import admin

from import_export.admin import ImportExportModelAdmin

from models import OrderHistory, BoughtItems, PromoCode, PromoCodeOffer, Transactions, Payee
from forms import PromoCodeForm


class BoughtItemsInline(admin.StackedInline):
    model = BoughtItems


class OrderHistoryAdmin(ImportExportModelAdmin):
    inlines = [BoughtItemsInline]
    list_display = ['order_id']


class PromoCodeAdmin(ImportExportModelAdmin):
    form = PromoCodeForm


class PromoCodeOfferAdmin(ImportExportModelAdmin):
    pass


class BoughtItemsAdmin(ImportExportModelAdmin):
    pass


class TransactionsAdmin(ImportExportModelAdmin):
    list_display = ['user', 'price_calculated', 'status', 'resolved']


admin.site.register(OrderHistory, OrderHistoryAdmin)
admin.site.register(PromoCode, PromoCodeAdmin)
admin.site.register(PromoCodeOffer, PromoCodeOfferAdmin)
admin.site.register(BoughtItems, BoughtItemsAdmin)
admin.site.register(Transactions, TransactionsAdmin)

admin.site.register(Payee)