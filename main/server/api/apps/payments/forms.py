from django import forms

from .models import PromoCode


class PromoCodeForm(forms.ModelForm):

    class Meta:
        model = PromoCode
        fields = '__all__'

    def clean(self):
        form_data = self.cleaned_data
        discount_percentage = form_data.get('discount_percentage')
        discount_amount = form_data.get('discount_amount')
        if discount_amount and discount_percentage:
            raise forms.ValidationError('Provide discount percentage or amount')
        return form_data
