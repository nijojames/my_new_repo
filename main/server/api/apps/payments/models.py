import uuid
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.core.exceptions import ValidationError

from django_extensions.db.models import TimeStampedModel

from api.apps.videos.models import Video
from api.apps.carts.models import Cart


PAID = 'paid'
SCHEDULED = 'scheduled'

STATUS = ((SCHEDULED, 'Scheduled'),
          (PAID, 'Paid')
)


def validate_percentage(value):
    if value < 0 or value > 100:
        raise ValidationError(_('Provide a valid percentage value between 0-100'))


class OrderHistory(TimeStampedModel):
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    user = models.ForeignKey(User, related_name='orders', null=True, blank=True)
    name = models.CharField(max_length=120)
    email = models.EmailField(verbose_name=_('Email of buyer'))
    order_id = models.CharField(max_length=128, editable=False, unique=True)
    amount_paid = models.DecimalField(max_digits=6, decimal_places=2)
    success = models.BooleanField(default=False)
    token = models.CharField(max_length=50, null=True)
    card_token = models.CharField(max_length=50, null=True)
    customer_id = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.order_id

    class Meta:
        verbose_name = _('Order History')
        verbose_name_plural = _('Order Histories')

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.pk is None:
            self.order_id = str(uuid.uuid4())
        super(OrderHistory, self).save()


class BoughtItems(models.Model):
    order = models.ForeignKey(OrderHistory, related_name='get_order_items')
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    item = generic.GenericForeignKey('content_type', 'object_id')
    # video = models.ForeignKey(Video)
    price_paid = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return '%s in order %s' % (self.item, self.order_id)

    class Meta:
        verbose_name = _('Bought Item')
        verbose_name_plural = _('Bought Items')
        unique_together = ['order', 'content_type', 'object_id']


class PromoCode(models.Model):
    code = models.CharField(max_length=8)
    discount_percentage = models.FloatField(null=True, blank=True, validators=[validate_percentage])
    discount_amount = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    expired = models.BooleanField(default=False)

    def __str__(self):
        return self.code


class PromoCodeOffer(TimeStampedModel):
    user = models.ForeignKey(User, related_name='get_offers')
    promo_code = models.ForeignKey(PromoCode,
                                   limit_choices_to={'expired': False})
    cart = models.ForeignKey(Cart,
                             related_name='get_applied_offers',
                             null=True,
                             blank=True)
    applied = models.BooleanField(default=False)
    applied_time = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return 'Promo code %s for %s' % (self.promo_code, self.user)

    class Meta:
        unique_together = ['user', 'promo_code']

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if self.promo_code.expired:
            raise ValidationError("This promo code is no longer valid")
        return super(PromoCodeOffer, self).save(force_insert=False,
                                                force_update=False,
                                                using=None,
                                                update_fields=None)


class Transactions(TimeStampedModel):
    user = models.ForeignKey(User, related_name='get_transactions')
    video_id = models.CharField(max_length=250)
    original_amount_paid_buyer = models.DecimalField(max_digits=8,
                                                     decimal_places=3)
    price_calculated = models.DecimalField(max_digits=8, decimal_places=3)
    status = models.CharField(choices=STATUS, max_length=10, default=SCHEDULED)
    resolved = models.BooleanField(default=False)

    def __str__(self):
        return 'Amount ' + self.status + ' for ' + self.user.username


OWNER = 'owner'
REVIEWER = 'reviewer'

PAYEE_TYPE = ((OWNER, 'Owner'),
              (REVIEWER, 'Reviewer')
)


class Payee(models.Model):
    user = models.ForeignKey(User, related_name='payee_users')
    # ToDo: why video is not a foreign key ?
    video_id = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)
    type = models.CharField(choices=PAYEE_TYPE, max_length=10)
    num_of_times_payed = models.IntegerField(default=0)
    commission = models.DecimalField(decimal_places=2, max_digits=5)
    max_num_of_times_to_be_payed = models.IntegerField(null=True, blank=True)


UNPAID = 'unpaid'
SCHEDULED = 'scheduled'
PAID = 'paid'

STATUS_TYPE = (
    (UNPAID, 'Unpaid'),
    (SCHEDULED, 'Scheduled'),
    (PAID, 'Paid')
)


class Payment(models.Model):
    user = models.ForeignKey(User, related_name='payment_user')
    video = models.ForeignKey(Video, related_name='payment_video')
    task_performed = models.CharField(max_length=15, choices=PAYEE_TYPE)
    datetime_of_task = models.DateTimeField(auto_now_add=True)
    datetime_of_sale = models.DateTimeField(auto_now_add=True)
    price_paid_by_buyer = models.DecimalField(max_digits=8, decimal_places=2)
    commission_for_task = models.DecimalField(max_digits=8, decimal_places=2)
    status = models.CharField(max_length=15, choices=STATUS_TYPE)

    def __str__(self):
        return self.user + ' ' + self.user.username
