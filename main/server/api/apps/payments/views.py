from django.contrib.contenttypes.models import ContentType
from django_downloadview import ObjectDownloadView
from django.utils import timezone

from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions

from models import OrderHistory, BoughtItems, PromoCodeOffer
from api.apps.videos.models import Video
from api.apps.carts.models import Cart
from api.apps.email.services import send_templated_email
from serializers import OrderHistorySerializers, PlaceOrderSerializer, PromoCodeOfferSerializer
from utils import StripeUtils

from .services import PaymentService

from api.apps.contests.models import ContestEntry
from api.apps.general.models import ContestConfig
from django.utils import timezone
from datetime import timedelta

class OrderHistoryView(ModelViewSet):
    http_method_names = ['get']
    # permission_classes = [permissions.AllowAny]
    permission_classes = [permissions.IsAuthenticated]
    model = OrderHistory
    lookup_field = 'order_id'
    serializer_class = OrderHistorySerializers
    queryset = model.objects.all()

    def list(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            self.queryset = self.model.objects.filter(user=self.request.user)
            return super(OrderHistoryView, self).list(request, *args, **kwargs)
        else:
            return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


class PlaceOrderView(StripeUtils, ModelViewSet):
    model = OrderHistory
    serializer_class = PlaceOrderSerializer
    # permission_classes = [permissions.AllowAny]
    permission_classes = [permissions.IsAuthenticated]
    customer_id = None

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        name = serializer.data.get('name')
        email = serializer.data.get('email')
        cart = serializer.data.get('cart')
        amount_paid = serializer.data.get('amount_paid')
        token = serializer.data.get('token')
        remember_card = request.data.get('remember_card')
        use_saved_card = request.data.get('use_saved_card')
        cart = Cart.objects.get(id=cart)
        items = cart.get_items.all()
        data = serializer.data
        data.pop('cart')
        user = request.user
        try:
            if user.is_authenticated():
                self.customer_id = self.get_user_customer_id(user)
                data.update({'user': user})
                if use_saved_card:
                    description = 'Stripe customer for %s' % (request.user.first_name + request.user.last_name)
                    self.charge_card(amount_paid, description=description, customer=self.customer_id)
                else:
                    if self.customer_id:
                        description = 'Card changed for customer for %s' % (request.user.first_name + request.user.last_name)
                        self.update_customer_card(self.customer_id, token)

                    else:
                        description = 'Stripe customer for %s' % (request.user.first_name + request.user.last_name)
                        self.customer_id = self.create_customer(token, description=description)
                        request.user.user_profile.stripe_customer_id = self.customer_id
                        request.user.user_profile.save()
                        description = '%s is charged' % (name)
                        self.charge_card(amount_paid, token=token, description=description, customer=self.customer_id)
                        self.customer_id = self.get_user_customer_id(user)
            else:
                description = '%s is charged' % (name)
                self.charge_card(amount_paid, token=token, description=description, customer=self.customer_id)
                self.customer_id = self.get_user_customer_id(user)
        except self.STRIPE_ERROR as e:
            print "STRIPE PAYMENT EXCEPTION", e
            return Response(e._message, status=status.HTTP_400_BAD_REQUEST)
        order_history = OrderHistory.objects.create(**data)
        for item in items:
            content = item.get_product()
            content_type = ContentType.objects.get_for_model(content)
            if content_type.model == 'video':
                BoughtItems.objects.create(price_paid=content.price,
                                           order=order_history,
                                           content_type=content_type,
                                           object_id=content.id)
                # Create transaction entry
                service = PaymentService(content.id, content.price) # TODO check if to take this price
                service.make_an_entry_to_paid_for_owner()
                try:
                    entry = ContestEntry.objects.get(video=content)
                except ContestEntry.DoesNotExist:
                    continue

                if entry.contest.status != 'publish':
                    continue
                # set the closing date of the contest upon first purchase
                if not entry.contest.closing_date:
                    solo = ContestConfig.get_solo()
                    entry.contest.closing_date = (timezone.now() +
                                                  timedelta(days=solo.closing_margin))
                    entry.contest.save()
                # mark the entry as winner
                entry.winner = True
                entry.save()
                continue  # for item in items:

            if content_type.model == 'contest':
                content.status = 'publish'
                content.save()
                BoughtItems.objects.create(price_paid=content.award_amount,
                                           order=order_history,
                                           content_type=content_type,
                                           object_id=content.id)
        if order_history.user and remember_card:
            order_history.user.user_profile.remember_card = True
            order_history.user.user_profile.stripe_customer_id = self.customer_id
            order_history.user.user_profile.save()
        order_history.customer_id = self.customer_id
        order_history.success = True
        order_history.save()
        cart.delete()
        link = (request.META.get('HTTP_HOST') +
                '/#/order/' +
                order_history.order_id)
        email_context = {'order': order_history, 'link': link}
        send_templated_email('Purchase successful on SpyHop',
                             'email/invoice.html',
                             email_context,
                             recipients=email)

        return Response(order_history.order_id,
                        status=status.HTTP_200_OK)


# class DownloadContentView(ObjectDownloadView, APIView):
#     http_method_names = ['get']
#     permission_classes = []
#     model = BoughtItems

#     def get(self, request, *args, **kwargs):
#         order_id = kwargs.get('order_id')
#         video_id = kwargs.get('pk')
#         video = Video.objects.get(pk=video_id)
#         content_type = ContentType.objects.get_for_model(video)
#         try:
#             item = BoughtItems.objects.get(order__order_id=order_id,
#                                            content_type=content_type,
#                                            object_id=video_id)
#         except BoughtItems.DoesNotExist:
#             return Response(status=status.HTTP_401_UNAUTHORIZED)
#         self.object = item.item
#         self.file_field = 'video'
#         return super(ObjectDownloadView, self).get(request, *args, **kwargs)


from django.conf import settings
from django import http
from django.views.generic import RedirectView

import boto3
from botocore.client import Config

import os


# class DownloadContentView(RedirectView):
#     def get(self, request, *args, **kwargs):
#         if not request.user.is_authenticated():
#             raise http.Http404

#         order_id = kwargs.get('order_id')
#         video_id = kwargs.get('pk')
#         video = Video.objects.get(pk=video_id)
#         content_type = ContentType.objects.get_for_model(video)
#         # check if this user ever ordered this video
#         try:
#             item = BoughtItems.objects.get(order__order_id=order_id,
#                                            order__user=request.user,
#                                            content_type=content_type,
#                                            object_id=video_id)
#         except BoughtItems.DoesNotExist:
#             raise http.Http404

#         s3 = boto3.client('s3',
#                           config=Config(signature_version='s3v4',
#                                         s3={'addressing_style': 'virtual'}),
#                           aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
#                           aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
#                           region_name=settings.AWS_REGION)

#         url = s3.generate_presigned_url(ClientMethod='get_object',
#                                         Params={'Bucket': settings.AWS_ORIGINAL_VIDEO_S3_BUCKET,
#                                                 'Key': os.path.basename(item.item.video_link)},
#                                         ExpiresIn=60)
#         if not url:
#             return http.HttpResponseGone()

#         return http.HttpResponseRedirect(url)

class DownloadContentView(APIView):
    http_method_names = ['get']
    permission_classes = [permissions.IsAuthenticated]
    model = BoughtItems

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        order_id = kwargs.get('order_id')
        video_id = kwargs.get('pk')
        video = Video.objects.get(pk=video_id)
        content_type = ContentType.objects.get_for_model(video)
        # check if this user ever ordered this video
        try:
            item = BoughtItems.objects.get(order__order_id=order_id,
                                           order__user=request.user,
                                           content_type=content_type,
                                           object_id=video_id)
        except BoughtItems.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        s3 = boto3.client('s3',
                          config=Config(signature_version='s3v4',
                                        s3={'addressing_style': 'virtual'}),
                          aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                          aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
                          region_name=settings.AWS_REGION)

        url = s3.generate_presigned_url(ClientMethod='get_object',
                                        Params={'Bucket': settings.AWS_ORIGINAL_VIDEO_S3_BUCKET,
                                                'Key': os.path.basename(item.item.video_link)},
                                        ExpiresIn=60)
        if not url:
            return Response(status=status.HTTP_404_NOT_FOUND)

        return Response({'url': url}, status=status.HTTP_200_OK)


class ApplyPromoCodeView(APIView):
    http_method_names = ['patch', ]
    permission_classes = [permissions.IsAuthenticated]
    model = PromoCodeOffer
    serializer_class = PromoCodeOfferSerializer

    def patch(self, request):
        if hasattr(request.data, '_mutable'):
            request.data._mutable = True
        request.data['user'] = request.user.id

        try:
            offer = PromoCodeOffer.objects.get(user=request.user,
                                               promo_code__code=request.data['promo_code'],
                                               applied=False)
        except PromoCodeOffer.DoesNotExist:
            return Response("Promocode seems expired or invalid",
                            status=status.HTTP_403_FORBIDDEN)
        else:
            offer.applied = True
            offer.applied_time = timezone.now()
            offer.save()
        return Response(status=status.HTTP_200_OK)


class CheckSavedCardView(APIView, StripeUtils):
    http_method_names = ['get', ]
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        user = request.user
        if hasattr(user, 'user_profile'):
            if not (user.user_profile.remember_card and
                    user.user_profile.stripe_customer_id):
                return Response('You have no saved card available',
                                status=status.HTTP_401_UNAUTHORIZED)

            card_info = self.get_customer_saved_card_info(user.user_profile.stripe_customer_id)
            if not card_info:
                return Response('You have no saved card available',
                                status=status.HTTP_401_UNAUTHORIZED)
            return Response(card_info, status=status.HTTP_200_OK)

        return Response('Service not available',
                        status=status.HTTP_401_UNAUTHORIZED)
