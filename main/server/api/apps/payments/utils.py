import stripe

from django.conf import settings

stripe.api_key = settings.STRIPE_SECRET_KEY


class StripeUtils(object):
    STRIPE_ERROR = stripe.StripeError

    def charge_card(self, amount, description='', token=None, customer=None):
        amount = int(float(amount) * 100)
        if customer:
            charge = stripe.Charge.create(
                amount=amount,
                currency="usd",
                description=description,
                customer=customer
            )
        elif token:
            charge = stripe.Charge.create(
                amount=amount,
                currency="usd",
                source=token,
                description=description
            )
        return charge

    def create_customer(self, token, description=''):
        customer = stripe.Customer.create(
            source=token,
            description=description
        )

        return customer.id

    def update_customer_card(self, cust_id, token):
        customer = stripe.Customer.retrieve(cust_id)
        obj = customer.sources.create(source=token)
        new_card_id = obj['id']
        customer['default_source'] = new_card_id
        customer.save()
        return new_card_id

    def get_user_customer_id(self, user):
        if hasattr(user, 'user_profile'):
            return user.user_profile.stripe_customer_id
        return None

    def get_customer_saved_card_info(self, cust_id):
        customer = stripe.Customer.retrieve(cust_id)
        for src in customer['sources']['data']:
            if customer['default_source'] != src['id']:
                continue
            return {'brand': src['brand'],
                    'last4': src['last4']}
        return None
