from django.contrib.contenttypes.models import ContentType
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from api.apps.videos.models import Video
from api.apps.contests.models import Contest
from api.apps.contests.serializers import ContestSerializer
from api.apps.videos.serializers import VideoBriefSerializer
from api.apps.carts.models import Cart
from models import OrderHistory, PromoCodeOffer, BoughtItems


class BoughtItemSerializer(serializers.ModelSerializer):

    details = serializers.SerializerMethodField()
    content_type = serializers.SerializerMethodField()

    class Meta:
        model = BoughtItems
        fields = ('details', 'content_type', 'price_paid')

    def get_content_type(self, obj):
        return obj.content_type.model

    def get_details(self, obj):
        if obj.content_type.model == 'video':
            video = Video.objects.get(id=obj.object_id)
            data = VideoBriefSerializer(video, context={'request': self.context['request']})
        else:
            contest = Contest.objects.get(id=obj.object_id)
            data = ContestSerializer(contest, context={'request': self.context['request']})
        return data.data


class OrderHistorySerializers(serializers.ModelSerializer):
    items = serializers.SerializerMethodField()

    class Meta:
        model = OrderHistory
        fields = ('order_id', 'items', 'created')

    def get_items(self, obj):
        return BoughtItemSerializer(obj.get_order_items.all(),
                                    context={'request': self.context['request']}, many=True).data


class PlaceOrderSerializer(serializers.Serializer):
    cart = serializers.IntegerField()
    amount_paid = serializers.DecimalField(max_digits=6, decimal_places=2)
    email = serializers.EmailField()
    name = serializers.CharField(max_length=120)
    token = serializers.CharField(required=False)
    card_token = serializers.CharField(required=False)

    def validate_cart(self, attr):
        try:
            cart = Cart.objects.get(id=attr)
        except Cart.DoesNotExist:
            raise ValidationError('Cart does not exist')
        return attr

    def validate_token(self, attr):
        if not self.context['request'].data.get('use_saved_card') and not attr:
            raise ValidationError('This field is required')
        else:
            return attr

    def validate_card_token(self, attr):
        if not self.context.get('request').data.get('use_saved_card') and not attr:
            raise ValidationError('This field is required')
        else:
            return attr

    def validate(self, validated_data):
        user = self.context['request'].user
        use_saved_card = self.context['request'].data.get('use_saved_card')
        cart = validated_data.get('cart')
        amount_paid = validated_data.get('amount_paid')
        cart = Cart.objects.get(id=cart)
        total = 0
        for item in cart.get_items.all():
            total = total + item.total_price
        if total != amount_paid:
            raise ValidationError('Amount mismatch')
        if use_saved_card and user.is_authenticated() and hasattr(user, 'user_profile'):
            if not (user.user_profile.remember_card and user.user_profile.stripe_customer_id):
                raise ValidationError('You have no saved card available')
        return validated_data


class PromoCodeOfferSerializer(serializers.ModelSerializer):
    promo_code = serializers.CharField()

    class Meta:
        model = PromoCodeOffer

