from rest_framework import routers

from django.conf.urls import patterns, include, url

from views import OrderHistoryView, PlaceOrderView, DownloadContentView, ApplyPromoCodeView, CheckSavedCardView


router = routers.DefaultRouter()

# Register api urls to router here
router.register(r'order-history', OrderHistoryView, base_name='order-history')
router.register(r'place-order', PlaceOrderView, base_name='place-order')

urlpatterns = [
    url(r'^apply-promo-code/$', ApplyPromoCodeView.as_view (), name='apply-promo-code'),
    url(r'^check-saved-card/$', CheckSavedCardView.as_view(), name='check-saved-card'),
    # url(r'^place-order/$', PlaceOrderView.as_view (), name='place-order'),
    url(r'^download-content/(?P<order_id>.*)/(?P<pk>\d+)$', DownloadContentView.as_view (), name='download-content'),
    url('', include(router.urls)),
]