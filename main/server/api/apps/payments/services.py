from api.apps.videos.models import Video

from api.apps.reputations.level_privileges import LevelPrivilegeFilter

from .models import Transactions


class PaymentsBase(object):
    pass


class PaymentService(PaymentsBase):
    """
    Service methods for tracking users transactions(profit to be given monthly wise)
    """
    def __init__(self, video_id, original_paid):
        self.original_paid = original_paid
        self.setup(video_id)

    def setup(self, video_id):
        self.video = Video.objects.get(id=video_id)
        self.user = self.video.user

    def make_an_entry_to_paid_for_owner(self):
        """
        Amount to be paid is calculated from original paid amount with ref
        to default config commission
        :return:
        """
        level_service = LevelPrivilegeFilter(self.user)
        amount_to_be_paid = level_service.get_the_amount_to_be_paid_to_owner(self.original_paid)

        transaction = Transactions()
        transaction.original_amount_paid_buyer = self.original_paid
        transaction.price_calculated = amount_to_be_paid
        transaction.video_id = self.video.id
        transaction.user = self.user
        transaction.save()

        return amount_to_be_paid


