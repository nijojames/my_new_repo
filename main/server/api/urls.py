"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from flaggit.views import FlagView
from notifications import urls as notification_urls

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.views.generic import TemplateView, RedirectView
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView

from apps.profiles.views import FacebookLogin, TwitterLogin, GoogleLogin, InstagramLogin, CustomLoginView, CustomRegisterView

from apps.payments.views import DownloadContentView
from knox.views import LogoutView

# from apps.video import views as video_views
admin.site.site_header = 'Spyhop administration'
admin.site.site_title = 'Spyhop site admin'

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name="index.html"), name='home'),
    # url(r'^djangular/', include('djangular.urls')),
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/', include('admin_honeypot.urls',
                            namespace='admin_honeypot')),
    # ToDo: change it before production
    url(r'^shadmin/', include(admin.site.urls)),
    # url(r'^', include('django.contrib.auth.urls')),
    url(r'^api/videos/', include('api.apps.videos.urls')),
    url(r'^api/contests/', include('api.apps.contests.urls')),
    url(r'^api/profiles/', include('api.apps.profiles.urls')),
    url(r'^api/contact-us/', include('api.apps.contact.urls')),
    url(r'^api/accounts/', include('allauth.urls')),
    url(r'^api/flaggit/', include('api.apps.flaggit.urls')),
    url(r'^api/likes/', include('api.apps.userlikes.urls')),
    url(r'^api/payments/', include('api.apps.payments.urls')),
    url(r'^api/general/', include('api.apps.general.urls')),
    url(r'^api/reviews/', include('api.apps.reviews.urls')),
    url(r'^api/rest-auth/login/$', CustomLoginView.as_view(), name='rest_login'), # for the existing system
    url(r'^api/rest-auth/logout/$', LogoutView.as_view(), name='rest_logout'), # for the existing system
    url(r'^api/rest-auth/', include('rest_auth.urls')),
    url(r'^api/rest-auth/facebook/$', FacebookLogin.as_view(), name='fb_login'),
    url(r'^api/rest-auth/twitter/$', TwitterLogin.as_view(), name='tw_login'),
    url(r'^api/rest-auth/google/$', GoogleLogin.as_view(), name='ga_login'),
    url(r'^api/rest-auth/instagram/$', InstagramLogin.as_view(), name='im_login'),
    url(r'^api/rest-auth/registration/$', CustomRegisterView.as_view(), name='rest_register'),
    url(r'^api/rest-auth/registration/',
        include('rest_auth.registration.urls')),
    url('^flag/$', FlagView.as_view(), name='flaggit'),
    url('^activity/', include('actstream.urls')),
    url('^', include('api.apps.activities.follow_urls')),
    url('^api/activities/', include('api.apps.activities.urls')),
    url('^notifications/', include(notification_urls, namespace='notifications')),
    url('^api/cart/', include('api.apps.carts.urls')),
    url(r'^api/auth/login/', CustomLoginView.as_view(), name='rest_login'),
    url('^api/auth/', include('knox.urls')),

    # url(r'^auth/', include('rest_framework_social_oauth2.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += patterns('',
        (r'^static/(.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
        (r'^angular/(.*)$', 'django.views.static.serve', {'document_root': settings.ANGULAR_WEB_ROOT}),
    )

if settings.DEBUG is True:
    urlpatterns += url(r'^api/docs/', include('rest_framework_swagger.urls')),

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))
