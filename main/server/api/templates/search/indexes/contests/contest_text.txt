{{ object.type }}
{{ object.status }}
{{ object.award_amount }}
{{ object.contest_title }}
{{ object.concept }}
{{ object.permission }}
{{ object.orientation }}
{{ object.video_type }}
{{ object.resolution }}
{{ object.level_required }}
{{ object.created }}
{{ object.user }}
{{ object.private }}
{% for videographer in object.invited_videographers.all %}
    {{ videographer.videographers.user }}
{% endfor %}

