# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0002_profile_type'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contests', '0004_auto_20160121_1256'),
        ('videos', '0004_auto_20160125_1416'),
    ]

    operations = [
        migrations.CreateModel(
            name='Follow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(auto_now_add=True)),
                ('target_contest', models.ForeignKey(related_name='follow_contest', blank=True, to='contests.Contest', null=True)),
                ('target_profile', models.ForeignKey(related_name='follow_profile', blank=True, to='profiles.Profile', null=True)),
                ('target_video', models.ForeignKey(related_name='follow_video', blank=True, to='videos.Video', null=True)),
                ('user', models.ForeignKey(related_name='following', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
