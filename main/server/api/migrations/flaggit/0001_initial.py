# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Flag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.PositiveIntegerField()),
                ('status', models.PositiveIntegerField(default=1, choices=[(1, b'Flagged'), (2, b'Under review'), (3, b'Rejected'), (4, b'Approved')])),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('reviewed', models.DateTimeField(null=True, blank=True)),
                ('comment', models.TextField(null=True, blank=True)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('reviewer', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='FlagInstance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ip', models.IPAddressField(null=True, blank=True)),
                ('datetime', models.DateTimeField(auto_now_add=True)),
                ('comment', models.TextField(null=True, blank=True)),
                ('flag', models.ForeignKey(related_name='flags', to='flaggit.Flag')),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
    ]
