#!/usr/bin/env bash

cd /home/docker/src/clients/web/
bower --allow-root update

cd /home/docker/src/server/
pip install -r /home/docker/src/server/requirements.txt

rm /etc/nginx/sites-enabled/default
ln -s /home/docker/src/server/nginx-app.conf /etc/nginx/sites-enabled/
ln -s /home/docker/src/server/supervisor-app.conf /etc/supervisor/conf.d/
mkdir -p /home/docker/src/server/logs/

if [ "$ENV_RUN_MGMT_CMDS" == "true" ]; then
    python /home/docker/src/server/manage.py collectstatic --noinput
    python /home/docker/src/server/manage.py makemigrations --noinput
    python /home/docker/src/server/manage.py migrate --noinput
fi

if [ "$ENV_COPY_SRC_FROM_HOST" == "true" ]; then
    cp -r /home/docker/src /home/docker/
fi

supervisord -n
