"""
Selenium related base class.
"""
from urlparse import urlparse

from django.conf import settings
from django.core.urlresolvers import reverse
from django.test import Client, SimpleTestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait

from test.ft import config


class Sessions(object):
    """
    Stored sessions. Singleton.
    """

    _ids = {}  # session cookies by `email`

    def __new__(cls):
        """
        Creation
        """
        if not hasattr(cls, 'email'):
            cls.instance = super(Sessions, cls).__new__(cls)
        return cls.instance

    def has_id(self, email):
        """
        Checks if the session cookie for specific user is stored

        :param email: email of specific user
        :type email: string
        """
        return email in self._ids

    def add_id(self, email, morsel):
        """
        Adds/updates the session cookie for specific user by converting a
        Morsel cookie

        :param email: email of specific user
        :type email: string
        :param morsel: Morsel cookie object
        :type morsel: http.cookies.Morsel
        """
        uri = urlparse(settings.DJANGO_TEST_SERVER_ADDRESS)
        self._ids[email] = {
            'name': morsel.key,
            'value': morsel.value,
            'path': getattr(morsel, 'path', '/'),
            'domain': getattr(morsel, 'domain', uri.netloc.split(':')[0]),
            'secure': getattr(morsel, 'secure', False),
            'expiry': getattr(morsel, 'expires', None)
        }

    def get_id(self, email):
        """
        Returns the session cookie for specific user

        :param email: email of specific user
        :type email: string
        """
        return self._ids.get(email, None)

    def remove_id(self, email):
        """
        Removes the session cookie for specific user

        :param email: email of specific user
        :type email: string
        """
        self._ids.pop(email, None)


class WebDriverSingleton(object):
    user_agent = config.BROWSER_USER_AGENT

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(WebDriverSingleton, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        self.profile = webdriver.FirefoxProfile()
        self.profile.set_preference("general.useragent.override", self.user_agent)
        self.profile.set_preference('browser.cache.memory.enable', True)
        self.profile.set_preference('browser.cache.disk.enable', True)
        self.profile.set_preference('browser.cache.offline.enable', True)
        self.profile.set_preference('network.http.use-cache', True)
        self.profile.set_preference('browser.cache.disk.parent_directory', '/tmp')
        self.browser = webdriver.Firefox(self.profile)
        self.browser.desired_capabilities["applicationCacheEnabled"] = True
        self.browser.set_window_size(1024, 768)

    def teardown(self):
        self.browser.quit()


class BrowserMixin(object):
    """
    Browser integrates methods of Selenium to make interaction with browser easier.
    """
    _sessions = Sessions()


    def wait_for_css_selector(self, selector):
        """
        Wait until webdriver loads a HTML element with specified CSS selector

        :param webdriver: Selenium webdriver
        :param selector: Specified CSS selector
        """
        WebDriverWait(self.browser, settings.TEST_SELENIUM_WAIT_TIMEOUT).until(
            lambda driver: len(driver.find_elements_by_css_selector(selector)),
            "URL: %s | Waiting for %s, but didn't show up in time" % (
                self.browser.current_url, selector
            )
        )

    def find_element(self, selector):
        """
        Find single element and return it.
        """
        return self.browser.find_element_by_css_selector(selector)

    def click(self, selector):
        """
        find a field on the page to click, like link, button, text input, check box, radio input.
        :selector: css selector to locate the field
        """
        field = self.browser.find_element_by_css_selector(selector)
        field.click()
        return field

    def open(self):
        """
        close the current running browser,
        and open a new one.
        """
        if hasattr(self, 'browser') and self.browser:
            self.close()
        self.browser = WebDriverSingleton().browser
        self.browser.maximize_window()

    def close(self):
        """
        close the current running browser.
        """
        if self.browser:
            self.browser.quit()
            self.browser = None

    def current_url(self):
        return self.browser.current_url

    def is_displayed(self, selector):
        element = self.browser.find_element_by_css_selector(selector)
        return element.is_displayed()


class Browser(BrowserMixin):
    def __init__(self, *args, **kwargs):
        self.open()


class SeleniumTestCase(BrowserMixin, StaticLiveServerTestCase, SimpleTestCase):
    """
    Base class for our selenium test cases, providing the browser instance.
    """

    browser = None
    user_agent = config.BROWSER_USER_AGENT
    base_url = settings.DJANGO_TEST_SERVER_ADDRESS

    @classmethod
    def setUpClass(cls):
        cls.browser = WebDriverSingleton().browser
        cls.browser.delete_all_cookies()
        super(SeleniumTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(SeleniumTestCase, cls).tearDownClass()
        cls.browser.close()

