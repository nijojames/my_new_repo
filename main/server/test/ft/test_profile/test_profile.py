import os
import time

from django.conf import settings
from django.core.urlresolvers import reverse

from django.contrib.auth.models import User

from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
from django.test import Client

from test.ft import config
from test.ft import utils
from api.utl.utl import UtlRandom, UtlTest
from test.ft.base_selenium import SeleniumTestCase
from api.apps.videos.tests import TestUtils


class RegisterLoginTest(SeleniumTestCase, TestUtils, UtlTest):

    def setUp(self):
        self.random = UtlRandom(os.environ.get('TEST_SEED'))
        self.test_password = self.random.random_str(10)
        self.test_email = self.random.random_email(10)
        self.test_first_name = self.random.random_name(10)
        self.test_last_name = self.random.random_name(10)

    def test_login_register_forgot_password(self):
        self.browser.get(self.live_server_url)

        # CHECK INVALID LOGIN
        login_popup = self.browser.find_element_by_id('login_link')
        login_popup.click()
        time.sleep(5)
        utils.do_login(self.browser, self.random.random_email(10), self.random.random_str(10))
        elements = self.browser.find_elements_by_css_selector('p[ng-bind="general_error"]')
        self.browser.find_element_by_id('login_close').click()
        time.sleep(5)

        # DO SIGNUP
        signup_popup = self.browser.find_element_by_id('signup_link')
        signup_popup.click()
        time.sleep(5)
        utils.do_register(self.browser, self.test_first_name, self.test_last_name,
                          self.test_email, self.test_password)
        time.sleep(5)

        logout_link = self.browser.find_element_by_id('logout_link')
        if not logout_link.is_displayed():
            self.assertGreater(len(logout_link), 0, "Element %s does not exists!" % logout_link)
        else:
            logout_link.click()
            self.assertTrue(logout_link.is_displayed(), 'Not properly logged out')
        time.sleep(5)

        #check valid login
        login_popup = self.browser.find_element_by_id('login_link')
        login_popup.click()
        time.sleep(5)
        utils.do_login(self.browser, self.test_email, self.test_password)
        time.sleep(5)
        logout_link = self.browser.find_element_by_id('logout_link')
        if not logout_link.is_displayed():
            self.assertGreater(len(logout_link), 0, "Element %s does not exists!" % logout_link)
        logout_link.click()

    def test_forgot_password(self):
        self.register_user()
        # Forgot password invalid email
        self.browser.get(self.live_server_url + '#forgot-password')
        utils.forgot_password_action(self.browser, self.random.random_email(10))
        self.browser.find_element_by_css_selector('strong[ng-repeat="error in emailErrors"]').is_displayed()
        time.sleep(5)
        # Forgot password valid email
        time.sleep(5)
        # utils.forgot_password_action(self.browser, 'krishna.sayone@gmail.com')
        # time.sleep(10)
        # elements = self.browser.find_elements_by_css_selector('strong[class="ng-binding"]')
        # self.wait_for_css_selector('strong[class="ng-binding"]')
        # self.assertGreater(len(elements), 0, "Element %s does not exists!" % elements)

    @classmethod
    def tearDownClass(cls):
        super(SeleniumTestCase, cls).tearDownClass()
        cls.browser.close()
