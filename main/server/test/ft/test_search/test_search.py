import os
import time

from selenium import webdriver
import selenium.webdriver.support.expected_conditions as EC
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.by import By
from django.conf import settings
from django.core.urlresolvers import reverse

from django.contrib.auth.models import User
from rest_framework import status

from test.ft import config
from test.ft import utils
from api.utl.utl import UtlRandom, UtlTest
from test.ft.base_selenium import SeleniumTestCase
from api.apps.videos.tests import TestUtils


class SearchTest(SeleniumTestCase, TestUtils, UtlTest):

    def setUp(self):
        self.random = UtlRandom(os.environ.get('TEST_SEED'))
        self.test_key = self.random.random_str(10)

    def test_header_search(self):
        self.browser.get(self.live_server_url)
        wait = ui.WebDriverWait(self.browser, 10)
        search_box = self.browser.find_element_by_id('clear')
        search_box.send_keys(self.test_key)
        submit_button = self.browser.find_element_by_xpath("//button[contains(@class, 'btn btn-default btn-orange') "
                                                           "and text()='Submit']")
        submit_button.click()
        wait.until(lambda driver: self.browser.find_element_by_id('no_results'))
        self.register_user()
        self.post_video(status.HTTP_201_CREATED)
        title = self.data.get('title')
        slug = self.data.get('slug')
        self.build_search_index()
        time.sleep(5)
        # title = 'nature'
        self.browser.get(self.live_server_url)
        time.sleep(5)
        search_box = self.browser.find_element_by_id('clear')
        search_box.send_keys(title)
        submit_button = self.browser.find_element_by_xpath("//button[contains(@class, 'btn btn-default btn-orange') "
                                                           "and text()='Submit']")
        submit_button.click()
        time.sleep(10)
        videos = self.browser.find_elements(By.CLASS_NAME, 'thevideo')
        el = videos[0]
        self.assertGreater(len(videos), 0, "Element %s does not exists!" % videos)
        # self.browser.get(self.live_server_url + '/#/video/' + slug)
        video_detail_links = self.browser.find_elements_by_class_name('video_link')
        el = video_detail_links[0]
        self.browser.maximize_window()
        # # webdriver.ActionChains(self.browser).move_to_element(el).click(el).perform()
        video_detail_links[0].click()


    @classmethod
    def tearDownClass(cls):
        super(SeleniumTestCase, cls).tearDownClass()
        # cls.browser.close()
