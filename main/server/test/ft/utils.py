import time

def do_login(webdriver, email, password,
             email_dom_id='id_email', password_dom_id='Password'):
    """
    Performs a login given that selenium webdriver, already loaded (get) the
    login page by URL

    :param webdriver: Selenium webdriver
    :param username_or_email: Text to be entered in email's input element.
    :param password: Text to be entered in password's input element.
    :param username_dom_id: HTML DOM id to identify the email input element.
    :param password_dom_id: HTML DOM id to identify the password input element.
    """
    # Fills Login form
    login_input = webdriver.find_element_by_id(email_dom_id)
    password_input = webdriver.find_element_by_id(password_dom_id)

    login_input.click()
    login_input.clear()
    login_input.send_keys(email)

    password_input.click()
    password_input.clear()
    password_input.send_keys(password)

    # Sign in
    submit_button = webdriver.find_element_by_id('login_submit')
    submit_button.click()


def do_register(webdriver, first_name, last_name, email, password):
    """
    Performs a user registration given that selenium webdriver, already loaded (get) the
    login page by URL

    :param webdriver: Selenium webdriver
    :param first_name: Text to be entered in first_name input element.
    :param last_name: Text to be entered in last_name input element.
    :param email: Text to be entered in email input element
    :param password: Text to be entered in password, confirm_password input element
    """
    # Fills Login form
    first_name_input = webdriver.find_element_by_id('first_name')
    last_name_input = webdriver.find_element_by_id('last_name')
    email_input = webdriver.find_element_by_id('email')
    password_input = webdriver.find_element_by_id('new_password')
    confirm_password_input = webdriver.find_element_by_id('confirm_password')
    first_name_input.send_keys(first_name)
    last_name_input.send_keys(last_name)
    email_input.send_keys(email)
    password_input.send_keys(password)
    confirm_password_input.send_keys(password)
    webdriver.find_element_by_css_selector("label[for='radio-2']").click()
    webdriver.find_element_by_css_selector("label[for='checkbox-2']").click()

    # Sign up
    submit_button = webdriver.find_element_by_id('email_signup_submit')
    submit_button.click()


def forgot_password_action(webdriver, email):
    """
    Performs actions related to forgot password
    :param webdriver:
    :param email: Email ID to be entered to get password reset link
    """
    email_input = webdriver.find_element_by_id('emailInput')
    email_input.clear()
    email_input.send_keys(email)
    submit_button = webdriver.find_element_by_id('send_reset_email')
    submit_button.click()
