#!/bin/bash

# NB: it needs a data volume named "elasticsearch_data"
#     docker volume create --name elasticsearch_data
docker rm -f se
docker run -d -v elasticsearch_data:/usr/share/elasticsearch/data -p 9200:9200 -p 9300:9300 --name=se "spyhop/elasticsearch" -Des.network.host=0.0.0.0
