boto3==1.3.1
boto==2.26.1
django-allauth==0.24.1
django-braces==1.8.1
django-haystack==2.4.1
django-oauth-toolkit==0.9.0
django-rest-auth==0.6.0
django-rest-framework-social-oauth2==1.0.2
django-rest-swagger==0.3.4
django-storages==1.1.8
django-taggit==0.17.3
django-taggit-serializer==0.1.5
djangorestframework==3.2.4
drf-haystack==1.5.5
elasticsearch==1.9.0
envdir==0.7
gevent==1.0.2
greenlet==0.4.9
oauthlib==1.0.1
psycopg2==2.5.2
PyJWT==1.4.0
python-openid==2.2.5
python-social-auth==0.2.13
PyYAML==3.11
requests==2.8.1
requests-oauthlib==0.5.0
six==1.10.0
urllib3==1.12
uWSGI==2.0.11.2
wheel==0.24.0
django-likes==0.2
django-flaggit==0.0.3
django-activity-stream==0.6.0
git+https://github.com/mcroydon/django-qsstats.git
python-dateutil==2.4.2
django-extensions==1.6.1
django-voting==0.2
django-follow==0.6.1
django-notifications-hq==1.0
django-analytical==2.0.0
django-solo==1.1.2
django-autoslug==1.9.3
Pillow==3.1.1
django-geoposition==0.2.2
django-cart==1.0.3
django-downloadview==1.9
django-dirtyfields==0.8.2
django-filter==0.13.0
django-push-notifications==1.4.1
celery==3.1.23
redis==2.10.5
django-celery==3.1.17
Django==1.8.5
stripe==1.32.2
django-import-export==0.4.5
django-admin-honeypot==1.0.0
django-rest-knox==2.2.0
