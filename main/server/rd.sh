#!/bin/bash

envfile=$1
if [ $# -eq 0 ] ; then
    echo "ERROR: no --env-file specified."
    echo "Usage: $0 [dev.env | prod.env | local.env]"
    exit
fi
cat $envfile | grep -Ev '#|unset' | cut -d' ' -f2 > dockers.env
envfile=dockers.env
cat $envfile
docker-compose stop
docker-compose up -d
echo "Use: 'docker-compose logs' to check the logs"
