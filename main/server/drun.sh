#!/bin/bash

#docker run -d -p 80:80 --env-file=dev.env --name=sm "spyhop/main"
#docker run -d -p 80:80 --env-file=prod.env --name=sm "spyhop/main"
envfile=$1
if [ $# -eq 0 ] ; then
    echo "ERROR: no --env-file specified"
    echo "Usage: $0 [dev.env | prod.env]"
    exit
fi
cat $envfile | grep -Ev '#|unset' | cut -d' ' -f2 > temp_env_file
envfile=temp_env_file
cat $envfile
docker rm -f sm
docker run -d -p 80:80 --env-file=$envfile -v $PWD/../:/home/docker/src --name=sm "spyhop/main"
