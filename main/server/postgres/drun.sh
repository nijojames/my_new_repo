#!/bin/bash

# NB: it needs a data volume named "postgres_data"
#     docker volume create --name postgres_data
docker rm -f sp
docker run -d -v postgres_data:/var/lib/postgresql/data -p 5432:5432 --name=sp "spyhop/postgres"
