#!/bin/bash

# NB: it needs a data volume named "redis_data"
#     docker volume create --name redis_data
docker rm -f sr
docker run -d -v redis_data:/data -p 6379:6379 --name=sr "spyhop/redis"
