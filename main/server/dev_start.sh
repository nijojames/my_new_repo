#!/usr/bin/env bash
cd /home/docker/server
./manage.py collectstatic --noinput
./manage.py makemigrations --noinput
./manage.py migrate --noinput

/home/docker/server/start.sh

