exports.config = {
    allScriptsTimeout: 11000,
    
    //The address of a running selenium server.
    //seleniumAddress: 'http://localhost:4444/wd/hub',
    
    specs: [
	'*.js'
    ],
    
    capabilities: {
	'browserName': 'chrome'
	//'browserName': 'firefox'
    },
    
    baseUrl: 'http://localhost:8000/',
    
    framework: 'jasmine',
    
    jasmineNodeOpts: {
	defaultTimeoutInterval: 30000,
	onComplete: null,
	isVerbose: false,
	showColors: true,
	includeStackTrace: true
    },
};
