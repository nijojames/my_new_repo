'use strict';

describe('SpyHop Web', function() {
    var userEmail="";
    var userPassword="";
    
    var baseUrl = 'http://127.0.0.1:8000/';
    if (typeof process.env.SPYHOP_URL !== "undefined") {
	baseUrl = process.env.SPYHOP_URL;
    }
    console.log("***************************");
    console.log("INFO: Using url=" + baseUrl); 
    console.log("***************************");

    // helper functions
    var waitForUrlToChangeTo = function(urlRegex) {
	browser.wait(function() {
            return browser.getCurrentUrl().then(function(url) {
                return (urlRegex).test(url);
            });
	});
    };
    
    var waitUntilElementReady = function(elm, msg) {
        browser.wait(function () {
	    console.log("INFO: isPresent="+msg);
            return elm.isPresent();
        },10000);
        browser.wait(function () {
	    /*elm.getText().then(function (text) {
	      console.log(text);
	      });*/
	    console.log("INFO: isDisplayed="+msg);
            return elm.isDisplayed();
        },10000);
    };

    var getRandomNum = function(min, max){
	return(Math.floor(Math.random()*(max-min+1)+min));
    };

    var getRandomStrHelper = function(len, val) {
        var str = "";
	console.assert(len!=="undefined");
        for (var i = 0; i < len; i++) {
	    str = str + val.charAt(Math.round(val.length * Math.random()));
        }
        return str;
    };

    var getRandomStr = function(len) {
	return getRandomStrHelper(len, "abcdefghijklmnopqrstuvwxyz0123456789");
    };

    var getRandomAlphaStr = function(len) {
	return getRandomStrHelper(len, "abcdefghijklmnopqrstuvwxyz");
    };

    
    // NB: if sendkeys is sending keys partially,
    //     then use this function as a workaround
    var sendKeysCharByChar = function(str, elm){
        str.split('').forEach((c) => elm.sendKeys(c));
    }

    var clickWhenClickable = function(element) {
	return browser.wait(function() {
	    return element.click().then(
		function() {
		    return true;
		},
		function() {
		    console.log('INFO: not clickable');
		    return false;
		});
	});
    };
    
    ///////////////////////////////////////////////
    // testcases
    ///////////////////////////////////////////////
    it('Successful Signup Test', function(done) {
	console.log("INFO: running signup test");
	browser.get(baseUrl);
	var signup_popUp = ($('[data-target="#sign_up"]'));
	signup_popUp.click();

	var elm = "";
	var val = "";
	
	elm = element(by.id('email_signup_submit'));
	waitUntilElementReady(elm, "email_signup_submit");
        expect(elm.isDisplayed());
	var signupButton = elm;

	elm = element(by.model('form.first_name'));
	waitUntilElementReady(elm, "first_name");
        expect(elm.isDisplayed());
	val = getRandomAlphaStr(getRandomNum(1,30));
	console.log("INFO: firstname="+val);
	elm.sendKeys(val);

	elm = element(by.model('form.last_name'));
	waitUntilElementReady(elm, "last_name");
	expect(elm.isDisplayed());
	val = getRandomAlphaStr(getRandomNum(1,30));
	console.log("INFO: lastname="+val);
	elm.sendKeys(val);

	elm = element(by.model('form.email'));
	waitUntilElementReady(elm, "email");
	expect(elm.isDisplayed());
	userEmail = (getRandomStr(getRandomNum(2,30))+
		     "@"+
		     getRandomStr(getRandomNum(2,30))+
		     ".com");
        //elm.sendKeys(userEmail);
	sendKeysCharByChar(userEmail, elm);

	elm = element(by.model('form.password1'));
	waitUntilElementReady(elm, "password1");
	expect(elm.isDisplayed());
	userPassword = getRandomStr(getRandomNum(8,30));
        elm.sendKeys(userPassword);

	elm = element(by.model('form.password2'));
	waitUntilElementReady(elm, "password2");
	expect(elm.isDisplayed());
        elm.sendKeys(userPassword);

	elm = element(by.css("label[for='radio-2']"));
	waitUntilElementReady(elm, "radio");
	expect(elm.isDisplayed());
	elm.click();

	elm = element(by.css("label[for='checkbox-2']"));
	waitUntilElementReady(elm, "checkbox");
	expect(elm.isDisplayed());
        elm.click();

	signupButton.click();
	waitForUrlToChangeTo(/profile/);

	expect(browser.getCurrentUrl()).toEqual(baseUrl+'#/profile');
	elm = element(by.id('logout_link'));
	waitUntilElementReady(elm, "logout_link");
	expect(elm.isDisplayed());
	// 
	// ToDo: need to make the following working.
	//       somehow the logout link is not clickable.
	//
	//var EC = protractor.ExpectedConditions;
	//browser.wait(EC.elementToBeClickable(elm), 10000);
	//clickWhenClickable(elm);
	//elm.click();
	//browser.actions().mouseMove(elm).click();
	//waitForUrlToChangeTo(/home/);

	done()
    });

    // ToDo: once the signup test is able to logout
    //       uncomment this test case to use the same
    //       username and password for login test
    //
    /*
    it('Successful Login Test', function(done) {
	console.log("INFO: running login test");
	browser.get(baseUrl);
	browser.sleep(6000);
	
	var login_popUp = ($('[data-target="#myModal"]'));
	login_popUp.click();

	var email = element(by.model('email'));
	var password = element(by.model('password'));
	var loginButton = element(by.id('login_submit'));

	waitUntilElementReady(email, "email");
	email.clear();
	//console.log("INFO: userEmail="+userEmail);
	sendKeysCharByChar(userEmail, email);
	
	waitUntilElementReady(password, "password");
        password.clear();
	password.sendKeys(userPassword);

	waitUntilElementReady(loginButton,"login_submit");
        loginButton.click();
	
	waitForUrlToChangeTo(/profile/);
	
	expect(browser.getCurrentUrl()).toEqual(baseUrl+'#/profile');
	var logoutLink = element(by.id('logout_link'));
	waitUntilElementReady(logoutLink, "logout_link");
	browser.actions().mouseMove(logoutLink).click();
	//logoutLink.click();
	waitForUrlToChangeTo(/home/);
	
	done()
    });
    */
    
    
 /*
  it(' Invalid Login Test', function(done) {
    browser.get('http://127.0.0.1:8000/#/home');
    var login_popUp = ($('[data-target="#myModal"].ng-scope'));
    login_popUp.click();
    browser.sleep(3000);
    var email = element(by.model('email'));
    var password = element(by.model('password'));
    var loginButton = element(by.id('login_submit'));
        email.clear();
        password.clear();
        email.sendKeys('admin@admin.com');
        password.sendKeys('wrwer');
        loginButton.click();

    expect($('[ng-show=general_error].text-danger').isDisplayed()).toBeTruthy();
    done()
    browser.sleep(6000);

  });
  it(' Suucess Login Test', function(done) {
    browser.get('http://127.0.0.1:8000/#/home');
    var login_popUp = ($('[data-target="#myModal"].ng-scope'));
    login_popUp.click();
    browser.sleep(3000);
    var email = element(by.model('email'));
    var password = element(by.model('password'));
    var loginButton = element(by.id('login_submit'));
        email.clear();
        password.clear();
        email.sendKeys('admin@admin.com');
        password.sendKeys('admin');
        loginButton.click();

    browser.driver.wait(function() {
            return browser.driver.getCurrentUrl().then(function(url) {
                return (/profile/).test(url);
            });
        });
    expect(browser.getCurrentUrl()).toEqual('http://127.0.0.1:8000/#/profile');
    expect(element(by.id('logout_link')).isDisplayed());
    element(by.id('logout_link')).click();
  done()
  browser.sleep(6000);
  });
*/

    
/*
    it(' Forgot Password', function(done) {
    browser.get('http://127.0.0.1:8000/#/home');
    var login_popUp = ($('[data-target="#myModal"].ng-scope'));
    login_popUp.click();
    browser.sleep(6000);
    element(by.css(".reset_label")).click();
    expect(browser.getCurrentUrl()).toEqual('http://127.0.0.1:8000/#/forgot-password');
    var email = element(by.model('send_password_email'));
    var button = element(by.id('send_reset_email'));
    email.sendKeys('sfsdfsdfsdfsfd');
    button.click();
    browser.sleep(6000);
    expect(element(by.css('.alert-danger')).isDisplayed()).toBeTruthy();
    email.clear();
    email.sendKeys('admin@admin.com');
    button.click();
    browser.sleep(6000);
    expect(element(by.css('.alert-success')).isDisplayed()).toBeTruthy();
    done()
  });

  it(' Video Search Valid', function(done) {
    browser.get('http://127.0.0.1:8000/#/home');
    var query = element(by.model('q'));
    var button = element(by.css('.glyphicon.lense-icon-big.glyphicon-search'));
    query.sendKeys('light');
    button.click();
    browser.sleep(6000);
    expect(browser.getCurrentUrl()).toEqual('http://127.0.0.1:8000/#/videos-search?q=light');
    expect(element(by.css('.video-thumbs')).isDisplayed()).toBeTruthy();
    done()
  });
it(' Video Search Invalid', function(done) {
    browser.get('http://127.0.0.1:8000/#/home');
    var query = element(by.model('q'));
    var button = element(by.css('.glyphicon.lense-icon-big.glyphicon-search'));
    query.sendKeys('adadad');
    button.click();
    browser.sleep(6000);
    expect(browser.getCurrentUrl()).toEqual('http://127.0.0.1:8000/#/videos-search?q=adadad');
    expect(element(by.css('h1')).isDisplayed()).toBeTruthy();
    done()
  });

//  it('Contact Us', function(done) {
//    browser.get('http://127.0.0.1:8000/#/contact-us');
//    element.all(by.css('.custom_select')).click();
//    element.all(by.css('.options')).get(1).click();
//    var name = element(by.model('contactForm.name'));
//    var email = element(by.model('contactForm.email'));
//    var type = element(by.css("label[for='radio-3']"));
//    var message = element(by.model('contactForm.message'));
//    var button = element(by.css('.btn.btn-default.main-btn-1'));
//    name.sendKeys('Test');
//    email.sendKeys('test@test.com');
//    type.click();
//    message.sendKeys('Test');
//    button.click();
//    browser.sleep(6000);
//    expect(element(by.css('.contact-alert')).isDisplayed()).toBeTruthy();
//    done()
//  });
*/
});

