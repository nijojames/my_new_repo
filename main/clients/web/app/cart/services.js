angular.module('orderServices', [])
.service('OrderServices', function(){
   var promoCode = '';
   var checkingoutCart = ''
   var payableAmount = ''
   this.setOrderDetails = function(promo_code, cart, amount) {
      promoCode = promo_code;
      checkingoutCart = cart;
      payableAmount = amount;
   };
   this.getOrderDetails = function () {
       return {'promoCode': promoCode, 'checkingoutCart': checkingoutCart, 'payableAmount': payableAmount};
   }
});