'use strict';

angular.module('spyhop.cart', ['ngRoute', 'ui.router'])

    .controller('CartCtrl',
                ['$rootScope',
                 '$scope',
                 '$state',
                 //'$http',
                 '$location',
                 '$auth',
                 'OrderServices',
                 'Cart',
                 'HttpServices',
                 function ($rootScope,
                           $scope,
                           $state,
                           //$http,
                           $location,
                           $auth,
                           OrderServices,
                           Cart,
                           HttpServices) {
                     $scope.permissions = [{value:'royalty_free',
                                            label: 'Royalty Free'},

                                           {value:'commercial',
                                            label: 'Commercial'},

                                           {value: 'free',
                                            label: 'Free'}];
                     
                     Cart.get_cart().then(function(response) {
                         var cart_data = response.data;
                         $scope.cart = cart_data.results[0];
                         $rootScope.cart = cart_data.results[0];
                     }, function(response) {
                         console.log("ERROR: get_cart=", response.data);
                         alert("ERROR: Unable to access the cart now");
                     });

                     $scope.removeItem = function(item_id, item_type){
                         Cart.delete_from_cart(item_id, item_type)
                             .then(function(response) {
                                 //console.log("DEBUG: delete_cart=", response);
                                 var cart_data = response.data;
                                 if(cart_data) {
                                     $scope.cart = cart_data;
                                     $rootScope.cart = cart_data;
                                 }
                             }, function (response) {
                                 console.log("ERROR: delete_cart=", response.data);
                                 alert("ERROR: Unable to acess the cart now");
                             });
                     };

                     $scope.placeOrder = function(){
                         OrderServices.setOrderDetails($scope.promoCode,
                                                       $rootScope.cart.id,
                                                       $rootScope.cart.total_price);
                         $state.go('payment');
                     }
                 }])

    .controller('PlaceOrderCtrl',
                ['$rootScope',
                 '$scope',
                 '$state',
                 '$localStorage',
                 '$parse',
                 '$location',
                 '$auth',
                 '$window',
                 'toaster',
                 'OrderServices',
                 'Cart',
                 'Profile',
                 function ($rootScope,
                           $scope,
                           $state,
                           $localStorage,
                           $parse,
                           $location,
                           $auth,
                           $window,
                           toaster,
                           OrderServices,
                           Cart,
                           Profile) {
                     // $window.Stripe.setPublishableKey('pk_test_dyIO81Q3L0JnR8yDTyNHzh5q');
                     $scope.paymentData = {use_saved_card: false};
                     if($localStorage.userName){
                         Profile.get_profile($localStorage.userName)
                             .then(function(response) {
                                 $scope.paymentData.email = response.data.email
                             }, function(response) {
                                 console.log("ERROR: get_profile=", response);
                             });
                        }
                     $scope.sessionValues = OrderServices.getOrderDetails();
                     if ($scope.sessionValues.checkingoutCart === ''){
                         toaster.pop({
                             type: 'error',
                             title: 'Error',
                             body: 'Session Expired.Go to cart and try checkout again',
                             showCloseButton: true
                         });
                         return false;
                     }
                     else{
                         toaster.pop({
                             type: 'info',
                             title: 'Warning',
                             body: 'Do not refresh the page during this process',
                             showCloseButton: true,
                         });
                         $scope.stripeCallback = function (status, response) {
                             $scope.paymentForm.isSubmitted = true;
                             if (response.error) {
                                 if (typeof (response.error) == 'object') {
                                     if (response.error.type == "card_error" &&
                                         response.error.param == 'exp_year') {
                                         $scope.expiry_error = response.error.message;
                                     }
                                     else if(response.error.type == "card_error" &&
                                             response.error.param == 'number'){
                                         $scope.number_error = response.error.message;
                                     }
                                 }
                             }
                             else {
                                 var paymentData = {};
                                 paymentData.token = response.id;
                                 paymentData.card_token = response.card.id;
                                 paymentData.email = $scope.paymentForm.email.$viewValue;
                                 paymentData.name = $scope.paymentForm.name.$viewValue;
                                 paymentData.cart = $rootScope.cart.id;
                                 paymentData.amount_paid = $scope.sessionValues.payableAmount;
                                 if($scope.paymentForm.remember_card){
                                     paymentData.remember_card = $scope.paymentForm.remember_card.$viewValue;
                                 }
                                 paymentData.use_saved_card = false;
                                 $scope.placeOrder(paymentData);
                                 // got stripe token, now charge it or smt
                             }
                         };
                     }
                     
                     $scope.removeError = function(error_variable){
                         error_variable = undefined;
                     };
                     
                     $scope.placeOrderUseCard = function(paymentData){
                         $scope.paymentForm1.isSubmitted = true;
                         var paymentData = {};
                         paymentData.email = $scope.paymentForm.email.$viewValue;
                         paymentData.name = $scope.paymentForm.name.$viewValue;
                         paymentData.cart = $rootScope.cart.id;
                         paymentData.amount_paid = $scope.sessionValues.payableAmount;
                         paymentData.use_saved_card = true;
                         $scope.placeOrder(paymentData);
                     };
                     
                     // $scope.amount_payable = 50;
                     $scope.placeOrder = function(form_data){
                         $scope.showSpinner = true;
                         $rootScope.redirect_to_contest = undefined;
                         var contestStr = undefined;
                         if ($scope.cart.item_count === 1) {
                             if ($scope.cart.items[0].content_type === 'contest') {
                                 $rootScope.redirect_to_contest =
                                     $scope.cart.items[0].details.slug;
                             }
                         }
                         else if ($scope.cart.item_count > 1) {
                             for (var i = 0; i < $scope.cart.item_count; i++) {
                                 if ($scope.cart.items[i].content_type !== 'contest') {
                                     continue;
                                 }
                                 if (contestStr === undefined) {
                                     contestStr = 'Contest';
                                     continue;
                                 }
                                 contestStr = 'Contests'
                                 break;
                             }
                         }
                         // $scope.paymentData.cart = $rootScope.cart.id;
                         // $scope.paymentData.amount_payable = $scope.amount_payable;
                         Cart.place_order(form_data).then(function(response) {
                             $rootScope.cart.item_count = 0;
                             $scope.showSpinner = false;
                             if ($rootScope.redirect_to_contest) {
                                 $state.go('contest_detail',
                                           {slug: $rootScope.redirect_to_contest});
                                 // toaster.pop({type: 'success',
                                 //              title: "Successful",
                                 //              body: "Contest Published successfully",
                                 //              showCloseButton: true});
                             }
                             else {
                                 if (contestStr !== undefined) {
                                     toaster.pop({type: 'success',
                                                  title: "Successful",
                                                  body: contestStr+" Published successfully",
                                                  showCloseButton: true});
                                 }
                                 $state.go('order', {order_id: response.data});
                             }
                             // toaster.pop({type: 'success',
                             //              title: "Successful",
                             //              body: "Order placed successfully",
                             //              showCloseButton: true});
                         }, function (response) {
                             $scope.showSpinner = false;
                             console.log('ERROR: place_order=', response.data);
                             toaster.pop({
                                 type: 'error',
                                 title: 'Error',
                                 body: ('Unable to place order. ' +
                                        'Please go to cart and try again.'),
                                 showCloseButton: true
                             });
                         });
                     }
                     
                     $scope.checkIfCardSaved = function (use_card) {
                         if (use_card == true) {
                             // $scope.paymentData.cart = $rootScope.cart.id;
                             // $scope.paymentData.amount_payable = $scope.amount_payable;
                             Cart.check_saved_card().then(function(response) {
                                 $scope.card_info = response.data;
                                 $scope.can_use_card = true;
                                 $scope.paymentData.use_saved_card = true;
                             }, function(response) {
                                 console.log("ERROR: check_saved_card=", response.data);
                                 $scope.can_use_card = false;
                                 $scope.saved_card_info = response.data;
                                 $scope.paymentData.use_saved_card = false;
                             });
                         }
                         else {
                             $scope.can_use_card = false;
                             $scope.saved_card_info = ''
                             $scope.paymentData.use_saved_card = false;
                         }
                     }
                 }])

    .controller('OrderHistoryCtrl',
                ['$rootScope',
                 '$scope',
                 '$state',
                 '$stateParams',
                 //'$http',
                 '$location',
                 '$localStorage',
                 '$auth',
                 'Cart',
                 'HttpServices',
                 function($rootScope,
                          $scope,
                          $state,
                          $stateParams,
                          //$http,
                          $location,
                          $localStorage,
                          $auth,
                          Cart,
                          HttpServices) {
                     if ($stateParams.order_id !== undefined){
                         Cart.order_history($stateParams.order_id)
                             .then(function(response) {
                                 $scope.order_history = response.data;
                                 $scope.single_order = response.data
                                 //console.log("DEBUG: order_history=", response);
                             }, function(response) {
                                 console.log("ERROR: order_history=", response);
                                 $state.go('404');
                             });
                         return;
                     }
                     if ($localStorage.userKey !== undefined) {
                         Cart.order_history($scope.paymentData)
                             .then(function(response) {
                                 $scope.order_history = response.data.results;
                                 //console.log("DEBUG: order_history=", response);
                             }, function(response) {
                                 console.log("ERROR: order_history=", response);
                                 $state.go('404');
                             });
                     }
                      
                     // ToDo:
                     // $scope.downloadContent = function(item){
                     //     alert("INFO: downloadContent=not implemented");
                     // }
                     $scope.downloadContent = function(order_id, item_id){
                         HttpServices.get("api/payments/download-content/" +
                                          order_id +
                                          "/" +
                                          item_id)
                             .then(function(response){
                                 //console.log("DEBUG: downloadContent=", response);
                                 $scope.downloadUrl = response.data.url;
                                 var anchor = angular.element('<a/>');
                                 anchor.attr({href: $scope.downloadUrl})[0].click();
                             }, function(response){
                                 console.log("ERROR: downloadContent=", response.data);
                                 $state.go('404');
                             });
                     };
                 }])
                
    .factory('Cart',
             ['$localStorage',
              'HttpServices',
              function($localStorage,
                       //$http,
                       HttpServices) {
  	          return {
  	              get_cart: function() {
                          return (HttpServices.get("api/cart/cart/"));
  	              },

                      add_to_cart: function(item_id, item_type) {
                          return (HttpServices.post("api/cart/cart/",
                                                    {item_id: item_id,
                                                     item_type: item_type}));
  	              },

                      delete_from_cart: function(item_id, item_type) {
                          //return (HttpServices.delete("api/cart/cart/"+item_id+'/'));
                          return (HttpServices.delete("api/cart/cart/"+item_id+"/",
                                                      {item_type: item_type}));
  	              },

                      place_order: function(data) {
                          return (HttpServices.post("api/payments/place-order/",
                                                    data));
                      },

                      order_history: function(order_id) {
                          var endpoint = "api/payments/order-history/";
                          if(order_id){
                              endpoint = "api/payments/order-history/" + order_id;
                          }
                          return(HttpServices.get(endpoint));
                      },

                      download_video: function(order_id, video_id) {

                          return (HttpServices.get("api/payments/download-content/"+
                                                   order_id+
                                                   video_id));
                      },
                      
                      check_saved_card: function() {
                          return (HttpServices.get("api/payments/check-saved-card"));
  	              }
                  }    
              }])

    .filter('trusted', ['$sce', function ($sce) {
        return function(url) {
            return $sce.trustAsResourceUrl(url);
        };
    }]);
