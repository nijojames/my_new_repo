'use strict';

angular.module('spyhop.search', ['ngRoute'])

    .controller('GeneralSearchCtrl',
                [//'$rootScope',
                 '$scope',
                 //'$http',
                 /*'$anchorScroll',
                 '$location',
                 '$state',
                 '$localStorage',
                 '$timeout',
                 'Cart',
                 'toaster',
                 'levelFactory',
                 'HttpServices',*/
                 'searchFactory',
                 function(//$rootScope,
                          $scope,
                          //$http,
                          /*$anchorScroll,
                          $location,
                          $state,
                          $localStorage,
                          $timeout,
                          Cart,
                          toaster,
                          levelFactory,
                          HttpServices,*/
                          searchFactory) {

                     searchFactory.setupVideoSearch($scope, false/*true*/);
                     
                     
                     /*
                     var substringMatcher = function(strs) {
                         return function findMatches(q, cb) {
                             var matches, substrRegex;
                             
                             // an array that will be populated with substring matches
                             matches = [];
                             
                             // regex used to determine if a string contains the substring `q`
                             substrRegex = new RegExp(q, 'i');
                             
                             // iterate through the pool of strings and for any string that
                             // contains the substring `q`, add it to the `matches` array
                             $.each(strs, function(i, str) {
                                 if (substrRegex.test(str)) {
                                     matches.push(str);
                                 }
                             });
                             
                             cb(matches);
                         };
                     };
                     
                     // $http.get($localStorage.host + 'api/videos/title/').success(function(data) {
                     //     $scope.recommentations = data
                     // });
                     HttpServices.get('api/videos/title/')
                         .then(function(response) {
                             $scope.recommentations = response.data
                         });
                     
                     $('.typeahead').on('typeahead:selected', function(e, datum) {
                         $scope.q = datum;
                     });
                     
                     var states = $scope.recommentations;
                     //console.log(states)
                     
                     $('.typeahead').typeahead({
                         hint: true,
                         highlight: true,
                         minLength: 1
                     }, {name: 'states',
                         source: substringMatcher(states)});
                     
                     // $scope.addToCart = function(video_id) {
                     //     Cart.add_to_cart(video_id, 'video').then(function(cart_data) {
                     //         $rootScope.cart = cart_data.data;
                     //         toaster.pop('success', "Success", "Item added to cart");
                     //     }, function(error) {
                     //         toaster.pop('info', "Warning", "This item is already in your cart");
                     //     });
                     // };

                     $scope.addToCart = function(video_id) {
                         Cart.add_to_cart(video_id, 'video')
                             .then(function(response) {
                                 $rootScope.cart = response.data;
                                 toaster.pop('success', "Success", "Item added to cart");
                             }, function(response) {
                                 toaster.pop('info', "Warning", "This item is already in your cart");
                             });
                     };
                     
                     $scope.getList = function(num) {
                         var list = [];
                         for (var i = 0; i < num; i++) {
                             list.push(i);
                         }
                         return list;
                     };
                     
                     $scope.searchVideos = function(params_obj, faceted_url) {
                         $scope.videoCollection = [];
                         //$scope.noCollection = false;
                         
                         
                         //    $scope.params_obj = {
                         //        page_size: 10,
                         //        page: 1
                         //    };
                         $('.spinner').show();
                         var url = 'api/videos/search/facets/';
                         if (faceted_url !== undefined) {
                             url = url + faceted_url;
                         }
                         // $http.get($localStorage.host + url, {
                         //     params: params_obj
                         // }).success(function(response) {
                         //     $('.spinner').hide();
                         //     $scope.videoCollection = response.objects.results;
                         //     $scope.locations=[]
                         //     for(var i=0;i<$scope.videoCollection.length;i++) {
                                 
                         //         if($scope.videoCollection[i].location_name !== null){
                         //             var k = $scope.videoCollection[i].location_name.split(',')
                         //             if($scope.locations.indexOf(k[2]) == -1){
                         //                 $scope.locations.push(k[2]);
                         //             }
                                     
                         //         }
                         //     }
                         //     // if (!$scope.videoCollection.length) {
                         //     //     $scope.noCollection = true;
                         //     // }
                         //     $scope.key = $scope.q;
                         //     $scope.count = response.objects.count;
                         //     $scope.page_count = Math.floor((parseInt($scope.count) + parseInt(params_obj.page_size) - 1) / parseInt(params_obj.page_size));
                         //     $scope.pages = $scope.getList($scope.page_count);
                         //     $scope.page_active = $scope.params_obj.page;
                         //     $timeout(function(){
                         //         enableAutoPlay("GeneralSearchCtrl.searchVideos");
                         //     }, 0, false);
                             
                         // }).error(function(data, status) {
                         //     console.log("Error status : " + status);
                         // });
                         HttpServices.get(url, params_obj)
                             .then(function(response) {
                                 $('.spinner').hide();
                                 $scope.videoCollection = response.data.objects.results;
                                 $scope.locations=[]
                                 for(var i=0;i<$scope.videoCollection.length;i++) {
                                     if($scope.videoCollection[i].location_name !== null){
                                         var k = $scope.videoCollection[i].location_name.split(',')
                                         if($scope.locations.indexOf(k[2]) == -1){
                                             $scope.locations.push(k[2]);
                                         }
                                     }
                                 }
                                 // if (!$scope.videoCollection.length) {
                                 //     $scope.noCollection = true;
                                 // }
                                 $scope.key = $scope.q;
                                 $scope.count = response.data.objects.count;
                                 $scope.page_count = Math.floor((parseInt($scope.count) +
                                                                 parseInt(params_obj.page_size) - 1) /
                                                                parseInt(params_obj.page_size));
                                 $scope.pages = $scope.getList($scope.page_count);
                                 $scope.page_active = $scope.params_obj.page;
                                 $timeout(function(){
                                     enableAutoPlay("GeneralSearchCtrl.searchVideos");
                                 }, 0, false);
                             }, function(response) {
                                 $('.spinner').hide();
                                 console.log("ERROR: searchVideos=", response);
                             });
                     };
                     
                     $scope.profileSearch = function() {
                         $('.contest-spinner').show();
                         $('.profile-spinner').show();
                         //profile search
                         // $http.get($localStorage.host + 'api/profiles/search/facets/', {
                         //     params: {
                         //         q: $scope.q
                         //     }
                         // }).success(function(response) {
                         //     $scope.profileCollection = response.objects.results;
                         //     $scope.profile_count = response.objects.count
                         //     $('.profile-spinner').hide();
                         // }).error(function(data, status) {
                         //     console.log("Error status : " + status);
                         // });
                         HttpServices.get('api/profiles/search/facets/',
                                          {q: $scope.q})
                             .then(function(response) {
                                 $scope.profileCollection = response.data.objects.results;
                                 $scope.profile_count = response.data.objects.count
                                 $('.profile-spinner').hide();
                             }, function(response) {
                                 $('.profile-spinner').hide();
                                 console.log("ERROR: profileSearch=", response);
                             });
                         
                         // contest search
                         // $http.get($localStorage.host + 'api/contests/search/facets/', {
                         //     params: {
                         //         q: $scope.q
                         //     }
                         // }).success(function(response) {
                         //     $scope.contestCollection = response.objects.results;
                         //     $scope.contest_count = response.objects.count
                         //     $('.contest-spinner').hide();
                         // }).error(function(data, status) {
                         //     console.log("Error status : " + status);
                         // });
                         HttpServices.get('api/contests/search/facets/',
                                          {q: $scope.q})
                             .then (function(response) {
                                 $scope.contestCollection = response.data.objects.results;
                                 $scope.contest_count = response.data.objects.count
                                 $('.contest-spinner').hide();
                             }, function(response) {
                                 $('.contest-spinner').hide();
                                 console.log("ERROR: contestSearch=", response);
                             });
                     };
                     
                     // to call the function on slider stop.
                     $('.span2').slider().on('slideStop', function(ev) {
                         var newVal = $(this).data('slider').getValue();
                         $(this).val(newVal);
                         $(this).trigger('input');
                         $scope.filterVideos();
                     });
                     
                     
                     $scope.filterVideos = function() {
                         if ($scope.category == 'contest') {
                             $state.go('search_request', {
                                 q: $scope.q
                             })
                         } else if ($scope.category == 'member') {
                             $state.go('search_user', {
                                 q: $scope.q
                             })
                         } else if ($scope.category == 'videos') {
                             $state.go('video-search', {
                                 q: $scope.q
                             })
                         } else {
                             $location.search('q', $scope.q);
                             $location.search('price_range', $scope.filter.price);
                             $location.search('permissions', $scope.filter.permissions);
                             $location.search('orientation', $scope.filter.orientation);
                             $location.search('location', $scope.filter.location);
                             $location.search('levels', $scope.filter.level);
                             $location.search('rating', $scope.filter.rating);
                             $location.search('age', $scope.filter.age);
                             $location.search('page_size', $scope.filter.page_size);
                             if ($scope.filter.page_size === undefined) {
                                 $scope.filter.page_size = $scope.params_obj.page_size;
                             }
                             $scope.createUrl();
                         }
                     };
                     
                     $scope.createUrl = function() {
                         $scope.faceted_url = $scope.buildFacetedUrl();
                         $scope.params_obj.page_size = $scope.filter.page_size;
                         $scope.params_obj.page = 1;
                         $scope.searchVideos($scope.params_obj, $scope.faceted_url);
                     };
                     $scope.getPageData = function(page_num) {
                         $scope.leftPager = true;
                         $scope.rightPager = true;
                         $scope.params_obj.page = page_num;
                         if ($scope.params_obj.page == 1) {
                             $scope.leftPager = false;
                         }
                         if ($scope.params_obj.page == $scope.page_count) {
                             $scope.rightPager = false;
                         }
                         
                         $scope.searchVideos($scope.params_obj);
                     };
                     $scope.leftPage = function(page_num) {
                         if ($scope.params_obj.page != 1) {
                             page_num = $scope.params_obj.page - 1;
                             $scope.getPageData(page_num)
                         }
                     };
                     $scope.rightPage = function(page_num) {
                         if ($scope.params_obj.page != $scope.page_count) {
                             page_num = $scope.params_obj.page + 1;
                             $scope.getPageData(page_num)
                         }
                     };
                     
                     $scope.buildFacetedUrl = function() {
                         var url = '?selected_facets=';
                         if ($scope.filter.location != undefined) {
                             url = url + 'location:' + $scope.filter.location;
                         }
                         if ($scope.q != undefined) {
                             url = url + '&q=' + $scope.q;
                         }
                         if ($scope.filter.price != undefined) {
                             url = url + '&price_range=' + $scope.filter.price;
                         }
                         if ($scope.filter.permissions != undefined) {
                             url = url + '&permissions=' + $scope.filter.permissions;
                         }
                         if ($scope.filter.orientation != undefined) {
                             url = url + '&orientation=' + $scope.filter.orientation;
                         }
                         if ($scope.filter.level != undefined) {
                             url = url + '&levels=' + $scope.filter.level;
                         }
                         if ($scope.filter.rating != undefined) {
                             url = url + '&rating=' + $scope.filter.rating;
                         }
                         if ($scope.filter.age != undefined) {
                             url = url + '&age=' + $scope.filter.age;
                         }
                         return url;
                     };
                     
                     //    $scope.q = $location.search().q;
                     //    if ($scope.q !== undefined) {
                     //        $scope.params_obj.q = $scope.q;
                     //    }
                     //    $scope.searchVideos($scope.params_obj);
                     //    $scope.profileSearch()
                     //    $scope.searchQuery = function() {
                     //        $state.go('video-search', {
                     //            q: $scope.q
                     //        })
                     //    };
                     
                     $scope.init = function() {
                         $scope.filter = {};
                         $scope.params_obj = {
                             page_size: 10,
                             page: 1
                         };
                         // ToDo: get it from the API server
                         $scope.permissions_list = [{
                             label: 'Free',
                             value: 'free'
                         }, {
                             label: 'Royalty Free',
                             value: 'royalty_free'
                         }, {
                             label: 'Commercial',
                             value: 'commercial'
                         }];
                         $scope.orientations_list = [{
                             label: 'Square',
                             value: 'square'
                         }, {
                             label: 'Landscape',
                             value: 'landscape'
                         }, {
                             label: 'Portrait',
                             value: 'portrait'
                         }];
                         $scope.result_page_list = [{
                             label: '10',
                             value: '10'
                         }, {
                             label: '20',
                             value: '20'
                         }, {
                             label: '30',
                             value: '30'
                         }, {
                             label: '50',
                             value: '50'
                         }, {
                             label: '100',
                             value: '100'
                         }];
                         $scope.sort_result = [{
                             label: 'Newest',
                             value: 'new'
                         }, {
                             label: 'Oldest',
                             value: 'old'
                         }];
                         
                         function get_url_value(key) {
                             var value = $location.search()[key] || undefined;
                             if (typeof(value) == 'string')
                                 value = [value];
                             return value
                             
                         }
                         $scope.filter.location = get_url_value('location');
                         $scope.filter.permissions = get_url_value('permissions');
                         $scope.filter.orientation = get_url_value('orientation');
                         $scope.filter.level = get_url_value('levels');
                         $scope.filter.rating = get_url_value('rating');
                         $scope.filter.age = get_url_value('age');
                         $scope.filter.price = get_url_value('price_range');
                         $scope.filter.page_size = get_url_value('page_size');
                         $scope.q = $location.search().q;
                         if ($scope.q !== undefined) {
                             $scope.params_obj.q = $scope.q;
                         }
                         //         $scope.searchVideos($scope.params_obj);
                         $scope.profileSearch();
                         $scope.filterVideos();
                         levelFactory.async().then(function(data) {
                             $scope.levelCollection = data[0].data;
                             $scope.leftPager = true;
                             $scope.rightPager = true;
                         });
                     };
                     $scope.init();*/
                 }])


    .controller('VideoSearchCtrl',
                [//'$rootScope',
                 '$scope',
                 //'$http',
                 /*'$anchorScroll',
                 '$location',
                 '$state',
                 '$localStorage',
                 'Cart',
                 'toaster',
                 'levelFactory',
                 'HttpServices',*/
                 'searchFactory',
                 function(//$rootScope,
                          $scope,
                          //$http,
                          /*$anchorScroll,
                          $location,
                          $state,
                          $localStorage,
                          Cart,
                          toaster,
                          levelFactory,
                          HttpServices,*/
                          searchFactory) {

                     searchFactory.setupVideoSearch($scope, false);
                     
                     /*
                     var substringMatcher = function(strs) {
                         return function findMatches(q, cb) {
                             var matches, substrRegex;
                             // an array that will be populated with substring matches
                             matches = [];
                             // regex used to determine if a string contains the substring `q`
                             substrRegex = new RegExp(q, 'i');
                             // iterate through the pool of strings and for any string that
                             // contains the substring `q`, add it to the `matches` array
                             $.each(strs, function(i, str) {
                                 if (substrRegex.test(str)) {
                                     matches.push(str);
                                 }
                             });
                             cb(matches);
                         };
                     };
                     // $http.get($localStorage.host + 'api/videos/title/').success(function(data) {
                     //     $scope.recommentations = data
                     // });

                     HttpServices.get('api/videos/title/')
                         .then(function(response) {
                             $scope.recommentations = response.data
                         });
                     
                     $('.typeahead').on('typeahead:selected', function(e, datum) {
                         $scope.q = datum;
                     });
                     
                     var states = $scope.recommentations;
                     console.log(states)
                     
                     $('.typeahead').typeahead({
                         hint: true,
                         highlight: true,
                         minLength: 1
                     }, {
                         name: 'states',
                         source: substringMatcher(states)
                     });
                     
                     // $scope.addToCart = function(video_id) {
                     //     Cart.add_to_cart(video_id, 'video').then(function(cart_data) {
                     //         $rootScope.cart = cart_data.data;
                     //         toaster.pop('success', "Success", "Item added to cart");
                     //     }, function(error) {
                     //         toaster.pop('info', "Warning", "This item is already in your cart");
                     //     });
                     // };

                     $scope.addToCart = function(video_id) {
                         Cart.add_to_cart(video_id, 'video')
                             .then(function(response) {
                                 $rootScope.cart = response.data;
                                 toaster.pop('success', "Success", "Item added to cart");
                             }, function(response) {
                                 toaster.pop('info', "Warning", "This item is already in your cart");
                             });
                     };
                     
                     $scope.getList = function(num) {
                         var list = [];
                         for (var i = 0; i < num; i++) {
                             list.push(i);
                         }
                         return list;
                     };

                     $scope.searchVideos = function(params_obj, faceted_url) {
                         $scope.videoCollection = [];
                         //$scope.noCollection = false;
                         //    $scope.params_obj = {
                         //        page_size: 10,
                         //        page: 1
                         //    };
                         
                         $('.spinner').show();
                         var url = 'api/videos/search/facets/';
                         if (faceted_url !== undefined) {
                             url = url + faceted_url;
                         }
                         // $http.get($localStorage.host + url, {
                         //     params: params_obj
                         // }).success(function(response) {
                         //     $('.spinner').hide();
                         //     $scope.videoCollection = response.objects.results;
                         //     $scope.locations=[]
                         //     for(var i=0;i<$scope.videoCollection.length;i++) {
                         //         if($scope.videoCollection[i].location_name !== null){
                         //             var k = $scope.videoCollection[i].location_name.split(',')
                         //             $scope.locations.push(k[2])
                         //         }
                         //     }
                         //     // if (!$scope.videoCollection.length) {
                         //     //     $scope.noCollection = true;
                         //     // }
                         //     $scope.key = $scope.q;
                         //     $scope.count = response.objects.count;
                         //     $scope.page_count = Math.floor((parseInt($scope.count) + parseInt(params_obj.page_size) - 1) / parseInt(params_obj.page_size));
                         //     $scope.pages = $scope.getList($scope.page_count);
                         //     $scope.page_active = $scope.params_obj.page;
                             
                         // }).error(function(data, status) {
                         //     console.log("Error status : " + status);
                         // });
                         HttpServices.get(url, params_obj)
                             .then(function(response) {
                                 $('.spinner').hide();
                                 $scope.videoCollection = response.data.objects.results;
                                 $scope.locations=[]
                                 for(var i=0;i<$scope.videoCollection.length;i++) {
                                     if($scope.videoCollection[i].location_name !== null){
                                         var k = $scope.videoCollection[i].location_name.split(',')
                                         if($scope.locations.indexOf(k[2]) == -1){
                                             $scope.locations.push(k[2]);
                                         }
                                     }
                                 }
                                 // if (!$scope.videoCollection.length) {
                                 //     $scope.noCollection = true;
                                 // }
                                 $scope.key = $scope.q;
                                 $scope.count = response.data.objects.count;
                                 $scope.page_count = Math.floor((parseInt($scope.count) +
                                                                 parseInt(params_obj.page_size) - 1) /
                                                                parseInt(params_obj.page_size));
                                 $scope.pages = $scope.getList($scope.page_count);
                                 $scope.page_active = $scope.params_obj.page;
                                 $timeout(function(){
                                     enableAutoPlay("GeneralSearchCtrl.searchVideos");
                                 }, 0, false);
                             }, function(response) {
                                 $('.spinner').hide();
                                 console.log("ERROR: searchVideos=", response);
                             });
                     };
                     
                     // to call the function on slider stop.
                     $('.span2').slider().on('slideStop', function(ev) {
                         var newVal = $(this).data('slider').getValue();
                         $(this).val(newVal);
                         $(this).trigger('input');
                         $scope.filterVideos();
                     });

                     $scope.filterVideos = function() {
                         if ($scope.category == 'contest') {
                             $state.go('search_request', {
                                 q: $scope.q
                             })
                         } else if ($scope.category == 'member') {
                             $state.go('search_user', {
                                 q: $scope.q
                             })
                         } else {
                             $location.search('q', $scope.q);
                             $location.search('price_range', $scope.filter.price);
                             $location.search('permissions', $scope.filter.permissions);
                             $location.search('orientation', $scope.filter.orientation);
                             $location.search('location', $scope.filter.location);
                             $location.search('levels', $scope.filter.level);
                             $location.search('rating', $scope.filter.rating);
                             $location.search('age', $scope.filter.age);
                             $location.search('page_size', $scope.filter.page_size);
                             if ($scope.filter.page_size === undefined) {
                                 $scope.filter.page_size = $scope.params_obj.page_size;
                             }
                             $scope.createUrl();
                         }
                     };

                     $scope.createUrl = function() {
                         $scope.faceted_url = $scope.buildFacetedUrl();
                         $scope.params_obj.page_size = $scope.filter.page_size;
                         $scope.params_obj.page = 1;
                         $scope.searchVideos($scope.params_obj, $scope.faceted_url);
                     };
                     
                     $scope.getPageData = function(page_num) {
                         $scope.leftPager = true;
                         $scope.rightPager = true;
                         $scope.params_obj.page = page_num;
                         if ($scope.params_obj.page == 1) {
                             $scope.leftPager = false;
                         }
                         if ($scope.params_obj.page == $scope.page_count) {
                             $scope.rightPager = false;
                         }
                         $scope.searchVideos($scope.params_obj);
                     };

                     $scope.leftPage = function(page_num) {
                         if ($scope.params_obj.page != 1) {
                             page_num = $scope.params_obj.page - 1;
                             $scope.getPageData(page_num)
                         }
                     };
                     
                     $scope.rightPage = function(page_num) {
                         if ($scope.params_obj.page != $scope.page_count) {
                             page_num = $scope.params_obj.page + 1;
                             $scope.getPageData(page_num)
                         }
                     };
                     
                     $scope.buildFacetedUrl = function() {
                         var url = '?selected_facets=';
                         if ($scope.filter.location != undefined) {
                             url = url + 'location:' + $scope.filter.location;
                         }
                         if ($scope.q != undefined) {
                             url = url + '&q=' + $scope.q;
                         }
                         if ($scope.filter.price != undefined) {
                             url = url + '&price_range=' + $scope.filter.price;
                         }
                         if ($scope.filter.permissions != undefined) {
                             url = url + '&permissions=' + $scope.filter.permissions;
                         }
                         if ($scope.filter.orientation != undefined) {
                             url = url + '&orientation=' + $scope.filter.orientation;
                         }
                         if ($scope.filter.level != undefined) {
                             url = url + '&levels=' + $scope.filter.level;
                         }
                         if ($scope.filter.rating != undefined) {
                             url = url + '&rating=' + $scope.filter.rating;
                         }
                         if ($scope.filter.age != undefined) {
                             url = url + '&age=' + $scope.filter.age;
                         }
                         return url;
                     };
                     
                     //    $scope.q = $location.search().q;
                     //    if ($scope.q !== undefined) {
                     //        $scope.params_obj.q = $scope.q;
                     //    }
                     //    $scope.searchVideos($scope.params_obj);
                     //    $scope.profileSearch()
                     //    $scope.searchQuery = function() {
                     //        $state.go('video-search', {
                     //            q: $scope.q
                     //        })
                     //    };
                     
                     $scope.init = function() {
                         $scope.filter = {};
                         $scope.params_obj = {
                             page_size: 10,
                             page: 1
                         };
                         $scope.permissions_list = [{
                             label: 'Free',
                             value: 'free'
                         }, {
                             label: 'Royalty Free',
                             value: 'royalty_free'
                         }, {
                             label: 'Commercial',
                             value: 'commercial'
                         }];
                         $scope.orientations_list = [{
                             label: 'Square',
                             value: 'square'
                         }, {
                             label: 'Landscape',
                             value: 'landscape'
                         }, {
                             label: 'Portrait',
                             value: 'portrait'
                         }];
                         $scope.result_page_list = [{
                             label: '10',
                             value: '10'
                         }, {
                             label: '20',
                             value: '20'
                         }, {
                             label: '30',
                             value: '30'
                         }, {
                             label: '50',
                             value: '50'
                         }, {
                             label: '100',
                             value: '100'
                         }];
                         $scope.sort_result = [{
                             label: 'Newest',
                             value: 'new'
                         }, {
                             label: 'Oldest',
                             value: 'old'
                         }];
                         
                         function get_url_value(key) {
                             var value = $location.search()[key] || undefined;
                             if (typeof(value) == 'string')
                                 value = [value];
                             return value
                             
                         }
                         $scope.filter.location = get_url_value('location');
                         $scope.filter.permissions = get_url_value('permissions');
                         $scope.filter.orientation = get_url_value('orientation');
                         $scope.filter.level = get_url_value('levels');
                         $scope.filter.rating = get_url_value('rating');
                         $scope.filter.age = get_url_value('age');
                         $scope.filter.price = get_url_value('price_range');
                         $scope.filter.page_size = get_url_value('page_size');
                         $scope.q = $location.search().q;
                         if ($scope.q !== undefined) {
                             $scope.params_obj.q = $scope.q;
                         }
                         //         $scope.searchVideos($scope.params_obj);
                         $scope.filterVideos();
                         levelFactory.async().then(function(data) {
                             $scope.levelCollection = data[0].data;
                             $scope.leftPager = true;
                             $scope.rightPager = true;
                         });
                     };
                     $scope.init();*/
                 }])

// profile search controller
    .controller('ProfileSearchCtrl',
                ['$scope',
                 //'$http',
                 '$anchorScroll',
                 '$location',
                 '$state',
                 '$localStorage',
                 'levelFactory',
                 'HttpServices',
                 'searchFactory',
                 function($scope,
                          //$http,
                          $anchorScroll,
                          $location,
                          $state,
                          $localStorage,
                          levelFactory,
                          HttpServices,
                          searchFactory) {

                     $('.spinner').show();
                     // $scope.getList = function(num) {
                     //     var list = [];
                     //     for (var i = 0; i < num; i++) {
                     //         list.push(i);
                     //     }
                     //     return list;
                     // };
                     $scope.getList = searchFactory.getList;

                     $scope.searchUsers = function(params_obj, faceted_url) {
                         $scope.profileCollection = [];
                         var url = 'api/profiles/search/facets/';
                         if (faceted_url !== undefined) {
                             url = url + faceted_url;
                         }
                         // $http.get($localStorage.host + url, {
                         //     params: params_obj
                         // }).success(function(response) {
                         //     $scope.profileCollection = response.objects.results;
                         //     $scope.count = response.objects.count;
                         //     $('.spinner').hide();
                         //     $scope.key = $scope.q;
                         //     $scope.count = response.objects.count;
                         //     $scope.page_count = Math.floor((parseInt($scope.count) + parseInt(params_obj.page_size) - 1) / parseInt(params_obj.page_size));
                         //     $scope.pages = $scope.getList($scope.page_count);
                         //     $scope.page_active = $scope.params_obj.page;
                             
                         // }).error(function(data, status) {
                         //     console.log("Error status : " + status);
                         // });
                         HttpServices.get(url, params_obj)
                             .then(function(response) {
                                 $scope.profileCollection = response.data.objects.results;
                                 $scope.count = response.data.objects.count;
                                 $('.spinner').hide();
                                 $scope.key = $scope.q;
                                 $scope.count = response.data.objects.count;
                                 $scope.page_count = Math.floor((parseInt($scope.count) +
                                                                 parseInt(params_obj.page_size) - 1) /
                                                                parseInt(params_obj.page_size));
                                 $scope.pages = $scope.getList($scope.page_count);
                                 $scope.page_active = $scope.params_obj.page;
                             }, function(response) {
                                 console.log("ERROR: searchUsers=", response);
                             });
                     };

                     $scope.filterUsers = function() {
                         $location.search('q', $scope.q);
                         $location.search('levels', $scope.filter.level);
                         $scope.createUrl();
                     };
                     
                     $scope.createUrl = function() {
                         $scope.faceted_url = $scope.buildFacetedUrl();
                         //            $scope.params_obj.page_size = 10
                         //            $scope.params_obj.page = 1;
                         $scope.searchUsers($scope.params_obj, $scope.faceted_url);
                     };
                     
                     // $scope.getPageData = function(page_num) {
                     //     $scope.leftPager = true;
                     //     $scope.rightPager = true;
                     //     $scope.params_obj.page = page_num;
                     //     $scope.searchUsers($scope.params_obj);
                     //     if ($scope.params_obj.page == 1) {
                     //         $scope.leftPager = false;
                     //     }
                     //     if ($scope.params_obj.page == $scope.page_count) {
                     //         $scope.rightPager = false;
                     //     }
                     // };
                     // $scope.leftPage = function(page_num) {
                     //     if ($scope.params_obj.page != 1) {
                     //         page_num = $scope.params_obj.page - 1;
                     //         $scope.getPageData(page_num)
                     //     }
                     // };
                     // $scope.rightPage = function(page_num) {
                     //     if ($scope.params_obj.page != $scope.count) {
                     //         page_num = $scope.params_obj.page + 1;
                     //         $scope.getPageData(page_num)
                     //     }
                     // };
                     searchFactory.setupPageNavigation($scope,
                                                       $scope.searchUsers);
                     
                     $scope.buildFacetedUrl = function() {
                         var url = '?';
                         
                         if ($scope.filter.level != undefined) {
                             url = url + 'levels=' + $scope.filter.level;
                         }
                         return url;
                     };
                     //        $scope.leftPager = true;
                     //        $scope.rightPager = true;
                     //        $scope.q = $location.search().q;
                     //    if ($scope.q !== undefined) {
                     //        $scope.params_obj.q = $scope.q;
                     //    }
                     //    $scope.searchUsers($scope.params_obj);
                     //    $scope.searchQuery = function() {
                     //        $state.go('search_user', {
                     //            q: $scope.q
                     //        })
                     //    };
                     
                     $scope.init = function() {
                         $scope.filter = {};
                         $scope.params_obj = {
                             page_size: 12,
                             page: 1
                         };
                         
                         function get_url_value(key) {
                             var value = $location.search()[key] || undefined;
                             if (typeof(value) == 'string')
                                 value = [value];
                             return value
                             
                         }
                         $scope.filter.level = get_url_value('levels');
                         $scope.q = $location.search().q;
                         if ($scope.q !== undefined) {
                             $scope.params_obj.q = $scope.q;
                         }
                         //         $scope.searchVideos($scope.params_obj);
                         $scope.filterUsers();
                         levelFactory.async().then(function(data) {
                             $scope.levelCollection = data[0].data;
                             $scope.leftPager = true;
                             $scope.rightPager = true;
                         });
                     };
                     $scope.init();
                 }])


    .controller('ContestSearchCtrl',
                ['$scope',
                 //'$http',
                 '$anchorScroll',
                 '$location',
                 '$state',
                 '$localStorage',
                 'levelFactory',
                 'HttpServices',
                 'searchFactory',
                 function($scope,
                          //$http,
                          $anchorScroll,
                          $location,
                          $state,
                          $localStorage,
                          levelFactory,
                          HttpServices,
                          searchFactory) {
                     
                     // $scope.getList = function(num) {
                     //     var list = [];
                     //     for (var i = 0; i < num; i++) {
                     //         list.push(i);
                     //     }
                     //     return list;
                     // };
                     $scope.getList = searchFactory.getList;
                     
                     $scope.searchContest = function(params_obj, faceted_url) {
                         $scope.contestCollection = [];
                         $('.spinner').show();
                         
                         /*var url = 'api/contests/search/facets/';
                           if (faceted_url !== undefined) {
                           url = url + faceted_url;
                           }
                           $http.get($localStorage.host + url, {
                           params: params_obj
                           }).success(function(response) {
                           $('.spinner').hide();
                           $scope.contestCollection = response.objects.results;
                           $scope.count = response.objects.count;
                           $scope.key = $scope.q;
                           $scope.count = response.objects.count;
                           $scope.page_count = Math.floor((parseInt($scope.count) + parseInt(params_obj.page_size) - 1) / parseInt(params_obj.page_size));
                           $scope.pages = $scope.getList($scope.page_count);
                           $scope.page_active = $scope.params_obj.page;
                           
                           }).error(function(data, status) {
                           console.log("Error status : " + status);
                           });*/
                         
                         var url = 'api/contests/search/facets/';
                         if (faceted_url !== undefined) {
                             url = url + faceted_url;
                         }
                         // var headers = {};
                         // if($localStorage.userKey !== undefined){
                         //     headers = { 'Authorization': 'Token ' + $localStorage.userKey };
                         // }
                         // console.log("INFO: authorization headers", headers);
                         // $http.get($localStorage.host + url, {
                         //     params: params_obj,
                         //     headers: headers
                         // }).success(function(response) {
                         //     $('.spinner').hide();
                         //     $scope.contestCollection = response.objects.results;
                         //     $scope.count = response.objects.count;
                         //     $scope.key = $scope.q;
                         //     $scope.count = response.objects.count;
                         //     $scope.page_count = (Math.floor((parseInt($scope.count) +
                         //                                      parseInt(params_obj.page_size)-1) /
                         //                                     parseInt(params_obj.page_size)));
                         //     $scope.pages = $scope.getList($scope.page_count);
                         //     $scope.page_active = $scope.params_obj.page;
                             
                         // }).error(function(data, status) {
                         //     console.log("Error status : " + status);
                         // });
                         HttpServices.get(url, params_obj)
                             .then(function(response) {
                                 $('.spinner').hide();
                                 $scope.contestCollection = response.data.objects.results;
                                 $scope.count = response.data.objects.count;
                                 $scope.key = $scope.q;
                                 $scope.count = response.data.objects.count;
                                 $scope.page_count = (Math.floor((parseInt($scope.count) +
                                                                  parseInt(params_obj.page_size)-1) /
                                                                 parseInt(params_obj.page_size)));
                                 $scope.pages = $scope.getList($scope.page_count);
                                 $scope.page_active = $scope.params_obj.page;
                             }, function(response) {
                                 $('.spinner').hide();
                                 console.log("ERROR: searchContest=", response);
                             });
                     };
                     
                     // $('.span2').slider().on('slideStop', function(ev) {
                     //     var newVal = $(this).data('slider').getValue();
                     //     $(this).val(newVal);
                     //     $(this).trigger('input');
                     //     $scope.filterContest();
                     // });
                     $scope.filterContest = function() {
                         $location.search('q', $scope.q);
                         $location.search('price_range', $scope.filter.price);
                         $location.search('permissions', $scope.filter.permissions);
                         $location.search('video_type', $scope.filter.video_type);
                         $location.search('orientation', $scope.filter.orientation);
                         $location.search('levels', $scope.filter.level);
                         $location.search('age', $scope.filter.age);
                         $location.search('status', $scope.filter.status);
                         $location.search('page_size', $scope.filter.page_size);
                         if ($scope.filter.page_size === undefined) {
                             $scope.filter.page_size = $scope.params_obj.page_size;
                         }
                         $scope.createUrl();
                     };
                     
                     searchFactory.setupSlider($scope.filterContest);
                     
                     $scope.createUrl = function() {
                         $scope.faceted_url = $scope.buildFacetedUrl();
                         $scope.params_obj.page_size = $scope.filter.page_size;
                         $scope.params_obj.page = 1;
                         $scope.searchContest($scope.params_obj, $scope.faceted_url);
                         
                     };
                     
                     // $scope.getPageData = function(page_num) {
                     //     $scope.leftPager = true;
                     //     $scope.rightPager = true;
                     //     $scope.params_obj.page = page_num;
                     //     if ($scope.params_obj.page == 1) {
                     //         $scope.leftPager = false;
                     //     }
                     //     if ($scope.params_obj.page == $scope.page_count) {
                     //         $scope.rightPager = false;
                     //     }
                         
                     //     $scope.searchContest($scope.params_obj);
                     // };

                     // $scope.leftPage = function(page_num) {
                     //     if ($scope.params_obj.page != 1) {
                     //         page_num = $scope.params_obj.page - 1;
                     //         $scope.getPageData(page_num)
                     //     }
                     // };
                     // $scope.rightPage = function(page_num) {
                     //     if ($scope.params_obj.page != $scope.count) {
                     //         page_num = $scope.params_obj.page + 1;
                     //         $scope.getPageData(page_num)
                     //     }
                     // };
                     searchFactory.setupPageNavigation($scope,
                                                       $scope.searchContest);
                     
                     
                     $scope.buildFacetedUrl = function() {
                         var url = '?selected_facets=';
                         
                         if ($scope.q != undefined) {
                             url = url + '&q=' + $scope.q;
                         }
                         if ($scope.filter.price != undefined) {
                             url = url + '&price_range=' + $scope.filter.price;
                         }
                         if ($scope.filter.permissions != undefined) {
                             url = url + '&permissions=' + $scope.filter.permissions;
                         }
                         if ($scope.filter.video_type != undefined) {
                             url = url + '&video_type=' + $scope.filter.video_type;
                         }
                         if ($scope.filter.orientation != undefined) {
                             url = url + '&orientation=' + $scope.filter.orientation;
                         }
                         if ($scope.filter.level != undefined) {
                             url = url + '&levels=' + $scope.filter.level;
                         }
                         if ($scope.filter.status != undefined) {
                             url = url + '&status=' + $scope.filter.status;
                         }
                         if ($scope.filter.age != undefined) {
                             url = url + '&age=' + $scope.filter.age;
                         }
                         return url;
                     };
                     
                     //    $scope.q = $location.search().q;
                     //    if ($scope.q !== undefined) {
                     //        $scope.params_obj.q = $scope.q;
                     //    }
                     //    $scope.searchContest($scope.params_obj);
                     //    $scope.searchQuery = function() {
                     //        $state.go('search_request', {
                     //            q: $scope.q
                     //        })
                     //    };
                     
                     $scope.init = function() {
                         $scope.filter = {};
                         $scope.params_obj = {
                             page_size: 10,
                             page: 1
                         };
                         $scope.permissions_list = [{
                             label: 'Free',
                             value: 'free'
                         }, {
                             label: 'Royalty Free',
                             value: 'royalty_free'
                         }, {
                             label: 'Commercial',
                             value: 'commercial'
                         }];
                         $scope.video_type_list = [{
                             label: 'Real Time',
                             value: 'real_time'
                         }, {
                             label: 'Slow Motion',
                             value: 'slow_motion'
                         }, {
                             label: 'Time Lapse',
                             value: 'time_lapse'
                         }];
                         $scope.orientations_list = [{
                             label: 'Square',
                             value: 'square'
                         }, {
                             label: 'Landscape',
                             value: 'landscape'
                         }, {
                             label: 'Portrait',
                             value: 'portrait'
                         }];
                         $scope.result_page_list = [{
                             label: '10',
                             value: '10'
                         }, {
                             label: '20',
                             value: '20'
                         }, {
                             label: '30',
                             value: '30'
                         }, {
                             label: '50',
                             value: '50'
                         }, {
                             label: '100',
                             value: '100'
                         }];
                         $scope.sort_result = [{
                             label: 'Newest',
                             value: 'new'
                         }, {
                             label: 'Oldest',
                             value: 'old'
                         }];
                         $scope.status_list = [{
                             label: 'Open',
                             value: 'open'
                         }, {
                             label: 'Closed',
                             value: 'closed'
                         }];
                         
                         function get_url_value(key) {
                             var value = $location.search()[key] || undefined;
                             if (typeof(value) == 'string')
                                 value = [value];
                             return value
                             
                         }
                         $scope.filter.permissions = get_url_value('permissions');
                         $scope.filter.video_type = get_url_value('video_type');
                         $scope.filter.orientation = get_url_value('orientation');
                         $scope.filter.level = get_url_value('levels');
                         $scope.filter.age = get_url_value('age');
                         $scope.filter.price = get_url_value('price_range');
                         $scope.filter.status = get_url_value('status');
                         $scope.filter.page_size = get_url_value('page_size');
                         $scope.q = $location.search().q;
                         if ($scope.q !== undefined) {
                             $scope.params_obj.q = $scope.q;
                         }
                         //         $scope.searchVideos($scope.params_obj);
                         $scope.filterContest();
                         levelFactory.async().then(function(data) {
                             $scope.levelCollection = data[0].data;
                             $scope.leftPager = true;
                             $scope.rightPager = true;
                         });
                     };
                     $scope.init();
                 }])


    .factory('levelFactory',
             ['$localStorage',
              //'$http',
              'HttpServices',
              function($localStorage,
                       //$http,
                       HttpServices) {
    var levelFactory = {
        async: function() {
            // var promise = $http.get(
            //     $localStorage.host + 'api/profiles/level/'
            // ).then(function(response) {
            //     return response.data.results;
            // });
            // return promise;
            var promise = HttpServices.get('api/profiles/level/')
                .then(function(response) {
                    return response.data.results;
                });
            return promise;
        }

    };
    return levelFactory;
}])

.factory('searchFactory',
         ['$rootScope',
          //'$scope',
          //'$http',
          '$anchorScroll',
          '$location',
          '$state',
          '$localStorage',
          '$timeout',
          'Cart',
          'toaster',
          'levelFactory',
          'HttpServices',
          'Utils',
          function($rootScope,
                   //$scope,
                   //$http,
                   $anchorScroll,
                   $location,
                   $state,
                   $localStorage,
                   $timeout,
                   Cart,
                   toaster,
                   levelFactory,
                   HttpServices,
                   Utils) {
              //
              // factory functions
              //
              var sf = {};

              sf.getList = function(num) {
                  var list = [];
                  for (var i = 0; i < num; i++) {
                      list.push(i);
                  }
                  return list;
              };

              // to call the function on slider stop.
              sf.setupSlider = function(filterFunction){
                  $('.span2').slider().on('slideStop', function(ev) {
                      var newVal = $(this).data('slider').getValue();
                      $(this).val(newVal);
                      $(this).trigger('input');
                      filterFunction();
                  });
              };
              
              sf.setupPageNavigation = function(scope, searchFunction){
                  scope.getPageData = function(page_num){
                      scope.leftPager = true;
                      scope.rightPager = true;
                      scope.params_obj.page = page_num;
                      if (scope.params_obj.page == 1) {
                          scope.leftPager = false;
                      }
                      if (scope.params_obj.page == scope.page_count) {
                          scope.rightPager = false;
                      }
                      searchFunction(scope.params_obj);
                  };

                  scope.leftPage = function(page_num) {
                      if (scope.params_obj.page != 1) {
                          page_num = scope.params_obj.page - 1;
                          scope.getPageData(scope, page_num);
                      }
                  };

                  scope.rightPage = function(page_num) {
                      if (scope.params_obj.page != scope.page_count) {
                          page_num = scope.params_obj.page + 1;
                          scope.getPageData(page_num);
                      }
                  };
              }; // sf.setupPageNavigation()

              sf.setupTypeAhead = function(scope){
                  // $http.get($localStorage.host + 'api/videos/title/').success(function(data) {
                  //     $scope.recommentations = data
                  // });
                  // ToDo: this might very long when the # of videos grow
                  HttpServices.get('api/videos/title/')
                      .then(function(response) {
                          scope.recommentations = response.data
                      });
                  
                  $('.typeahead').on('typeahead:selected', function(e, datum) {
                      scope.q = datum;
                  });
                  
                  var states = scope.recommentations;
                  //console.log(states)
                  
                  $('.typeahead').typeahead({
                      hint: true,
                      highlight: true,
                      minLength: 1
                  }, {name: 'states',
                      source: Utils.substringMatcher(states)});
              };
              
              sf.setupVideoSearch = function(scope, doProfileContestSearch){
                  //sf.setupTypeAhead(scope);
                  // $scope.addToCart = function(video_id) {
                  //     Cart.add_to_cart(video_id, 'video').then(function(cart_data) {
                  //         $rootScope.cart = cart_data.data;
                  //         toaster.pop('success', "Success", "Item added to cart");
                  //     }, function(error) {
                  //         toaster.pop('info', "Warning", "This item is already in your cart");
                  //     });
                  // };
                  
                  scope.addToCart = function(video_id) {
                      Cart.add_to_cart(video_id, 'video')
                          .then(function(response) {
                              $rootScope.cart = response.data;
                              toaster.pop('success', "Success", "Item added to cart");
                          }, function(response) {
                              toaster.pop('info',
                                          "Warning",
                                          response.data.error);
                              //"This item is already in your cart");
                          });
                  };
                  
                  scope.getList = sf.getList;
                  
                  scope.searchVideos = function(params_obj, faceted_url) {
                      scope.videoCollection = [];
                      //$scope.noCollection = false;
                      
                      
                      //    $scope.params_obj = {
                      //        page_size: 10,
                      //        page: 1
                      //    };
                      //$('.spinner').show();
                      scope.showSpinner = true;
                      var url = 'api/videos/search/facets/';
                      if (faceted_url !== undefined) {
                          url = url + faceted_url;
                      }
                      HttpServices.get(url, params_obj)
                          .then(function(response) {
                              //$('.spinner').hide();
                              scope.videoCollection = response.data.objects.results;
                              scope.locations=[]
                              for(var i=0;i<scope.videoCollection.length;i++) {
                                  if(scope.videoCollection[i].location_name !== null){
                                      var k = scope.videoCollection[i].location_name.split(',')
                                      if(scope.locations.indexOf(k[2]) == -1){
                                          scope.locations.push(k[2]);
                                      }
                                  }
                              }
                              // if (!$scope.videoCollection.length) {
                              //     $scope.noCollection = true;
                              // }
                              scope.key = scope.q;
                              scope.count = response.data.objects.count;
                              scope.page_count = Math.floor((parseInt(scope.count) +
                                                             parseInt(params_obj.page_size) - 1) /
                                                            parseInt(params_obj.page_size));
                              scope.pages = scope.getList(scope.page_count);
                              scope.page_active = scope.params_obj.page;
                              $timeout(function(){
                                  enableAutoPlay("setupVideoSearch");
                              }, 0, false);
                              scope.showSpinner = false;
                          }, function(response) {
                              //$('.spinner').hide();
                              scope.showSpinner = false;
                              console.log("ERROR: searchVideos=", response);
                          });
                  };
                  
                  scope.profileSearch = function() {
                      $('.contest-spinner').show();
                      $('.profile-spinner').show();
                      
                      HttpServices.get('api/profiles/search/facets/',
                                       {q: scope.q})
                          .then(function(response) {
                              scope.profileCollection = response.data.objects.results;
                              scope.profile_count = response.data.objects.count
                              $('.profile-spinner').hide();
                          }, function(response) {
                              $('.profile-spinner').hide();
                              console.log("ERROR: profileSearch=", response);
                          });
                      
                      HttpServices.get('api/contests/search/facets/',
                                       {q: scope.q})
                          .then (function(response) {
                              scope.contestCollection = response.data.objects.results;
                              scope.contest_count = response.data.objects.count
                              $('.contest-spinner').hide();
                          }, function(response) {
                              $('.contest-spinner').hide();
                              console.log("ERROR: contestSearch=", response);
                          });
                  };
                  
                  // to call the function on slider stop.
                  // $('.span2').slider().on('slideStop', function(ev) {
                  //     var newVal = $(this).data('slider').getValue();
                  //     $(this).val(newVal);
                  //     $(this).trigger('input');
                  //     scope.filterVideos();
                  // });
                  
                  scope.filterVideos = function() {
                      if (scope.category == 'contest') {
                          $state.go('search_request', {
                              q: scope.q
                          })
                      } else if (scope.category == 'member') {
                          $state.go('search_user', {
                              q: scope.q
                          })
                      } else if (scope.category == 'videos') {
                          $state.go('video-search', {
                              q: scope.q
                          })
                      } else {
                          $location.search('q', scope.q);
                          $location.search('price_range', scope.filter.price);
                          $location.search('permissions', scope.filter.permissions);
                          $location.search('video_type', scope.filter.video_type);
                          $location.search('orientation', scope.filter.orientation);
                          $location.search('location', scope.filter.location);
                          $location.search('levels', scope.filter.level);
                          $location.search('rating', scope.filter.rating);
                          $location.search('age', scope.filter.age);
                          $location.search('page_size', scope.filter.page_size);
                          if (scope.filter.page_size === undefined) {
                              scope.filter.page_size = scope.params_obj.page_size;
                          }
                          scope.createUrl();
                      }
                  };
                  sf.setupSlider(scope.filterVideos);
                  
                  scope.createUrl = function() {
                      scope.faceted_url = scope.buildFacetedUrl();
                      scope.params_obj.page_size = scope.filter.page_size;
                      scope.params_obj.page = 1;
                      scope.searchVideos(scope.params_obj, scope.faceted_url);
                  };

                  // scope.getPageData = function(page_num) {
                  //     scope.leftPager = true;
                  //     scope.rightPager = true;
                  //     scope.params_obj.page = page_num;
                  //     if (scope.params_obj.page == 1) {
                  //         scope.leftPager = false;
                  //     }
                  //     if (scope.params_obj.page == scope.page_count) {
                  //         scope.rightPager = false;
                  //     }
                      
                  //     scope.searchVideos(scope.params_obj);
                  // };

                  // scope.leftPage = function(page_num) {
                  //     if (scope.params_obj.page != 1) {
                  //         page_num = scope.params_obj.page - 1;
                  //         scope.getPageData(page_num)
                  //     }
                  // };
                  
                  // scope.rightPage = function(page_num) {
                  //     if (scope.params_obj.page != scope.page_count) {
                  //         page_num = scope.params_obj.page + 1;
                  //         scope.getPageData(page_num)
                  //     }
                  // };
                  sf.setupPageNavigation(scope, scope.searchVideos);
                  
                  scope.buildFacetedUrl = function() {
                      var url = '?selected_facets=';
                      if (scope.filter.location != undefined) {
                          url = url + 'location:' + scope.filter.location;
                      }
                      if (scope.q != undefined) {
                          url = url + '&q=' + scope.q;
                      }
                      if (scope.filter.price != undefined) {
                          url = url + '&price_range=' + scope.filter.price;
                      }
                      if (scope.filter.permissions != undefined) {
                          url = url + '&permissions=' + scope.filter.permissions;
                      }
                      if (scope.filter.video_type != undefined) {
                          url = url + '&video_type=' + scope.filter.video_type;
                      }
                      if (scope.filter.orientation != undefined) {
                          url = url + '&orientation=' + scope.filter.orientation;
                      }
                      if (scope.filter.level != undefined) {
                          url = url + '&levels=' + scope.filter.level;
                      }
                      if (scope.filter.rating != undefined) {
                          url = url + '&rating=' + scope.filter.rating;
                      }
                      if (scope.filter.age != undefined) {
                          url = url + '&age=' + scope.filter.age;
                      }
                      return url;
                  };
                  
                  //    $scope.q = $location.search().q;
                  //    if ($scope.q !== undefined) {
                  //        $scope.params_obj.q = $scope.q;
                  //    }
                  //    $scope.searchVideos($scope.params_obj);
                  //    $scope.profileSearch()
                  //    $scope.searchQuery = function() {
                  //        $state.go('video-search', {
                  //            q: $scope.q
                  //        })
                  //    };
                  
                  scope.init = function() {
                      scope.filter = {};
                      scope.params_obj = {
                          page_size: 10,
                          page: 1
                      };
                      // scope.permissions_list = [{
                      //     label: 'Free',
                      //     value: 'free'
                      // }, {
                      //     label: 'Royalty Free',
                      //     value: 'royalty_free'
                      // }, {
                      //     label: 'Commercial',
                      //     value: 'commercial'
                      // }];
                      // scope.video_type_list = [{
                      //     label: 'Real Time',
                      //     value: 'real_time'
                      // }, {
                      //     label: 'Slow Motion',
                      //     value: 'slow_motion'
                      // }, {
                      //     label: 'Time Lapse',
                      //     value: 'time_lapse'
                      // }];
                      // scope.orientations_list = [{
                      //     label: 'Square',
                      //     value: 'square'
                      // }, {
                      //     label: 'Landscape',
                      //     value: 'landscape'
                      // }, {
                      //     label: 'Portrait',
                      //     value: 'portrait'
                      // }];
                      var init_video_filters = function(){
                          HttpServices.get("api/general/default-config/")
                              .then(function(response){
                                  var defaultCfg = response.data;
                                  scope.video_type_list = [];
                                  for (var i = 0;
                                       i < defaultCfg.video_type.results.length;
                                       i++){
                                      var vt = defaultCfg.video_type.results[i];
                                      scope.video_type_list.push({label: vt.display_name,
                                                                  value: vt.video_type});
                                  }
                                  scope.orientations_list = [];
                                  for (var i = 1; // don't display "any"
                                       i < defaultCfg.video_orientation.results.length;
                                       i++){
                                      var vo = defaultCfg.video_orientation.results[i];
                                      scope.orientations_list.push({label: vo.display_name,
                                                                    value: vo.orientation});
                                  }
                              }, function(response){
                                  console.log("ERROR: init_video_filters default-config=",
                                             response)
                              });
                      };
                      init_video_filters();

                      scope.result_page_list = [{
                          label: '10',
                          value: '10'
                      }, {
                          label: '20',
                          value: '20'
                      }, {
                          label: '30',
                          value: '30'
                      }, {
                          label: '50',
                          value: '50'
                      }, {
                          label: '100',
                          value: '100'
                      }];
                      scope.sort_result = [{
                          label: 'Newest',
                          value: 'new'
                      }, {
                          label: 'Oldest',
                          value: 'old'
                      }];
                      
                      function get_url_value(key) {
                          var value = $location.search()[key] || undefined;
                          if (typeof(value) == 'string')
                              value = [value];
                          return value
                          
                      }
                      scope.filter.location = get_url_value('location');
                      scope.filter.permissions = get_url_value('permissions');
                      scope.filter.video_type = get_url_value('video_type');
                      scope.filter.orientation = get_url_value('orientation');
                      scope.filter.level = get_url_value('levels');
                      scope.filter.rating = get_url_value('rating');
                      scope.filter.age = get_url_value('age');
                      scope.filter.price = get_url_value('price_range');
                      scope.filter.page_size = get_url_value('page_size');
                      scope.q = $location.search().q;
                      if (scope.q !== undefined) {
                          scope.params_obj.q = scope.q;
                      }
                      //scope.searchVideos(scope.params_obj);
                      if (doProfileContestSearch) {
                          scope.profileSearch();
                      }
                      scope.filterVideos();
                      levelFactory.async().then(function(data) {
                          scope.levelCollection = data[0].data;
                          scope.leftPager = true;
                          scope.rightPager = true;
                      });
                  };
                  scope.init();
              }; // sf.setupVideoSearch()

              return (sf);
          }])

