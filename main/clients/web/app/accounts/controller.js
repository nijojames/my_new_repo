'use strict';

angular
    .module('spyhop.accounts',
            ['ngRoute',
             'ui.router',
             'ngAnimate',
             'ui.bootstrap'])

    .controller('ProfileViewCtrl',
                ['$scope',
                 '$stateParams',
                 '$state',
                 '$location',
                 '$localStorage',
                 'ProfileList',
                 function($scope,
                          $stateParams,
                          $state,
                          $location,
                          $localStorage,
                          ProfileList) {

                     $scope.follow_page_data = {page: 1,
                                                page_size: 10,
                                                data: [],
                                                loading: true};
                     $scope.followers_page_data = {page: 1,
                                                   page_size: 10,
                                                   data: [],
                                                   loading: true};
                     $scope.uploads_page_data = {page: 1,
                                                 page_size: 10,
                                                 /*data:[],*/
                                                 loading: true};
                     $scope.liked_page_data = {page: 1,
                                               page_size: 10,
                                               data: [],
                                               loading: true};
                     $scope.contest_page_data = {page: 1,
                                                 page_size: 10,
                                                 data: [],
                                                 loading: true};
                     if ($state.current.name === 'profile' &&
                         $localStorage.userName &&
                         $localStorage.userKey){
                         $scope.own_profile = true;
                         $scope.username = $localStorage.userName;
                     }else if($state.current.name === 'user_profile' &&
                              $stateParams.username){
                        $scope.username = $stateParams.username;
                     }
                     $scope.ownContestEdit=function(slug,status){
                         if(status=='publish' ||
                            status=='closed' ||
                            status=='review'){
                             $state.go('contest_detail',({slug: slug}));
                         }
                         else if(status=='draft' || status=='incomplete'){
                             $state.go('review', {slug: slug});
                         }
                     };
                     ProfileList.get_profile($scope.username)
                         .then(function(response) {
                             $scope.user = response.data;
                             console.log($scope.user, "user");
                         }, function(response) {
                             console.log("ERROR: get_profile=", response);
                         });
                     $scope.get_following = function(page){
                         $scope.$emit('loadmorefollowing');
                         ProfileList.get_profile_follow($scope.username,
                                                        {page: page,
                                                         page_size: $scope.follow_page_data.page_size})
                             .then(function(response) {
                                 angular.forEach(response.data.results, function(contest) {
                                    $scope.follow_page_data.data.push(contest);
                                 });
                                 $scope.follow_page_data.page++;
                             }, function(response) {
                                 $scope.follow_page_data.loading = false;
                                 console.log("ERROR: get_profile_follow=", response);

                             });
                     };
                     $scope.get_following($scope.follow_page_data.page);
                     $scope.get_followers = function(page) {
                         $scope.$emit('loadmorefollowers');
                         ProfileList.get_profile_followers($scope.username,
                                                           {page: page,
                                                            page_size: $scope.followers_page_data.page_size})
                             .then(function(response) {
                                 angular.forEach(response.data.results, function(contest) {
                                    $scope.followers_page_data.data.push(contest);
                                 });
                                 $scope.followers_page_data.page++;
                             }, function(response) {
                                 $scope.followers_page_data.loading = false;
                                 console.log("ERROR: get_profile_followers=", response);

                             });
                     };
                     $scope.get_followers($scope.followers_page_data.page);
                     
                     $scope.get_videos = function(page){
                         $scope.$emit('loadmoreuploads');
                         ProfileList.get_profile_videos($scope.username,
                                                        {page: page,
                                                         page_size: $scope.uploads_page_data.page_size})
                             .then(function(response) {
                                 $scope.uploads_page_data.data = [];
                                 angular.forEach(response.data.results, function(video) {
                                    $scope.uploads_page_data.data.push(video);
                                 });
                                 // console.log("DEBUG: get_videos=",
                                 //             $scope.uploads_page_data.data);
                                 $scope.uploads_page_data.page++;
                             }, function(response) {
                                 if (!$scope.uploads_page_data.data) {
                                     $scope.uploads_page_data.data = [];
                                 }
                                 $scope.uploads_page_data.loading = false;
                                 console.log ("ERROR: get_profile_videos=",
                                              response);
                             });
                     };
                     $scope.get_videos($scope.uploads_page_data.page);
                     
                     $scope.get_contests = function(page) {
                        $scope.$emit('loadmorecontests');
                         ProfileList.get_profile_created_contest($scope.username,
                                                                 {page: page,
                                                                  page_size:$scope.contest_page_data.page_size})
                             .then(function(response) {
                                 angular.forEach(response.data.results, function(contest) {
                                    $scope.contest_page_data.data.push(contest);
                                 });
                                 $scope.contest_page_data.page++;
                             }, function(response) {
                                 $scope.contest_page_data.loading = false;
                                 console.log("ERROR: get_profile_created_contest=", response);
                             });
                     };
                     $scope.get_contests($scope.contest_page_data.page);
                     
                     $scope.get_liked = function(page){
                         $scope.$emit('loadmoreliked');
                         ProfileList.get_videos_liked($scope.username,
                             {page: page, page_size:$scope.liked_page_data.page_size})
                             .then(function(response) {

                                 angular.forEach(response.data.results, function(liked) {
                                    $scope.liked_page_data.data.push(liked);
                                 });
                                 $scope.liked_page_data.page++;
                                 console.log($scope.liked_page_data.data, "scope.liked_page_data.data")
                             }, function(response) {
                                 $scope.liked_page_data.loading = false;
                                 console.log("ERROR: get_videos_liked=", response);
                             });
                     };
                     $scope.get_liked($scope.liked_page_data.page);
                     // $scope.loadMoreContests = function(){
                     //     $scope.get_contests($scope.contest_page_data.page);
                     // };
                 }])

    .factory('ProfileList',
             ['$localStorage',
              'Main',
              'HttpServices',
              function($localStorage,
                       Main,
                       HttpServices) {
                  
                  var pl = {};
                  
                  pl.get_profile = function(username){
                      return (Main.get_profile(username));
                  },

                  pl.get_profile_follow = function(username, params){
                      return (HttpServices.get("api/profiles/profile-following?username="+
                                               username, params));
                  },
                  
                  pl.get_profile_followers = function(username, params){
                      return (HttpServices.get("api/profiles/profile-followers?username="+
                                               username, params));
                  },
                  
                  pl.get_profile_videos = function(username, params){
                      return (HttpServices.get("api/profiles/profile-videos-uploaded?username="+
                                               username, params));
                  },
                  
                  pl.get_profile_created_contest = function(username, params){
                      return (HttpServices.get("api/profiles/profile-created-contest?username="+
                                               username, params));
                  },
                  
                  pl.get_videos_liked = function(username, params){
                      return (HttpServices.get("api/profiles/profile-videos-liked?username="+
                                               username, params));
                  };
                  
                  return (pl);
              }]);
