'use strict';

describe('spyhop.accounts module', function() {

  beforeEach(module('spyhop.accounts'));

  describe('accounts controller', function(){
    var scope;
    it('should run :', inject(function($rootScope, $controller, $injector) {
      scope = $rootScope.$new();
      var view1Ctrl = $controller('LoginViewCtrl',{
            $scope: scope
        });
      expect(view1Ctrl).toBeDefined();

      var view1Ctrl = $controller('ProfileViewCtrl',{
            $scope: scope
        });
      expect(view1Ctrl).toBeDefined();

      var view1Ctrl = $controller('LogoutViewCtrl',{
            $scope: scope
        });
      expect(view1Ctrl).toBeDefined();
    }));

  });
});