'use strict';

angular.module('home.controllers', ['ngRoute', 'ui.router'])

    .controller('MainCrtl',
                ['$rootScope',
                 '$scope',
                 '$state',
                 //'$http',
                 '$location',
                 '$auth',
                 '$localStorage',
                 '$timeout',
                 'Cart',
                 'toaster',
                 'Main',
                 'HttpServices',
                 'vcRecaptchaService',
                 'searchFactory',
                 function($rootScope,
                          $scope,
                          $state,
                          //$http,
                          $location,
                          $auth,
                          $localStorage,
                          $timeout,
                          Cart,
                          toaster,
                          Main,
                          HttpServices,
                          vcRecaptchaService,
                          searchFactory) {

                     $scope.resetAll = function(){
                         $scope.form = {};
                         $scope.email = null;
                         $scope.password = null;
                     };

                     $scope.resetAll();

                     // HttpServices.get('api/videos/title/').then(function(response) {
                     //     $scope.recommentations = response.data
                     // });
                     searchFactory.setupTypeAhead($scope);
                     
                     $scope.searchQuery = function() {
                         var query = $scope.query;
                         $scope.query = undefined;
                         $state.go('search', {
                             q: query
                         });
                     };

                     $scope.searchTermSelected = function(item, model, label, event){
                         //console.log(item, model, label, event, $scope.query);
                         $scope.searchQuery();
                     };

                     $scope.getNotifications = function(){
                         Main.update_notification_data();
                     };
                     
                     $scope.goto = function(notification) {
                         if(notification.target_details.type=='video'){
                             $state.go('video', {slug: notification.target_details.slug})
                         }
                         else if(notification.target_details.type='user'){
                             $state.go('user_profile', {username: notification.user.username})
                         }
//                         Main.update_notification(notification.id)
//                             .then(function(response){
//                             },
//                                   function(response){
//                                       console.log("ERROR: Main.update_notification=",
//                                                   response)
//                                   });
                     }
                     $scope.get_feed = function(id) {
                         Main.update_feed(id)
                             .then(function(response){},
                                   function(response){
                                       console.log("ERROR: Main.update_feed=",
                                                   response);
                                   });
                     }
                     
                     $scope.loginSubmit = function() {
                         $scope.has_error = false;
                         var data = {
                             'email': $scope.email,
                             'password': $scope.password
                         };
                         
                         // $http.post($localStorage.host +
                         //            'api/rest-auth/login/',
                         //            data).success(function(data, status) {
                         //                $localStorage.userKey = data.key;
                         //                $localStorage.userName = data.username;
                         //                $rootScope.isUserLoggedIn = true;
                         //                $('#myModal').modal('hide').one('hidden.bs.modal', function() {
                         //                    //console.log("INFO: login_close");
                         //                    $('#login_close').trigger('click');
                         //                    //$('body').removeClass('modal-open');
                         //                    //$('.modal-backdrop').remove();
                         //                    Main.get_user_data(false);
                         //                });
                         //            }).error(function(data, status, headers, config) {
                         //                if (data.non_field_errors) {
                         //                    $scope.general_error = 'Invalid email or password.';
                         //                }
                         //                switch (status) {
                         //                case 400:
                         //                    //$scope.email = '';
                         //                    $scope.password = '';
                         //                    $scope.has_error = true;
                         //                    $scope.error_message = 'Invalid email password.';
                         //                    break;
                         //                case -1:
                         //                    $scope.has_error = true;
                         //                    $scope.error_message = 'Backend server error.';
                         //                    break;
                         //                default:
                         //                    $scope.has_error = true;
                         //                    $scope.error_message = 'Some error has occurred. Please try again.';
                         //                }
                         //            })

                         HttpServices.post('api/rest-auth/login/', data)
                             .then(function(response) {
                                 //console.log("INFO: /login/=", response);
                                 $localStorage.userKey = response.data.key;
                                 $localStorage.userName = response.data.username;
                                 $rootScope.isUserLoggedIn = true;
                                 $('#myModal').modal('hide').one('hidden.bs.modal', function() {
                                     //console.log("INFO: login_close");
                                     $('#login_close').trigger('click');
                                     //$('body').removeClass('modal-open');
                                     //$('.modal-backdrop').remove();
                                     $scope.clearForm($scope.loginForm);
                                     Main.get_user_data(false);
                                 });
                             }, function(response) {
                                 console.log("ERROR: /login/=", response);
                                 if (response.data.non_field_errors) {
                                     $scope.general_error = 'Invalid email or password.';
                                 }
                                 switch (response.status) {
                                 case 400:
                                     //$scope.email = '';
                                     $scope.password = '';
                                     $scope.has_error = true;
                                     $scope.error_message = 'Invalid email password.';
                                     break;
                                 case -1:
                                     $scope.has_error = true;
                                     $scope.error_message = 'Backend server error.';
                                     break;
                                 default:
                                     $scope.has_error = true;
                                     $scope.error_message = 'Some error has occurred. Please try again.';
                                 }
                             });
                     }
                     $scope.clearForm = function (formObj) {
                         $('#myModal').find('form')[0].reset();
                         $('#sign_up').find('form')[0].reset();
                         formObj.$setPristine();
                         formObj.$setUntouched();
                         $scope.resetAll();
                     };
                     //Closes the first opened Pop up on opening another one recursively
                     $scope.showSignUp = function() {
                         $("#myModal").removeClass("fade").modal("hide");
                         //$("#sign_up").addClass("fade").modal("show");
                     }

                     $scope.loginSignUpClick = function() {
                         $scope.showSignUp();
                     }
                     $scope.logOut = function (){
                         // $.sidr('toggle', 'right-menu');
                         // if ($localStorage.userKey === null ||
                         //     $localStorage.userName === null) {
                         //     $location.path('/');
                         // }
                         // $http.post($localStorage.host + 'api/rest-auth/logout/', {
                         //     headers: {
                         //         'Authorization': 'Token '+$localStorage.userKey
                         //     }
                         // }).success(function(response){
                         //     $rootScope.cart.item_count = 0;
                         //     $rootScope.notification_count = 0;
                         //     $timeout.cancel($rootScope.refreshNotificationTimeout);
                         //     delete $localStorage.userKey;
                         //     delete $localStorage.userName;
                         //     $('#myModal').modal('hide');
                         //     $rootScope.isUserLoggedIn = false;
                         //     $location.path('/');
                         // });
                         Main.logout();
                     };
                     
                     $scope.showLogin = function() {
                         $("#sign_up").removeClass("fade").modal("hide");
                         //$("#sign_up").addClass("fade").modal("show");
                     }
                     
                     $scope.signUpLoginClick = function() {
                         $scope.showLogin();
                     }
                     
                     //End Section
                     
                     
                     //password reset window pop up close when on same state
                     $scope.sameStateResetPassword = function() {
                         if ($state.current.name == 'forgot_password')
                             $scope.showSignUp();
                         $scope.showLogin();
                     }
                     //End Section
                     
                     
                     //Function that clears the server validation error of email on key release
                     $scope.clearServeremailValidationLogin = function() {
                         $scope.general_error = false;
                     }
                     $scope.order = function() {
                         $state.go('order-list');
                     }

                     // ToDo: all this needs to change when we switch to uibmodal
                     //       we will use promise then
                     $('#myModal').on('shown.bs.modal', function () {
                          $('#id_email').focus();
                     });
                     
                     $('#myModal').on('hidden.bs.modal', function(e) {
                         // if user did not logged in and modal is hidden,
                         // it means either user cancelled the login or
                         // clicked on signup so delete the toStateAfterLogin
                         if (!$rootScope.isUserLoggedIn) {
                             delete $rootScope.toStateAfterLogin;
                             delete $rootScope.toParamsAfterLogin;
                         }
                         $scope.login_submitted = false;
                         //console.log(e);
                         $timeout(function() {
                             $('#login_close').trigger('click');
                         }, 0, false);
                     });

                     $('#sign_up').on('shown.bs.modal', function () {
                         $('#first_name').focus();
                     });
                     
                     $('#sign_up').on('hidden.bs.modal', function(e) {
                         $scope.submitted = false;
                         //console.log(e);
                         $timeout(function() {
                             vcRecaptchaService.reload($scope.recaptchaWidgetId);
                             $('#signup_close').trigger('click');
                         }, 0, false);
                     });
                     
                     
                     //signup
                     $scope.recaptchaResponse = null;
                     $scope.recaptchaWidgetId = null;
                     $scope.recaptchaServerError = false;

                     $scope.setResponse = function (response) {
                         $scope.recaptchaServerError = false;
                     };
                     $scope.setWidgetId = function(widgetId) {
                         //console.log('DEBUG: rc widgetId=', widgetId);
                         $scope.recaptchaWidgetId = widgetId;
                     };

                     $scope.cbExpiration = function() {
                         //console.log('DEBUG: rc expired');
                         vcRecaptchaService.reload($scope.recaptchaWidgetId);
                         //$scope.response = null;
                     };

                     $scope.signupSubmit = function() {
                         HttpServices.post('api/general/recaptcha/',
                                           {'recaptcha': $scope.recaptchaResponse})
                             .then(function(response){
                                 //console.log("DEBUG: recaptcha=", response);
                                 $scope.signupHelper();
                             }, function(response){
                                 //console.log("ERROR: recaptcha=", response);
                                 $scope.recaptchaServerError = true;
                                 vcRecaptchaService.reload($scope.recaptchaWidgetId);
                             });
                     };

                     $scope.signupHelper = function(){
                         // $http.post($localStorage.host +
                         //            'api/rest-auth/registration/',
                         //            $scope.form).success(function(data, status) {
                         //                $localStorage.userKey = data.key;
                         //                $localStorage.userName = data.username;
                         //                $rootScope.isUserLoggedIn = true;
                         //                //document.getElementById('signup_close').click();
                         //                $('#sign_up').modal('hide').one('hidden.bs.modal', function() {
                         //                    //console.log("INFO: signup_close");
                         //                    $('#signup_close').trigger('click');
                         //                    $scope.clearForm($scope.signupForm);
                         //                    Main.get_user_data(true);
                         //                });
                         //            }).error(function(data, status, headers, config) {
                         //                //console.log("INFO: data=", data);
                         //                //console.log("INFO: status=", status);
                         //                // if (data.email)
                         //                //     console.log('email.............',data.email)
                         //                // $scope.email_error = data.email[0];
                         //                if (data.password2)
                         //                    $scope.password_error = data.password2[0];
                                        
                         //                $scope.signupErrors = data;
                         //            })
                         $scope.form.type = 'video_buyer';
                         HttpServices.post('api/rest-auth/registration/', $scope.form)
                             .then(function(response) {
                                 //console.log("INFO: response=", response);
                                 $localStorage.userKey = response.data.key;
                                 $localStorage.userName = response.data.username;
                                 $rootScope.isUserLoggedIn = true;
                                 //document.getElementById('signup_close').click();
                                 $('#sign_up').modal('hide').one('hidden.bs.modal', function() {
                                     //console.log("INFO: signup_close");
                                     $('#signup_close').trigger('click');
                                     $scope.clearForm($scope.signupForm);
                                     Main.get_user_data(false);
                                 });
                             }, function(response) {
                                 console.log("ERROR: response=", response);
                                 //console.log("INFO: data=", response.data);
                                 //console.log("INFO: status=", response.status);
                                 if (response.data.email) {
                                     //console.log('email.............',response.data.email)
                                     $scope.email_error = response.data.email[0];
                                 }
                                 if (response.data.password2)
                                     $scope.password_error = response.data.password2[0];
                                 $scope.signupErrors = response.data;
                             });
                     }
                     
                     // Clears the email validation from server when new input is given on sign-up
                     $scope.clear_server_validation_email = function() {
                         $scope.email_error = '';
                     }
                     
                     // Clears the password validation from server when new input is given on sign-up
                     $scope.clear_server_validation_password = function() {
                         $scope.password_error = '';
                     }
                     
                     
                     $scope.authenticate = function(provider) {
                         $auth.authenticate(provider).then(function(response) {
                             if (response.data.username == "") {
                                 document.getElementById('login_close').click();
                                 document.getElementById('signup_close').click();
                                 toaster.pop('info', "Warning", "User already exist");
                             } else {
                                 $localStorage.userKey = response.data.key;
                                 $localStorage.userName = response.data.username;
                                 $rootScope.isUserLoggedIn = true;
                                 document.getElementById('login_close').click();
                                 document.getElementById('signup_close').click();
                                 $location.path('/profile');
                             }
                             
                         });
                     };
                 }])


    .controller('ContactUsCtrl',
                ['$scope',
                 //'$http',
                 '$state',
                 '$location',
                 '$anchorScroll',
                 '$stateParams',
                 '$localStorage',
                 '$window',
                 'HttpServices',
                 function($scope,
                          //$http,
                          $state,
                          $location,
                          $anchorScroll,
                          $stateParams,
                          $localStorage,
                          $window,
                          HttpServices) {
                     $scope.contactForm = {};
                     $scope.$watchCollection('$stateParams', function() {
                         $anchorScroll();
                     });
                     // Clears the email validation from server when new input is given on sign-up
                     $scope.clear_server_validation_email = function() {
                         $scope.email_error = '';
                     };
                     $scope.setTopic = function(type) {
                         if (type !== undefined) {
                             $scope.contactForm.topic = type;
                             $scope.contactForm.hidden = 1;
                             $scope.topicSelect = false;
                         } else {
                             $scope.contactForm.topic = undefined;
                             $scope.contactForm.hidden = undefined;
                         }
                     };
                     // $http.get($localStorage.host + 'api/contact-us/topics/', {
                         
                     // }).success(function(response) {
                     //     $scope.topicsType = response.results;
                         
                     // }).error(function(data, status) {
                     //     console.log("Error status : " + status);
                     // });
                     HttpServices.get('api/contact-us/topics/')
                         .then(function(response) {
                             //console.log("INFO: /topics=", response);
                             $scope.topicsType = response.data.results;
                         }, function(response) {
                             console.log("ERROR: /topics=", response);
                         });

                     // $http.get('api/contact-us/information/', {
                         
                     // }).success(function(response) {
                     //     $scope.contactInfo = response.results[0];
                     //     $window.map = new google.maps.Map(document.getElementById('map'), {
                     //         center: {
                     //             lat: parseFloat(response.results[0].latitude),
                     //             lng: parseFloat(response.results[0].longitude)
                     //         },
                     //         zoom: 17
                     //     });
                     //     var marker = new google.maps.Marker({
                     //         position: new google.maps.LatLng(response.results[0].latitude,
                     //                                          response.results[0].longitude),
                     //         map: map,
                     //         title: 'SpyHop'
                     //     });
                         
                     // }).error(function(data, status) {
                     //     console.log("Error status : " + status);
                     // });
                     HttpServices.get('api/contact-us/information/')
                         .then (function(response) {
                             $scope.contactInfo = response.data.results[0];
                             //console.log("INFO: /contact-us/=", response);
                             $window.map = new google.maps.Map(document.getElementById('map'), {
                                 center: {
                                     lat: parseFloat(response.data.results[0].latitude),
                                     lng: parseFloat(response.data.results[0].longitude)
                                 },
                                 zoom: 17
                             });
                             var marker = new google.maps.Marker({
                                 position: new google.maps.LatLng(response.data.results[0].latitude,
                                                                  response.data.results[0].longitude),
                                 map: map,
                                 title: 'SpyHop, Inc.'
                             });
                         
                         }, function(response) {
                             console.log("ERROR: /contact-us/information=",
                                         response);
                         });
                     
                     //Contact Us
                     $scope.contactUs = function(){
                         //         $scope.contactForm.topic = $scope.contactusForm.topic;
                         if (!$scope.contactForm.$valid) {
                             $scope.contact_submitted = true;
                         }
                         
                         if (!$scope.contactForm.topic) {
                             $scope.topicSelect = true;
                         }
                         
                         //        $scope.contactForm.topic = $scope.contactusForm.topic;
                         // $http.post($localStorage.host +
                         //            'api/contact-us/contact/',
                         //            $scope.contactForm).success(function(data, status) {
                         //                $scope.contact_sent = true;
                         //                $('#contactClear').find('form')[0].reset();
                                        
                         //            }).error(function(data, status, headers, config) {
                         //                if (data.email)
                         //                    $scope.email_error = data.email[0];
                         //                switch (status) {
                         //                case 400:
                         //                    $scope.error_message = 'Invalid name password.';
                         //                    break;
                         //                default:
                         //                    $scope.has_error = true;
                         //                    $scope.error_message = 'Some error has occurred. Please try again.';
                         //                }
                         //            })
                         HttpServices.post('api/contact-us/contact/', $scope.contactForm)
                             .then(function(response) {
                                 $scope.contact_sent = true;
                                 $('#contactClear').find('form')[0].reset();
                             }, function(response) {
                                 if (response.data.email)
                                     $scope.email_error = response.data.email[0];
                                 switch (response.status) {
                                 case 400:
                                     $scope.error_message = 'Invalid name password.';
                                     break;
                                 default:
                                     $scope.has_error = true;
                                     $scope.error_message = 'Some error has occurred. Please try again.';
                                 }
                             });
                     }
                 }])

    .controller('HomeViewCtrl',
                ['$scope',
                 //'$http',
                 '$state',
                 '$location',
                 '$anchorScroll',
                 '$stateParams',
                 '$localStorage',
                 'toaster',
                 'HttpServices',
                 'Utils',
                 function($scope,
                          //$http,
                          $state,
                          $location,
                          $anchorScroll,
                          $stateParams,
                          $localStorage,
                          toaster,
                          HttpServices,
                          Utils) {
                     $('#myModal').modal('hide');
                     // $http.get($localStorage.host + 'api/videos/title/').success(function(data) {
                     //     $scope.recommentation = data
                     // });
                     HttpServices.get('api/videos/title/').then(function(response) {
                         $scope.recommentation = response.data
                     });
                     
                     
                     $('.typeahead').on('typeahead:selected', function(e, datum) {
                         $scope.q = datum;
                     });
                     
                     var states = $scope.recommentation;
                     
                     $('.typeahead').typeahead({
                         hint: true,
                         highlight: true,
                         minLength: 1
                     }, {
                         name: 'states',
                         source: Utils.substringMatcher(states)
                     })
                     $scope.$watchCollection('$stateParams', function() {
                         $anchorScroll();
                     });
                     $scope.reset_form = {};
                     $scope.tab = 0;
                     
                     $scope.resetPassword = function(send_password_email) {
                         $scope.submitted = true;
                         // $http.post($localStorage.host + 'api/rest-auth/password/reset/', {
                         //     email: send_password_email
                         // }).success(function(data, status) {
                         //     $scope.emailSent = data.success;
                         //     $scope.submitted = false;
                         // }).error(function(data, status, headers, config) {
                         //     $scope.emailErrors = data.email;
                         //     $scope.submitted = false;
                             
                         // })
                         HttpServices.post('api/rest-auth/password/reset/',
                                           {email: send_password_email})
                             .then(function(response) {
                                 $scope.emailSent = response.data.success;
                                 $scope.submitted = false;
                             }, function(response) {
                                 $scope.emailErrors = response.data.email;
                                 $scope.submitted = false;
                             })
                     };
                     $scope.saveNewPassword = function() {
                         $scope.submitted = true;
                         $scope.reset_form.token = $stateParams.token;
                         $scope.reset_form.uid = $stateParams.uid;
                         // $http.post($localStorage.host +
                         //            'api/rest-auth/password/reset/confirm/',
                         //            $scope.reset_form).success(function(data, status) {
                         //                $scope.success = data.success;
                         //                $scope.submitted = false;
                         //            }).error(function(data, status, headers, config) {
                         //                $scope.resetFormErrors = data;
                         //                $scope.submitted = false;
                                        
                         //            })
                         HttpServices.post('api/rest-auth/password/reset/confirm/',
                                           $scope.reset_form)
                             .then(function(response) {
                                 $scope.success = response.data.success;
                                 $scope.submitted = false;
                             }, function(response) {
                                 $scope.resetFormErrors = response.data;
                                 $scope.submitted = false;
                             })
                     }
                     $scope.searchQuery = function() {
                         $state.go('video-search', {
                             q: $scope.q
                         })
                     };
                 }])

    .controller('NewsFeedCtrl',
                ['$scope',
                 //'$http',
                 '$state',
                 '$location',
                 '$anchorScroll',
                 '$stateParams',
                 '$localStorage',
                 'Main',
                 function($scope,
                          //$http,
                          $state,
                          $location,
                          $anchorScroll,
                          $stateParams,
                          $localStorage,
                          Main) {
                     Main.get_newsfeeds().then(function(response) {
                         if (response.data) {
                             $scope.news = response.data.results;
                         }
                     }, function(response) {
                         alert("ERROR: Unable to access the notifications now")
                     });
                 }])

    .controller('NotificationViewCtrl',
                ['$scope',
                 //'$http',
                 '$state',
                 '$location',
                 '$anchorScroll',
                 '$stateParams',
                 '$localStorage',
                 'Main',
                 function($scope,
                          //$http,
                          $state,
                          $location,
                          $anchorScroll,
                          $stateParams,
                          $localStorage,
                          Main) {
                     // Main.read_all_notifications().then(function(data) {
                     //     if (data) {
                     //         $scope.notifications = data;
                     //     }
                     // }, function(error) {
                     //     alert("Unable to access the notifications now")
                     // });
                     Main.update_notification_data();
                 }])

    .factory('Main',
             ['$rootScope',
              '$localStorage',
              '$state',
              '$timeout',
              '$location',
              '$q',
              'Cart',
              'HttpServices',
              'Profile',
              function($rootScope,
                       $localStorage,
                       $state,
                       $timeout,
                       $location,
                       $q,
                       Cart,
                       HttpServices,
                       Profile) {
                  var main = {};

                  main.get_profile = function(userName){
                      if(userName){
                          var url = "api/profiles/profile-list/" + userName;
                      }
                      else{
                          var url = "api/profiles/profile-list/" + $localStorage.userName;
                      }
                      return (HttpServices.get(url));
                  };
                  
                  main.get_notifications = function() {
                      return (HttpServices.get("api/activities/notification/"));
                  };
                  
                  main.get_unread_notification_count = function() {
                      return (HttpServices.get("api/activities/unread-count/"));
                  }
                  
                  main.update_notification = function(id) {
                      return (HttpServices.put("api/activities/notification/"+id));
                  };
                  
                  main.update_feed = function(id) {
                      return (HttpServices.put("api/activities/notification/"+id));
                  };
                  
                  main.get_newsfeeds = function(id) {
                      return (HttpServices.get("api/activities/feeds/"));
                  };
                      
                  main.read_all_notifications = function() {
                      return (HttpServices.post("api/activities/read-all-notifications/"));
                  };

                  main.get_user_data = function(gotoProfile){
                      // add any other data that is specific to user here
                      $q.all({'cart': Cart.get_cart(),
                              'profile': main.get_profile()})
                          .then(function(results){
                              // cart
                              var cart_data = results.cart.data;
                              $rootScope.cart = cart_data.results[0];
                              // profile
                              $rootScope.loggedUser = results.profile.data;
                              if ($rootScope.loggedUser.type === 'videographer') {
                                  Profile.update_profile($rootScope.loggedUser.username,
                                                         {'type': 'both'})
                                      .then(function(response){
                                          $rootScope.loggedUser.type = 'both';
                                      }, function(response){
                                          console.log("ERROR: update_profile=",
                                                      response.data);
                                      });
                              }
                              if ($rootScope.toStateAfterLogin) {
                                  //console.log("DEBUG: main.get_user_data=>tsal=",
                                  //$rootScope.toStateAfterLogin);
                                  $state.go($rootScope.toStateAfterLogin,
                                            $rootScope.toParamsAfterLogin);
                                  delete $rootScope.toStateAfterLogin;
                                  delete $rootScope.toParamsAfterLogin;
                              }
                              else if (gotoProfile || $state.current.name == 'home') {
                                  $state.go('profile');
                              }
                              else if ($state.current.name == 'cart') {
                                  // ToDo: do all pages require reload ?
                                  $state.reload();
                              }
                          }, function(results) {
                              console.log("ERROR: get_user_data=", response)
                          });
                      
                      $rootScope.refreshNotificationCount = function(){
                          main.get_unread_notification_count()
                              .then(function(response) {
                                  if (response.data) {
                                      $rootScope.notification_count =
                                          response.data.unread_count;
                                      // console.log("INFO: notifications count=",
                                      //             $rootScope.notification_count);
                                  }
                              }, function(response) {
                                  console.log("ERROR: notifications count=",
                                              response);
                              });
                          $rootScope.refreshNotificationTimeout = $timeout(function(){
                              $rootScope.refreshNotificationCount();
                          }, 30000)
                      };
                      $rootScope.refreshNotificationCount();
                  }; // main.get_user_data()

                  main.update_notification_data = function(){
                      //$rootScope.notification_visted = true;
                      main.get_notifications().then(function(response) {
                          if (response.data) {
                              //$scope.notifications = data;
                              $rootScope.notifications = response.data;
                              main.read_all_notifications().then(function(response){
                                  $rootScope.notification_count = 0;
                              }, function(response){
                                  console.log("ERROR: main.read_all_notifications=",
                                              response)
                              });
                          }
                      }, function(response) {
                          console.log("ERROR: get_notifications=",
                                      response);
                      });
                  }; // main.update_notification_view()

                  main.logout = function(){
                      //localStorage.clear();
                      $.sidr('toggle', 'right-menu');
                      if ($localStorage.userKey === null ||
                          $localStorage.userName === null) {
                          $location.path('/');
                      }
                      HttpServices.post('api/rest-auth/logout/')
                          .then(function(response){
                              $rootScope.cart.items = [];
                              $rootScope.cart.item_count = 0;
                              $rootScope.notification_count = 0;
                              $timeout.cancel($rootScope.refreshNotificationTimeout);
                              delete $localStorage.userKey;
                              delete $localStorage.userName;
                              $('#myModal').modal('hide');
                              $rootScope.isUserLoggedIn = false;
                              $location.path('/');
                          }, function(response){
                              console.log("ERROR: logOut=", response);
                          });
                  };

                  main.get_default_config = function(){
                      return (HttpServices.get("api/general/default-config/"));
  		  },
                  
                  main.init_default_config = function(scope){
                      return main.get_default_config().then(function(response){
                          // console.log("DEBUG: get_default_config=",
			  //             response);
                          if (response.data) {
                              scope.defaultCfg = response.data;
                              
                              scope.get_video_type_display_name = function(type, res, fps){
                                  if (!scope.defaultCfg || !scope.defaultCfg.video_type){
                                      return (null);
                                  }
                                  for (var i = 0;
                                       i < scope.defaultCfg.video_type.results.length;
                                       i++) {
                                      if (type !== scope.defaultCfg.video_type.results[i].video_type) {
                                          continue;
                                      }
                                      if (res !== scope.defaultCfg.video_type.results[i].resolution) {
                                          continue;
                                      }
                                      if (fps !== scope.defaultCfg.video_type.results[i].fps_rate) {
                                          continue;
                                      }
                                      return (scope.defaultCfg.video_type.results[i].display_name);
                                  }
                                  return (null);
                              };
                              scope.get_permission_display_name = function(permission){
                                  if (!scope.defaultCfg || !scope.defaultCfg.video_permission){
                                      return (null);
                                  }
                                  for (var i = 0;
                                       i < scope.defaultCfg.video_permission.results.length;
                                       i++) {
                                      if (permission === scope.defaultCfg.video_permission.results[i].permission) {
                                          return (scope.defaultCfg.video_permission.results[i].display_name);
                                      }
                                  }
                                  return (null);
                              };
                          }
                      });
                  };
                  return (main);
              }])

