'use strict';

// Declare app level module which depends on views, and components
angular.module('spyhop',
               ['ngRoute',
                'spyhop.accounts',
                'home.controllers',
                'spyhop.videos',
                'spyhop.view2',
                'spyhop.contests',
                'spyhop.cart',
                'spyhop.search',
                'spyhop.version',
                'spyhop.reviews',
                'satellizer',
                //'restangular',
                'ngStorage',
                'toaster',
                'ngAnimate',
                'spyhop.user_profile',
                'flow',
                'rzModule',
                'ui.select',
                'ngFileUpload',
                'google.places',
                'ui.bootstrap',
                'angularUtils.directives.dirPagination',
                'angularPayments',
                'orderServices',
                'utils',
                'vcRecaptcha',
                'infinite-scroll',
                'angulartics',
                'angulartics.google.analytics'])

    .run(function($rootScope,
                  $localStorage,
                  $location,
                  $state,
                  $timeout,
                  Cart,
                  toaster,
                  Main) {
        //console.log("DEBUG: userKey=", $localStorage.userKey);
        //delete $localStorage.userKey;
        Cart.get_cart().then(function(response) {
            var cart_data = response.data;
            $rootScope.cart = cart_data.results[0];
            console.log("cart=", response, cart_data);
        }, function(response) {
            console.log("ERROR: get_cart=", response.data);
            alert("Unable to acess the cart now")
        });
        $localStorage.host = '/';
        if ($localStorage.userKey !== undefined) {
            $rootScope.isUserLoggedIn = true;
            Main.get_unread_notification_count().then(function(response) {
                if (response.data) {
                    $rootScope.notification_count = response.data.unread_count;
                }
            }, function (response) {
                alert("ERROR: Unable to access the notifications now")
            });
            Main.get_profile().then(function (response) {
                if (response.data) {
                    $rootScope.loggedUser = response.data
                }
            }, function (response) {
                console.log("ERROR: Main.get_profile=", response);
            });
            if ($location.path() == '/notifications') {
                Main.update_notification_data();
            }
        }
        $rootScope.toStateAfterLogin = undefined;
        $rootScope.$on('$stateChangeStart', function(event,
                                                     toState,
                                                     toParams,
                                                     fromState,
                                                     fromParams){
            //console.log("INFO: ", fromState.name, "->", toState.name);
            // special handling for the states which require users
            // to be logged in.
            // . save the toStateAfterLogin
            // . go to home first
            // . present user with the login prompt
            // . if user logs in successfully, then goto toStateAfterLogin
            // . if user cancels the login prompt, then we show home page
            // . if user signs up then we take to profile page
            if (toState.data.requiresLogin && !$rootScope.isUserLoggedIn) {
                event.preventDefault();
                $rootScope.toStateAfterLogin = toState.name;
                $rootScope.toParamsAfterLogin = toParams;
                console.log("DEBUG: ", fromState.name, "->", toState.name);
                var showLoginModal = function(){
                    //console.log("DEBUG: home-> login_click:");
                    $timeout(function(){
                        $('#login_link').trigger('click');
                    }, 0, false);
                };
                if (fromState.name) {
                    showLoginModal();
                    return;
                }
                $state.go("home").then(function(){
                    showLoginModal();
                });
                // toaster.pop('info',
                //             "Warning",
                //             "Please login to access this page");
                return;
            }
            $rootScope.pageTitle = toState.data.pageTitle;
        });
        $rootScope.$on('$stateChangeSuccess', function () {
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    })
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/home');

        $stateProvider.
            state('profile', {
                url: "/profile",
                templateUrl: '/angular/app/accounts/profile.html',
                controller: 'ProfileViewCtrl',
                data: { pageTitle: 'Profile', requiresLogin: true }
            })
            .state('profile_edit', {
                url: "/profile-edit",
                templateUrl: '/angular/app/profile/user_profile_edit.html',
                controller: 'ProfileEditCtrl',
                data: { pageTitle: 'Edit Profile', requiresLogin: true }
            })
            .state('forgot_password', {
                url: "/forgot-password",
                templateUrl: '/angular/app/accounts/forgot_password.html',
                controller: 'HomeViewCtrl',
                data: { pageTitle: 'Forgot Password' }
            })
            .state('reset_password', {
                url: "/reset-password?uid&token",
                templateUrl: '/angular/app/accounts/reset_password.html',
                controller: 'HomeViewCtrl',
                data: { pageTitle: 'Reset Password' }
            })
            .state('videos', {
                url: "/videos",
                templateUrl: '/angular/app/videos/video_list.html',
                controller: 'VideoListViewCtrl',
                data: { pageTitle: 'Videos' }
            })
            .state('video', {
                url: "/video/:slug",
                templateUrl: '/angular/app/videos/video_detail.html',
                controller: 'VideoDetailCtrl',
                data: { pageTitle: 'Video' }
            })
            .state('video-search', {
                url: "/video-search?q",
                templateUrl: '/angular/app/search/video_search.html',
                controller: 'VideoSearchCtrl',
                data: { pageTitle: 'Search' }
            })
            .state('search', {
                url: "/search?q",
                templateUrl: '/angular/app/search/general_search.html',
                controller: 'GeneralSearchCtrl',
                data: { pageTitle: 'Search' }
            })
            .state('search_user', {
                url: "/user-search?q",
                templateUrl: '/angular/app/search/user_search.html',
                controller: 'ProfileSearchCtrl',
                data: { pageTitle: 'Search' }
            })
            .state('search_request', {
                url: "/request-search?q",
                templateUrl: '/angular/app/search/contest_search.html',
                controller: 'ContestSearchCtrl',
                data: { pageTitle: 'Search' }
            })
            .state('home', {
//                url: "/home",
//                templateUrl: '/angular/app/home/home.html',
//                controller: 'HomeViewCtrl',
//                data: { pageTitle: 'Welcome' }
                url: "/home",
                templateUrl: '/angular/app/contests/create_contest.html',
                controller: 'CreateContestCtrl',
                data: { pageTitle: 'New Request' }
            })
            .state('privacy_policy', {
                url: "/privacy-policy",
                templateUrl: '/angular/app/home/privacy_policy.html',
                controller: 'HomeViewCtrl',
                data: { pageTitle: 'Privacy Policy' }
            })
            .state('contact_us', {
                url: "/contact-us",
                templateUrl: '/angular/app/home/contact_us.html',
                controller: 'ContactUsCtrl',
                data: { pageTitle: 'Contact Us' }
            })
            .state('create_contest', {
                url: "/create-request",
                templateUrl: '/angular/app/contests/create_contest.html',
                controller: 'CreateContestCtrl',
                data: { pageTitle: 'New Request' }
            })
            .state('edit_contest', {
                url: "/edit-request/:slug",
                templateUrl: '/angular/app/contests/create_contest.html',
                controller: 'CreateContestCtrl',
                data: { pageTitle: 'Edit Request', requiresLogin: true }
            })
            .state('review', {
                url: "/request-review/:slug",
                templateUrl: '/angular/app/contests/review_edit.html',
                controller: 'ReviewCtrl',
                data: { pageTitle: 'Review', requiresLogin: true }
            })
            .state('cart', {
                url: "/cart",
                templateUrl: '/angular/app/cart/cart.html',
                controller: 'CartCtrl',
                data: { pageTitle: 'Cart' }
            })
            .state('order-history', {
                url: "/order-history",
                templateUrl: '/angular/app/cart/order_history.html',
                controller: 'OrderHistoryCtrl',
                data: { pageTitle: 'Order History', requiresLogin: true }
            })
            .state('order', {
                url: "/order/:order_id",
                templateUrl: '/angular/app/cart/my_order.html',
                controller: 'OrderHistoryCtrl',
                data: { pageTitle: 'Order Details', requiresLogin: true }
            })
            .state('payment', {
                url: "/payment",
                templateUrl: '/angular/app/cart/add_credit_card.html',
                controller: 'PlaceOrderCtrl',
                data: { pageTitle: 'Payment', requiresLogin: true }
            })
            // .state('order_placed', {
            //     url: "/order-placed",
            //     templateUrl: '/angular/app/cart/orderplaced.html',
            //     controller: 'CartCtrl',
            //     data: { pageTitle: 'Order Placed' }
            // })
            .state('how_it_works', {
                url: "/how-it-works",
                templateUrl: '/angular/app/home/how_it_works.html',
                controller: 'HomeViewCtrl',
                data: { pageTitle: 'How It Works' }
            })
            .state('about_us', {
                url: "/about-us",
                templateUrl: '/angular/app/home/about_us.html',
                controller: 'HomeViewCtrl',
                data: { pageTitle: 'About Us' }
            })
            .state('notification_list', {
                url: "/notifications",
                templateUrl: '/angular/app/profile/notifications.html',
                controller: 'NotificationViewCtrl',
                data: { pageTitle: 'Notifications', requiresLogin: true }
            })
            .state('news_feed', {
                url: "/news-feed",
                templateUrl: '/angular/app/profile/news_feed.html',
                controller: 'NewsFeedCtrl',
                data: { pageTitle: 'NewsFeed', requiresLogin: true }
            })
            .state('contest_detail', {
                url: "/request-detail/:slug",
                templateUrl: '/angular/app/contests/contest_detail.html',
                controller: 'ContestDetailCtrl',
                data: { pageTitle: 'Request Detail' }
            })
            .state('user_profile', {
                url: "/user_profile/:username",
                templateUrl: '/angular/app/accounts/profile.html',
                controller: 'ProfileViewCtrl',
                data: { pageTitle: 'Profile' }
            })
            .state('404', {
                url: "/404",
                templateUrl: '/angular/app/home/404.html',
                data: { pageTitle: '404' }
            })
            .state('500', {
                url: "/500",
                templateUrl: '/angular/app/home/500.html',
                data: { pageTitle: '500' }
            })
            .state('terms_and_conditions', {
                url: "/terms-and-conditions",
                templateUrl: '/angular/app/home/terms_conditions.html',
                controller: 'HomeViewCtrl',
                data: { pageTitle: 'Terms and Conditions' }
            })
            // .state('review_list', {
            //     url: "/review-list",
            //     templateUrl: '/angular/app/reviews/video_listing.html',
            //     controller: 'reviewController',
            //     data: { pageTitle: 'Video List' }
            // })
            // .state('review_video', {
            //     url: "/review-video",
            //     templateUrl: '/angular/app/reviews/video_review.html',
            //     controller: 'reviewController',
            //     data: { pageTitle: 'Review Video' }
            // });
    }])
//social auth
    .config(function ($authProvider,
                      vcRecaptchaServiceProvider,
                      $httpProvider) {
        $authProvider.google({
            clientId: '447865112142-tdr8nibdb9odqi1ajsrrelcet1s4mt22.apps.googleusercontent.com'
        });
        $authProvider.facebook({
            clientId: '855826514536966'
        });
        $authProvider.twitter({
            clientId: 'g15ZApavlpW1hvAxRKbtAP8W5'
        });
        $authProvider.instagram({
            clientId: '812333e0a3874655827e5ca45c063410'
        });

        vcRecaptchaServiceProvider.setSiteKey('6Lc7CiETAAAAAJma4pgMpGsM5bt67mbhXxudnAgL')
        // vcRecaptchaServiceProvider.setTheme('---- light or dark ----')
        // vcRecaptchaServiceProvider.setStoken('--- YOUR GENERATED SECURE TOKEN ---')
        // vcRecaptchaServiceProvider.setSize('---- compact or normal ----')
        // vcRecaptchaServiceProvider.setType('---- audio or image ----')

        $httpProvider.interceptors.push('httpErrorResponseInterceptor');
    })

    .factory('httpErrorResponseInterceptor',
             ['$q',
              '$location',
              'toaster',
              function($q, $location, toaster) {
                  return {
                      'response': function(response) {
                          return response;
                      },
                      'responseError': function(response) {
                          switch (response.status) {
                          case 401:
                              // ToDo: show login modal box ?
                              // toaster.pop('info',
                              //             "Warning",
                              //             "Please login to perform this operation");
                              break;

//                          case 404:
//                              $location.path('/404');
//                              break;

                          case 500:
                              $location.path('/500');
                              break;

                          default: // let the original caller handle it
                              //$location.path('/error');
                          }
                          //console.log(response);
                          return $q.reject(response);
                      }
                  };
              }]);
