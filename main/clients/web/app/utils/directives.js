'use strict';

angular.module('utils')

    .directive('requireMultiple', function () {
        //
        // directive to make multiple choices like categories in create_contest
        // a required field
        //
        return {
            require: 'ngModel',
            link: function postLink(scope, element, attrs, ngModel) {
                ngModel.$validators.required = function (value) {
                    return angular.isArray(value) && value.length > 0;
                };
            }
        };
    })

    .directive('equalTo', function () {
        //
        // directive to check one form fields against other
        // e.g. password and confirm-password fields
        //
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=equalTo"
            },
            link: function(scope, element, attributes, ngModel) {
                ngModel.$validators.equalTo = function(modelValue) {
                    //console.log("INFO: equalTo");
                    return (modelValue == scope.otherModelValue);
                };
                
                scope.$watch("otherModelValue", function() {
                    //console.log("INFO: validate");
                    ngModel.$validate();
                });
            }
        };
    })
