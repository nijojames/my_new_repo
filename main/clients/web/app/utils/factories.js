'use strict';

angular.module('utils')
    .factory('RouterTracker',
             ['$rootScope',
              function($rootScope){
                  var routeHistory = {};
                  var service = {
                      getRouteHistory: getRouteHistory
                  };
                  
                  $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
                      routeHistory = {route: from, routeParams: fromParams};
                  });
                  
                  function getRouteHistory() {
                      return routeHistory;
                  }
                  
                  return service;
              }])

// add all generic utility functions here
    .factory('Utils',
             ['$window',
              function($window){
                  var ut = {};
                  
                  ut.substringMatcher = function(strs) {
                      return function findMatches(q, cb) {
                          var matches, substrRegex;
                          
                          // an array that will be populated with substring matches
                          matches = [];
                          
                          // regex used to determine if a string contains the substring `q`
                          substrRegex = new RegExp(q, 'i');
                          
                          // iterate through the pool of strings and for any string that
                          // contains the substring `q`, add it to the `matches` array
                          $.each(strs, function(i, str) {
                              if (substrRegex.test(str)) {
                                  matches.push(str);
                              }
                          });
                          
                          cb(matches);
                      };
                  }; // ut.substringMatcher()
                  //
                  // prompts the user if the form is dirty and
                  // user tries to navigate away from the page
                  // or try to reload the page
                  //
                  ut.promptOnDirtyExit = function(scope, name){
                      scope.$watch(name, function(form) {
                          if(form) {
                              scope.$on('$stateChangeStart', function(event) {
                                  var msg = "There are unsaved changes on this page. " +
                                      "Are you sure you want to leave this page ?"
                                  if (!form.$dirty){
                                      return;
                                  }
                                  var answer = confirm(msg);
                                  if (!answer) {
                                      event.preventDefault();
                                  }
                              });
                              $window.onbeforeunload = function(event) {
                                  if (!form.$dirty){
                                      return;
                                  }
                                  // NB: chrome adds its own message
                                  //     safari/firefox ignore this message
                                  var msg = "There are unsaved changes."
                                  return(msg);
                              };
                              scope.$on('$destroy', function() {
                                  //console.log("INFO: destroy");
                                  $window.onbeforeunload = undefined;
                              });
                          }
                      });
                  };

                  ut.ucFirst = function(str) {
                      var firstLetter = str.slice(0,1);
                      return firstLetter.toUpperCase() + str.substring(1);
                  };
                  
                  return (ut);
              }])

