'use strict';

angular.module('utils', [])

    .service("HttpServices",
             ["$q",
              "$http",
              "$localStorage",
              function($q,
                       $http,
                       $localStorage) {
                  
                  var makeRequest = function(method, endpoint, data, params){
                      var url = endpoint;
                      var request_data = {method: method,
                                          url: url};
                      if (data != null){
                          request_data['data'] = data;
                      }
                      if (params != null){
                          request_data['params'] = params;
                      }
                      //console.log("INFO: makeRequest->rd=", request_data);
                      if ($localStorage.userKey !== undefined){
                          request_data['headers'] = { 'Authorization':
                                                      'Token ' +
                                                      $localStorage.userKey };
                      }
                      var deferred = $q.defer();
                      $http(request_data).then(function(result) {
                          deferred.resolve(result);
                      }, function(result) {
                          deferred.reject(result);
                      });
                      return (deferred.promise);
                  };

                  this.get = function(endpoint, params){
                      return (makeRequest("GET", endpoint, null, params));
                  };
                  this.post = function(endpoint, data){
                      return (makeRequest("POST", endpoint, data, null));
                  };
                  this.put = function(endpoint, data){
                      return (makeRequest("PUT", endpoint, data, null));
                  };
                  this.patch = function(endpoint, data){
                      return (makeRequest("PATCH", endpoint, data, null));
                  };
                  this.delete = function(endpoint, params){
                      return (makeRequest("DELETE", endpoint, null, params));
                  };
              }]);
