'use strict';

angular.module('spyhop.version', [
  'spyhop.version.interpolate-filter',
  'spyhop.version.version-directive'
])

.value('version', '0.1');
