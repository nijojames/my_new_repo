'use strict';

angular.module('spyhop.videos', ['ngRoute'])

    .controller('VideoListViewCtrl',
                ['$scope',
                 //'$http',
                 '$location',
                 '$localStorage',
                 'HttpServices',
                 function($scope,
                          //$http,
                          $location,
                          $localStorage,
                          HttpServices) {
                     $('.spinner').show();
                     // $http.get($localStorage.host + 'api/videos/search/', {

                     // }).success(function(response){
                     //     $scope.videoCollection = response.results;
                     //     $('.spinner').hide();
                     // }).error(function (data, status){
                     //     console.log("Error status : " + status);
                     // });
                     HttpServices.get('api/videos/search/')
                         .then(function(response){
                             $scope.videoCollection = response.data.results;
                             $('.spinner').hide();
                         }, function (response){
                             $('.spinner').hide();
                             console.log("ERROR: VideoListViewCtrl=", response);
                         });
                 }])

    .controller('VideoDetailCtrl',
                ['$rootScope',
                 '$scope',
                 //'$http',
                 '$location',
                 '$localStorage',
                 '$stateParams',
                 'Cart',
                 'Video',
                 'toaster',
                 'HttpServices',
                 'Main',
                 function($rootScope,
                          $scope,
                          //$http,
                          $location,
                          $localStorage,
                          $stateParams,
                          Cart,
                          Video,
                          toaster,
                          HttpServices,
                          Main) {
                     // destroy the player when the controller is destroyed
                     var videojsPlayer;
                     $scope.$on('$destroy', function() {
                         if ((videojsPlayer !== undefined) && (videojsPlayer !== null)) {
                             videojsPlayer.dispose();
                         }
                     });
                     
                     videojs("my-video").ready(function() {
                         videojsPlayer = this;
                     })

                     // Video.get_details($stateParams.slug)
                     //     .then(function (response) {
                     //         var video_data = response.data;
                     //         console.log("INFO: video.get_details=", response);
                     //         if (video_data) {
                     //             $scope.video = video_data;
                     //             if($scope.video.permission == "royalty_free"){
                     //                 $scope.video.permission = "royalty free"
                     //             }
                     //             $("video").attr("src", $scope.video.water_marked_video);
                     //         }
                     //     }, function (response) {
                     //         console.log("ERROR: video.get_details=", response);
                     //         alert("ERROR: unable to get video details");
                     //     });

                     Main.init_default_config($scope)
                         .then(function(){
                             return Video.get_details($stateParams.slug)
                                 .then(function (response) {
                                     var videoData = response.data;
                                     //console.log("DEBUG: video.get_details=", response);
                                     if (videoData) {
                                         $scope.video = videoData;
                                         $("video").attr("src", $scope.video.water_marked_video);
                                     }
                                 });
                         })
                         .catch(function(response){
                             console.log("ERROR: video_detail=", response);
                         });
                     
                     // ToDo: put the addToCart to factory with a callback function
                     $scope.addToCart = function (video_id) {
                         Cart.add_to_cart(video_id, 'video')
                             .then(function(response) {
                                 $rootScope.cart = response.data;
                                 toaster.pop('success', "Success", "Item added to cart");
                             }, function(response) {
                                 // ToDo: check status
                                 console.log("ERROR: video.addToCart=", response);
                                 console.log(response.data);
                                 toaster.pop('info',
                                             "Warning",
                                             //"This item is already in your cart");
                                             response.data.error);
                             });
                     }

                     $scope.likeVideo = function (video_id, type) {
                         Video.like(video_id, type).then(function (response) {
                             if (response.status == 204) {
                                 $scope.video.likes_count = $scope.video.likes_count - 1;
                                 $scope.video.is_liked = false;
                             } else if (response.status == 201) {
                                 $scope.video.likes_count = $scope.video.likes_count + 1;
                                 $scope.video.is_liked = true;
                             }
                         }, function (response) {
                             // ToDo: check status
                             console.log("ERROR: video.likeVideo=", response);
                             toaster.pop('info', "Warning", "Please login to like");
                         });
                     };

                     $scope.followVideo = function (video_id, type) {
                         Video.follow(video_id, type).then(function (response) {
                             if (response.status == 204) {
                                 $scope.video.followers_count = $scope.video.followers_count - 1;
                                 $scope.video.is_following = false;
                             } else if (response.status == 201) {
                                 $scope.video.followers_count = $scope.video.followers_count + 1;
                                 $scope.video.is_following = true;
                             }
                         }, function (response) {
                             // ToDo: check status
                             console.log("ERROR: video.followVideo=", response);
                             toaster.pop('info', "Warning", "Please login to follow");
                         });
                     };

                     $scope.follow_User = function (user_id, type) {
                         Video.follow(user_id, type).then(function(response) {
                             if (response.status == 204) {
                                 $scope.video.user_detail.is_following = false;
                             } else if (response.status == 201) {
                                 $scope.video.user_detail.is_following = true;
                             }
                         }, function(response) {
                             // ToDo: check status
                             console.log("ERROR: video.followUser=", response);
                             toaster.pop('info', "Warning", "Please login to follow");
                         });
                     };

                     $scope.spamVideo = function (video_id, type) {
                         Video.spam(video_id, type).then(function(response) {
                             if (response.status == 204) {
                                 $scope.video.is_spammed = false;
                             } else if (response.status == 201) {
                                 $scope.video.is_spammed = true;
                             }
                         }, function(response) {
                             // ToDo: check status
                             console.log("ERROR: video.spamVideo=", response);
                             toaster.pop('info', "Warning", "Please login to mark spam");
                         });

                     };

                     $scope.rateVideo = function (video_id, rating) {
                         Video.rate(video_id, rating, $scope.video.rating_id)
                             .then(function (response) {
                                 $scope.video.rating = response.data.value;
                             }, function (response) {
                                 // ToDo: check status
                                 console.log("ERROR: video.rateVideo=", response);
                                 toaster.pop('info', "Warning", "Please login to rate");
                                 $scope.video.rating = undefined;
                             });
                     };
                 }])


    .factory('Video',
             ['$localStorage',
              //'$http',
              'HttpServices',
              function($localStorage,
                       //$http
                       HttpServices) {
                  var vf = {};
                  
                  vf.get_details = function(slug) {
                      // var url = $localStorage.host + "api/videos/video/" + slug;
                      // var request_data = {method: 'GET', url: url};
                      // if($localStorage.userKey !== undefined){
                      //     request_data['headers'] = { 'Authorization': 'Token ' + $localStorage.userKey };
                      // }
  		      // return $http(request_data)
                      //     .success(function(response) {
                      //         if (typeof response.data === 'object') {
                      //             return response.data;
                      //         } else { return null; }
                      //     });
                      
                      return (HttpServices.get("api/videos/video/"+slug));
  	          },

  	          vf.like = function(obj_id, type) {
                      // var url = $localStorage.host + "api/likes/like/";
                      // var request_data = {method: 'POST',
                      //                     url: url,
                      //                     data:{object_id: obj_id,
                      //                           content_type: type}};
                      // if($localStorage.userKey !== undefined){
                      //     request_data['headers'] = { 'Authorization': 'Token ' + $localStorage.userKey };
                      // }
  		      // return $http(request_data)
                      //     .success(function(response) {
                      //         if (typeof response.data === 'object') {
                      //             return response.data;
                      //         } else { return null; }
                      //     })
                      //     .error(function(error){
                      //         return null;
                      //     });
                      return (HttpServices.post("api/likes/like/",
                                                {object_id: obj_id,
                                                 content_type: type}));
  	          },

                  vf.follow = function(obj_id, type) {
                      //   var url = $localStorage.host + "api/activities/follow/";
                      //     var request_data = {method: 'POST',
                      //                         url: url,
                      //                         data:{obj_id: obj_id,
                      //                               content_type: type}};
                      //     if($localStorage.userKey !== undefined){
                      //         request_data['headers'] = { 'Authorization': 'Token ' + $localStorage.userKey };
                      //     }
  		      //     return $http(request_data)
                      //         .success(function(response) {
                      //             if (typeof response.data === 'object') {
                      //                 return response.data;
                      //             } else { return null; }
                      //         })
                      //         .error(function(error){
                      //             return null;
                      //         });
  	              //
                      return (HttpServices.post("api/activities/follow/",
                                                {obj_id: obj_id,
                                                 content_type: type}));
                  },

                  vf.spam = function(obj_id, type) {
                      // var url = $localStorage.host + "api/flaggit/flag-it/";
                      // var request_data = {method: 'POST',
                      //                     url: url,
                      //                     data:{object_id: obj_id,
                      //                           content_type: type}};
                      // if($localStorage.userKey !== undefined){
                      //     request_data['headers'] = { 'Authorization': 'Token ' + $localStorage.userKey };
                      // }
  		      // return $http(request_data)
                      //     .success(function(response) {
                      //         if (typeof response.data === 'object') {
                      //             return response.data;
                      //         } else { return null; }
                      //     })
                      //     .error(function(error){
                      //         return null;
                      //     });
                      return (HttpServices.post("api/flaggit/flag-it/",
                                                {object_id: obj_id,
                                                 content_type: type}))
  	          },

                  vf.rate = function(obj_id, rating, rate_id) {
                      //     if(rate_id === null){
                      //         var url = $localStorage.host + "api/videos/rating/";
                      //         var request_data = {method: 'POST',
                      //                             url: url,
                      //                             data:{video: obj_id,
                      //                                   value: rating}};
                      //     }
                      //     else{
                      //         var url = $localStorage.host + "api/videos/rating/" + rate_id;
                      //         var request_data = {method: 'PATCH',
                      //                             url: url,
                      //                             data:{video: obj_id,
                      //                                   value: rating}};
                      //     }
                      //     if($localStorage.userKey !== undefined){
                      //         request_data['headers'] = { 'Authorization': 'Token ' + $localStorage.userKey };
                      //     }
  		      //     return $http(request_data)
                      //         .success(function(response) {
                      //             if (typeof response.data === 'object') {
                      //                 return response.data;
                      //             } else { return null; }
                      //         })
                      //         .error(function(error){
                      //             return null;
                      //         });
                      var url = "api/videos/rating/";
                      var data = {video: obj_id, value: rating};
                      if(rate_id === null){
                          return (HttpServices.post(url, data));
                      }
                      return (HttpServices.patch(url+rate_id, data));
                  };
                      
                  return (vf);
              }]);
