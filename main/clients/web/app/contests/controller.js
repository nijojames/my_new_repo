'use strict';

angular.module('spyhop.contests',
	       ['ngRoute',
		'timer',
		'ui.bootstrap',
		'ngAnimate'])

    .controller('CreateContestCtrl',
		['$scope',
		 //'$http',
		 '$location',
		 '$state',
		 //'$rootScope',
		 '$stateParams',
		 //'$anchorScroll',
		 '$localStorage',
		 //'toaster',
		 'Contest',
                 'RouterTracker',
		 //'Upload',
		 //'levelFactory',
		 //'Map',
		 function($scope,
			  //$http,
			  $location,
			  $state,
			  //$rootScope,
			  $stateParams,
			  //$anchorScroll,
			  $localStorage,
			  //toaster,
			  Contest,
                          RouterTracker
			  //Upload,
			  //levelFactory,
			  /*Map*/) {
                     $scope.forms = {};
                     // $scope.slider = {
                     //     value: 10,
                     //     options: {
                     //         floor: 10,
                     //         ceil: 120
                     //     }
                     // };
                     // $scope.route_history = RouterTracker.getRouteHistory();
                     // if($scope.route_history.route !== undefined &&
                     //    $state.current.name=='create_contest' &&
                     //    $scope.route_history.route.name != 'review'){
                     //     console.log("INFO: Deleting contest:",
                     //                 $localStorage.contest_slug);
                     //     delete $localStorage.contest_slug
                     // }
                     if ($state.current.name == 'create_contest') {
                         // console.log("INFO: Deleting contest:",
                         //             $localStorage.contest_slug);
                         delete $localStorage.contest_slug;
                     }
                     // should not be any slug for create_contest
                     // console.log("DEBUG: state=", $state.current.name);
                     // console.log("DEBUG: slug=", $stateParams.slug);
                     $scope.validForm = true;
                     if ($stateParams.slug) {
                         $scope.showForm = false;
                         $localStorage.contest_slug = $stateParams.slug;
                         Contest.create_or_edit_contest($scope, false);
                     }
                     else {
                         $scope.showForm = true;
                         Contest.create_or_edit_contest($scope, true);
                     }
                     
		     /*
		     $scope.place = {};
		     $scope.contestForm = {};
		     $scope.contestForm.invited_people = [];
		     $scope.showmap = false;
		     $scope.pagination = {
			 current: 1,
			 items_per_page:3
		     };
		     Contest.get_categories($localStorage.contest_id).then(function (data) {
			 if (data){
			     $scope.categoryCollection = data.data.results;
			 }
		     }, function (error) {
			 alert("ERROR")
			 
		     });
		     $scope.search = function() {
			 // alert($scope.searchPlace.name)
			 $scope.apiError = false;
			 Map.search($scope.searchPlace.name)
			     .then(function(res) { // success
				 Map.addMarker(res.geometry.location);
				 $scope.place.name = res.name;
				 $scope.contestForm.location =
				     "" +
				     res.geometry.location.lat() +
				     ',' +
				     res.geometry.location.lng();
			     }, function(status) { // error
				 console.log("ERROR: Map.search failed["+status+"]");
				 $scope.apiError = true;
				 $scope.apiStatus = status;
			     });
		     }
		     
		     // use the current location as default
		     var userResponded = false;
		     var opt = {
			 enableHighAccuracy: false,
			 timeout: 5000, // wait 5 seconds
		     };
		     navigator.geolocation.getCurrentPosition(function(pos) {
			 userResponded = true;
			 Map.init(pos.coords.latitude,
				  pos.coords.longitude);
		     }, function(err) {
			 userResponded = true;
			 console.log("ERROR: getCurrentPosition=[", err,"]");
			 Map.init(40.748817, -73.985428);
		     }, opt);
		     // check if user responded
		     setTimeout(function () {
			 if(!userResponded){
			     window.console.log("INFO: Using default location");
			     Map.init(40.748817, -73.985428);
			 }
			 else{
			     console.log("INFO: User responded");
			 }
		     }, opt.timeout+1000);
		     
		     if($localStorage.contest_id != null){
			 Contest.get_contest($localStorage.contest_id).then(function (contest_data) {
			     if (contest_data) {
				 if (contest_data.story_board_images[0]) {
				     $scope.story_1 = contest_data.story_board_images[0].image;
				 }
				 if (contest_data.story_board_images[1]) {
				     $scope.story_2 = contest_data.story_board_images[1].image;
				 }
				 if (contest_data.story_board_images[2]) {
				     $scope.story_3 = contest_data.story_board_images[2].image;
				 }
				 if (contest_data.story_board_images[3]) {
				     $scope.story_4 = contest_data.story_board_images[3].image;
				 }
				 if (contest_data.story_board_images[4]) {
				     $scope.story_5 = contest_data.story_board_images[4].image;
				 }

				 $scope.review_contest = contest_data;
				 $scope.slider_ticks.value = contest_data.level_required - 1;
				 $scope.contestForm = contest_data;
				 $scope.trimlevel($scope.contestForm.level_required + 1);
				 if ($scope.contestForm.invited_people.length > 0) {
				     $scope.needinvite = true;
				     $scope.contestForm.invited_people = contest_data.invited_people
				 }
				 if ($scope.contestForm.location !== null) {
				     var location = $scope.contestForm.location.split(',')
				     Map.init(parseFloat(location[0]), parseFloat(location[1]));
				     $scope.showmap = true;
				 }
				 if ($scope.contestForm.space_for_copy == true) {
				     $scope.contestForm.space_for_copy = "true";
				     //                    $scope.space = 'Yes'
				 }
				 else {
				     $scope.contestForm.space_for_copy = "false";
				     //                    $scope.space = 'No'
				 }
				 if ($scope.contestForm.accept_curated_videos == true) {
				     $scope.contestForm.accept_curated_videos = "true";
				     //                    $scope.space = 'Yes'
				 }
				 else {
				     $scope.contestForm.accept_curated_videos = "false";
				     //                    $scope.space = 'No'

				 }
				 var expiry = new Date(contest_data.expiry_date);
				 $scope.contestForm.expiry_date = expiry;
			     }
			 }, function(error) {
			     alert("ERROR")
			 });
		     };
		     levelFactory.async().then(function(data) {
			 $scope.levelCollection = data[0].data;
			 $scope.levels_list = [];
			 angular.forEach(data[0].data, function (value, key) {
			     $scope.levels_list.push(value.level);
			     $scope.slider_ticks = {
				 value: 0,
				 options: {
				     ceil: 5,
				     floor: 0,
				     showTicks: true,
				     stepsArray: $scope.levels_list,
				     showTicksValues: true,
				     onChange: function(sliderId, modelValue, highValue){
					 $scope.contestForm.level_required = modelValue + 1;
					 $scope.trimlevel(modelValue + 1);

				     }
				 }
			     };
			 });
		     });


		     Contest.get_prices().then(function (data) {
			 $scope.price_config = data.data.results[0];
			 $scope.suggestedPrice = parseFloat($scope.price_config.level_one_min_price);
		     }, function (error) {
			 console.log("Unable to create the contest now")
		     });


		     $scope.contestForm.video_max_length=10;
		     var today = new Date();
		     var h = today.getMonth() + 1;
		     $scope.d = today.getFullYear() + '-' + '0' + h + '-' + today.getDate();
		     $scope.$watchCollection('$stateParams', function() {
			 $anchorScroll();
		     });

		     $scope.clearImage = function(image_field){
			 $scope[image_field] = undefined;
		     };
		     $scope.gotoUserProfile = function(username){
			 $('#choose_favorite_model').modal('hide');
			 $state.go('user_profile', {username: username})
		     }

		     $scope.videographerLevel = function(level, page){
			 if(level !== undefined){
			     $scope.selected_level = level;
			 }
			 var params_obj = {levels:level,
					   page_size:$scope.pagination.items_per_page,
					   page:page,
					   q:$scope.videographer_name}
			 Contest.inviteVideographers(params_obj).then(function (data){
			     $scope.videographerCollection = data.objects.results;
			     $scope.total_count = data.objects.count;
			 }, function (error) {
			     console.log("Unable to get the level")
			 });

		     };
		     $scope.newContest = function(a) {
			 
			 //$scope.kl= $scope.contestForm.image;

			 //if ($scope.createcontestForm.$valid) {

			 //contestService.set($scope.contestForm.contest_title,
			 $scope.contestForm.purpose, $scope.contestForm.type,
			 //    $scope.contestForm.orientation, $scope.contestForm.environment,
			 $scope.contestForm.lighting,
			 //    $scope.contestForm.quality, $scope.contestForm.award_amount,
			 $scope.contestForm.art_direction, $scope.contestForm.expiry_date,
			 //    $scope.contestForm.space, $scope.contestForm.level,
			 $scope.contestForm.duration);
			 //$state.go('review');
			 //}
			 if (!$scope.createcontestForm.$valid) {
			     //console.log($scope.contestForm.image,'asdasdasd')
			     $scope.contestSubmitted = true;
			 }

		     };

		     $scope.trimlevel = function (level) {
			 if (level===1){
			     $scope.suggestedPrice = $scope.price_config.level_one_min_price;
			 }
			 else if (level===2){
			     $scope.suggestedPrice = $scope.price_config.level_two_min_price;
			 }
			 else if (level===3){
			     $scope.suggestedPrice = $scope.price_config.level_three_min_price;
			 }
			 else if (level===4){
			     $scope.suggestedPrice = $scope.price_config.level_four_min_price;
			 }
			 else if (level===5){
			     $scope.suggestedPrice = $scope.price_config.level_five_min_price;
			 }

			 $scope.invitelevels = [];
			 for(var i=level-1; i>=0; i--){
			     $scope.invitelevels.push($scope.levelCollection[i]);
			 }
		     };

		     $scope.clearallVideographers = function(){
			 $scope.contestForm.invited_people = [];
		     };

		     $scope.inviteVideographer = function(user){
			 if($scope.contestForm.invited_people.length<=10){
			     user.invited = true;
			     $scope.contestForm.invited_people.push(user);
			 }
			 else{
			     toaster.pop('info', "Warning", "You can only invite maximum 10 people");
			 }
		     }
		     
		     $scope.removeInvite = function(index){
			 $scope.contestForm.invited_people[index].invited = false;
			 $scope.contestForm.invited_people.splice(index, 1);
		     };

		     $scope.contestSave = function() {
			 if(parseFloat($scope.contestForm.award_amount) <
			    parseFloat($scope.suggestedPrice)){
			     $scope.award_amount_invalid = true;
			     return false;
			 }
			 if (!$scope.showmap){
			     $scope.contestForm.location = undefined;
			 }
			 if($scope.contestForm.status == undefined){
			     $scope.contestForm.status = 'incomplete';
			 }
			 $scope.contestForm.type = 'S';
			 var filesMulti = {};
			 for(var i=1;i<=5;i++) {
			     var variable = 'story_' + i;
			     filesMulti[i] = $scope[variable]
			 }
			 $scope.contestForm.file = filesMulti;
			 $scope.people = $scope.contestForm.invited_people;
			 $scope.contestForm.invited_people = '';
			 angular.forEach($scope.people, function(value, key){
			     $scope.contestForm.invited_people =
				 $scope.contestForm.invited_people + value.id + ',';
			 });
			 if($localStorage.contest_id != null){
			     Contest.update_contest($localStorage.contest_id,
						    $scope.contestForm).then(function (contest_data){
							Upload.upload({
							    url: 'api/contests/save-story-board/' + $localStorage.contest_id,
							    method: 'PUT',
							    data: $scope.contestForm.file
							}).then(function (resp) {
							    $state.go('review');
							}, function (resp) {
							    console.log('Error status: ' + resp.status);
							}, function (evt) {
							});
						    }, function (error) {
							console.log("Unable to create the contest now")
						    });
			 }
			 else {
			     Contest.create_contest($scope.contestForm).then(function (contest_data) {
				 if(contest_data) {
				     Upload.upload({
					 url: 'api/contests/save-story-board/',
					 method: 'PUT',
					 data: $scope.contestForm.file
				     }).then(function (resp) {
					 $state.go('review');
					 $localStorage.contest_id = resp.data.id;
				     }, function (resp) {
					 console.log('Error status: ' + resp.status);
				     }, function (evt) {
				     });
				 }
			     }, function (error) {
				 console.log("Unable to create the contest now")
			     });

			 }
		     }*/
		 }])

// NB: unused controller
    .controller('EditContestCtrl',
		['$scope',
		 //'$http',
		 //'$location',
		 //'$state',
		 //'$rootScope',
		 //'$stateParams',
		 //'$anchorScroll',
		 //'$localStorage',
		 //'toaster',
		 'Contest',
		 //'Upload',
		 //'levelFactory',
		 //'Map',
		 function($scope,
			  // $http,
			  // $location,
			  // $state,
			  // $rootScope,
			  // $stateParams,
			  // $anchorScroll,
			  // $localStorage,
			  // toaster,
			  Contest
			  // Upload,
			  // levelFactory,
			  /*Map*/) {
		     Contest.create_or_edit_contest($scope, false);
		     /*
		     $scope.place = {};
		     $scope.contestForm = {};
		     $scope.contestForm.invited_people = [];
		     $scope.showmap = false;
		     $scope.pagination = {
			 current: 1,
			 items_per_page:3
		     };
		     Contest.get_categories($localStorage.contest_id).then(function (data) {
			 if (data){
			     $scope.categoryCollection = data.data.results;
			 }
		     }, function (error) {
			 alert("ERROR")
			 
		     });
		     $scope.search = function() {
			 //        alert($scope.searchPlace.name)
			 $scope.apiError = false;
			 Map.search($scope.searchPlace.name)
			     .then(
				 function(res) { // success
				     Map.addMarker(res);
				     $scope.place.name = res.name;
				     $scope.contestForm.location =
					 "" + res.geometry.location.lat() + ',' + res.geometry.location.lng();
				 },
				 function(status) { // error
				     $scope.apiError = true;
				     $scope.apiStatus = status;
				 }
			     );
		     }
		     
		     Map.init(40.748817, -73.985428);
		     if($stateParams.id != null){
			 Contest.get_contest($stateParams.id).then(function (contest_data) {
			     if (contest_data) {
				 if (contest_data.story_board_images[0]) {
				     $scope.story_1 = contest_data.story_board_images[0].image;
				 }
				 if (contest_data.story_board_images[1]) {
				     $scope.story_2 = contest_data.story_board_images[1].image;
				 }
				 if (contest_data.story_board_images[2]) {
				     $scope.story_3 = contest_data.story_board_images[2].image;
				 }
				 if (contest_data.story_board_images[3]) {
				     $scope.story_4 = contest_data.story_board_images[3].image;
				 }
				 if (contest_data.story_board_images[4]) {
				     $scope.story_5 = contest_data.story_board_images[4].image;
				 }
				 
				 $scope.review_contest = contest_data;
				 $scope.slider_ticks.value = contest_data.level_required - 1;
				 $scope.contestForm = contest_data;
				 $scope.trimlevel($scope.contestForm.level_required + 1);
				 if ($scope.contestForm.invited_people.length > 0) {
				     $scope.needinvite = true;
				     $scope.contestForm.invited_people = contest_data.invited_people
				 }
				 if ($scope.contestForm.location !== null) {
				     var location = $scope.contestForm.location.split(',')
				     Map.init(parseFloat(location[0]), parseFloat(location[1]));
				     $scope.showmap = true;
				 }
				 if ($scope.contestForm.space_for_copy == true) {
				     $scope.contestForm.space_for_copy = "true";
				     //                    $scope.space = 'Yes'
				 }
				 else {
				     $scope.contestForm.space_for_copy = "false";
				     //                    $scope.space = 'No'
				 }
				 if ($scope.contestForm.accept_curated_videos == true) {
				     $scope.contestForm.accept_curated_videos = "true";
				     //                    $scope.space = 'Yes'
				 }
				 else {
				     $scope.contestForm.accept_curated_videos = "false";
				     //                    $scope.space = 'No'

				 }
				 var expiry = new Date(contest_data.expiry_date);
				 $scope.contestForm.expiry_date = expiry;
			     }
			 }, function(error) {
			     alert("ERROR")
			 });
		     };
		     levelFactory.async().then(function(data) {
			 $scope.levelCollection = data[0].data;
			 $scope.levels_list = [];
			 angular.forEach(data[0].data, function (value, key) {
			     $scope.levels_list.push(value.level);
			     $scope.slider_ticks = {
				 value: 0,
				 options: {
				     ceil: 5,
				     floor: 0,
				     showTicks: true,
				     stepsArray: $scope.levels_list,
				     showTicksValues: true,
				     onChange: function(sliderId, modelValue, highValue){
					 $scope.contestForm.level_required = modelValue + 1;
					 $scope.trimlevel(modelValue + 1);

				     }
				 }
			     };
			 });
		     });


		     Contest.get_prices().then(function (data) {
			 $scope.price_config = data.data.results[0];
			 $scope.suggestedPrice = parseFloat($scope.price_config.level_one_min_price);
		     }, function (error) {
			 console.log("Unable to create the contest now")
		     });


		     $scope.contestForm.video_max_length=10;
		     var today = new Date();
		     var h = today.getMonth() + 1;
		     $scope.d = today.getFullYear() + '-' + '0' + h + '-' + today.getDate();
		     $scope.$watchCollection('$stateParams', function() {
			 $anchorScroll();
		     });

		     $scope.clearImage = function(image_field){
			 $scope[image_field] = undefined;
		     };
		     $scope.gotoUserProfile = function(username){
			 $('#choose_favorite_model').modal('hide');
			 $state.go('user_profile', {username: username})
		     }

		     $scope.videographerLevel = function(level){
			 if(level !== undefined){
			     $scope.selected_level = level;
			 }
			 var params_obj = {levels:level,
					   page_size:$scope.pagination.items_per_page,
					   page:page,
					   q:$scope.videographer_name}
			 Contest.inviteVideographers(params_obj).then(function (data) {
			     $scope.videographerCollection = data.objects.results;
			     $scope.total_count = data.objects.count;
			 }, function (error) {
			     console.log("Unable to get the level")
			 });

		     }
		     $scope.newContest = function(a) {
		     
			 //$scope.kl= $scope.contestForm.image;
			 
			 //if ($scope.createcontestForm.$valid) {

			 //contestService.set($scope.contestForm.contest_title,
			 //$scope.contestForm.purpose, $scope.contestForm.type,
			 //    $scope.contestForm.orientation, $scope.contestForm.environment, 
			 //$scope.contestForm.lighting,
			 //    $scope.contestForm.quality, $scope.contestForm.award_amount,
			 //$scope.contestForm.art_direction, $scope.contestForm.expiry_date,
			 //    $scope.contestForm.space, $scope.contestForm.level,
			 //$scope.contestForm.duration);
			 //$state.go('review');
			 //}
			 if (!$scope.createcontestForm.$valid) {
			     //console.log($scope.contestForm.image,'asdasdasd')
			     $scope.contestSubmitted = true;
			 }
		     };

		     $scope.trimlevel = function (level) {
			 if (level===1){$scope.suggestedPrice = $scope.price_config.level_one_min_price;}
			 else if (level===2){$scope.suggestedPrice = $scope.price_config.level_two_min_price;}
			 else if (level===3){$scope.suggestedPrice = $scope.price_config.level_three_min_price;}
			 else if (level===4){$scope.suggestedPrice = $scope.price_config.level_four_min_price;}
			 else if (level===5){$scope.suggestedPrice = $scope.price_config.level_five_min_price;}

			 $scope.invitelevels = [];
			 for(var i=level-1; i>=0; i--){
			     $scope.invitelevels.push($scope.levelCollection[i]);
			 }
		     };

		     $scope.clearallVideographers = function(){
			 $scope.contestForm.invited_people = [];
		     };

		     $scope.inviteVideographer = function(user){
			 if($scope.contestForm.invited_people.length<=10){
			     user.invited = true;
			     $scope.contestForm.invited_people.push(user);
			 }
			 else{
			     toaster.pop('info', "Warning", "You can only invite maximum 10 people");
			 }
		     }

		     $scope.removeInvite = function(index){
			 $scope.contestForm.invited_people[index].invited = false;
			 $scope.contestForm.invited_people.splice(index, 1);
		     };

		     $scope.contestSave = function() {
			 if(parseFloat($scope.contestForm.award_amount) <
			    parseFloat($scope.suggestedPrice)){
			     $scope.award_amount_invalid = true;
			     return false;
			 }
			 if (!$scope.showmap){
			     $scope.contestForm.location = undefined;
			 }
			 if($scope.contestForm.status == undefined){
			     $scope.contestForm.status = 'incomplete';
			 }
			 $scope.contestForm.type = 'S';
			 var filesMulti = {};
			 for(var i=1;i<=5;i++) {
			     var variable = 'story_' + i;
			     filesMulti[i] = $scope[variable]
			 }
			 $scope.contestForm.file = filesMulti;
			 $scope.people = $scope.contestForm.invited_people;
			 $scope.contestForm.invited_people = '';
			 angular.forEach($scope.people, function (value, key) {
			     $scope.contestForm.invited_people = $scope.contestForm.invited_people + value.id + ',';
			 });
			 if($localStorage.contest_id != null){
			     Contest.update_contest($localStorage.contest_id, $scope.contestForm).then(function (contest_data) {
				 Upload.upload({
				     url: 'api/contests/save-story-board/' + $localStorage.contest_id,
				     method: 'PUT',
				     data: $scope.contestForm.file
				 }).then(function (resp) {
				     $state.go('review');
				 }, function (resp) {
				     console.log('Error status: ' + resp.status);
				 }, function (evt) {


				 });
			     }, function (error) {
				 console.log("Unable to create the contest now")
			     });
			 }
			 else {
			     Contest.create_contest($scope.contestForm).then(function (contest_data) {
				 if(contest_data) {
				     Upload.upload({
					 url: 'api/contests/save-story-board/',
					 method: 'PUT',
					 data: $scope.contestForm.file
				     }).then(function (resp) {
					 $state.go('review');
					 $localStorage.contest_id = resp.data.id;
				     }, function (resp) {
					 console.log('Error status: ' + resp.status);
				     }, function (evt) {
				     });
				 }
			     }, function (error) {
				 console.log("Unable to create the contest now")
			     });

			 }
		     }*/

		 }])

    .controller('ReviewCtrl',
		['$scope',
		 //'$http',
		 '$location',
		 '$state',
		 '$rootScope',
		 '$stateParams',
		 '$anchorScroll',
		 '$localStorage',
		 'Contest',
		 'Cart',
		 'toaster',
                 'HttpServices',
                 'Main',
		 function($scope,
			  //$http,
			  $location,
			  $state,
			  $rootScope,
			  $stateParams,
			  $anchorScroll,
			  $localStorage,
			  Contest,
			  Cart,
			  toaster,
                          HttpServices,
                          Main) {
                     //
                     // ToDo: refactor using Contest factory
                     //
                     $scope.showPage = false;
                     $scope.publish = true;
		     if ($stateParams.slug === undefined) {
                         //$scope.page_not_found = true;
		         $state.go('404');
		     }
		     $scope.contestEdit = function() {
			 if($scope.contestForm.status !== undefined/*'incomplete'*/){
			     $state.go('edit_contest', {slug:$scope.contestForm.slug});
			 }
			 else{
			     $state.go('create_contest');
			 }
		     };

                     Main.init_default_config($scope);

                     Contest.get_categories().then(function(response) {
			 if (response.data){
			     $scope.categoryCollection = response.data.results;
			 }
		     }, function (response) {
                         console.log("ERROR: get_categories=", response);
		     });

                     $scope.contestPublish = function() {
			 Contest.patch_contest($stateParams.slug, 'draft')
                             .then(function(response) {
			         $scope.contestForm.status = 'draft';
                                 Cart.add_to_cart($scope.contestForm.id, 'contest')
                                     .then(function(response) {
                                         $rootScope.cart = response.data;
				         toaster.pop('success',
                                                     "Success",
                                                     "Contest added to cart");
                                         $state.go('cart')
                                         delete $localStorage.contest_slug;
                                         $scope.publish=false
			             }, function(response) {
                                         console.log("ERROR: contestPublish=",
                                                     response.data);
                                         toaster.pop('info',
                                                     "Warning",
                                                     //"This item is already in your cart");
                                                     response.data.error);
			             });
                                 // delete $localStorage.contest_id;
			     }, function(response) {
                                 // ToDo: check the response.status for appropriate message
			         console.log("ERROR: patch_contest=", response);
			     });
		     };

                     $scope.contestSaveDraft = function() {
			 Contest.patch_contest($stateParams.slug, 'draft')
                             .then(function(response) {
			         $scope.contestForm.status = 'draft';
			         toaster.pop('success', "Success", "Contest saved as draft");
			     }, function(response) {
                                 // ToDo: check the response.status for appropriate message
			         console.log("ERROR: patch_contest=", response);
			     });
		     }

		     $scope.nameView = function() {
                         if ($localStorage.userKey != null) {
			     // $http.get($localStorage.host +
			     //           'api/profiles/profile-list/' +
			     //           $localStorage.userName, {
			     //     headers: {
			     //         'Authorization': 'Token ' + $localStorage.userKey
			     //     }
			     // }).success(function(response) {
			     //     $scope.first_name = response.first_name;
			     //     $scope.last_name = response.last_name;
			     // });
                             HttpServices.get('api/profiles/profile-list/'+
			                      $localStorage.userName)
                                 .then(function(response){
                                     $scope.first_name = response.data.first_name;
			             $scope.last_name = response.data.last_name;
                                 });
			 }
                     }

                     $scope.deleteContest = function() {
			 Contest.delete_contest($stateParams.slug)
                             .then(function(response) {
			         delete $localStorage.contest_slug;
			         $scope.contestCreated = false;
			         $scope.contestDeleted = true;
			         $state.go('create_contest');
			     }, function(response) {
			         console.log("ERROR: delete_contest=", response);
			     });
		     }
                     // ToDo: fix this - refer to create_or_edit_contest
		     Contest.get_contest($stateParams.slug)
                         .then(function(response){
			     if (response.data) {
                                 var contest_data = response.data;
                                 Contest.check_and_redirect(contest_data,
                                                            $stateParams.slug);
                                 $scope.showPage = true;
			         $scope.review_contest = contest_data;
			         $scope.contestForm = contest_data;
			         if ($scope.contestForm.space_for_copy==true){
				     $scope.contestForm.space_for_copy='true'
                                     $scope.space = 'Yes'
			         } else {
				     $scope.contestForm.space_for_copy = 'false'
				     $scope.space = 'No'
			         }
			         var expiry = new Date(contest_data.expiry_date);
			         $scope.contestForm.expiry_date = expiry;
			         $scope.contestForm.award_amount = Math.round(contest_data.award_amount);
                                 $scope.endtime = expiry.getTime();
			         //*******timer*****//
                                 
			         // var timer;
			         // var compareDate = new Date();
			         // var date1 = compareDate
			         // var date2 = $scope.contestForm.expiry_date
			         // var timeDiff = Math.abs(date2.getTime() - date1.getTime());
			         // var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
			         // compareDate.setDate(compareDate.getDate() + diffDays);
			         // timer = setInterval(function() {
				 //     timeBetweenDates(compareDate);
			         // }, 1000);
			     }
			     
			     // function timeBetweenDates(toDate) {
			     //     var dateEntered = toDate;
			     //     var now = new Date();
			     //     var difference = dateEntered.getTime() - now.getTime();
			     //     if (difference <= 0) {
			     //         // Timer done
			     //         clearInterval(timer);
			     //     }
                             //     else {
                             // 	     var seconds = Math.floor(difference / 1000);
			     //         var minutes = Math.floor(seconds / 60);
			     //         var hours = Math.floor(minutes / 60);
			     //         var days = Math.floor(hours / 24);
                                     
			     //         hours %= 24;
			     //         minutes %= 60;
			     //         seconds %= 60;
                                     
			     //         $("#days").text(days);
			     //         $("#hours").text(hours);
			     //         $("#minutes").text(minutes);
			     //         $("#seconds").text(seconds);
			     //     }
			     // }
			     //****End of timer****//
		         }, function(response) {
                             console.log("ERROR: get_contest=", response.data);
                             $state.go('404');
			     //alert("ERROR: failed to get contest");
		         });
                 }])

    .controller('ContestDetailCtrl',
		['$scope',
		 //'$http',
		 '$location',
		 '$state',
		 '$rootScope',
		 '$stateParams',
		 '$anchorScroll',
		 '$localStorage',
                 '$timeout',
		 'Contest',
		 'toaster',
		 'HttpServices',
                 'Main',
                 'Cart',
		 function($scope,
			  //$http,
			  $location,
			  $state,
			  $rootScope,
			  $stateParams,
			  $anchorScroll,
			  $localStorage,
                          $timeout,
			  Contest,
			  toaster,
                          HttpServices,
                          Main,
                          Cart) {
                     $scope.showPage = false;
		     $scope.tab = 0;
		     $scope.follow_User = function (uploader, type) {
			 Contest.follow(uploader.id, type)
                             .then(function(response) {
			         if (response.status == 204) {
				     uploader.is_following = false;
			         }
                                 else if (response.status == 201) {
				     uploader.is_following = true;
			         }
			     }, function(response) {
			         toaster.pop('info', "Warning", "Please login to follow");
			     });
		     };
                     
                     Main.init_default_config($scope);
                     
                     // $scope.publishContestEdit = function(id){
                     //     $localStorage.contest_id = id
                     //     $state.go('edit_contest',({id:id}))
                     // }
                     // $scope.contestWinner=function(id,entry){
                     //     Contest.winner(id,entry)
                     //         .then(function (response) {
                     //             toaster.pop('success',"Won","Contest winner selected")
		     //         }, function (response) {
                     //             if (response.status == 403) {
		     //                 toaster.pop('info', "Warning", "Already selected a winner");
		     //             }
                     //             else {
                     //                 console.log("ERROR: Contest.winner=",
                     //                             response);
                     //             }
		     //         });
                     // }

		     $scope.msg=function (msg) {
			 $('#input-comment').val('')
			 var msg_data ={'contest': $scope.contestData.id,
					'message': msg};
			 Contest.send_message(msg_data)
                             .then(function(response){
			         Contest.get_messages($scope.contestData.id)
                                     .then(function(response) {
				         $scope.messages = response.data.results
			             });
			     }, function(response){
			         console.log("ERROR: send_message=", response);
                                 alert("ERROR: failed to send message");
			     });
		     }
                     Contest.get_categories($stateParams.slug)
                         .then(function(response) {
			     if (response.data){
			         $scope.categoryCollection = response.data.results;
			     }
		         }, function (response) {
                             console.log("ERROR: get_categories=", response);
			     //alert("ERROR: unable to get_categories");
		         });

                     $scope.addToCart = function(video_id) {
                         Cart.add_to_cart(video_id, 'video')
                             .then(function(response) {
                                 $rootScope.cart = response.data;
                                 toaster.pop('success', "Success", "Item added to cart");
                             }, function(response) {
                                 console.log("ERROR: addToCart=", response.data);
                                 toaster.pop('info',
                                             "Warning",
                                             response.data.error);
                                 //"This item is already in your cart");
                             });
                     };
                     
                     Contest.get_contest($stateParams.slug)
                         .then(function(response) {
                             //console.log("DEBUG: contest=", response);
                             var contest_data = response.data;
			     if (contest_data) {
                                 if (contest_data.status == 'draft' ||
                                     contest_data.status == 'incomplete') {
                                     $state.go('review', {slug: $stateParams.slug});
                                 }
                                 $scope.showPage = true;
                                 if ($rootScope.redirect_to_contest){
                                     toaster.pop({
                                         type: 'success',
                                         title: 'Contest Published',
                                         body: 'Your contest "' + contest_data.contest_title + '" got published',
                                         showCloseButton: true
                                     });
                                     // $rootScope.cart.items_count = 0;
                                     $rootScope.redirect_to_contest = undefined;
                                 }
			         $scope.m=1;
			         $scope.contestData = contest_data;
                                 Contest.get_messages($scope.contestData.id).then(function(response) {
			             $scope.messages = response.data.results
		                 })
                                 $scope.purchasedCnt = 0;
                                 Contest.get_entries($scope.contestData.id).then(function(response) {
                                     //console.log("DEBUG: entries=", response.data);
			             $scope.entries=response.data
                                     // ToDo: this will become slow with lot of entries
			             var up=[]
			             for(var i=0;i<$scope.entries.results.length;i++){
                                         if ($scope.entries.results[i].winner){
                                             $scope.purchasedCnt++;
                                         }
			                up.push($scope.entries.results[i].video_data.user)
			            }
			            var uniqueNames = [];
			            $.each(up, function(i, el){
			                if($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
			            });
			            $scope.uploads=uniqueNames.length;
                                     $timeout(function(){
                                         enableAutoPlay("get_entries");
                                     }, 0, false);
		                 });

			         $scope.contestData.award_amount =
                                     Math.round(contest_data.award_amount)
			         var expiry = new Date(contest_data.expiry_date);
			         $scope.contestData.expiry_date = expiry;
			         //*******timer*****//
			         //var timer;
			         var compareDate = new Date();
                                 // contest could have been force closed
                                 if ($scope.contestData.status == 'closed') {
                                     $scope.m = 0;
                                 }
			         else if(expiry>compareDate){
				     //var date1 = compareDate
				     //var date2 = $scope.ContestData.expiry_date
				     //$scope.endtime=date2.getTime();
                                     $scope.endtime = expiry.getTime();
				     /*var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                                       
				       var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
				       compareDate.setDate(compareDate.getDate() + diffDays);
				       timer = setInterval(function() {
				       timeBetweenDates(compareDate);
				       }, 1000);*/
			         }
			         else {
				     $scope.m=0;
			         }
			     }
			     /*
			       function timeBetweenDates(toDate) {
			       var dateEntered = toDate;
			       var now = new Date();
			       var difference = dateEntered.getTime() - now.getTime();
			       if (difference <= 0) {
			       // Timer done
			       clearInterval(timer);
			       } else {
                               
			       var seconds = Math.floor(difference / 1000);
			       var minutes = Math.floor(seconds / 60);
			       var hours = Math.floor(minutes / 60);
			       var days = Math.floor(hours / 24);
                               
			       hours %= 24;
			       minutes %= 60;
			       seconds %= 60;
                               
			       $("#days").text(days);
			       $("#hours").text(hours);
			       $("#minutes").text(minutes);
			       $("#seconds").text(seconds);
			       }
			       }*/
			     //****End of timer****//
		         }, function(response) {
                             console.log("ERROR: get_contest=", response.data);
                             $state.go('404');
			     //alert("ERROR: unable to get contest");
		         });
		 }])

    .factory('Contest',
	     ['$location',
              //'$http',
	      '$state',
	      '$stateParams',
	      '$anchorScroll',
	      '$localStorage',
              '$timeout',
              '$q',
	      'toaster',
	      'Upload',
	      'levelFactory',
	      'Map',
              'HttpServices',
              'Utils',
              'Main',
              function($location,
                       //$http,
		       $state,
		       $stateParams,
		       $anchorScroll,
		       $localStorage,
                       $timeout,
                       $q,
		       toaster,
		       Upload,
		       levelFactory,
		       Map,
                       HttpServices,
                       Utils,
                       Main) {
                  // default location
                  var dlat = 40.748817;
		  var dlng = -73.985428;
                  
		  var ct = {};
		  ct.get_contest = function(slug) {
                      // var url = $localStorage.host + "api/contests/contest/" + id;
		      // var request_data = {
		      //     method: 'GET',
		      //     url: url
		      // };
		      // if($localStorage.userKey !== undefined){
		      //     request_data['headers'] = { 'Authorization': 'Token ' + $localStorage.userKey };
		      // }
		      // return $http(request_data).then(function(response) {
		      //     if (typeof response.data === 'object') {
		      //         return response.data;
		      //     } else {
                      //         console.log("ERROR: ct.get_contest failed")
		      //         return "Error";
		      //     }
		      // }, function(error) {
                      //     console.log("ERROR: ct.get_contest failed. error=",
                      //                 error);
		      //     return null;
		      // });
                      return (HttpServices.get("api/contests/contest/"+slug));
		  };
		  
		  ct.update_contest = function(id, data){
		      // var url = $localStorage.host + "api/contests/contest/" + id;
		      // var request_data = {method: 'PUT', url: url, data: data };
		      // if($localStorage.userKey !== undefined){
		      //     request_data['headers'] = {'Authorization': 'Token ' + $localStorage.userKey};
		      // }
		      // return $http(request_data).then(function(response) {
		      //     if (typeof response.data === 'object') {
		      //         return response.data;
		      //     } else {
                      //         console.log("ERROR: ct.update_contest failed")
		      //         return "Error";
		      //     }
		      // }, function(error) {
                      //     console.log("ERROR: ct.update_contest failed. error=",
                      //                 error)
		      //     return null;
		      // });
                      return (HttpServices.put("api/contests/contest/"+id,
                                               data));
  		  };
		  
		  ct.patch_contest = function(id, status){
		      // var url = $localStorage.host + "api/contests/contest/" + id;
		      // var request_data = {method: 'PATCH', url: url, data: {status: status}};
		      // if($localStorage.userKey !== undefined){
		      //     request_data['headers'] = { 'Authorization': 'Token ' + $localStorage.userKey };
		      // }
		      // return $http(request_data).then(function(response) {
		      //     if (typeof response.data === 'object') {
		      //         return response.data;
		      //     } else {
		      //         return "Error";
		      //     }
		      // }, function(error) {
		      //     return null;
		      // });
                      return (HttpServices.patch("api/contests/contest/"+id,
                                                 {status: status}));
  		  };
		  
		  ct.follow = function(obj_id, type){
		      // var url = $localStorage.host + "api/activities/follow/";
		      // var request_data = {method: 'POST', url: url, data:{obj_id: obj_id, content_type: type}};
		      // if($localStorage.userKey !== undefined){
		      //     request_data['headers'] = { 'Authorization': 'Token ' + $localStorage.userKey };
		      // }
  		      // return $http(request_data)
		      //     .success(function(response) {
		      //         if (typeof response.data === 'object') {
		      //   	  return response.data;
		      //         } else { return null; }
		      //     })
		      //     .error(function(error){
		      //         return null;
		      //     });
                      return (HttpServices.post("api/activities/follow/",
                                                {obj_id: obj_id,
                                                 content_type: type}));
  		  };

                  ct.winner = function(contest_slug, entry){
		      // var url = $localStorage.host + "api/contests/contest-entries/"+entry+"?contest="+contest_id
		      // var request_data = {method: 'PATCH', url: url, data:{winner: 1}};
		      // if($localStorage.userKey !== undefined){
		      //     request_data['headers'] = { 'Authorization': 'Token ' + $localStorage.userKey };
		      // }
  		      // return $http(request_data)
		      //     .success(function(response) {
		      //         if (typeof response.data === 'object') {
		      //   	  return response.data;
		      //         } else { return null; }
		      //     })
		      //     .error(function(error){
		      //         return null;
		      //     });
                      return (HttpServices.patch("api/contests/contest-entries/"+
                                                 entry+
                                                 "?contest="+
                                                 contest_slug,
                                                 {winner: 1}));
  		  };
                  
		  ct.inviteVideographers = function(params_obj){
		      // var url = "api/profiles/search/facets/";
		      // var request_data = {method: 'GET', url: url, params: params_obj };
  		      // return $http(request_data).then(function(response) {
		      //     if (typeof response.data === 'object') {
		      //         return response.data;
		      //     } else { return "Error"; }
		      // }, function(error) {
		      //     console.log("ERROR: inviteVideographers failed. error=",
		      //   	      error);
		      //     return null;
		      // });
                      return (HttpServices.get("api/profiles/search/facets/",
                                               params_obj));
		  };
		  
		  ct.create_contest = function(data){
		      // var url = $localStorage.host + "api/contests/contest/";
		      // var request_data = {
		      //     method: 'POST',
		      //     url: url,
		      //     data: data
		      // };
		      // return $http(request_data)
		      //     .success(function(response){
		      //         $localStorage.contest_id = response.id;
		      //         if (typeof response.data === 'object') {
		      //   	  return response.data;
		      //         }
                      //         else {
                      //             return null;
                      //         }
		      //     })
		      //     .error(function(error) {
                      //         console.log("ERROR: ct.create_contest failed. error=",
                      //                     error)
		      //         return null;
		      //     });
                      return (HttpServices.post("api/contests/contest/",
                                                data));
		  };
		  
		  ct.get_prices = function(data){
		      // var url = "/api/general/price-config/";
		      // var request_data = {method: 'GET', url: url };
  		      // return $http(request_data)
		      //     .success(function(response) {
		      //         if (typeof response === 'object') {
		      //   	  return response.results;
		      //         } else { return null; }
		      //     })
		      //     .error(function(error){
		      //         return null;
		      //     });
                      return (HttpServices.get("api/general/price-config/"));
  		  },
		  
		  ct.get_categories = function(){
		      // var url = "/api/general/categories/";
		      // var request_data = {method: 'GET', url: url };
  		      // return $http(request_data)
		      //     .success(function(response) {
		      //         if (typeof response === 'object') {
		      //   	  return response.results;
		      //         } else { return null; }
		      //     })
		      //     .error(function(error){
		      //         return null;
		      //     });
                      return (HttpServices.get("api/general/categories/"));
  		  },
		  
          	  ct.get_messages = function(id){
		      // var url = $localStorage.host + "api/contests/messages/?contest__id="+id;
		      // var request_data = {
		      //     method: 'GET',
		      //     url: url
		      // };
		      // return $http(request_data)
		      //     .success(function(response) {
		      //         if (typeof response === 'object') {
		      //   	  return response.results;
		      //         } else { return null; }
		      //     })
		      //     .error(function(error){
		      //         return null;
		      //     }); 
                      return (HttpServices.get("api/contests/messages/?contest__id="+id));
		  };
		  
		  ct.get_entries = function(id){
		      // var url = $localStorage.host + "api/contests/contest-entries/?contest="+id;
		      // var request_data = {
		      //     method: 'GET',
		      //     url: url
		      // };
		      // return $http(request_data)
		      //     .success(function(response) {
		      //         if (typeof response === 'object') {
		      //   	  return response.results;
		      //         } else { return null; }
		      //     })
		      //     .error(function(error){
		      //         return null;
		      //     });
                      return (HttpServices.get("api/contests/contest-entries/?contest="+id));
		  };
		  
		  ct.send_message = function(msg){
		      // var url = $localStorage.host + "api/contests/messages/";
		      // var request_data = {
		      //     method: 'POST',
		      //     url: url,
		      //     data: msg
		      // };
		      // if($localStorage.userKey !== undefined){
		      //     request_data['headers'] = { 'Authorization': 'Token ' + $localStorage.userKey };
		      // }
		      // return $http(request_data)
		      //     .success(function(response) {
			      
		      //         if (typeof response.data === 'object') {
		      //   	  return response.data;
		      //         } else { return null; }
		      //     })
		      //     .error(function(error) {
		      //         return null;
		      //     });
                      // ToDo: should include contest id in API ?
                      return (HttpServices.post("api/contests/messages/", msg));
		  };
		  
		  ct.delete_contest = function(slug){
		      // var url = $localStorage.host + "api/contests/contest/" + id;
		      // var request_data = {
		      //     method: 'DELETE',
		      //     url: url
		      // };
		      // return $http(request_data).then(function(response) {
		      //     if (typeof response.data === 'object') {
		      //         return response.data;
		      //     } else {
		      //         return "Error";
		      //     }
		      // }, function(error) {
		      //     return null;
		      // });
                      return (HttpServices.delete("api/contests/contest/"+slug));
		  };

		  ct.default_location = function(lat, lng, callback){
		      var userResponded = false;
		      var opt = {
			  enableHighAccuracy: false,
			  timeout: 5000, // wait 5 seconds
		      };
		      navigator.geolocation.getCurrentPosition(function(pos) {
			  userResponded = true;
			  console.log("INFO: Using current location");
			  Map.init(pos.coords.latitude,
                                   pos.coords.longitude);
                          if (callback != null){
                              callback(pos.coords.latitude,
                                       pos.coords.longitude);
                          }
		      }, function(error) {
			  userResponded = true;
			  console.log("ERROR: getCurrentPosition failed. error=",
				      error);
		      }, opt);
		      // check if user responded
		      setTimeout(function () {
			  if(!userResponded){
			      console.log("INFO: Using default location");
			      if (lat && lng) {
				  Map.init(lat, lng);
                                  if (callback != null){
                                      callback(pos.coords.latitude,
                                               pos.coords.longitude);
                                  }
			      }
			  }
			  else{
			      console.log("INFO: User responded");
			  }
		      }, opt.timeout+1000);
		  };

                  ct.check_and_redirect = function(contest_data, contest_slug){
                      console.log(contest_data);
                      // only owners should be able to edit the contest
                      if (contest_data.user_detail &&
                          !contest_data.contest_creator) {
                          if (contest_data.status == 'draft' ||
                              contest_data.status == 'incomplete' ||
                              contest_data.private) {
                              $state.go('404');                              
                          }
                          $state.go('contest_detail', {slug: contest_slug});
                      }
                      // contest can only be edited in draft/incomplete state
                      if (contest_data.status != 'draft' &&
                          contest_data.status != 'incomplete') {
                          $state.go('contest_detail', {slug: contest_slug});
                      }
                  };

		  ct.create_or_edit_contest = function(scope, create){
                      var locToStr = function(lat, lng){
                          return (""+lat+','+lng);
                      };
                      scope.invitedPeopleLookup = {};
                      scope.userEnteredPrice = false;
	              scope.slider_ticks = {};
                      scope.place = {};
		      scope.contestForm = {};
                      scope.contestForm.purpose = "unused";
		      scope.contestForm.invited_people = [];
		      scope.showmap = false;
		      scope.pagination = {
			  current: 1,
			  items_per_page:3
		      };
                      Utils.promptOnDirtyExit(scope, 'forms.createcontestForm');

                      scope.update_video_type = function(index){
                          scope.contestForm.fps_rate =
                              scope.defaultCfg.video_type.results[index].fps_rate;
                          scope.contestForm.resolution =
                              scope.defaultCfg.video_type.results[index].resolution;
                          //console.log("DEBUG: contestForm=", scope.contestForm);
                      };

                      var set_default_config = function(response, create){
                          if (!response.data) {
                              console.log("ERROR: set_default_cfg=no data");
                              return;
                          }
                          scope.defaultCfg = response.data;
                          scope.slider = {
                              options: {
                                  floor: scope.defaultCfg.video_misc_cfg.results[0].min_duration_sec,
                                  ceil: scope.defaultCfg.video_misc_cfg.results[0].max_duration_sec,
                                  keyboardInput: true, 
                              }
                          };
                          if (!create) {
                              return;
                          }
                          // results[0] has the default values
                          scope.contestForm.space_for_copy =
                              scope.defaultCfg.video_misc_cfg.results[0].space_for_copy.toString();
                          scope.contestForm.accept_curated_videos =
                              scope.defaultCfg.video_misc_cfg.results[0].accept_curated_videos.toString();
                          scope.contestForm.video_max_length =
                              scope.defaultCfg.video_misc_cfg.results[0].min_duration_sec;
                          
                          scope.contestForm.video_type =
                              scope.defaultCfg.video_type.results[0].video_type;
                          scope.update_video_type(0);
                          scope.contestForm.lighting =
                              scope.defaultCfg.video_lighting.results[0].lighting;
                          scope.contestForm.environment =
                              scope.defaultCfg.video_environment.results[0].environment;
                      };
                      // build the contest form with default config
                      scope.defaultCfg = {};
                      var set_categories = function(response){
                          if (!response.data){
                              console.log("ERROR: set_categories=no data");
                              return;
                          }
			  scope.categoryCollection = response.data.results;
		      }

                      var set_prices = function(response){
                          if (!response.data){
                              console.log("ERROR: set_prices=no data");
                              return;
                          }
                          scope.price_config = response.data.results[0];
                          scope.price_config.level_one_min_price =
                              parseFloat(scope.price_config.level_one_min_price);
                          scope.price_config.level_two_min_price =
                              parseFloat(scope.price_config.level_two_min_price);
                          scope.price_config.level_three_min_price =
                              parseFloat(scope.price_config.level_three_min_price);
                          scope.price_config.level_four_min_price =
                              parseFloat(scope.price_config.level_four_min_price);
                          scope.price_config.level_five_min_price =
                              parseFloat(scope.price_config.level_five_min_price);
                          scope.price_config.private_request_price =
                              parseFloat(scope.price_config.private_request_price);
                          scope.price_config.invite_price =
                              parseFloat(scope.price_config.invite_price);
                          scope.price_config.model_release_price =
                              parseFloat(scope.price_config.model_release_price);
                          scope.price_config.location_price =
                              parseFloat(scope.price_config.location_price);
                          
                          scope.suggestedPrice =
                              scope.price_config.level_one_min_price;
                          // if (1 ||
                          //     scope.contestForm.award_amount === undefined ||
                          //     scope.contestForm.award_amount < scope.suggestedPrice) {
                          //     scope.contestForm.award_amount = scope.suggestedPrice;
                          // }
                          scope.updateAwardAmount();
                      };

                      var set_level_slider = function(data){
                          if (!data){
                              console.log("ERROR: set_level_slider=no data");
                              return;
                          }
                          scope.levelCollection = data[0].data;
                          scope.invitelevels=[];
                          scope.invitelevels.push(scope.levelCollection[0]);
			  scope.levels_list = [];
			  angular.forEach(data[0].data, function (value, key) {
			      scope.levels_list.push(value.level);
			      scope.slider_ticks = {
				  value: 0,
				  options: {
				      ceil: 5,
				      floor: 0,
				      showTicks: true,
				      stepsArray: scope.levels_list,
				      showTicksValues: true,
				      onChange: function(sliderId, modelValue, highValue){
					  scope.contestForm.level_required = modelValue + 1;
					  scope.trimlevel(modelValue + 1);
				      }
				  }
			      };
			  });
                      };
		      
                      scope.extrasPrice = 0;
		      scope.search = function(/*event*/) {
                          // if (!scope.searchPlace) {
                          //     return;
                          // }
                          // if (!scope.searchPlace.name) {
                          //     return;
                          // }
                          // /*if (scope.searchForm.$invalid) {
                          //   return;
                          //   }*/
                          // if (event.charCode != 13) {
                          //     return;
                          // }
                          // if (event.which != 13) {
                          //     return;
                          // }
                          // if (event.keyCode != 13) {
                          //     return;
                          // }
			  // alert(scope.searchPlace.name)
			  scope.apiError = false;
			  Map.search(scope.searchPlace.name).then(function(res) { // success
			      Map.addMarker(res.geometry.location);
			      scope.place.name = res.name;
			      scope.contestForm.location =
                                  locToStr(res.geometry.location.lat(),
				           res.geometry.location.lng());
			  }, function(error) { // error
			      console.log("ERROR: Map.search failed. error=",
					  error);
			      scope.apiError = true;
			      scope.apiStatus = error;
			  });
		      }

		      scope.showLocation = function(newValue){
                          if (newValue) {
                              scope.extrasPrice += scope.price_config.location_price;
                              scope.updateAwardAmount();
                              
			      var location_specified = false;
			      if (scope.contestForm.location) {
			          var location = scope.contestForm.location.split(',');
			          if (location[0] && location[1]) {
				      location_specified = true;
                                      console.log("INFO: contest has location", location);
                                      $timeout(function(){
				          Map.init(parseFloat(location[0]), parseFloat(location[1]));
                                      }, 0, false);
                                  }
                              }
			      else if (scope.searchPlace && scope.searchPlace.name) {
				  console.log("INFO: last used location=",
					      scope.searchPlace.name);
				  scope.search();
				  location_specified = true;
			      }
			      if (!location_specified) {
			          console.log("INFO: no/invalid location");
                                  ct.default_location(dlat,
                                                      dlng,
                                                      function(lat, lng){
                                                          scope.contestForm.location =
                                                              locToStr(lat,lng);
                                                      });
			      }
                              return;
                          }
                          scope.extrasPrice -= scope.price_config.location_price;
                          scope.updateAwardAmount();
                          scope.contestForm.location = null;  
		      }
                      //
                      // wait for all the APIs promises to resolve
                      //
                      $q.all({'default_cfg': Main.get_default_config(),
                              'categories': ct.get_categories(),
                              'prices': ct.get_prices(),
                              'level_slider': levelFactory.async()})
                          .then(function(results){
                              //console.log("DEBUG: $q.all=", results);
                              set_default_config(results.default_cfg, create);
                              set_categories(results.categories);
                              set_prices(results.prices);
                              set_level_slider(results.level_slider);
                              setup_contest();
                          }, function(results){
                              console.log("ERROR: $q.all=", results);
                          });
                      
                      var setup_contest = function(){
                          var contest_slug;
		          //var lat = 40.748817;
		          //var lng = -73.985428;
		          // use the current location as default
		          if (create) {
			      contest_slug = $localStorage.contest_slug;
                              //console.log("INFO: already a contest id exists=", contest_id);
                              //contest_id = $localStorage.contest_id = undefined;
                              
			      /*
			        var userResponded = false;
			        var opt = {
			        enableHighAccuracy: false,
			        timeout: 5000, // wait 5 seconds
			        };
			        navigator.geolocation.getCurrentPosition(function(pos) {
			        userResponded = true;
			        //Map.init(pos.coords.latitude,
			        //       pos.coords.longitude);
			        lng = pos.coords.longitude;
			        lat = pos.coords.latitude;
			        }, function(err) {
			        userResponded = true;
			        console.log("ERROR: getCurrentPosition=[", err,"]");
			        Map.init(lat, lng);
			        }, opt);
			        // check if user responded
			        setTimeout(function () {
			        if(!userResponded){
				window.console.log("INFO: Using default location");
				Map.init(lat, lng);
			        }
			        else{
				console.log("INFO: User responded");
			        }
			        }, opt.timeout+1000);*/
		          }
		          else {
			      //contest_slug = $stateParams.slug;
                              contest_slug = $localStorage.contest_slug;
			      // Map.init(lat, lng);
		          }
		          var ed = new Date ();
		          ed.setDate(ed.getDate()+1);
                          
		          scope.dateOptions = {
			      formatYear: 'yy',
			      //maxDate: new Date(2020, 5, 22),
			      minDate: ed,
                              showWeeks: false
		          };
                          
		          scope.open = function(){
			      scope.popup.opened = true;
                              //scope.popup.focus = true;
		          };
                          
		          /*scope.setDate = function(year, month, day){
			    scope.expiry_date = new Date(year, month, day);
		            };*/
                          
		          //scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
		          scope.format = 'MM/dd/yyyy'
		          scope.altInputFormats = [];//['M!/d!/yyyy'];
		          scope.popup = {
			      opened: false
		          };
		          // set default as morrow's date
		          scope.contestForm.expiry_date = ed;
                          // create invited people lookup
                          scope.createInvitedPeopleLookup = function(invited_people){
                              angular.forEach(invited_people, function(iv, key){
                                  scope.invitedPeopleLookup[iv.id] = 1;
                              });
                          };
                          
		          if(contest_slug != null){
			      ct.get_contest(contest_slug).then(function(response) {
			          if (response.data) {
                                      //console.log("INFO: get_contest=", response.data);
                                      var contest_data = response.data;
                                      ct.check_and_redirect(contest_data, contest_slug);
                                      scope.showForm = true;
                                      scope.contestForm = contest_data;
                                      scope.userEnteredPrice = true;
				      if (contest_data.story_board_images[0]) {
				          scope.story_1 = contest_data.story_board_images[0].image;
				      }
				      if (contest_data.story_board_images[1]) {
				          scope.story_2 = contest_data.story_board_images[1].image;
				      }
				      if (contest_data.story_board_images[2]) {
				          scope.story_3 = contest_data.story_board_images[2].image;
				      }
				      if (contest_data.story_board_images[3]) {
				          scope.story_4 = contest_data.story_board_images[3].image;
				      }
				      if (contest_data.story_board_images[4]) {
				          scope.story_5 = contest_data.story_board_images[4].image;
				      }
                                      if (scope.contestForm.private){
                                          scope.changePrivateContest(true);
                                      }
                                      if (scope.contestForm.model_release) {
                                          scope.changeModelRelease(true);
                                      }
                                      scope.review_contest = contest_data;
				      scope.slider_ticks.value = contest_data.level_required - 1;
				      scope.trimlevel(scope.contestForm.level_required);
                                      //console.log("DEBUG: iv=", scope.contestForm.invited_people);
				      if (scope.contestForm.invited_people.length > 0) {
                                          scope.createInvitedPeopleLookup(scope.contestForm.invited_people);
				          scope.needinvite = true;
                                          if (!scope.contestForm.private) {
                                              scope.changeInvite(scope.needinvite);
                                          }
				          //scope.contestForm.invited_people = contest_data.invited_people
                                      }
				      // var location_specified = false;
				      // if (scope.contestForm.location !== null) {
				      //     var location = scope.contestForm.location.split(',')
				      //     console.log("INFO: contest has location", location);
				      //     if (location[0] && location[1]) {
				      //         scope.showmap = true;
				      //         location_specified = true;
				      //         Map.init(parseFloat(location[0]), parseFloat(location[1]));
				      //     }
				      // }
				      // if (!location_specified) {
				      //     console.log("INFO: no/invalid location");
				      //     ct.default_location(/*lat, lng*/);
				      // }
                                      if (scope.contestForm.location !== null) {
                                          scope.showmap = true;
                                          scope.showLocation(true);
                                      }
				      if (scope.contestForm.space_for_copy == true) {
				          scope.contestForm.space_for_copy = "true";
				          //                    $scope.space = 'Yes'
				      }
				      else {
				          scope.contestForm.space_for_copy = "false";
				          //                    $scope.space = 'No'
				      }
				      if (scope.contestForm.accept_curated_videos == true) {
				          scope.contestForm.accept_curated_videos = "true";
				          //                    $scope.space = 'Yes'
				      }
				      else {
				          scope.contestForm.accept_curated_videos = "false";
				          //                    $scope.space = 'No'
				          
				      }
				      var expiry = new Date(contest_data.expiry_date);
				      scope.contestForm.expiry_date = expiry;
                                  }
			      }, function(response){
			          console.log("ERROR: get_contest=", response);
                                  $state.go('404');
			      });
		          }
		          else {
			      ct.default_location(dlat, dlng, null);
                              scope.slider_ticks.value = 0;
		          }
		          
		          //scope.contestForm.video_max_length=10;
		          var today = new Date();
		          var h = today.getMonth() + 1;
		          scope.d = today.getFullYear() + '-' + '0' + h + '-' + today.getDate();
		          scope.$watchCollection('$stateParams', function() {
			      $anchorScroll();
		          });
                      }
		      //console.log("INFO: expiry = ", scope.contestForm.expiry_date);
		      
		      scope.clearImage = function(image_field){
			  scope[image_field] = undefined;
		      };
                      
		      scope.gotoUserProfile = function(username){
			  $('#choose_favorite_model').modal('hide');
			  $state.go('user_profile', {username: username})
		      };
                      
		      scope.videographerLevel = function(level, page){
			  if(level !== undefined){
			      scope.selected_level = level;
			  }
			  var params_obj = {levels:level,
					    page_size:scope.pagination.items_per_page,
					    page:page,
					    q:scope.videographer_name}
			  ct.inviteVideographers(params_obj)
                              .then(function(response){
				  scope.videographerCollection =
                                      response.data.objects.results;
				  scope.total_count = response.data.objects.count;
                                  //console.log("DEBUG: vc=", scope.videographerCollection);
                                  // videographer has already been invited ?
                                  angular.forEach(scope.videographerCollection, function(vg, key){
                                      if (scope.invitedPeopleLookup.hasOwnProperty(vg.user.id)){
                                          vg.user.invited = true;
                                      }
                                  });
			      }, function(response) {
				  console.log("ERROR: inviteVideographers=",
                                              response);
			      });
		      };
                      
                      scope.newContest = function(a) {
			  //alert(scope.contestForm.expiry_date);
			  /*
			  //$scope.kl= $scope.contestForm.image;
			  
			  //if ($scope.createcontestForm.$valid) {
			  
			  //contestService.set($scope.contestForm.contest_title,
			  $scope.contestForm.purpose, $scope.contestForm.type,
			  //    $scope.contestForm.orientation, $scope.contestForm.environment,
			  $scope.contestForm.lighting,
			  //    $scope.contestForm.quality, $scope.contestForm.award_amount,
			  $scope.contestForm.art_direction, $scope.contestForm.expiry_date,
			  //    $scope.contestForm.space, $scope.contestForm.level,
			  $scope.contestForm.duration);
			  //$state.go('review');
			  //}*/
			  //if (!scope.forms.createcontestForm.$valid) {
                          if (!a.$valid) {
			      scope.contestSubmitted = true;
			  }
		      };
                      
                      scope.changePrice = function(newValue){
                          scope.userEnteredPrice = true;
                      };
                      
                      scope.updateAwardAmount = function(){
                          var min = scope.suggestedPrice + scope.extrasPrice;
                          // if (scope.contestForm.award_amount == undefined ||
                          //     parseFloat(scope.contestForm.award_amount) < am) {
                          //     if ($localStorage.contest_id != null &&
                          //         parseFloat(scope.contestForm.award_amount) >= am){
                          //         //
                          //     }
                          //     else {
                          //         scope.contestForm.award_amount = am;
                          //     }
                          // }
                          
                          // if user enters a price, then we will not change
                          // the price if the user selects a lower level videographer.
                          // we will change the price only when user selects a higher
                          // level videographer whose price is higher than the one
                          // specified by the user.
                          //
                          // but once we go to automode where the app selects the
                          // price, then we will update the price whenever
                          // the videographer level is changed [both lower and higher
                          // level videographers]
                          if (scope.contestForm.award_amount == undefined) {
                              scope.contestForm.award_amount = min;
                              scope.userEnteredPrice = false;
                              return;
                          }
                          scope.contestForm.award_amount=parseFloat(scope.contestForm.award_amount);
                          if (scope.contestForm.award_amount < min){
                              scope.contestForm.award_amount = min;
                              scope.userEnteredPrice = false;
                              return;
                          }
                          if (scope.userEnteredPrice){
                              return;
                          }
                          scope.contestForm.award_amount = min;
                      };
                      
		      scope.trimlevel = function (level) {
			  if (level===1){
			      scope.suggestedPrice = scope.price_config.level_one_min_price;
			  }
			  else if (level===2){
			      scope.suggestedPrice = scope.price_config.level_two_min_price;
			  }
			  else if (level===3){
			      scope.suggestedPrice = scope.price_config.level_three_min_price;
			  }
			  else if (level===4){
			      scope.suggestedPrice = scope.price_config.level_four_min_price;
			  }
			  else if (level===5){
			      scope.suggestedPrice = scope.price_config.level_five_min_price;
			  }
                          scope.updateAwardAmount();
			  scope.invitelevels = [];
			  for(var i=level-1; i>=0; i--){
			      scope.invitelevels.push(scope.levelCollection[i]);
			  }
		      };
		      
		      scope.clearallVideographers = function(){
                          angular.forEach(scope.videographerCollection,
                                          function(vg, key){
                                              vg.user.invited = false;
                                          });
                          scope.invitedPeopleLookup = {};
                          scope.contestForm.invited_people = [];
		      };

                      // ToDo: backend needs to mark the videographer.user.invited
		      scope.inviteVideographer = function(user){
                          // ToDo: query the limit from backend
			  if (scope.contestForm.invited_people.length<=10){
			      user.invited = true;
                              scope.contestForm.invited_people.push(user);
                              scope.invitedPeopleLookup[user.id] = 1;
                              //scope.invited_videographers=scope.contestForm.invited_people
			  }
			  else {
			      // ToDo: make it configurable based upon the pricing
			      toaster.pop('info',
					  "Warning",
					  "You can only invite maximum 10 people");
			  }
		      }

                      scope.removeInviteOne = function(id){
                          for (var i = 0;
                               i < scope.videographerCollection.length;
                               i++) {
                              var vg = scope.videographerCollection[i];
                              if (vg.user.id == id) {
                                  vg.user.invited = false;
                                  return;
                              }
                          }
                      };
                      
		      scope.removeInvite = function(index){
                          delete scope.invitedPeopleLookup[scope.
                                                           contestForm.
                                                           invited_people[index].
                                                           id];
                          scope.removeInviteOne(scope.contestForm.invited_people[index].id);
	                  scope.contestForm.invited_people[index].invited = false;
			  scope.contestForm.invited_people.splice(index, 1);
                      };
                      // for a private contest videographers need to be invited
                      // so select the option automatically
//                      scope.privateContestChanged = function(newValue){
//                          //console.log("INFO: privateContestChanged=", newValue);
//                          if (!newValue || newValue === false) {
//                              return;
//                          }
//                          scope.needinvite = true;
//                      }
                      scope.changePrivateContest = function(value){
                          if(value){
                              if(!scope.needinvite){
                                  scope.extrasPrice += scope.price_config.invite_price;
                              }
                              scope.needinvite = true
                              scope.extrasPrice += scope.price_config.private_request_price;
                          }
                          else /*if (scope.slider_ticks.value == 0 && !value)*/{
                              scope.needinvite = false;
                              scope.extrasPrice -= scope.price_config.private_request_price;
                              scope.changeInvite(scope.needinvite);
                          }
                          // private contest requires needinvite
                          // scope.changeInvite(scope.needinvite);
                          scope.updateAwardAmount();
                      }

                      scope.changeInvite = function(value){
                          if (!value){
                              if(scope.contestForm.private){
                                  scope.extrasPrice -= scope.price_config.private_request_price;
                              }
                              scope.contestForm.private = false;
                              scope.extrasPrice -= scope.price_config.invite_price;
                          }
                          else {
                              scope.extrasPrice += scope.price_config.invite_price;
                          }
                          scope.updateAwardAmount();
                      }

                      scope.changeModelRelease = function(value){
                          if (value){
                              scope.extrasPrice += scope.price_config.model_release_price;
                          }
                          else {
                              scope.extrasPrice -= scope.price_config.model_release_price;
                          }
                          scope.updateAwardAmount();
                      };

                      scope.category = function(p){
                          if(p == '') {
                              scope.valid_category = false
                              scope.validForm = false
                          }
                          else{
                              scope.valid_category = true
                              scope.validForm = true
                          }
                      }

		      scope.contestSave = function(){
                          scope.award_amount_invalid=false
			  if(parseFloat(scope.contestForm.award_amount) <
			     parseFloat(scope.suggestedPrice)){
			      scope.award_amount_invalid = true;
			      scope.contestSubmitted=true
			      return false;
			  }
			  if(!scope.showmap){
			      scope.contestForm.location = "";//undefined;
			  }
			  if(scope.contestForm.status == undefined){
			      scope.contestForm.status = 'incomplete';
			  }
			  scope.contestForm.type = 'S';
			  var filesMulti = [];
			  for(var i=1;i<=5;i++) {
			      var variable = 'story_' + i;
                              //console.log("INFO: story_", i, "=", scope[variable]);
                              if (scope[variable]) {
			          filesMulti[i] = scope[variable]
                              }
			  }
                          // ToDo: fix this changing from array to string
                          //       changes the length of the variable
			  scope.contestForm.file = filesMulti;
			  scope.people = scope.contestForm.invited_people;
			  scope.contestForm.invited_people = [];
			  angular.forEach(scope.people, function(value, key){
			      // scope.contestForm.invited_people =
			      //     scope.contestForm.invited_people + value.id + ',';
                              scope.contestForm.invited_people.push(value.id);
			  });
                          //console.log("INFO: storyboard files=", scope.contestForm.file);
                          //console.log("INFO: contestSave=", scope.contestForm);
                          var uploadToS3 = function(file){
                              HttpServices.get("api/general/s3signature")
                                  .then(function(response){
                                      console.log("DEBUG: s3signature=", response);
                                      var bucket = response.data.bucket;
                                      var dir = response.data.dir;
                                      var signature = response.data.signature;
                                      var policyBase64 = response.data.policy;
                                      var date = response.data.date;
                                      var credentials = response.data.credentials;
                                      var expiration = response.data.expiration;
                                      console.log("DEBUG: file=", file);
                                      console.log("DEBUG: date=", date);
                                      console.log("DEBUG: expiration=", expiration);
                                      Upload.upload({
                                          url: 'https://'+bucket+'.s3.amazonaws.com/',
                                          method: 'POST',
                                          data: {
                                              key: dir+file.name,
                                              acl: 'private',
                                              Policy: policyBase64,
                                              'X-Amz-Credential' : credentials,
                                              'X-Amz-Algorithm': 'AWS4-HMAC-SHA256',
                                              'X-Amz-Date': date,
                                              'X-Amz-Signature': signature,
                                              "Content-Type": 'image/*',
                                              'file': file
                                          }
                                      }).then(function(response){
                                          console.log("DEBUG: Upload.load=", response);
                                      }, function(response){
                                          console.log("ERROR: Upload.load=", response);
                                      }, function(response){
                                          console.log("DEBUG2: Upload.load=", response);
                                      });
                                  }, function(response){
                                      console.log("ERROR: s3signature=", response);
                                  });
                          };
                          
                          var handleFailure = function (error, action_str){
			      console.log("ERROR: ", action_str, "=", error);
			  };

                          var uploadSuccess = function(contest_slug,
                                                       contest_data,
                                                       files,
                                                       action_str){
                              var gotoReview = function(){
                                  scope.forms.createcontestForm.$setPristine();
                                  $state.go('review',{slug:contest_slug});
                              };
                              
                              if (files != null) {
                                  //console.log("DEBUG: files=", files);
                                  //uploadToS3(files[1]);
			          Upload.upload({url: 'api/contests/save-story-board/'+contest_slug,
			        	         method: 'PUT',
			        	         data: files})
                                      .then(function (resp){
                                          //console.log("INFO: ", action_str);
                                          gotoReview();
			              }, function (resp) {
			        	  handleFailure(resp.status, action_str);
			              }, function (evt) {
			        	  //
			              });
                              }
                              else {
                                  gotoReview();
                              }
                          };
                          // first create/update contest and then upload files
                          // ToDo: upload files to S3
                          if($localStorage.contest_slug != null){
			      ct.update_contest($localStorage.contest_slug,
                                                scope.contestForm)
                                  .then(function(response){
                                      uploadSuccess($localStorage.contest_slug,
                                                    response.data,
                                                    scope.contestForm.file,
                                                    "update_contest(upload)");
                                  }, function(response){
                                      handleFailure(response, "update_contest(update)");
                                  });
                          }
                          else {
                              ct.create_contest(scope.contestForm)
                                  .then(function(response) {
			              if(response.data) {
                                          $localStorage.contest_slug = response.data.slug;
                                          //scope.contestForm.invited_people=scope.invited_videographers
                                          uploadSuccess($localStorage.contest_slug,
                                                        response.data,
                                                        scope.contestForm.file.length ?
                                                        scope.contestForm.file : null,
                                                        "create_contest(upload)");
                                      }
                                  }, function(response){
                                      //scope.contestForm.invited_people=scope.invited_videographers
                                      handleFailure(response, "create_contest(create)");
                                  });
                          }

                          /*
                          if($localStorage.contest_id != null){
			      ct.update_contest($localStorage.contest_id,
                                                scope.contestForm).then(function (contest_data){
				                    Upload.upload({url: 'api/contests/save-story-board/'+$localStorage.contest_id,
						                   method: 'PUT',
						                   data: scope.contestForm.file}).then(function (resp){
                                                                       console.log("INFO: update_contest");
						                       $state.go('review');
						                   }, function (resp) {
						                       console.log("ERROR: update_contest:upload failed. error=",
								                   resp.status);
						                   }, function (evt) {
						                       //
						                   });
                                                }, function (error) {
				                    console.log("ERROR: update_contest failed. error=",
					                        error);
			                        });
                          }
			  else {
			      ct.create_contest(scope.contestForm).then(function (contest_data) {
				  if(contest_data) {
				      Upload.upload({url: 'api/contests/save-story-board/',
						     method: 'PUT',
						     data: scope.contestForm.file}).then(function (resp) {
                                                         console.log("INFO: create_contest");
							 $state.go('review');
							 $localStorage.contest_id = resp.data.id;
						     }, function (resp) {
							 console.log("ERROR: create_contest:upload failed. error=",
								     resp.status);
						     }, function (evt) {
							 //
						     });
                                  }
			      }, function (error) {
				  console.log("ERROR: create_contest failed. error=",
					      error);
			      });
			  }*/
		      } // scope.contestSave() 
		  }; // ct.create_or_edit_contest
                  
		  return ct;
	      }])

    .filter('inArray', function($filter){
	return function(list, arrayFilter, element){
            if(arrayFilter){
		return $filter("filter")(list, function(listItem){
                    return arrayFilter.indexOf(listItem[element]) != -1;
		});
            }
	};
    })

    .service('Map', function($q) {

	this.init = function(lat, lng) {
	    var pos = new google.maps.LatLng(lat, lng);
            var options = {
		center: pos,
		zoom: 5,
		disableDefaultUI: true,
            }
            this.map = new google.maps.Map(document.getElementById("map"), options);
            this.places = new google.maps.places.PlacesService(this.map);
	    this.addMarker(pos);
            // this.geocoder = new google.maps.Geocoder();

            // var d = $q.defer();
            // this.geocoder.geocode({'latLng': pos},
            //                       function (results, status) {
            //                           if (status === google.maps.GeocoderStatus.OK) {
            //                               //console.log("geocode=", results);
            //                               d.resolve(results[0]);
            //                           }
            //                           else {
            //                               d.reject(status);
            //                           }
            //                       });
            // return (d.promise);
	}

	this.search = function(str) {
            var d = $q.defer();
            this.places.textSearch({query: str}, function(results, status) {
		if (status == 'OK') {
                    d.resolve(results[0]);
		}
		else d.reject(status);
            });
            return d.promise;
	}

	this.addMarker = function(pos) {
            if(this.marker) this.marker.setMap(null);
            this.marker = new google.maps.Marker({
		map: this.map,
		position: pos,
		animation: google.maps.Animation.DROP,
            });
            this.map.setCenter(pos);
	}
    });
