'use strict';

angular.module('spyhop.user_profile', ['ngRoute','ui.router'])

    .controller('ProfileEditCtrl',
                ['$rootScope',
                 '$scope',
                 //'$http',
                 '$location',
                 '$stateParams',
                 '$localStorage',
                 '$state',
                 'Profile',
                 'Upload',
                 'toaster',
                 function($rootScope,
                          $scope,
                          //$http,
                          $location,
                          $stateParams,
                          $localStorage,
                          $state,
                          Profile,
                          Upload,
                          toaster) {
                     if ($localStorage.userKey === null || $localStorage.userName === null) {
	                 $state.go('home');
                     }
                     $scope.profile_cancel = function(){
                         $state.go('profile');
                     }

                     $scope.profile_save = function(){
                         // upload cover, profile pics if they changed
                         var data = {from_web: true};
                         var files = [];
                         var i = 0;
                         if ($scope.coverPicChanged) {
                             if ($scope.cover_picture) {
                                 files[i] = $scope.cover_picture;
                                 console.log('DEBUG: upload cover_picture');
                                 data.cover_picture = files[i];
                                 ++i;
                             }
                             else {
                                 data.cover_picture = '';
                             }
                         }
                         if ($scope.profilePicChanged) {
                             if ($scope.profile_picture) {
                                 console.log('DEBUG: upload profile_picture');
                                 files[i] = $scope.profile_picture;
                                 data.profile_picture = files[i];
                                 ++i;
                             }
                             else {
                                 data.profile_picture = '';
                             }
                         }
                         if (i) {
                             data.file = files;
                         }
                         // update other fields if they changed
                         if ($scope.form.first_name.$dirty) {
                             data.first_name = $scope.user.first_name;
                         }
                         if ($scope.form.last_name.$dirty) {
                             data.last_name = $scope.user.last_name;
                         }
                         if ($scope.form.location.$dirty) {
                             data.location = $scope.user.location;
                             if($scope.user.location.formatted_address){
                                 data.location = $scope.user.location.formatted_address
                             }
                         }
                         Upload.upload({
                             url: 'api/profiles/profile-list/'+$localStorage.userName,
                             method: 'patch',
                             data: data,
                             headers: { 'Authorization': 'Token '+$localStorage.userKey},
                         }).then(function(response) {
                             // file is uploaded successfully
                             toaster.pop('success', "Success", "Profile updated");
                             $state.go('profile');
                         }, function(response){
                             console.log("ERROR: ", response);
                             toaster.pop('error', "Error", "Unable to update profile");
                         });
                     };

                     $scope.select_picture = function(field, file){
                         if (field == 'profile') {
                             console.log("DEBUG: profilepic=", file);
                             $scope.profilePicChanged = true;
                             $scope.profile_picture = file;
                             return;
                         }
                         if (field == 'cover') {
                             console.log("DEBUG: coverpic=", file);
                             $scope.coverPicChanged = true;
                             $scope.cover_picture = file;
                             return;
                         }
                         console.log("ERROR: unexpected call !!!");
                     }

                     Profile.get_profile($localStorage.userName)
                         .then(function(response) {
                             //console.log("INFO: get_profile=", response);
                             $scope.cover_picture = response.data.cover_picture
                             $scope.profile_picture = response.data.profile_picture
                             $scope.user = response.data;
                         }, function(response) {
                             console.log("ERROR: get_profile=", response);
                         });
                 }])


    .factory('Profile',
             ['$localStorage',
              //'$http',
              'HttpServices',
              function($localStorage,
                       //$http,
                       HttpServices) {
                  return {
                      get_profile: function(username){
                          return(HttpServices.get("api/profiles/profile-list/"+
                                                  username));
                      },

                      update_profile: function(username, data){
                          //data.from_web = true;
                          // var url = "api/profiles/profile-list/" + $localStorage.userName;
                          // var request_data = {method: 'PATCH', url: url, data: data };
                          // if($localStorage.userKey !== undefined){
                          //     request_data['headers'] = { 'Authorization': 'Token ' + $localStorage.userKey};
                          // }
  		          // return $http(request_data).then(function(response) {
                          //     if (typeof response.data === 'object') {
                          //         return response.data;
                          //     } else { return "Error"; }
                          // },function(error) {
                          //     return null;
                          // });
                          return(HttpServices.patch("api/profiles/profile-list/"+
                                                    username,
                                                    data));
                      }
                  };
              }]);
