tgt_dir=$1
src_dir=$2
repo_url=https://pradeep_spyhop@bitbucket.org/spyhop/main.git
if [ $# -ne 2 ] ; then
    echo "Usage: $0 <target dir path> <src dir path>"
    echo "Example: gpc.sh server server/api"
    exit
fi
git init $tgt_dir
cd $tgt_dir
git remote add origin $repo_url
git config core.sparsecheckout true
echo "$src_dir/*" >> .git/info/sparse-checkout
git pull --depth=1 origin master
